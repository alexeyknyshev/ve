#include "uitextlabel.h"

#include <framework/ui/uimanager.h>
#include <framework/ui/uiframe_p.h>
#include <framework/ui/3rdparty/Gorilla.h>

static UiAlignment convertAlignment(Gorilla::TextAlignment horiz, Gorilla::VerticalAlignment vert)
{
    int result;

    switch (horiz)
    {
    case Gorilla::TextAlign_Left:   result = UA_Left;
    case Gorilla::TextAlign_Centre: result = UA_CenterHorizontal;
    case Gorilla::TextAlign_Right:  result = UA_Right;
    }

    switch (vert)
    {
    case Gorilla::VerticalAlign_Top:    result |= UA_Top;
    case Gorilla::VerticalAlign_Middle: result |= UA_CenterVertical;
    case Gorilla::VerticalAlign_Bottom: result |= UA_Bottom;
    }

    return (UiAlignment)result;
}

class UiTextLabelPrivate : public UiFramePrivate
{
public:
    UiTextLabelPrivate(const std::string &name, Gorilla::Screen *screen)
        : UiFramePrivate(name, screen),
          mLabel(nullptr)
    {
        mLabel = getLayer()->createCaption(getLayer()->_getDefaultGlyphDataIndex(),
                                           0.0f, 0.0f, VE_STR_BLANK);
        setTextAlignment(UA_Center);
    }

    UiTextLabelPrivate(const std::string &name, UiFrame *parent)
        : UiFramePrivate(name, parent),
          mLabel(nullptr)
    {
        mLabel = getLayer()->createCaption(getLayer()->_getDefaultGlyphDataIndex(),
                                           0.0f, 0.0f, VE_STR_BLANK);
        setTextAlignment(UA_Center);
    }

    UiTextLabelPrivate(UiFrame *parent)
        : UiFramePrivate(parent),
          mLabel(nullptr)
    {
        mLabel = getLayer()->createCaption(getLayer()->_getDefaultGlyphDataIndex(),
                                           0.0f, 0.0f, VE_STR_BLANK);
        setTextAlignment(UA_Center);
    }

    ~UiTextLabelPrivate()
    {
        if (ui(false) && getLayer())
        {
            getLayer()->destroyCaption(mLabel);
        }
        mLabel = nullptr;
    }

    void setText(const std::string &text)
    {
        mLabel->text(text);
    }

    const std::string &getText() const
    {
        return mLabel->text();
    }

    void setTextColor(const Ogre::ColourValue &color)
    {
        mLabel->colour(color);
    }

    const Ogre::ColourValue &getTextColor() const
    {
        return mLabel->colour();
    }

    void setTextFontSize(size_t size)
    {
        mLabel->font(size);
    }

    size_t getTextFontSize() const
    {
        return mLabel->font();
    }

    void setGeometry(const ve::PointF &position, const ve::SizeF &size)
    {
        UiFramePrivate::setGeometry(position, size);

        mLabel->left(position.x);
        mLabel->top(position.y);
        mLabel->width(size.width);
        mLabel->height(size.height);
    }

    void setTextAlignment(UiAlignment align)
    {
        switch (align)
        {
        case UA_Top:
            mLabel->vertical_align(Gorilla::VerticalAlign_Top);
            break;
        case UA_Bottom:
            mLabel->vertical_align(Gorilla::VerticalAlign_Bottom);
            break;
        case UA_Left:
            mLabel->align(Gorilla::TextAlign_Left);
            break;
        case UA_Right:
            mLabel->align(Gorilla::TextAlign_Right);
            break;
        case UA_CenterVertical:
            mLabel->vertical_align(Gorilla::VerticalAlign_Middle);
            break;
        case UA_CenterHorizontal:
            mLabel->align(Gorilla::TextAlign_Centre);
            break;
        case UA_Center:
            mLabel->vertical_align(Gorilla::VerticalAlign_Middle);
            mLabel->align(Gorilla::TextAlign_Centre);
            break;
        case UA_TopLeft:
            mLabel->vertical_align(Gorilla::VerticalAlign_Top);
            mLabel->align(Gorilla::TextAlign_Left);
            break;
        case UA_TopRight:
            mLabel->vertical_align(Gorilla::VerticalAlign_Top);
            mLabel->align(Gorilla::TextAlign_Right);
            break;
        case UA_BottomLeft:
            mLabel->vertical_align(Gorilla::VerticalAlign_Bottom);
            mLabel->align(Gorilla::TextAlign_Left);
            break;
        case UA_BottomRight:
            mLabel->vertical_align(Gorilla::VerticalAlign_Bottom);
            mLabel->align(Gorilla::TextAlign_Right);
            break;
        }
    }

    UiAlignment getTextAlignment() const
    {
        return convertAlignment(mLabel->align(), mLabel->vertical_align());
    }

    ve::SizeF getMinimumSizeHint() const
    {
        Ogre::Vector2 drawSize;
        mLabel->_calculateDrawSize(drawSize);
        return ve::SizeF(drawSize);
    }

private:
    Gorilla::Caption *mLabel;
};

UiTextLabel::UiTextLabel(const std::string &name, Gorilla::Screen *screen, const std::string &text)
    : UiFrame(new UiTextLabelPrivate(name, screen))
{
    setText(text);
}

UiTextLabel::UiTextLabel(const std::string &name, UiFrame *parent, const std::string &text)
    : UiFrame(new UiTextLabelPrivate(name, parent))
{
    setText(text);
}

UiTextLabel::UiTextLabel(UiFrame *parent)
    : UiFrame(new UiTextLabelPrivate(parent))
{ }

ve::SizeF UiTextLabel::getMinimumSizeHint() const
{
    return d->to<UiTextLabelPrivate>()->getMinimumSizeHint();
}

void UiTextLabel::setText(const std::string &text)
{
    d->to<UiTextLabelPrivate>()->setText(text);
}

const std::string &UiTextLabel::getText() const
{
    return d->to<UiTextLabelPrivate>()->getText();
}

void UiTextLabel::clear()
{
    setText(VE_STR_BLANK);
}

void UiTextLabel::setTextColor(const Ogre::ColourValue &color)
{
    d->to<UiTextLabelPrivate>()->setTextColor(color);
}

const Ogre::ColourValue &UiTextLabel::getTextColor() const
{
    return d->to<UiTextLabelPrivate>()->getTextColor();
}

void UiTextLabel::setTextFontSize(size_t size)
{
    d->to<UiTextLabelPrivate>()->setTextFontSize(size);
}

size_t UiTextLabel::getTextFontSize() const
{
    return d->to<UiTextLabelPrivate>()->getTextFontSize();
}

void UiTextLabel::setTextAlignment(UiAlignment align)
{
    d->to<UiTextLabelPrivate>()->setTextAlignment(align);
}

UiAlignment UiTextLabel::getTextAlignment() const
{
    return d->to<UiTextLabelPrivate>()->getTextAlignment();
}
