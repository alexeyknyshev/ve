#ifndef UIFRAME_H
#define UIFRAME_H

#include <functional>

#include <global.h>

#include <framework/size.h>
#include <framework/point.h>

#include <OGRE/OgreViewport.h>

#include <framework/controller/abstractinputlistener.h>

enum UiBorder
{
    UB_North = 0,
    UB_East,
    UB_South,
    UB_West
};

enum UiCorner
{
    UC_TopLeft = 0,
    UC_TopRight,
    UC_BottomRight,
    UC_BottomLeft
};

enum UiGradient
{
    UG_NorthSouth = 0,
    UG_WestEast,
    UG_Diagonal
};

enum UiAlignment
{
    UA_Top = 0,
    UA_Bottom,
    UA_Left,
    UA_Right,
    UA_CenterVertical,
    UA_CenterHorizontal,
    UA_Center,
    UA_TopLeft,
    UA_TopRight,
    UA_BottomLeft,
    UA_BottomRight
};

namespace Gorilla
{
    class Screen;
}

class UiLayout;
class UiFramePrivate;
class UiDebugColors;

class UiFrame : public Ogre::Viewport::Listener,
                public AbstractInputListener
{
    friend class UiFramePrivate;

public:
    UiFrame(const std::string &name, Gorilla::Screen *screen);
    UiFrame(const std::string &name, UiFrame *parent);
    UiFrame(UiFrame *parent);

    UiFrame(UiFramePrivate *d_);

    virtual ~UiFrame();

    bool event(const Event *event);

    void update(float deltaTime, float realTime) override
    { VE_UNUSED(deltaTime); VE_UNUSED(realTime); }

    const std::string &getName() const override;

    void invalidate();

    UiFrame *getParent() const;

    void setLayout(UiLayout *layout);
    inline UiLayout *getLayout() const { return mLayout; }

    void setSize(const ve::SizeF &size);
    ve::SizeF getSize() const;

    void setPosition(const ve::PointF &position);
    ve::PointF getPosition() const;

    virtual void setGeometry(const ve::PointF &position, const ve::SizeF &size);

    virtual ve::SizeF getMinimumSizeHint() const;
    virtual bool isExpanding() const
    { return false; }

    virtual void setVisible(bool visible);
    bool isVisible() const;

    bool isTopLevel() const;

    void setBorderWidth(float width);
    float getBorderWidth() const;

    void setBorderColor(UiBorder border, const Ogre::ColourValue &color);
    void setBorderColor(const Ogre::ColourValue &color);

    void setBackgroundGradient(UiGradient gradient,
                               const Ogre::ColourValue &from,
                               const Ogre::ColourValue &to);

    void setBackgroundColor(const Ogre::ColourValue &color);
    void setBackgroundColor(UiCorner corner, const Ogre::ColourValue &color);

    const Ogre::ColourValue &getBackgroundColor(UiCorner corner) const;
    const Ogre::ColourValue &getBackgroundColor() const;

    const Ogre::ColourValue &getBorderColor(UiBorder border) const;
    const Ogre::ColourValue &getBorderColor() const;


    /** Ogre::Viewport callback (Only for top-level Frame) */
    void viewportDimensionsChanged(Ogre::Viewport *viewport) override;

    /// ------ USER INPUT ------ ///

    bool keyPressed(const KeyboardEvent &event) override;
    bool keyReleased(const KeyboardEvent &event) override;

    bool mouseMoved(const MouseEvent &event) override;
    bool mousePressed(const MouseEvent &event) override;
    bool mouseReleased(const MouseEvent &event) override;

    /// ------ DEBUG ------ ///

    void setDebugEnabled(bool debug, bool recursive = true);
    bool isDebugEnabled() const;

    void setDebugBackgroundColor(const Ogre::ColourValue &color);

    void setDebugBackgroundColorRecursive(const UiDebugColors &colors);

    void _setDebugBackgroundColorRecursive(const UiDebugColors &colors, size_t depth, size_t index);

    Ogre::ColourValue getDebugBackgroundColor() const;

protected:
    bool mouseEvent(const MouseEvent *event);

    UiFramePrivate *d;
    UiLayout *mLayout;

    void registredAsInputListener(bool registred, InputController *controller)
    { VE_UNUSED(registred); VE_UNUSED(controller); }
};

#endif // UIFRAME_H
