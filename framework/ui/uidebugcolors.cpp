#include "uidebugcolors.h"

void UiDebugColors::addDebugColor(size_t depth, size_t index, const Ogre::ColourValue &color)
{
    auto it = mColors.find(depth);
    if (it == mColors.end())
    {
        it = mColors.insert({ depth, ColorMap() }).first;
    }

    ColorMap &map = it->second;

    auto mapIt = map.find(index);
    if (mapIt != map.end())
    {
        map.erase(mapIt);
    }

    map.insert({ index, color });
}

void UiDebugColors::setDebugColorsForDepth(size_t depth, const std::list<Ogre::ColourValue> &colors)
{
    auto it = mColors.find(depth);
    if (it == mColors.end())
    {
        it = mColors.insert({ depth, ColorMap() }).first;
    }
    else
    {
        it->second.clear();
    }

    size_t index = 0;
    for (const Ogre::ColourValue &val : colors)
    {
        it->second.insert({ index, val });
        ++index;
    }
}

bool UiDebugColors::isEmpty() const
{
    const auto end = mColors.end();
    for (auto it = mColors.begin(); it != end; ++it)
    {
        if (!it->second.empty())
        {
            return false;
        }
    }

    return true;
}

const Ogre::ColourValue &UiDebugColors::getDebugColor(size_t depth, size_t index) const
{
    const size_t avaliableDepthSize = mColors.size();
    if (avaliableDepthSize  == 0)
    {
        return Ogre::ColourValue::ZERO;
    }

    const size_t adjustedDepth = avaliableDepthSize == 1 ? 0 : depth % avaliableDepthSize;

    const size_t avaliableIndexSize = mColors.at(adjustedDepth).size();

    if (avaliableDepthSize == 0)
    {
        return Ogre::ColourValue::ZERO;
    }

    const size_t adjustedIndex = avaliableIndexSize == 1 ? 0 : index % avaliableIndexSize;

    return mColors.at(adjustedDepth).at(adjustedIndex);
}
