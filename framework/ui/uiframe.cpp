#include "uiframe.h"
#include "uiframe_p.h"

#include <global.h>

#include <framework/ui/uilayout.h>
#include <framework/ui/uimanager.h>
#include <framework/ui/uidebugcolors.h>

#include <OGRE/OgreString.h>

#include <framework/rect.h>

UiFrame::UiFrame(const std::string &name, Gorilla::Screen *screen)
    : d(new UiFramePrivate(name, screen)),
      mLayout(nullptr)
{
    if (VE_CHECK_NULL_PTR(Ogre::Viewport, screen, Logger::LOG_COMPONENT_UI))
    {
        Ogre::Viewport *viewport = screen->getViewport();
        if (viewport)
        {
            viewport->addListener(this);
        }
    }

    ui()->addTopLevelFrame(this, screen);
}

UiFrame::UiFrame(const std::string &name, UiFrame *parent)
    : d(new UiFramePrivate(name, parent)),
      mLayout(nullptr)
{ }

UiFrame::UiFrame(UiFrame *parent)
    : d(new UiFramePrivate(parent)),
      mLayout(nullptr)
{ }

/*
UiFrame::UiFrame(UiFramePrivate *d_, Ogre::Viewport *viewport)
    : d(d_),
      mLayout(nullptr),
      mParent(nullptr)
{
    veLogAndAssert(d,
                   VE_CLASS_NAME_STR(UiFrame),
                   VE_SCOPE_NAME_STR(UiFrame::UiFrame(UiFramePrivate *, Ogre::Viewport *)),
                   VE_STR_BLANK,
                   "Nullptr of " + VE_CLASS_NAME_STR(UiFramePrivate) + " passed to constructor!");
}
*/

UiFrame::UiFrame(UiFramePrivate *d_)
    : d(d_),
      mLayout(nullptr)
{
    veLogAndAssert(d,
                   VE_CLASS_NAME(UiFrame),
                   VE_SCOPE_NAME(UiFrame(UiFramePrivate *d)),
                   VE_STR_BLANK,
                   "Nullptr of " + VE_CLASS_NAME(UiFramePrivate).toString() + " passed to constructor!",
                   Logger::LOG_COMPONENT_UI);

    if (isTopLevel())
    {
        Ogre::Viewport *viewport = d->getViewport();
        if (viewport)
        {
            viewport->addListener(this);
        }
    }
}

UiFrame::~UiFrame()
{
    ui()->removeTopLevelFrame(this);

    Ogre::Viewport *viewport = d->getViewport();
    if (viewport)
    {
        viewport->removeListener(this);
    }

    setLayout(nullptr);

    delete d;
    d = nullptr;
}

bool UiFrame::event(const Event *event)
{
    const CallbackFunc *func = getCallback(event->getType());
    if (func)
    {
        Params params;
        event->toStdMap(params);
        (*func)(params);
    }

    return AbstractInputListener::event(event);
}

/*
bool UiFrame::event(const Event *event)
{
    const Event::EventType eventType = event->getType();
    if (eventType == Event::ET_Keyboard)
    {
        const KeyboardEvent *keyEvent = static_cast<const KeyboardEvent *>(event);
        if (keyEvent->data.keyState == ve::KeyData::KBS_Pressed)
        {
            keyPressed(*keyEvent);
        }
        else if (keyEvent->data.keyState == ve::KeyData::KBS_Released)
        {
            keyReleased(*keyEvent);
        }

        return true;
    }
    else if (eventType == Event::ET_Mouse)
    {
        const MouseEvent *mouseEvent = static_cast<const MouseEvent *>(event);
        if (mouseEvent->data.xRelative != 0 || mouseEvent->data.yRelative != 0)
        {
            mouseMoved(*mouseEvent);
        }
        if (mouseEvent->data.button != ve::MouseData::MB_UNASSIGNED)
        {
            mousePressed(*mouseEvent);
        }
    }

    return false;
}
*/
bool UiFrame::keyPressed(const KeyboardEvent &event)
{ VE_UNUSED(event); return true; }

bool UiFrame::keyReleased(const KeyboardEvent &event)
{ VE_UNUSED(event); return true; }

bool UiFrame::mouseMoved(const MouseEvent &event)
{
    return mouseEvent(&event);
}

bool UiFrame::mousePressed(const MouseEvent &event)
{
    return mouseEvent(&event);
}

bool UiFrame::mouseReleased(const MouseEvent &event)
{
    return mouseEvent(&event);
}

const std::string &UiFrame::getName() const
{
    return d->getName();
}

void UiFrame::invalidate()
{
    Ogre::Viewport *viewport = d->getViewport();
    if (viewport)
    {
        viewportDimensionsChanged(viewport);
    }
    else
    {
        setGeometry(getPosition(), getSize());
    }
}

UiFrame *UiFrame::getParent() const
{
    return d->getParent();
}

void UiFrame::setLayout(UiLayout *layout)
{
    delete mLayout;
    mLayout = layout;
    if (layout)
    {
        layout->setParent(this);
    }
}

void UiFrame::setGeometry(const ve::PointF &position, const ve::SizeF &size)
{
    ve::SizeF size_ = size;
    ve::SizeF minimumSize = getMinimumSizeHint();
    if (size_.width < minimumSize.width)
    {
        size_.width = minimumSize.width;
    }
    if (size_.height < minimumSize.height)
    {
        size_.height = minimumSize.height;
    }

    d->setGeometry(position, size_);
    if (mLayout)
    {
        mLayout->parentFrameGeometryChanged(position, size_);
    }
}

/** Only for top-level Frame */
void UiFrame::viewportDimensionsChanged(Ogre::Viewport *viewport)
{
    setGeometry({ 0.0f, 0.0f },
                ve::SizeF(viewport->getActualWidth(), viewport->getActualHeight()));
}

ve::SizeF UiFrame::getMinimumSizeHint() const
{
    if (mLayout)
    {
        return mLayout->getMinimumSizeHint();
    }
    return ve::SizeF(0.0f, 0.0f);
}

void UiFrame::setSize(const ve::SizeF &size)
{
    d->setGeometry(getPosition(), size);
}

ve::SizeF UiFrame::getSize() const
{
    return d->getSize();
}

void UiFrame::setPosition(const ve::PointF &position)
{
    d->setGeometry(position, getSize());
}

ve::PointF UiFrame::getPosition() const
{
    return d->getPosition();
}

void UiFrame::setVisible(bool visible)
{
    d->setVisible(visible);
    if (mLayout)
    {
        mLayout->setVisible(visible);
    }
}

bool UiFrame::isVisible() const
{
    return d->isVisible();
}

bool UiFrame::isTopLevel() const
{
    return d->isTopLevel();
}

void UiFrame::setBorderWidth(float width)
{
    d->setBorderWidth(width);
}

float UiFrame::getBorderWidth() const
{
    return d->getBorderWidth();
}

void UiFrame::setBorderColor(UiBorder border, const Ogre::ColourValue &color)
{
    d->setBorderColor(border, color);
}

void UiFrame::setBorderColor(const Ogre::ColourValue &color)
{
    d->setBorderColor(color);
}

void UiFrame::setBackgroundGradient(UiGradient gradient,
                                    const Ogre::ColourValue &from,
                                    const Ogre::ColourValue &to)
{
    d->setBackgroundGradient(gradient, from, to);
}

void UiFrame::setBackgroundColor(const Ogre::ColourValue &color)
{
    d->setBackgroundColor(color);
}

void UiFrame::setBackgroundColor(UiCorner corner, const Ogre::ColourValue &color)
{
    d->setBackgroundColor(corner, color);
}

const Ogre::ColourValue &UiFrame::getBackgroundColor(UiCorner corner) const
{
    return d->getBackgroundColor(corner);
}

const Ogre::ColourValue &UiFrame::getBackgroundColor() const
{
    return getBackgroundColor(UC_TopLeft);
}

const Ogre::ColourValue &UiFrame::getBorderColor(UiBorder border) const
{
    return d->getBorderColor(border);
}

const Ogre::ColourValue &UiFrame::getBorderColor() const
{
    return d->getBorderColor(UB_North);
}

/// ------------- DEBUG ----------------- ///

void UiFrame::setDebugEnabled(bool debug, bool recursive)
{
    d->setDebugEnabled(debug);

    if (recursive && getLayout())
    {
        getLayout()->setChildsDebugEnabled(debug, recursive);
    }
}

bool UiFrame::isDebugEnabled() const
{
    return d->isDebugEnabled();
}

void UiFrame::setDebugBackgroundColor(const Ogre::ColourValue &color)
{
    d->setDebugBackgroundColor(color);
}

void UiFrame::setDebugBackgroundColorRecursive(const UiDebugColors &colors)
{
    if (colors.isEmpty())
    {
        return;
    }

    _setDebugBackgroundColorRecursive(colors, 0, 0);
}

void UiFrame::_setDebugBackgroundColorRecursive(const UiDebugColors &colors, size_t depth, size_t index)
{    
    d->setDebugBackgroundColorRecursive(colors, depth, index);

    if (mLayout)
    {
        mLayout->setChildsDebugColorRecursive(colors, depth + 1);
    }
}

Ogre::ColourValue UiFrame::getDebugBackgroundColor() const
{
    return d->getDebugBackgroundColor();
}

// --------------------------------------------------------

bool UiFrame::mouseEvent(const MouseEvent *event)
{
    if (mLayout)
    {
        bool processed = false;
        std::list<UiFrame *> childFrames = mLayout->getChildFrames();
        for (UiFrame *frame : childFrames)
        {
            ve::PointF framePos = frame->getPosition();
            ve::SizeF frameSize = frame->getSize();

            if (framePos.x <= event->data.x && framePos.y <= event->data.y &&
                framePos.x + frameSize.width  >= event->data.x &&
                framePos.y + frameSize.height >= event->data.y)
            {
                processed |= frame->event(event);
            }
        }

        return processed;
    }

    return true;
}
