#include "scriptconsole.h"

#include <framework/ui/scriptconsole_p.h>

#include <framework/ui/uimanager.h>
#include <framework/ui/uimarkuptextedit.h>
#include <framework/ui/layouts/uiverticallayout.h>
#include <framework/ui/layouts/uilayoutspacer.h>
#include <framework/controller/inputcontroller.h>

#include <renderer/renderer.h>

#include <script/script.h>

ScriptConsole::ScriptConsole(Gorilla::Screen *screen, const ScriptConsoleOptions &opt)
    : UiFrame(new ScriptConsolePrivate(this, screen, opt)),
      mPromptLine(nullptr),
      mConsoleText(nullptr)

      /* mListener(nullptr)*/
{
    UiVerticalLayout *layout = new UiVerticalLayout;

    mConsoleText = new UiMarkupTextEdit(opt.name + "ConsoleText", this);
    mConsoleText->setTextFontSize(opt.fontSize);
    layout->addFrame(mConsoleText);

    mPromptLine = new UiMarkupTextEdit(opt.name + "PromptLine", this);
    mPromptLine->setTextFontSize(opt.fontSize);
    layout->addFrame(mPromptLine);

    setLayout(layout);

//    setVisibleLineCount(opt.visibleLineCount);

    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(ScriptConsole(Gorilla::Screen *screen,
                                                            const ScriptConsoleOptions &opt)),
                                InputController,
                                opt.inputController,
                                Logger::LOG_COMPONENT_SCRIPT))
    {
        opt.inputController->addInputListener(this, false);
    }
}

ScriptConsole::~ScriptConsole()
{
    d->to<ScriptConsolePrivate>()->getInputController()->removeInputListener(this);
}

//void ScriptConsole::setVisibleLineCount(size_t count)
//{
//    mConsoleText->setVisibleLineCount(count);
//}

size_t ScriptConsole::getVisibleLineCount() const
{
    return mConsoleText->getVisiableLineCount();
}

void ScriptConsole::setSlowInsertTimeout(float timeout)
{
    mConsoleText->setSlowInsertTimeout(timeout);
}

float ScriptConsole::getSlowInsertTimeout() const
{
    return mConsoleText->getSlowInsertTimeout();
}

void ScriptConsole::setFastInsertTimeout(float timeout)
{
    mConsoleText->setFastInsertTimeout(timeout);
}

float ScriptConsole::getFastInsertTimeout() const
{
    return mConsoleText->getFastInsertTimeout();
}

void ScriptConsole::setVisible(bool visible)
{
    UiFrame::setVisible(visible);

    InputController *controller = d->to<ScriptConsolePrivate>()->getInputController();
    if (controller)
    {
        if (visible)
        {
            controller->setModalInputListener(this);
        }
        else
        {
            controller->setModalInputListener(nullptr);
        }
    }
}

void ScriptConsole::update(float deltaTime, float realTime)
{
    VE_UNUSED(realTime);

    if (!isVisible())
    {
        return;
    }

    mConsoleText->update(deltaTime, realTime);
    mPromptLine->update(deltaTime, realTime);
}

void ScriptConsole::addText(const std::string &text)
{
    if (!d->getLayer() || text.empty())
    {
        return;
    }

    if (Ogre::StringUtil::endsWith(text, "\n", false))
    {
        mConsoleText->addText(text);
    }
    else
    {
        mConsoleText->addText(text + "\n");
    }
}

void ScriptConsole::addToHistory(const std::string& cmd)
{
    d->to<ScriptConsolePrivate>()->addToHistory(cmd);
}

void ScriptConsole::clearHistory()
{
    d->to<ScriptConsolePrivate>()->clearHistory();
}

void ScriptConsole::clear()
{
    mConsoleText->setText(VE_STR_BLANK);
    mPromptLine->setText(VE_STR_BLANK);
}

void ScriptConsole::registredAsInputListener(bool registred, InputController *controller)
{
    if (mPromptLine)
    {
        mPromptLine->registredAsInputListener(registred, controller);
    }
}

/*
void ScriptConsole::setConsoleListener(ScriptConsoleListener *listener)
{
    if (mListener != listener)
    {
        delete mListener;
        mListener = listener;
    }
}
*/
/*
void ScriptConsole::setTextChanged()
{
    mTextChanged = true;
    currentLineTextChanged(mCurrentLine);
}
*/
void ScriptConsole::_completeLine()
{
    const std::string &promptWelcomeText = getPromptWelcomeText();
    std::string completeLine = mPromptLine->getText();

    /// Erase promptText
    if (Ogre::StringUtil::startsWith(completeLine, promptWelcomeText, false))
    {
        completeLine.erase(0, promptWelcomeText.size());
    }

    d->to<ScriptConsolePrivate>()->completeLine(completeLine, mPromptLine->getCursor()->getPosition(
                                                    UiMarkupTextEdit::Cursor::MM_Char));

    setPromptText(getPromptWelcomeText() + completeLine);
}

const std::string &ScriptConsole::getNextHistoryLine(bool forward)
{
    return d->to<ScriptConsolePrivate>()->getNextHistoryLine(forward);
}

bool ScriptConsole::keyPressed(const KeyboardEvent &event)
{
    /// TODO
//    DefaultInputListener::keyPressed(event);

    if (event.data.key != ve::KeyData::KEY_GRAVE && !isVisible())
    {
        return false;
    }

    switch (event.data.key)
    {
    case ve::KeyData::KEY_GRAVE:
        setVisible(!isVisible());
        break;

    case ve::KeyData::KEY_ESCAPE:
        setVisible(false);
        break;

    case ve::KeyData::KEY_RETURN:
    case ve::KeyData::KEY_NUMPAD_RETURN:
        _execLine();
        break;

    case ve::KeyData::KEY_UP:
        setPromptText(getNextHistoryLine(true));
        break;

    case ve::KeyData::KEY_DOWN:
        setPromptText(getNextHistoryLine(false));
        break;

    case ve::KeyData::KEY_LEFT:
        mPromptLine->getCursor()->setPosition(UiMarkupTextEdit::Cursor::MM_Char |
                                              UiMarkupTextEdit::Cursor::MM_Relative, -1);
        break;

    case ve::KeyData::KEY_RIGHT:
        mPromptLine->getCursor()->setPosition(UiMarkupTextEdit::Cursor::MM_Char |
                                              UiMarkupTextEdit::Cursor::MM_Relative, 1);
        break;

    case ve::KeyData::KEY_TAB:   // Tab completion implemented in Script
        _completeLine();
        break;

    default:
        mPromptLine->keyPressed(event);
        break;
    }

    return true;
}

void ScriptConsole::setWatchLogger(bool watch)
{
    d->to<ScriptConsolePrivate>()->setWatchLogger(watch);
}

bool ScriptConsole::isWatchingLogger() const
{
    return d->to<ScriptConsolePrivate>()-isWatchingLogger();
}

void ScriptConsole::setMaxLineCount(uint32_t count)
{
    d->to<ScriptConsolePrivate>()->setMaxLineCount(count);
}

uint32_t ScriptConsole::getMaxLineCount() const
{
    return d->to<ScriptConsolePrivate>()->getMaxLineCount();
}

void ScriptConsole::setHistroyMaxLineCount(uint32_t count)
{
    d->to<ScriptConsolePrivate>()->setHistroyMaxLineCount(count);
}

uint32_t ScriptConsole::getHistoryMaxLineCount() const
{
    return d->to<ScriptConsolePrivate>()->getHistroyMaxLineCount();
}

void ScriptConsole::setPromptText(const std::string &text)
{
    mPromptLine->setText(text);
}

const std::string &ScriptConsole::getPromptText() const
{
    return mPromptLine->getText();
}

void ScriptConsole::setPromptWelcomeText(const std::string &text)
{
    d->to<ScriptConsolePrivate>()->setPromptWelcomeText(text);
}

const std::string &ScriptConsole::getPromptWelcomeText() const
{
    return d->to<ScriptConsolePrivate>()->getPromptWelcomeText();
}

/*
void ScriptConsole::pushChar(unsigned int c)
{
    const char _c = c;
    {
        const size_t pos = Ogre::Math::Clamp(
                    cursor().getPosition(), (size_t)0, mCurrentLine.size());

        mCurrentLine.insert(pos, 1, _c);
        cursor().setPosition(pos + 1); // move cursor after insertion

        setTextChanged();
    }
}
*/

/*
void ScriptConsole::_backspace()
{
    const size_t cursorPos = cursor().getPosition();

    if (mCurrentLine.size() == 1)
    {
        mCurrentLine.clear();
        setTextChanged();
    }
    else
    if (mCurrentLine.size() > 1 && cursorPos > 0)
    {
        mCurrentLine.erase(cursorPos - 1, 1);
        setTextChanged();
    }

    if (cursorPos > 0)
    {
        cursor().setPosition(cursorPos - 1);
    }
}
*/

void ScriptConsole::_execLine()
{
    d->to<ScriptConsolePrivate>()->execLine(getPromptText());
    setPromptText(getPromptWelcomeText());
}
