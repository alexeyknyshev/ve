#include "uilayout.h"

#include <framework/ui/uiframe.h>

UiLayout::UiLayout()
    : mSpacing(2),
      mParent(nullptr)
{ }

void UiLayout::setParent(UiFrame *parent)
{
    mParent = parent;
    invalidateParent();
}

void UiLayout::invalidateParent()
{
    if (mParent)
    {
        mParent->invalidate();
    }
}

void UiLayout::addFrame(UiFrame *frame, uint32_t index, uint32_t sizeFactor)
{
    VE_UNUSED(frame); VE_UNUSED(index); VE_UNUSED(sizeFactor);
    invalidateParent();
}

void UiLayout::addFrame(std::initializer_list<UiFrame *> frameList, uint32_t index, uint32_t sizeFactor)
{
    VE_UNUSED(frameList); VE_UNUSED(index); VE_UNUSED(sizeFactor);
    invalidateParent();
}

bool UiLayout::removeFrame(int index)
{
    VE_UNUSED(index);
    invalidateParent();
    return false;
}

bool UiLayout::removeFrame(UiFrame *frame)
{
    VE_UNUSED(frame);
    invalidateParent();
    return false;
}

void UiLayout::setSpacing(uint32_t spacing)
{
    mSpacing = spacing;
    invalidateParent();
}
