#ifndef UIMARKUPTEXTEDIT_H
#define UIMARKUPTEXTEDIT_H

#include <framework/ui/uiframe.h>

class UiMarkupTextEditPrivate;

class UiMarkupTextEdit : public UiFrame
{
public:
    UiMarkupTextEdit(const std::string &name, Gorilla::Screen *screen);
    UiMarkupTextEdit(const std::string &name, UiFrame *parent);
    ~UiMarkupTextEdit();

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_UI);
        VE_UNUSED(event);
        return false;
    }

    bool keyPressed(const KeyboardEvent &event) override;

    void update(float deltaTime, float realTime) override;

    ve::SizeF getMinimumSizeHint() const override;

    inline float getSlowInsertTimeout() const
    {
        return mSlowInsertTimeout;
    }

    inline void setSlowInsertTimeout(float timeout)
    {
        mSlowInsertTimeout = timeout;
    }

    inline float getFastInsertTimeout() const
    {
        return mFastInsertTimeout;
    }

    inline void setFastInsertTimeout(float timeout)
    {
        mFastInsertTimeout = timeout;
    }

    void setTextFontSize(uint32_t fontSize);

    void setMaxLineCount(uint32_t count);
    uint32_t getMaxLineCount() const;

//    void setVisibleLineCount(uint32_t count);
    uint32_t getVisiableLineCount() const;

    void setText(const std::string &text);
    const std::string &getText() const;

    void clear();

    void setTextColor(const Ogre::ColourValue &color);
    const Ogre::ColourValue &getTextColor() const;

    void addText(const std::string &text);

    void registredAsInputListener(bool registred, InputController *controller);

//    size_t countWords(size_t beginCharIdx, size_t charCount) const;

protected:
    void _backspace();

    bool isValidChar(unsigned int c) const;

    ve::KeyData mCurrentKeyData;

public:
    class Cursor
    {
    public:
        Cursor(UiMarkupTextEditPrivate *edit)
            : mEdit(edit),
              mIsBlinking(false),
              mState(Visiable),
              mBlinkTime(0.0f),
              mBlinkTimeInterval(0.5f),
              mPosition(0)
        {
        }

        enum State
        {
            Visiable = 0,
            Hidden = 1
        };

        inline bool isBlinking() const
        {
            return mIsBlinking;
        }

        inline void setBlinking(bool blinking)
        {
            mIsBlinking = blinking;
        }

        inline State getState() const
        {
            return mState;
        }

        inline void setState(State state)
        {
            mState = state;
        }

        enum MoveMode
        {
            MM_Char = BIT(1),
            MM_Word = BIT(2),
            MM_Line = BIT(3),
            MM_WholeText = BIT(4),

            MM_Relative = BIT(0),
            MM_Absolute = 0
        };

        size_t getPosition(MoveMode mode) const;

        void setPosition(int moveModeMask, int pos);

        std::string getText(int moveModeMask, int count) const;

        void insert(unsigned int c);
        void insert(const std::string &str);

        /** @returns true if need update */
        bool update(float deltaTime);

    private:
        UiMarkupTextEditPrivate * const mEdit;
        bool mIsBlinking;
        State mState;
        float mBlinkTime;
        float mBlinkTimeInterval;

        size_t mPosition;
    };

    Cursor *getCursor();

private:
    float mSlowInsertTimeout;
    float mFastInsertTimeout;

    bool mCurrentInsertCharFast;
    bool mIsFollowingTextUpdates;

    InputController *mInputController;
};

#endif // UIMARKUPTEXTEDIT_H
