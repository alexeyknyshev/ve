#ifndef SCRIPTCONSOLEPRIVATE_H
#define SCRIPTCONSOLEPRIVATE_H

#include <OGRE/OgreLog.h>
#include <global.h>

#include <framework/stringlist.h>
#include <framework/controller/abstractinputlistener.h>
#include <framework/ui/uiframe_p.h>

namespace Gorilla
{
    class GlyphData;
}

namespace Ogre
{
    class Viewport;
}

class ScriptConsole;
class ScriptConsoleOptions;
class UiMarkupTextEdit;
class Script;

class ScriptConsolePrivate : public UiFramePrivate,
                             public Ogre::LogListener
{
public:
    ScriptConsolePrivate(ScriptConsole *console,
                         Gorilla::Screen *screen,
                         const ScriptConsoleOptions &opt);

    ~ScriptConsolePrivate();

    void setPromptWelcomeText(const std::string &name)
    { mPromptWelcomeText = name; }

    inline const std::string &getPromptWelcomeText() const
    { return mPromptWelcomeText; }

    void setWatchLogger(bool watch)
    { mIsWatchingLogger = watch; }

    inline bool isWatchingLogger() const
    { return mIsWatchingLogger; }

    void addToHistory(const std::string &cmd);

    void clearHistory();

    const std::string &getNextHistoryLine(bool forward);

    void completeLine(std::string &line, size_t pos);
    std::string execLine(std::string line);

    void setSlowInsertTimeout(float timeout)
    { mSlowInsertTimeout = timeout; }

    inline float getSlowInsertTimeout() const
    { return mSlowInsertTimeout; }

    void setFastInsertTimeout(float timeout)
    { mFastInsertTimeout = timeout; }

    inline float getFastInsertTimeout() const
    { return mFastInsertTimeout; }

    void setMaxLineCount(size_t count)
    { mMaxLineCount = count; }

    inline size_t getMaxLineCount() const
    { return mMaxLineCount; }

    void setHistroyMaxLineCount(size_t count)
    { mHistoryMaxLineCount = count; }

    inline size_t getHistroyMaxLineCount() const
    { return mHistoryMaxLineCount; }

    /// Logger hook
    void messageLogged(const std::string &message,
                       Ogre::LogMessageLevel lml,
                       bool maskDebug,
                       const std::string &logName
#if OGRE_VERSION_MAJOR >= 1 && OGRE_VERSION_MINOR >= 8 /* Ogre 1.8.0+ */
                       , bool &skipThisMessage
#endif
                       );

    inline  InputController *getInputController() const
    { return mInputController; }

protected:
    virtual void currentLineTextChanged(const std::string &text)
    {
        VE_UNUSED(text);
    }

    void addText(const std::string &text);

    bool mVisible;
    bool mIsWatchingLogger;
    uint32_t mTabStop;

    Script *mScript;

    StringList mLines;
    StringList mHistory;

    InputController * const mInputController;

private:
    ScriptConsole *mConsole;
    Gorilla::GlyphData *mGlyphData;

    uint32_t mMaxLineCount;
    uint32_t mHistoryMaxLineCount;

    void _execLine();

    float mSlowInsertTimeout;
    float mFastInsertTimeout;

    ve::KeyData mCurrentKeyData;

    bool mCurrentInsertCharFast;

    StringList::const_iterator mCurrentHistoryLineIterator;

    std::string mPromptWelcomeText;
};

#endif // SCRIPTCONSOLEPRIVATE_H
