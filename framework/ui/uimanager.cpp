#include "uimanager.h"

#include <framework/controller/inputcontroller.h>
#include <framework/frontend/renderview.h>

#include <framework/ui/3rdparty/Gorilla.h>

#include <framework/ui/uiframe.h>

UiManager *UiManager::getSingletonPtr()
{
    return static_cast<UiManager *>(Silverback::getSingletonPtr());
}

bool UiManager::keyPressed(const KeyboardEvent &event)
{
    ViewportList viewports = event.sender->getControlledRenderView()->getViewports();

    const auto topLevelFramesEnd = mTopLevelFrames.end();

    for (Ogre::Viewport *vp : viewports)
    {
        for (auto it = mTopLevelFrames.begin(); it != topLevelFramesEnd; ++it)
        {
            Gorilla::Screen *screen = it->second;
            if (vp == screen->getViewport())
            {
                UiFrame *frame = it->first;

                if (frame->isVisible())
                {
                    frame->event(&event);
                }
            }
        }
    }

    return true;
}

bool UiManager::keyReleased(const KeyboardEvent &)
{ return false; }

bool UiManager::mouseMoved(const MouseEvent &e)
{
    return mouseEvent(e);
}

bool UiManager::mousePressed(const MouseEvent &e)
{
    return mouseEvent(e);
}

bool UiManager::mouseReleased(const MouseEvent &e)
{
    return mouseEvent(e);
}

Gorilla::TextureAtlas *UiManager::loadAtlas(const std::string &altlasName,
                                            const std::string &resGroup)
{
    Gorilla::TextureAtlas *atlas = OGRE_NEW Gorilla::TextureAtlas(altlasName, resGroup);
    mAtlases.insert(std::make_pair(altlasName, atlas));
    return atlas;
}

Gorilla::Screen *UiManager::createScreen(Ogre::Viewport *viewport,
                                         Ogre::SceneManager *sceneManager,
                                         const std::string &atlasName)
{
    return Silverback::createScreen(viewport, sceneManager, atlasName);
}

Gorilla::Screen *UiManager::createScreen(Ogre::Viewport *viewport,
                                         Ogre::SceneManager *sceneManager,
                                         Gorilla::TextureAtlas *atlas)
{
    return Silverback::createScreen(viewport, sceneManager, atlas);
}

Gorilla::ScreenRenderable *UiManager::createScreenRenderable(const Ogre::Vector2 &maxSize,
                                                             const std::string &atlasName)
{
    return Silverback::createScreenRenderable(maxSize, atlasName);
}

Gorilla::ScreenRenderable *UiManager::createScreenRenderable(const Ogre::Vector2 &maxSize,
                                                             Gorilla::TextureAtlas *atlas)
{
    return Silverback::createScreenRenderable(maxSize, atlas);
}

Gorilla::Screen *UiManager::getScreen(Ogre::Viewport *viewport) const
{
    for (Gorilla::Screen *screen : mScreens)
    {
        if (screen->getViewport() == viewport)
        {
            return screen;
        }
    }

    return nullptr;
}

const std::vector<Gorilla::Screen*> &UiManager::getScreens() const
{
    return mScreens;
}

// --------------------------------------

void UiManager::inputControllerRegistred(bool registred, InputController *controller)
{
    VE_UNUSED(controller);

    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(inputControllerRegistred(bool registred,
                                                                       InputController *controller)),
                                InputController,
                                controller,
                                Logger::LOG_COMPONENT_USERINPUT))
    {
        if (registred)
        {
            log("UiManager detected registration of " + VE_CLASS_NAME(InputController).toString() +
                " (" + controller->getName() + ")",
                Logger::LOG_LEVEL_INFO,
                Logger::LOG_COMPONENT_UI);

            controller->addInputListener(this, false);
        }
        else
        {
            log("UiManager detected removing of " + VE_CLASS_NAME(InputController).toString() +
                " (" + controller->getName() + ")",
                Logger::LOG_LEVEL_INFO,
                Logger::LOG_COMPONENT_UI);

            controller->removeInputListener(this);
        }
    }
}

void UiManager::registredAsInputListener(bool registred, InputController *controller)
{
    VE_UNUSED(registred);
    VE_UNUSED(controller);
}

bool UiManager::mouseEvent(const MouseEvent &event)
{
    ViewportList viewports = event.sender->getControlledRenderView()->getViewports();

    const auto topLevelFramesEnd = mTopLevelFrames.end();

    bool isEventProcessed = false;

    for (Ogre::Viewport *vp : viewports)
    {
        int left, top, width, height;

        vp->getActualDimensions(left, top, width, height);
        if ((uint32_t)left <= event.data.x && (uint32_t)(left + width) > event.data.x &&
            (uint32_t)top  <= event.data.y && (uint32_t)top + height > event.data.y)
        {
            for (auto it = mTopLevelFrames.begin(); it != topLevelFramesEnd; ++it)
            {
                Gorilla::Screen *screen = it->second;
                if (vp == screen->getViewport())
                {
                    UiFrame *frame = it->first;

                    isEventProcessed |= frame->event(&event);
                }
            }
        }
    }

    return isEventProcessed;
}

bool UiManager::addTopLevelFrame(UiFrame *frame, Gorilla::Screen *screen)
{
    return mTopLevelFrames.insert({frame, screen}).second;
}

bool UiManager::removeTopLevelFrame(UiFrame *frame)
{
    return mTopLevelFrames.erase(frame);
}

bool UiManager::isTopLevelFrame(UiFrame *frame) const
{
    return (mTopLevelFrames.find(frame) != mTopLevelFrames.end());
}

const UiManager::TopLevelFrames &UiManager::getTopLevelFrames() const
{
    return mTopLevelFrames;
}

UiManager *ui(bool checkPointer)
{
    UiManager *uiMgr = UiManager::getSingletonPtr();
    if (checkPointer)
    {
        VE_CHECK_NULL_PTR(UiManager, uiMgr, Logger::LOG_COMPONENT_UI);
    }
    return uiMgr;
}
