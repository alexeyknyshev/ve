#include "markuptext.h"

#include <framework/object.h>

#include <stack>

MarkupTextBlockIterator::MarkupTextBlockIterator(MarkupText &text)
    : mBlockList(&text.mTextBlocks)
{
    mBlockIt = mBlockList->begin();
    if (mBlockIt != mBlockList->end())
    {
        mTextIt = mBlockIt->mText.begin();
    }
}

MarkupTextBlockIterator &MarkupTextBlockIterator::operator++()
{
    const auto listEnd = mBlockList->end();
    if (mBlockIt != listEnd)
    {
        if (mTextIt != mBlockIt->mText.end())
        {
            ++mTextIt;
            if (mTextIt == mBlockIt->mText.end())
            {
                ++mBlockIt;
                if (mBlockIt != listEnd)
                {
                    mTextIt = mBlockIt->mText.begin();
                }
            }
        }
        else
        {
            ++mBlockIt;
            if (mBlockIt != listEnd)
            {
                mTextIt = mBlockIt->mText.begin();
            }
        }
    }
    return *this;
}

// ------------------------------------------------

MarkupTextCursor::MarkupTextCursor(MarkupText *text)
     : mText(text)
{
    if (VE_CHECK_NULL_PTR(MarkupText, text, Logger::LOG_COMPONENT_UI))
    {
        mIt = MarkupTextBlockIterator(*text);
    }
}

std::string MarkupTextCursor::getSelectedText() const
{
    if (!isValid())
    {
        return VE_STR_BLANK;
    }

    std::string output;

    /*
    const size_t charCount = getMarkupText()->mCharacters.size();
    const size_t textSize = getMarkupText()->mText.size();

    size_t from = mBegin < charCount ? mBegin : charCount - 1;
    size_t to = mEnd < charCount ? mEnd : charCount;

    for (; from < to; ++from)
    {
        const size_t index = getMarkupText()->mCharacters[from].mIndex;
        if (index < textSize)
        {
            output += getMarkupText()->mText.at(index);
        }
    }
    */

    return output;
}

size_t MarkupTextCursor::removeSelectedText()
{
    if (!isValid())
    {
        return 0;
    }

    /*
    const size_t charCount = getMarkupText()->mCharacters.size();
    const size_t textSize = getMarkupText()->mText.size();

    size_t from = mBegin < charCount ? mBegin : charCount - 1;
    size_t to = mEnd < charCount ? mEnd : charCount;
    */

    std::set<size_t> idxToRemove;

    /*
    for (; from < to; ++from)
    {
        const size_t index = getMarkupText()->mCharacters[from].mIndex;
        if (index < textSize)
        {
            idxToRemove.insert(index);
        }
    }

    auto end = idxToRemove.rend();
    for (auto it = idxToRemove.rbegin(); it != end; ++it)
    {
        getMarkupText()->mText.erase(*it, 1);
    }
    */

    return idxToRemove.size();
}

void MarkupTextCursor::setSelectionColor(const Ogre::ColourValue &color)
{
    if (!isValid())
    {
        return;
    }

    /*
    const size_t charCount = getMarkupText()->mCharacters.size();

    size_t from = mBegin < charCount ? mBegin : charCount - 1;
    size_t to = mEnd < charCount ? mEnd : charCount;

    for (; from < to; ++from)
    {
        getMarkupText()->mCharacters[from].mColour = color;
    }
    */
}

void MarkupTextCursor::setDefaultColor(const Ogre::ColourValue &color)
{
    if (isValid())
    {
        getMarkupText()->mLayer->_getAtlas()->setMarkupColour(0, color);
    }
}

// -------------------------------------------------------

MarkupText::MarkupText(uint32_t defaultGlyphIndex,
                       float left,
                       float top,
                       const std::string &text,
                       Gorilla::Layer *parent)
    : Gorilla::MarkupText(defaultGlyphIndex,
                          left,
                          top,
                          text,
                          parent)
{

}

class MarkupTextState
{
public:
    MarkupTextState(Gorilla::GlyphData *glyphData,
                    const Ogre::ColourValue &color,
                    size_t height)
        : mGlyphData(glyphData),
          mDefaultTextColor(color),
          mDefaultTextHeight(height),
          mFixedCharWidth(false),
          mKerning(0.0f)
    {
        VE_CHECK_NULL_PTR(Gorilla::GlyphData, glyphData, Logger::LOG_COMPONENT_UI);
    }

    inline const Gorilla::GlyphData *getGlyphData() const
    {
        return mGlyphData;
    }

    inline const Ogre::ColourValue &getTextColor() const
    {
        if (mTextColorStack.empty())
        {
            return mDefaultTextColor;
        }

        return mTextColorStack.top();
    }

    inline void restoreTextColor()
    {
        mTextColorStack.pop();
    }

    inline size_t getTextHeight() const
    {
        if (mTextHeightStack.empty())
        {
            return mDefaultTextHeight;
        }

        return mTextHeightStack.top();
    }

    inline void restoreTextHeight()
    {
        mTextHeightStack.pop();
    }

    void processFlag(const std::string &flag)
    {
        if (flag.empty())
        {
            return;
        }

        const size_t eqPos = flag.find('=');
        if (eqPos != std::string::npos && eqPos < flag.size() - 1)
        {
            const std::string key = flag.substr(0, eqPos);
            const std::string val = flag.substr(eqPos + 1);

            if (key == "color")
            {
                mTextColorStack.push(Ogre::StringConverter::parseColourValue(
                                         val, mDefaultTextColor));
            }
            else if (key == "height")
            {
                mTextHeightStack.push(Ogre::StringConverter::parseUnsignedInt(
                                          val, mDefaultTextHeight));
            }
        }
    }

    Gorilla::MarkupText::Character generateCharacter(
            Ogre::uint prevChar, Ogre::uint currentChar, size_t idx,
            const float cursorX, const float cursorY,
            const float texelOffsetX, const float texelOffsetY);

private:
    std::stack<Ogre::ColourValue> mTextColorStack;
    std::stack<size_t> mTextHeightStack;

    Gorilla::GlyphData *mGlyphData;
    const Ogre::ColourValue mDefaultTextColor;
    const size_t mDefaultTextHeight;
    bool mFixedCharWidth;
    float mKerning;
};

Gorilla::MarkupText::Character MarkupTextState::generateCharacter(
        Ogre::uint prevChar, Ogre::uint currentChar, size_t idx,
        const float cursorX, const float cursorY,
        const float texelOffsetX, const float texelOffsetY)
{
    Gorilla::Glyph *glyph = mGlyphData->getGlyph(currentChar);
    VE_CHECK_NULL_PTR(Gorilla::Glyph, glyph, Logger::LOG_COMPONENT_UI);

    if (!mFixedCharWidth)
    {
        mKerning = glyph->getKerning(prevChar);
        if (mKerning == 0.0f)
        {
            mKerning = mGlyphData->mLetterSpacing;
        }
    }

    // --------------------------------------------

    const float left   = cursorX;
    const float top    = cursorY + glyph->verticalOffset;
    const float right  = cursorX + glyph->glyphWidth + texelOffsetX;
    const float bottom = top + glyph->glyphHeight + texelOffsetY;

    Gorilla::MarkupText::Character c;

    c.mIndex = idx;

    // position on MarkupText
    c.mPosition[Gorilla::TopLeft].x     = left;
    c.mPosition[Gorilla::TopLeft].y     = top;
    c.mPosition[Gorilla::TopRight].x    = right;
    c.mPosition[Gorilla::TopRight].y    = top;
    c.mPosition[Gorilla::BottomLeft].x  = left;
    c.mPosition[Gorilla::BottomLeft].y  = bottom;
    c.mPosition[Gorilla::BottomRight].x = right;
    c.mPosition[Gorilla::BottomRight].y = bottom;

    // texture block in atlas
    c.mUV[0] = glyph->texCoords[0];
    c.mUV[1] = glyph->texCoords[1];
    c.mUV[2] = glyph->texCoords[2];
    c.mUV[3] = glyph->texCoords[3];

    return c;
}

void MarkupText::_buildTextBlocks(const std::string &text)
{
    float cursorX = mLeft;
    float cursorY = mTop;
    const float texelOffsetX = mLayer->_getTexelX();
    const float texelOffsetY = mLayer->_getTexelY();

    mMaxTextWidth = 0;
    mTextBlocks.clear();

    MarkupTextState state(mDefaultGlyphData,
                          mLayer->_getMarkupColour(0),
                          mDefaultGlyphData->mLineHeight);

    Ogre::uint currChar = 0;
    Ogre::uint prevChar = 0;

    for (size_t i = 0; i < text.size(); ++i)
    {
        currChar = mText[i];

        if (currChar == ' ')
        {
            prevChar = currChar;
            cursorX += state.getGlyphData()->mSpaceLength;
            continue;
        }

        if (currChar == '\n')
        {
            prevChar = currChar;
            cursorX = mLeft;
            cursorY += state.getTextHeight();
            state.restoreTextHeight();
            continue;
        }

        if (currChar < state.getGlyphData()->mRangeBegin ||
            currChar > state.getGlyphData()->mRangeEnd)
        {
            prevChar = 0;
            continue;
        }

        if (currChar == '%')
        {
            const size_t it = text.find('%', i);
            if (it != std::string::npos)
            {
                const size_t count = it - (i + 1);
                const std::string flag = text.substr(i + 1, count);
                state.processFlag(flag);
                i = it;
            }
            continue;
        }

        Character c = state.generateCharacter(prevChar, currChar, i,
                                              cursorX, cursorY,
                                              texelOffsetX, texelOffsetY);

        if (cursorX > mMaxTextWidth)
        {
            mMaxTextWidth = cursorX;
        }

        prevChar = currChar;
    }

    mMaxTextWidth -= mLeft;

    mTextDirty = true;
}

void MarkupText::_calculateCharacters(const std::string &text)
{
    if (!mTextDirty)
    {
        return;
    }

    float cursorX = mLeft,
          cursorY = mTop,
          kerning = 0.0f,
          texelOffsetX = mLayer->_getTexelX(),
          texelOffsetY = mLayer->_getTexelY();

    Ogre::uint lastChar = 0;

    mMaxTextWidth = 0;

    const Ogre::ColourValue color = mLayer->_getMarkupColour(0);
    VE_UNUSED(color);

    const Gorilla::GlyphData *glyphData = mDefaultGlyphData;

    float currentLineHeight = glyphData->mLineHeight;
    bool fixedWidth = false;
    bool inMarkupMode = false;

    for (const Ogre::uint &currentChar : text)
    {
        if (currentChar == ' ')
        {
            lastChar = currentChar;
            cursorX += glyphData->mSpaceLength;
            continue;
        }

        if (currentChar == '\n')
        {
            lastChar = currentChar;
            cursorX = mLeft;
            cursorY += currentLineHeight;
            continue;
        }

        if (currentChar < glyphData->mRangeBegin || currentChar > glyphData->mRangeEnd)
        {
            lastChar = 0;
            continue;
        }

        if (currentChar == '%' && !inMarkupMode)
        {
            inMarkupMode = true;
            continue;
        }

        const Gorilla::Glyph *glyph = glyphData->getGlyph(currentChar);
        if (!glyph)
        {
            continue;
        }

        if (!fixedWidth)
        {
            kerning = glyph->getKerning(lastChar);
            if (kerning == 0)
            {
                kerning = glyphData->mLetterSpacing;
            }
        }
        const float left   = cursorX;
        const float right  = left + glyph->glyphWidth + texelOffsetX;
        const float top    = cursorY + glyph->verticalOffset;
        const float bottom = top + glyph->glyphHeight + texelOffsetY;
        VE_UNUSED(bottom);

        Character c;

        c.mPosition[Gorilla::TopLeft].x = left;
        c.mPosition[Gorilla::TopLeft].y = top;

        c.mPosition[Gorilla::TopRight].x = right;
        c.mPosition[Gorilla::TopRight].y = top;

        /*
        c.mPosition[Gorilla::BottomLeft].x =
        c.mPosition[Gorilla::BottomLeft].y

        c.mPosition[Gorilla::BottomRight].x
        c.mPosition[Gorilla::BottomRight].y
        */
    }
}
/*
Gorilla::MarkupText *MarkupTextFactory::createMarkupText(Ogre::uint defaultGlyphIndex, Ogre::Real x, Ogre::Real y, const Ogre::String &text, Gorilla::Layer *layer)
{
    return new MarkupText(defaultGlyphIndex, x, y, text, layer);
}

void MarkupTextFactory::destroyMarkupText(Gorilla::MarkupText *markupText)
{
    delete markupText;
}
*/
