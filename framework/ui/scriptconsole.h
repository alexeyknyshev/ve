#ifndef SCRIPTCONSOLE_H
#define SCRIPTCONSOLE_H

#include <framework/controller/defaultinputlistener.h>
#include <framework/ui/uiframe.h>

class Script;
class InputController;

class UiMarkupTextEdit;
//class ScriptConsoleListener;

class ScriptConsolePrivate;
class ScriptConsoleOptions
{
public:
    ScriptConsoleOptions()
        : watchLogger(false),
          cursorBlinking(false),
          fontSize(12),
//          position(0.0f, 0.0f),
//          size(100.0f, 100.0f),
          borderWidth(5.0f),
//          visibleLineCount(24),
          maxLineCount(8192),
          historyMaxLineCount(32),
          promptText("> "),
          slowInsertTimeout(1.0f),
          fastInsertTimeout(0.25f),
          script(nullptr),
          inputController(nullptr)
    { }

    std::string name;
    bool watchLogger;
    bool cursorBlinking;
    uint32_t fontSize;
//    ve::PointF position;
//    ve::SizeF size;
    float borderWidth;
//    uint32_t visibleLineCount;
    uint32_t maxLineCount;
    uint32_t historyMaxLineCount;
    std::string promptText;
    float slowInsertTimeout;
    float fastInsertTimeout;
    Script *script; /// @remarks ScriptConsole takes ownership of @script
    InputController *inputController;
};

class ScriptConsole : public UiFrame
{
public:
    VE_DECLARE_TYPE_INFO(ScriptConsole)

    ScriptConsole(Gorilla::Screen *screen, const ScriptConsoleOptions &opt);
    ScriptConsole(ScriptConsolePrivate *d_);

    virtual ~ScriptConsole();

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_SCRIPT);
        VE_UNUSED(event);
        return false;
    }

    void setVisible(bool visible) override;

    virtual void addText(const std::string &text);

    void update(float deltaTime, float realTime) override;

    /// Keyboard hook
    bool keyPressed(const KeyboardEvent &event) override;

    void pushChar(unsigned int c);

    void setWatchLogger(bool watch);
    bool isWatchingLogger() const;

    void setMaxLineCount(uint32_t count);
    uint32_t getMaxLineCount() const;

    void setHistroyMaxLineCount(uint32_t count);
    uint32_t getHistoryMaxLineCount() const;

    void setPromptText(const std::string &text);
    const std::string &getPromptText() const;

    void setPromptWelcomeText(const std::string &text);
    const std::string &getPromptWelcomeText() const;

//    void setVisibleLineCount(size_t count);
    size_t getVisibleLineCount() const;

    void setSlowInsertTimeout(float timeout);
    float getSlowInsertTimeout() const;

    void setFastInsertTimeout(float timeout);
    float getFastInsertTimeout() const;

    void addToHistory(const std::string &cmd);
    void clearHistory();

    void clear();

//    void setConsoleListener(ScriptConsoleListener *listener);

    void registredAsInputListener(bool registred, InputController *controller);

//    ScriptConsoleListener *mListener;

protected:
    const std::string &getNextHistoryLine(bool forward);
    void _completeLine();
    void _execLine();

private:
    UiMarkupTextEdit *mPromptLine;
    UiMarkupTextEdit *mConsoleText;
};

/*
class ScriptConsoleListener
{
public:
    virtual ~ScriptConsoleListener()
    { }

    virtual void visiblityChanged(bool isVisiable);
    virtual void commandExecuted(const std::string &command);
};
*/

#endif // SCRIPTCONSOLE_H
