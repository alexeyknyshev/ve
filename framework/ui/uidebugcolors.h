#ifndef UIDEBUGCOLORS_H
#define UIDEBUGCOLORS_H

#include <OGRE/OgreColourValue.h>

class UiDebugColors
{
public:
    void addDebugColor(size_t depth, size_t index, const Ogre::ColourValue &color);
    void setDebugColorsForDepth(size_t depth, const std::list<Ogre::ColourValue> &colors);

    bool isEmpty() const;

    const Ogre::ColourValue &getDebugColor(size_t depth, size_t index) const;

private:
    typedef std::map<size_t, Ogre::ColourValue> ColorMap;
    typedef std::map<size_t, ColorMap> Colors;

    Colors mColors;
};

#endif // UIDEBUGCOLORS_H
