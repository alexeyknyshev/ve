#include "uiboxlayout.h"

#include <framework/ui/uiframe.h>

UiBoxLayout::~UiBoxLayout()
{
    for (FrameEntry &entry : mFrames)
    {
        delete entry.frame;
    }
    mFrames.clear();
}

void UiBoxLayout::setVisible(bool visible)
{
    for (FrameEntry &entry : mFrames)
    {
        entry.frame->setVisible(visible);
    }
}

void UiBoxLayout::addFrame(UiFrame *frame, uint32_t index, uint32_t sizeFactor)
{
    if (!frame)
    {
        return;
    }

    if (index == UINT32_MAX || mFrames.empty())
    {
        mFrames.push_back({ frame, sizeFactor });
    }
    else
    {
        auto it = mFrames.cbegin();
        std::advance(it, index);

        mFrames.insert(it, { frame, sizeFactor });
    }

    invalidateParent();
}

void UiBoxLayout::addFrame(std::initializer_list<UiFrame *> frameList, uint32_t index, uint32_t sizeFactor)
{
    /// TODO:

    for (UiFrame *frame : frameList)
    {
        addFrame(frame, index, sizeFactor);

        if (index != UINT32_MAX)
        {
            index++;
        }
    }
}

bool UiBoxLayout::removeFrame(UiFrame *frame)
{
    const auto end = mFrames.cend();
    for (auto it = mFrames.cbegin(); it != end; ++it)
    {
        if (it->frame == frame)
        {
            mFrames.erase(it);
            invalidateParent();
            return true;
        }
    }

    return false;
}

bool UiBoxLayout::removeFrame(int index)
{
    if (mFrames.removeAt(index))
    {
        invalidateParent();
        return true;
    }
    return false;
}

// --------------------------------------------

void UiBoxLayout::setChildsDebugEnabled(bool enable, bool recursive)
{
    for (FrameEntry &entry : mFrames)
    {
        entry.frame->setDebugEnabled(enable, recursive);
    }
}

void UiBoxLayout::setChildsDebugColorRecursive(const UiDebugColors &colors, size_t depth)
{
    size_t index = 0;
    for (FrameEntry &entry : mFrames)
    {
        entry.frame->_setDebugBackgroundColorRecursive(colors, depth, index);
    }
}

// --------------------------------------------

List<UiFrame *> UiBoxLayout::getChildFrames() const
{
    List<UiFrame *> result;

    for (const FrameEntry &entry : mFrames)
    {
        result += entry.frame;
    }

    return result;
}

// --------------------------------------------

size_t UiBoxLayout::calculateTotalSectionsCount(bool countExpanding) const
{
    size_t result = 0;

    for (const FrameEntry &entry : mFrames)
    {
        if (!countExpanding && entry.frame->isExpanding())
        {
            continue;
        }
        result += entry.sizeFactor;
    }

    return result;
}

size_t UiBoxLayout::expandingFramesCount() const
{
    return std::count_if(mFrames.begin(), mFrames.end(),
                         [](const FrameEntry &entry) -> bool { return entry.frame->isExpanding(); });
}

void UiBoxLayout::_checkRecursiveChildParentRelations(const UiFrame * const child) const
{
    if (!child)
    {
        return;
    }

    veAssert(child != getParent(),
             VE_CLASS_NAME(UiBoxLayout),
             VE_SCOPE_NAME(_checkRecursiveChildParentRelations(const UiFrame * const child)),
             "Parent " + VE_CLASS_NAME(UiFrame).toString() + " (" + getParent()->getName() +
             ") is presented as child of layout!",
             Logger::LOG_COMPONENT_UI);
}
