#ifndef UILAYOUTSPACER_H
#define UILAYOUTSPACER_H

#include <framework/ui/uiframe.h>

class UiLayoutSpacer : public UiFrame
{
public:
    UiLayoutSpacer(const std::string &name, UiFrame *parent);

    bool isExpanding() const override { return true; }
};

#endif // UILAYOUTSPACER_H
