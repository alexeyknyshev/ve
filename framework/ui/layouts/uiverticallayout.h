#ifndef UIVERTICALLAYOUT_H
#define UIVERTICALLAYOUT_H

#include <framework/ui/layouts/uiboxlayout.h>

class UiVerticalLayout : public UiBoxLayout
{
public:
    ve::SizeF getMinimumSizeHint() const;

protected:
    void parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size) override;
};

#endif // UIVERTICALLAYOUT_H
