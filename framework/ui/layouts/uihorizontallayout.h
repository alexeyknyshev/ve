#ifndef UIHORIZONTALLAYOUT_H
#define UIHORIZONTALLAYOUT_H

#include <map>

#include <framework/ui/layouts/uiboxlayout.h>

class UiHorizontalLayout : public UiBoxLayout
{
public:
    ve::SizeF getMinimumSizeHint() const;

protected:
    void parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size) override;
};

#endif // UIHORIZONTALLAYOUT_H
