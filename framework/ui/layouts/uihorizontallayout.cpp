#include "uihorizontallayout.h"

#include <framework/ui/uiframe.h>

ve::SizeF UiHorizontalLayout::getMinimumSizeHint() const
{
    ve::SizeF result(0.0f, 0.0f);
    const size_t framesCount = getUiFrameCount();
    if (framesCount)
    {
        for (const FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            ve::SizeF frameSizeHint = entry.frame->getMinimumSizeHint();

            if (frameSizeHint.height > result.height)
            {
                result.height = frameSizeHint.height;
            }

            result.width += frameSizeHint.height;
        }

        result.width += (framesCount - 1) * getSpacing();
    }

    return result;
}

void UiHorizontalLayout::parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size)
{
    const size_t spacesCount = getUiFrameCount() + 1;

    const size_t expCount = expandingFramesCount();
    const size_t sectionsCount = calculateTotalSectionsCount(true /* count expanding */);

    if (expCount == 0)
    {
        const float leftWidth = size.width - spacesCount * getSpacing();

        const float currentSectionWidth = leftWidth / sectionsCount;
        float currentWidth = position.x + getSpacing();

        for (FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            const float frameWidth = entry.sizeFactor * currentSectionWidth;

            const ve::PointF position_(currentWidth, position.y + getSpacing());
            const ve::SizeF size_(frameWidth, size.height - 2 * getSpacing());

            entry.frame->setGeometry(position_, size_);

            currentWidth += frameWidth + getSpacing();
        }
    }
    else
    {
        ve::SizeF minimumSize = getMinimumSizeHint();
        const float sectionWidthGeneral = minimumSize.width / calculateTotalSectionsCount(false);

        const float expandingWidth = size.width - minimumSize.width - 2 * getSpacing();
        const float sectionWidthExpanding = expandingWidth / expCount;

        float currentWidth = position.x + getSpacing();

        for (FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            float currentSectionWidth = entry.frame->isExpanding() ?
                                       sectionWidthExpanding : sectionWidthGeneral;

            float frameWidth = entry.sizeFactor * currentSectionWidth;

            const ve::PointF position_(currentWidth, position.y + getSpacing());
            const ve::SizeF size_(frameWidth, size.height - 2 * getSpacing());

            entry.frame->setGeometry(position_, size_);

            currentWidth += frameWidth + getSpacing();
        }
    }
}
