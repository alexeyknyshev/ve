#ifndef UIBOXLAYOUT_H
#define UIBOXLAYOUT_H

#include <framework/ui/uilayout.h>

class UiBoxLayout : public UiLayout
{
public:
    ~UiBoxLayout();

    void setVisible(bool visiable);

    void addFrame(UiFrame *frame, uint32_t index = UINT32_MAX, uint32_t sizeFactor = 1) override;
    void addFrame(std::initializer_list<UiFrame *> frameList, uint32_t index = UINT32_MAX, uint32_t sizeFactor = 1) override;

    bool removeFrame(UiFrame *frame) override;
    bool removeFrame(int index) override;

    inline size_t getUiFrameCount() const override
    { return mFrames.size(); }

protected:
    void setChildsDebugEnabled(bool enable, bool recursive) override;
    void setChildsDebugColorRecursive(const UiDebugColors &colors, size_t depth) override;

    List<UiFrame *> getChildFrames() const override;

    size_t calculateTotalSectionsCount(bool countExpanding) const;
    size_t expandingFramesCount() const;

    void _checkRecursiveChildParentRelations(const UiFrame * const child) const;

    struct FrameEntry
    {
        UiFrame *frame;
        size_t sizeFactor;
    };

    typedef List<FrameEntry> FrameEntryList;
    FrameEntryList mFrames;
};

#endif // UIBOXLAYOUT_H
