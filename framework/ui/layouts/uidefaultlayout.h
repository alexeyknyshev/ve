#ifndef UIDEFAULTLAYOUT_H
#define UIDEFAULTLAYOUT_H

#include <framework/ui/uilayout.h>

class UiDefaultLayout : public UiLayout
{
public:
    UiDefaultLayout();
    ~UiDefaultLayout();

    ve::SizeF getMinimumSizeHint() const;

    void setVisible(bool visible) override;

    void addFrame(UiFrame *frame, uint32_t index = UINT32_MAX, uint32_t sizeFactor = 1) override;

    bool removeFrame(UiFrame *frame) override;
    bool removeFrame(int index) override;

    size_t getUiFrameCount() const override
    { return 1; }

protected:
    void parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size) override;

    List<UiFrame *> getChildFrames() const override
    { return { mFrame }; }

    void setChildsDebugEnabled(bool enable, bool recursive) override;
    void setChildsDebugColorRecursive(const UiDebugColors &colors, size_t depth) override;

    UiFrame *mFrame;
};

#endif // UIDEFAULTLAYOUT_H
