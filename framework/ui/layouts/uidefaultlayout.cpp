#include "uidefaultlayout.h"

#include <framework/ui/uiframe.h>
#include <framework/ui/layouts/uilayoutspacer.h>
#include <framework/ui/uidebugcolors.h>

#include <global.h>

#include <framework/rect.h>

UiDefaultLayout::UiDefaultLayout()
    : mFrame(nullptr)
{ }

UiDefaultLayout::~UiDefaultLayout()
{
    delete mFrame;
    mFrame = nullptr;
}

ve::SizeF UiDefaultLayout::getMinimumSizeHint() const
{
    if (mFrame)
    {
        ve::SizeF result = mFrame->getMinimumSizeHint();
        result.height += 2 * getSpacing();
        result.width  += 2 * getSpacing();
        return result;
    }
    return ve::SizeF(0.0f, 0.0f);
}

void UiDefaultLayout::setVisible(bool visible)
{
    if (mFrame)
    {
        mFrame->setVisible(visible);
    }
}

void UiDefaultLayout::addFrame(UiFrame *frame, uint32_t index, uint32_t sizeFactor)
{
    mFrame = frame;
    invalidateParent();
    VE_UNUSED(index);
    VE_UNUSED(sizeFactor);
}

bool UiDefaultLayout::removeFrame(int index)
{
    mFrame = nullptr;
    invalidateParent();
    VE_UNUSED(index);
    return true;
}

bool UiDefaultLayout::removeFrame(UiFrame *frame)
{
    if (mFrame && mFrame == frame)
    {
        mFrame = nullptr;
        return true;
    }

    return false;
}

void UiDefaultLayout::parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size)
{
    if (mFrame)
    {
        const ve::PointF position_ = position + ve::PointF(getSpacing(), getSpacing());
        const ve::SizeF size_(size.width - 2 * getSpacing(), size.height - 2 * getSpacing());

        mFrame->setGeometry(position_, size_);
    }
}

void UiDefaultLayout::setChildsDebugEnabled(bool enable, bool recursive)
{
    VE_UNUSED(recursive);
    if (mFrame)
    {
        mFrame->setDebugEnabled(enable);
    }
}

void UiDefaultLayout::setChildsDebugColorRecursive(const UiDebugColors &colors, size_t depth)
{
    if (mFrame && !colors.isEmpty())
    {
        mFrame->_setDebugBackgroundColorRecursive(colors, depth, 0);
    }
}


