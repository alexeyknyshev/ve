#include "uilayoutspacer.h"

#include <framework/ui/uiframe_p.h>

class UiLayoutSpacerPrivate : public UiFramePrivate
{
public:
    UiLayoutSpacerPrivate(const std::string &name, UiFrame *parent)
        : UiFramePrivate(name, parent)
    { }
};

UiLayoutSpacer::UiLayoutSpacer(const std::string &name, UiFrame *parent)
    : UiFrame(new UiLayoutSpacerPrivate(name, parent))
{ }
