#include "uiverticallayout.h"

#include <global.h>

#include <framework/rect.h>
#include <framework/ui/uiframe.h>

ve::SizeF UiVerticalLayout::getMinimumSizeHint() const
{
    ve::SizeF result(0.0f, 0.0f);
    const size_t framesCount = getUiFrameCount();
    if (framesCount > 0)
    {
        for (const FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            ve::SizeF frameSizeHint = entry.frame->getMinimumSizeHint();

            if (frameSizeHint.width > result.width)
            {
                result.width = frameSizeHint.width;
            }

            result.height += frameSizeHint.height;
        }

        result.height += (framesCount - 1) * getSpacing();
    }

    return result;
}

void UiVerticalLayout::parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size)
{
    const size_t spacesCount = getUiFrameCount() + 1;

    const size_t expCount = expandingFramesCount();
    const size_t sectionsCount = calculateTotalSectionsCount(true /* count expanding */);

    if (expCount == 0)
    {
        const float leftHeight = size.height - spacesCount * getSpacing();

        const float currentSectionHeight = leftHeight / sectionsCount;

        float currentHeight = position.y + getSpacing();

        for (FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            const float frameHeight = entry.sizeFactor * currentSectionHeight;

            const ve::PointF position_(position.x + getSpacing(), currentHeight);
            const ve::SizeF size_(size.width - 2 * getSpacing(), frameHeight);

            entry.frame->setGeometry(position_, size_);

            currentHeight += frameHeight + getSpacing();
        }
    }
    else
    {
        ve::SizeF minimumSize = getMinimumSizeHint();
        const float sectionHeightGeneral = minimumSize.height / calculateTotalSectionsCount(false);

        const float expandingHeight = size.height - minimumSize.height - 2 * getSpacing();
        const float sectionHeightExpanding = expandingHeight / expCount;

        float currentHeight = position.y + getSpacing();

        for (FrameEntry &entry : mFrames)
        {
            _checkRecursiveChildParentRelations(entry.frame);

            float currentSectionHeight = entry.frame->isExpanding() ?
                                       sectionHeightExpanding : sectionHeightGeneral;

            const float frameHeight = entry.sizeFactor * currentSectionHeight;

            const ve::PointF position_(position.x + getSpacing(), currentHeight);
            const ve::SizeF size_(size.width - 2 * getSpacing(), frameHeight);

            entry.frame->setGeometry(position_, size_);

            currentHeight += frameHeight + getSpacing();
        }
    }
}
