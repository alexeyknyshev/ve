#ifndef UIFRAMEPRIVATE_H
#define UIFRAMEPRIVATE_H

#include "uiframe.h"

#include <OGRE/OgreNameGenerator.h>

namespace Gorilla
{
    class Layer;
    class Rectangle;
    class Screen;
}

class UiFramePrivate
{
public:
    UiFramePrivate(const std::string &name, Gorilla::Screen *screen);
    UiFramePrivate(const std::string &name, UiFrame *parent);
    UiFramePrivate(UiFrame *parent);
    UiFramePrivate() = delete;

    virtual ~UiFramePrivate();

    const std::string &getName() const { return mName; }

    UiFrame *getParent() const { return mParent; }

    virtual ve::SizeF getSize() const;
    virtual ve::PointF getPosition() const;
    virtual void setGeometry(const ve::PointF &position, const ve::SizeF &size);

    void setBorderWidth(float width);
    float getBorderWidth() const;

    void setVisible(bool visiable);
    bool isVisible() const;

    bool isTopLevel() const { return mIsTopLevel; }

    void setBorderColor(UiBorder border, const Ogre::ColourValue &color);
    void setBorderColor(const Ogre::ColourValue &color);
    const Ogre::ColourValue &getBorderColor(UiBorder border) const;

    void setBackgroundColor(UiCorner corner, const Ogre::ColourValue &color);
    void setBackgroundColor(const Ogre::ColourValue &color);
    const Ogre::ColourValue &getBackgroundColor(UiCorner corner) const;
    const Ogre::ColourValue &getBackgroundColor() const;

    void setBackgroundGradient(UiGradient gradient,
                               const Ogre::ColourValue &from,
                               const Ogre::ColourValue &to);

    inline Gorilla::Layer *getLayer() const { return mLayer; }
    inline Gorilla::Rectangle *getBackground() const { return mBackground; }

    template<typename D>
    D *to()
    {
        return static_cast<D *>(this);
    }

    template<typename D>
    const D *to() const
    {
        return static_cast<const D *>(this);
    }

    inline Gorilla::Screen *getScreen() const;
    Ogre::Viewport *getViewport() const;


    /// ------ DEBUG ------ ///

    void setDebugEnabled(bool debug);
    bool isDebugEnabled() const;

    void setDebugBackgroundColor(const Ogre::ColourValue &color);
    Ogre::ColourValue getDebugBackgroundColor();

    void setDebugBackgroundColorRecursive(const UiDebugColors &colors,
                                          size_t depth,
                                          size_t index);

private:
    const bool mIsTopLevel;
    UiFrame * const mParent;
    Gorilla::Screen *mScreen;
    Gorilla::Layer *mLayer;
    Gorilla::Rectangle *mBackground;

    /// ------ Naming ----- ///

    const std::string mName;
    static Ogre::NameGenerator mNameGenerator;

    /// ------ DEBUG ------ ///

    bool mDebugEnabled;
    Ogre::ColourValue mDebugBackgroundColour[4];

};

#endif // UIFRAMEPRIVATE_H
