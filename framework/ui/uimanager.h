#ifndef UIMANAGER_H
#define UIMANAGER_H

#include <OGRE/OgreSingleton.h>
#include <framework/ui/3rdparty/Gorilla.h>

#include <framework/filesystem.h>

#include <framework/controller/abstractinputlistener.h>

class InputController;
class UiFrame;

typedef std::list<UiFrame *> UiFrameList;

class UiManager : public Gorilla::Silverback,
                  public AbstractInputListener
{
    friend class InputController;
    friend class UiFrame;

public:
    VE_DECLARE_TYPE_INFO(UiManager)

    UiManager() { }

    ~UiManager() { }

    static UiManager *getSingletonPtr();

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_UI);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const override
    {
        static const std::string Name = "uiManager";
        return Name;
    }

    void update(float deltaTime, float realTime) override
    { VE_UNUSED(deltaTime); VE_UNUSED(realTime); }

    bool keyPressed(const KeyboardEvent &event) override;
    bool keyReleased(const KeyboardEvent &event) override;

    bool mouseMoved(const MouseEvent &event) override;
    bool mousePressed(const MouseEvent &event) override;
    bool mouseReleased(const MouseEvent &event) override;

    Gorilla::TextureAtlas *loadAtlas(const std::string &altlasName,
                                     const std::string &resGroup = FileSystem::RG_DEFAULT);

    Gorilla::Screen *createScreen(Ogre::Viewport *viewport,
                                  Ogre::SceneManager *sceneManager,
                                  const std::string &atlasName);

    Gorilla::Screen *createScreen(Ogre::Viewport *viewport,
                                  Ogre::SceneManager *sceneManager,
                                  Gorilla::TextureAtlas *atlas);

    Gorilla::ScreenRenderable *createScreenRenderable(const Ogre::Vector2 &maxSize,
                                                      const std::string &atlasName);

    Gorilla::ScreenRenderable *createScreenRenderable(const Ogre::Vector2 &maxSize,
                                                      Gorilla::TextureAtlas *atlas);

    Gorilla::Screen *getScreen(Ogre::Viewport *viewport) const;

    const std::vector<Gorilla::Screen*> &getScreens() const;

    bool isTopLevelFrame(UiFrame *frame) const;
    typedef std::map<UiFrame *, Gorilla::Screen *> TopLevelFrames;
    const TopLevelFrames &getTopLevelFrames() const;

protected:
    void inputControllerRegistred(bool registred, InputController *input);
    void registredAsInputListener(bool registred, InputController *controller) override;

    bool mouseEvent(const MouseEvent &event);

    bool addTopLevelFrame(UiFrame *frame, Gorilla::Screen *screen);
    bool removeTopLevelFrame(UiFrame *frame);

private:
    TopLevelFrames mTopLevelFrames;
};

UiManager *ui(bool checkPointer = true);

#endif // UIMANAGER_H
