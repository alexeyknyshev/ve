#ifndef MARKUPTEXT_H
#define MARKUPTEXT_H

#include <framework/ui/3rdparty/Gorilla.h>

// ------------------------------------------------

class MarkupTextBlock
{
public:
    MarkupTextBlock(const Ogre::ColourValue &color)
        : mColor(color)
    { }

    Ogre::ColourValue mColor;
    std::string mText;
};

typedef std::list<MarkupTextBlock> MarkupTextBlockList;

// ------------------------------------------------

class MarkupText;

class MarkupTextBlockIterator
{
public:
    MarkupTextBlockIterator() = default;
    MarkupTextBlockIterator(MarkupText &text);

    MarkupTextBlockIterator &operator++();

    bool operator==(const MarkupTextBlockIterator &other) const;

    bool operator!=(const MarkupTextBlockIterator &other) const;

private:
    MarkupTextBlockList *mBlockList;

    MarkupTextBlockList::iterator mBlockIt;
    std::string::iterator mTextIt;
};

// ------------------------------------------------

class MarkupTextCursor
{
public:
    MarkupTextCursor(MarkupText *text);

    bool isValid() const
    {
        return (bool)mText;
    }

    enum MoveMode
    {
        Forward = 0,
        Backward,
        ForwardWord,
        BackwardWord,
        Begin,
        End
    };

    size_t move(MoveMode mode, size_t stepCount);

//    inline size_t getSelectionEnd() const
//    {
//        return mEnd;
//    }

    enum SelectionType
    {
        All = 0,
        Line,
        Word
    };

    size_t select(SelectionType type);

    std::string getSelectedText() const;

    size_t removeSelectedText();

    void setSelectionColor(const Ogre::ColourValue &color);

    void setDefaultColor(const Ogre::ColourValue &color);

    inline const MarkupText *getMarkupText() const
    {
        return mText;
    }

protected:
    inline MarkupText *getMarkupText()
    {
        return mText;
    }

private:
    MarkupTextBlockIterator mIt;
    MarkupText *mText;
};

// ------------------------------------------------

class MarkupText : public Gorilla::MarkupText
{
    friend class MarkupTextCursor;
    friend class MarkupTextBlockIterator;

public:
    MarkupText(uint32_t defaultGlyphIndex,
               float left,
               float top,
               const std::string &text,
               Gorilla::Layer *parent);

private:
    virtual void _buildTextBlocks(const std::string &text);
    virtual void _calculateCharacters(const std::string &text);

    MarkupTextBlockList mTextBlocks;
};
/*
class MarkupTextFactory : public Gorilla::Layer::MarkupTextFactory
{
public:
    virtual Gorilla::MarkupText *createMarkupText(
            Ogre::uint defaultGlyphIndex, Ogre::Real x, Ogre::Real y,
            const Ogre::String &text, Gorilla::Layer *layer);

    virtual void destroyMarkupText(Gorilla::MarkupText *markupText);
};
*/
#endif // MARKUPTEXT_H
