#ifndef UILAYOUT_H
#define UILAYOUT_H

#include <map>
#include <initializer_list>

#include <OGRE/OgreColourValue.h>

#include <framework/size.h>
#include <framework/list.h>

class UiFrame;
class UiLayoutSpacer;
class UiDebugColors;

namespace ve
{
    template<typename Type> class PointT;
    typedef PointT<float> PointF;
}

class UiLayout
{
    friend class UiFrame;

public:
    UiLayout();
    virtual ~UiLayout() { }

    UiFrame *getParent() const { return mParent; }

    virtual ve::SizeF getMinimumSizeHint() const = 0;

    virtual void setVisible(bool visible) = 0;

    virtual void addFrame(UiFrame *frame, uint32_t index = UINT32_MAX, uint32_t sizeFactor = 1);
    virtual void addFrame(std::initializer_list<UiFrame *> frameList, uint32_t index = UINT32_MAX, uint32_t sizeFactor = 1);

    virtual bool removeFrame(UiFrame *frame);
    virtual bool removeFrame(int index);

    virtual size_t getUiFrameCount() const = 0;

    void setSpacing(uint32_t spacing);
    inline uint32_t getSpacing() const { return mSpacing; }

protected:
    virtual List<UiFrame *> getChildFrames() const = 0;

    virtual void parentFrameGeometryChanged(const ve::PointF &position, const ve::SizeF &size) = 0;

    void setParent(UiFrame *parent);

    /// Called on any internal Frames organisation modification
    void invalidateParent();

    /// ------ DEBUG ------ ///

    virtual void setChildsDebugEnabled(bool enable, bool recursive) = 0;
    virtual void setChildsDebugColorRecursive(const UiDebugColors &colors, size_t depth) = 0;

private:    
    uint32_t mSpacing;
    UiFrame *mParent;
};

#endif // UILAYOUT_H
