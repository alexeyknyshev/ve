#include "scriptconsole_p.h"

#include <framework/ui/uimanager.h>
#include <framework/ui/scriptconsole.h>
#include <framework/ui/uimarkuptextedit.h>
#include <framework/ui/layouts/uiverticallayout.h>
#include <framework/controller/inputcontroller.h>

#include <framework/ui/3rdparty/Gorilla.h>

#include <script/script.h>

#include <framework/logger/logger.h>

ScriptConsolePrivate::ScriptConsolePrivate(ScriptConsole *console, Gorilla::Screen *screen,
                                           const ScriptConsoleOptions &opt)
    : UiFramePrivate(opt.name, screen),
      mVisible(false),
      mTabStop(16),
      mScript(opt.script),
      mInputController(opt.inputController),
      mConsole(console),
      mGlyphData(nullptr),
      mCurrentInsertCharFast(false)
{
    if (VE_CHECK_NULL_PTR(Gorilla::Layer, getLayer(), Logger::LOG_COMPONENT_UI))
    {
        mGlyphData = getLayer()->_getGlyphData(opt.fontSize);
        VE_CHECK_NULL_PTR(Gorilla::GlyphData, mGlyphData, Logger::LOG_COMPONENT_UI);

        /// will be resized after

        /// TODO: spacer
//        layout->addFrame(new UiLayoutSpacer);
    }

    setWatchLogger(opt.watchLogger);
    setSlowInsertTimeout(opt.slowInsertTimeout);
    setFastInsertTimeout(opt.fastInsertTimeout);
    setMaxLineCount(opt.maxLineCount);
    setHistroyMaxLineCount(opt.historyMaxLineCount);

//    setGeometry(opt.position, opt.size);

    mCurrentHistoryLineIterator = mHistory.end();

    logger()->getDefaultLog()->addListener(this);
}

ScriptConsolePrivate::~ScriptConsolePrivate()
{
    delete mScript;
    mScript = nullptr;

    mGlyphData = nullptr;

    logger()->getDefaultLog()->removeListener(this);

    mConsole = nullptr;
}

void ScriptConsolePrivate::addToHistory(const std::string &cmd)
{
    if (cmd.empty())
    {
        return;
    }

    if (mHistory.size() >= mHistoryMaxLineCount)
    {
        const int lineCountToRemove = mHistory.size() - mHistoryMaxLineCount + 1;
        for (int counter = 0; counter < lineCountToRemove; ++counter)
        {
            mHistory.pop_back();
        }
    }

    mHistory.push_front(cmd);

    if (mHistory.size() == 1)
    {
        mCurrentHistoryLineIterator = mHistory.end();
    }
}

void ScriptConsolePrivate::clearHistory()
{
    mHistory.clear();
    mCurrentHistoryLineIterator = mHistory.end();
}

const std::string &ScriptConsolePrivate::getNextHistoryLine(bool forward)
{
    if (mHistory.empty())
    {
        mCurrentHistoryLineIterator = mHistory.end();
        return VE_STR_BLANK;
    }

    if (forward)
    {
        if (mCurrentHistoryLineIterator != mHistory.end())
        {
            ++mCurrentHistoryLineIterator;
            if (mCurrentHistoryLineIterator == mHistory.end())
            {
                return VE_STR_BLANK;
            }
            else
            {
                return *mCurrentHistoryLineIterator;
            }
        }
        else
        {
            mCurrentHistoryLineIterator = mHistory.begin();
            return *mCurrentHistoryLineIterator;
        }
    }
    else
    {
        if (mCurrentHistoryLineIterator == mHistory.begin())
        {
            mCurrentHistoryLineIterator = mHistory.end();
            return VE_STR_BLANK;
        }
        else
        {
            --mCurrentHistoryLineIterator;
            return *mCurrentHistoryLineIterator;
        }
    }
}

void ScriptConsolePrivate::completeLine(std::string &line, size_t pos)
{
    ScriptCompletions completions;

    std::string savedOrig = line;
    std::string savedEnd;

    // Save text following cursor (nothing if cursor at end of line)
    if (pos < line.size())
    {
        savedEnd = line.substr(pos);
        line.erase(pos);
    }

    std::string savedBegin;
    const size_t lastSeparator = line.find_last_of(" ;,()[]=+-*/");
    if (lastSeparator != std::string::npos)
    {
        savedBegin = line.substr(0, lastSeparator + 1);

        if (lastSeparator + 1 < line.size())
        {
            line.erase(0, lastSeparator + 1);
        }
        else
        {
            return;
        }
    }

    const size_t lastSpace = line.find_last_of(" ;");
    if (lastSpace != line.size() - 1)
    {
        if (lastSpace == std::string::npos)
        {
            completions = mScript->getCompletion(line, false);
        }
        else
        {
            line = line.substr(lastSpace + 1);
            completions = mScript->getCompletion(line, false);
        }

        const size_t count = completions.getAll().size();
        if (count == 1)
        {
            auto result = *completions.getAll().begin();
            std::string candidat;

            switch (result.first)
            {
            case ScriptCompletions::KeyWords:
                candidat = /*"%3" +*/ result.second /*+ "%R"*/;
                break;
            case ScriptCompletions::Brackets:
            case ScriptCompletions::Variables:
                candidat = result.second;
                break;
            default:;
            }

            std::string newPromptLine = savedBegin;
            if (lastSpace == std::string::npos)
            {
                newPromptLine += candidat;
            }
            else
            {
                newPromptLine += line.substr(0, lastSpace + 1) + candidat;
            }

            newPromptLine += savedEnd;

            line = newPromptLine;
        }
        else if (count > 0)
        {
            std::string currentText = line;

            addText(savedOrig);
            std::string commonPrefix = completions.getAll().begin()->second;
            for (const auto &variant : completions.getAll())
            {
                if (variant.second.size() != currentText.size())
                {
                    addText(variant.second);
                }

                // save shortest variant in commonPrefix
                if (variant.second.size() < commonPrefix.size())
                {
                    commonPrefix = variant.second;
                }
            }

            // lookup for longest common prefix
            for (const auto &variant : completions.getAll())
            {
                size_t i = 0;
                const size_t commonPrefixSize = commonPrefix.size();
                for (; i < commonPrefixSize; ++i)
                {
                    if (commonPrefix.at(i) != variant.second.at(i))
                    {
                        break;
                    }
                }

                // delete non-common ending
                commonPrefix = commonPrefix.substr(0, i);
            }

            if (!commonPrefix.empty())
            {
                currentText = savedBegin;
                if (lastSpace == std::string::npos)
                {
                    currentText += commonPrefix;
                }
                else
                {
                    currentText += line.substr(0, lastSpace + 1) + commonPrefix;
                }

                currentText += savedEnd;

                line = currentText;
            }
        }
    }
}

std::string ScriptConsolePrivate::execLine(std::string line)
{
    if (Ogre::StringUtil::startsWith(line, getPromptWelcomeText(), false))
    {
        line.erase(0, getPromptWelcomeText().size());
    }

    addText(line);
    addToHistory(line);
    mCurrentHistoryLineIterator = mHistory.end();

    std::string out = line + "\n";

    // notify listener
    /*
    if (mListener)
    {
        mListener->commandExecuted(mCurrentLine);
    }
    */

    mScript->setSource(line);
    if (mScript->compile())
    {
        mScript->run();
    }

    out += mScript->output();
    mScript->output().clear();
    return out;
}
/*
void ScriptConsolePrivate::setVisible(bool visible, ScriptConsole *console)
{
    if (VE_CHECK_NULL_PTR(InputController, mInputController, Logger::LOG_COMPONENT_UI))
    {
        if (visible)
        {
            mInputController->setModalInputListener(console);
        }
        else
        {
            mInputController->setModalInputListener(nullptr);
        }
    }
}
*/

void ScriptConsolePrivate::messageLogged(const std::string &message,
                                         Ogre::LogMessageLevel lml,
                                         bool maskDebug,
                                         const std::string &logName
#if OGRE_VERSION_MAJOR >= 1 && OGRE_VERSION_MINOR >= 8 /* Ogre 1.8.0+ */
                                         , bool &skipThisMessage
#endif
                                  )
{
    VE_UNUSED(lml); VE_UNUSED(maskDebug); VE_UNUSED(logName);

#if OGRE_VERSION_MAJOR >= 1 && OGRE_VERSION_MINOR >= 8 /* Ogre 1.8.0+ */
    VE_UNUSED(skipThisMessage);
#endif

    if (isWatchingLogger() && ui(false))
    {
        addText(message);
    }
}

void ScriptConsolePrivate::addText(const std::string &text)
{   
    if (!text.empty())
    {
        mConsole->addText(text);
    }
}
