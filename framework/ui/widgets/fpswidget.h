#ifndef FPSWIDGET_H
#define FPSWIDGET_H

#include <framework/ui/uitextlabel.h>
#include <framework/updatelistener.h>

class FPSWidget : public UiTextLabel,
                  public UpdateListener
{
public:
    /// FPS counter for viewport
    FPSWidget(UiFrame *parent, Ogre::RenderTarget *renderTarget);
    FPSWidget(const std::string &name, UiFrame *parent, Ogre::RenderTarget *renderTarget);

    void setUpdateInterval(float interval);
    float getUpdateInterval() const;

    void setRenderTarget(Ogre::RenderTarget *target);
    Ogre::RenderTarget *getRenderTarget() const;

    void update(float deltaTime, float realDelta) override;

private:
    float mUpdateInterval;
    float mTimeSinceLastUpdate;

    Ogre::RenderTarget *mRenderTarget;
};

#endif // FPSWIDGET_H
