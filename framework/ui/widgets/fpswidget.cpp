#include "fpswidget.h"

#include <OgreRenderTarget.h>

FPSWidget::FPSWidget(UiFrame *parent, Ogre::RenderTarget *renderTarget)
    : FPSWidget(VE_STR_BLANK, parent, renderTarget)
{ }

FPSWidget::FPSWidget(const std::string &name, UiFrame *parent, Ogre::RenderTarget *renderTarget)
    : UiTextLabel(name, parent),
      UpdateListener(false), /// will be managed by creator or ui stack
      mUpdateInterval(1.0f),
      mTimeSinceLastUpdate(0.0f),
      mRenderTarget(renderTarget)
{
    setAutoUpdateEnabled(true);
}

void FPSWidget::setUpdateInterval(float interval)
{
    mUpdateInterval = interval;
}

float FPSWidget::getUpdateInterval() const
{
    return mUpdateInterval;
}

void FPSWidget::setRenderTarget(Ogre::RenderTarget *target)
{
    mRenderTarget = target;
}

Ogre::RenderTarget *FPSWidget::getRenderTarget() const
{
    return mRenderTarget;
}

void FPSWidget::update(float deltaTime, float realDelta)
{
    VE_UNUSED(deltaTime);

    if (!mRenderTarget)
    {
        return;
    }

    mTimeSinceLastUpdate += realDelta;
    if (mTimeSinceLastUpdate >= getUpdateInterval())
    {
        mTimeSinceLastUpdate = std::fmod(mTimeSinceLastUpdate, getUpdateInterval());

        std::string text = StringConverter::toString(mRenderTarget->getLastFPS(),
                                                     2, 0, ' ', std::ios_base::fixed) + " FPS";
        setText(text);
    }
}
