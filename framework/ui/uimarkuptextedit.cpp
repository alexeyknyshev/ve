#include "uimarkuptextedit.h"

#include <framework/ui/uimanager.h>
#include <framework/ui/uiframe_p.h>
#include <framework/ui/3rdparty/Gorilla.h>

#include <framework/rect.h>

#include <framework/controller/inputcontroller.h>

#include <framework/logger/logger.h>
#include <framework/stringlist.h>

static float getCharWidth(uint32_t currentChar, uint32_t lastChar,
                          Gorilla::GlyphData *glyphData, bool mono)
{
    if (currentChar == ' ')
    {
        return glyphData->mSpaceLength;
    }

    if (currentChar < glyphData->mRangeBegin ||
        currentChar > glyphData->mRangeEnd)
    {
        return 0.0f;
    }

    float width = 0.0f;
    Gorilla::Glyph *glyph = glyphData->getGlyph(currentChar);
    if (glyph)
    {
        float kerning = glyph->getKerning(lastChar);
        if (kerning == 0.0f)
        {
            kerning = glyphData->mLetterSpacing;
        }
        width += kerning;

        if (mono)
        {
            width += glyphData->mMonoWidth;
        }
        else
        {
            width += glyph->glyphAdvance + kerning;
        }

        lastChar = currentChar;
    }
    return width;
}

static float getTextWidth(const std::string &text, Gorilla::GlyphData *glyphData, bool mono)
{
    float width = 0.0f;

    Ogre::uint lastChar = 0;
    for (Ogre::uint currentChar : text)
    {
        getCharWidth(currentChar, lastChar, glyphData, mono);
    }

    return width;
}

static bool isTextKey(ve::KeyData::KeyButton key)
{
    switch (key)
    {
    case ve::KeyData::KEY_1:
    case ve::KeyData::KEY_2:
    case ve::KeyData::KEY_3:
    case ve::KeyData::KEY_4:
    case ve::KeyData::KEY_5:
    case ve::KeyData::KEY_6:
    case ve::KeyData::KEY_7:
    case ve::KeyData::KEY_8:
    case ve::KeyData::KEY_9:
    case ve::KeyData::KEY_0:
    case ve::KeyData::KEY_MINUS:
    case ve::KeyData::KEY_EQUALS:
    case ve::KeyData::KEY_Q:
    case ve::KeyData::KEY_W:
    case ve::KeyData::KEY_E:
    case ve::KeyData::KEY_R:
    case ve::KeyData::KEY_T:
    case ve::KeyData::KEY_Y:
    case ve::KeyData::KEY_U:
    case ve::KeyData::KEY_I:
    case ve::KeyData::KEY_O:
    case ve::KeyData::KEY_P:
    case ve::KeyData::KEY_BRACKET_LEFT:
    case ve::KeyData::KEY_BRACKET_RIGHT:
    case ve::KeyData::KEY_A:
    case ve::KeyData::KEY_S:
    case ve::KeyData::KEY_D:
    case ve::KeyData::KEY_F:
    case ve::KeyData::KEY_G:
    case ve::KeyData::KEY_H:
    case ve::KeyData::KEY_J:
    case ve::KeyData::KEY_K:
    case ve::KeyData::KEY_L:
    case ve::KeyData::KEY_SEMICOLON:
    case ve::KeyData::KEY_APOSTROPHE:
    case ve::KeyData::KEY_BACKSLASH:
    case ve::KeyData::KEY_Z:
    case ve::KeyData::KEY_X:
    case ve::KeyData::KEY_C:
    case ve::KeyData::KEY_V:
    case ve::KeyData::KEY_B:
    case ve::KeyData::KEY_N:
    case ve::KeyData::KEY_M:
    case ve::KeyData::KEY_COMMA:
    case ve::KeyData::KEY_PERIOD:
    case ve::KeyData::KEY_SLASH:
    case ve::KeyData::KEY_MULTIPLY:
    case ve::KeyData::KEY_SPACE:
    case ve::KeyData::KEY_NUMPAD_MINUS:
    case ve::KeyData::KEY_NUMPAD_ADD:
    case ve::KeyData::KEY_NUMPAD_PERIOD:
    case ve::KeyData::KEY_NUMPAD_EQUALS:
    case ve::KeyData::KEY_NUMPAD_1:
    case ve::KeyData::KEY_NUMPAD_2:
    case ve::KeyData::KEY_NUMPAD_3:
    case ve::KeyData::KEY_NUMPAD_4:
    case ve::KeyData::KEY_NUMPAD_5:
    case ve::KeyData::KEY_NUMPAD_6:
    case ve::KeyData::KEY_NUMPAD_7:
    case ve::KeyData::KEY_NUMPAD_8:
    case ve::KeyData::KEY_NUMPAD_9:
    case ve::KeyData::KEY_NUMPAD_0:
        return true;
    default:;
    }

    return false;
}

/// -------------------------------------------------

class UiMarkupTextEditPrivate : public UiFramePrivate
{
    friend class UiMarkupTextEdit::Cursor;

public:
    UiMarkupTextEditPrivate(const std::string &name, Gorilla::Screen *screen)
        : UiFramePrivate(name, screen),
          mStartLine(0),
          mMaxLineCount(32),
          mRenderedTextBegin(0),
          mRenderedTextCharCount(0),
          mMarkupText(nullptr),
          mCursor(this)
    {
        mMarkupText = getLayer()->createMarkupText(getLayer()->_getDefaultGlyphDataIndex(),
                                                   0.0f, 0.0f, VE_STR_BLANK);
    }

    UiMarkupTextEditPrivate(const std::string &name, UiFrame *parent)
        : UiFramePrivate(name, parent),
          mStartLine(0),
          mMaxLineCount(32),
          mRenderedTextBegin(0),
          mRenderedTextCharCount(0),
          mMarkupText(nullptr),
          mCursor(this)
    {
        mMarkupText = getLayer()->createMarkupText(getLayer()->_getDefaultGlyphDataIndex(),
                                                   0.0f, 0.0f, VE_STR_BLANK);
    }

    ~UiMarkupTextEditPrivate()
    {
        if (ui(false) && getLayer())
        {
            getLayer()->destroyMarkupText(mMarkupText);
        }
        mMarkupText = nullptr;
    }

    virtual void update(float delta, float realTime)
    {
        /// Make sure that the start line is not bigger then last potential line
    }

    void setGeometry(const ve::PointF &position, const ve::SizeF &size)
    {
        UiFramePrivate::setGeometry(position, size);

        mMarkupText->left(position.x);
        mMarkupText->top(position.y);
        mMarkupText->width(size.width);
        mMarkupText->height(size.height);

        updateTextMetrics();
    }

    void setFontSize(uint32_t size)
    {
//        mMarkupText->defaultGlyphData(size);
        updateTextMetrics();
    }

    uint32_t getFontSize() const
    {
        return getLayer()->_getAtlas()->getGlyphDataIndex(mMarkupText->getDefaultGlyphData());
    }

    void setMaxLineCount(uint32_t count)
    {
        mMaxLineCount = count;
        updateTextMetrics();
    }

    uint32_t getMaxLineCount() const
    {
        return mMaxLineCount;
    }

    inline void setVisibleLineCount(uint32_t count)
    {
        mVisibleLineCount = count;
        updateTextMetrics();
    }

    inline uint32_t getVisibleLineCount() const
    {
        return mVisibleLineCount;
    }

    void setText(const std::string &text)
    {
        mText = text;
        getCursor().setPosition(UiMarkupTextEdit::Cursor::MM_WholeText, 1);
        updateTextMetrics();
    }

    const std::string &getText() const
    {
        return mText;
    }

    void setTextColor(const Ogre::ColourValue &color)
    {
//        getLayer()->_setMarkupColor(color);
    }

    const Ogre::ColourValue &getTextColor() const
    {
        return getLayer()->_getMarkupColour(0);
    }

    void addText(const std::string &text)
    {
        mText += text;

        updateTextMetrics();
    }

    UiMarkupTextEdit::Cursor &getCursor()
    {
        return mCursor;
    }

    /** String List of separated lines to renderer */
    StringList getLinesToRender(const std::string &rawLine, size_t startLine)
    {
        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEditPrivate::getLinesToRender, Logger::LOG_COMPONENT_UI);
    }

    /** Returnes compiled text exactly for passed @leftLines
     ** Should be accumulated in resulting std::string */
    static std::string getTextToRender(const StringList &lines, size_t leftLines)
    {
        /// TODO:
        // VE_NOT_IMPLEMENTED(UiMarkupTextEditPrivate::getTextToRender, Logger::LOG_COMPONENT_UI);
    }

    void updateTextMetrics()
    {
        StringList splittedLines(mText, "\n");

        Gorilla::GlyphData *currentGlyphData = mMarkupText->getDefaultGlyphData();

        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEditPrivate::updateTextMetrics, Logger::LOG_COMPONENT_UI);
        const float maxLineWidth = 0.0f; /// TODO !

        StringList linesToRender;
        for (const std::string &line : splittedLines)
        {
            const size_t lineSize = line.size();

            if (lineSize == 0)
            {
                continue;
            }

            if (lineSize == 1)
            {
                linesToRender += line;
            }

            std::string linesForWidth;
            Ogre::Real lineWidth = 0.0f;

            size_t charIndex = 0; 

            while (charIndex < lineSize - 1)
            {
                const char currChar = line[charIndex];
                Gorilla::Glyph *currGlyph = currentGlyphData->getGlyph(currChar);

                /// There is left space for glyph
                if (lineWidth + currGlyph->glyphWidth < maxLineWidth)
                {
                    lineWidth += currGlyph->glyphWidth;
                }
                else
                {
                    lineWidth = 0.0f;
                    linesForWidth += "\n";
                    continue;
                }
                linesForWidth += currChar;

                const char nextChar = line[charIndex + 1];

                /// There is space for kerning to next char
                if (lineWidth + currGlyph->getKerning(nextChar) < maxLineWidth)
                {
                    lineWidth += currGlyph->getKerning(nextChar);
                }
                else
                {
                    lineWidth = 0.0f;
                    linesForWidth += "\n";
                    continue;
                }

                charIndex++;
            }
        }
    }

    void pageUp()
    {
        if (mStartLine >= getVisibleLineCount())
        {
            mStartLine -= getVisibleLineCount();
        }
        else
        {
            mStartLine = 0;
        }

        updateTextMetrics();
    }

    void pageDown()
    {
        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEditPrivate::pageDown(), Logger::LOG_COMPONENT_UI);
        updateTextMetrics();
    }

    void backspace()
    {
        const size_t cursorPos = getCursor().getPosition(UiMarkupTextEdit::Cursor::MM_Char);

        if (cursorPos > 0 && mText.size() > 0)
        {
            mText.erase(cursorPos - 1, 1);

            getCursor().setPosition(UiMarkupTextEdit::Cursor::MM_Char, cursorPos - 1);

            updateTextMetrics();
        }
    }

    void del()
    {
        const size_t cursorPos = getCursor().getPosition(UiMarkupTextEdit::Cursor::MM_Char);

        if (cursorPos < mText.size())
        {
            mText.erase(cursorPos, 1);

            updateTextMetrics();
        }
    }

    void end()
    {
        getCursor().setPosition(UiMarkupTextEdit::Cursor::MM_WholeText, 1);
    }

    void home()
    {
        getCursor().setPosition(UiMarkupTextEdit::Cursor::MM_WholeText, -1);
    }

protected:
    std::string &_getText()
    {
        return mText;
    }

private:
    size_t mStartLine;
    uint32_t mFontSize;
    size_t mMaxLineCount;
    size_t mVisibleLineCount;

    /// Full text
    std::string mText;

    /// Rendered text
    size_t mRenderedTextBegin;
    size_t mRenderedTextCharCount;

    Gorilla::MarkupText *mMarkupText;

    UiMarkupTextEdit::Cursor mCursor;
};

/// ---------------------------------------------------------------------------------
/// ---------------------------------------------------------------------------------
/// ---------------------------------------------------------------------------------

UiMarkupTextEdit::UiMarkupTextEdit(const std::string &name, Gorilla::Screen *screen)
    : UiFrame(new UiMarkupTextEditPrivate(name, screen)),
      mSlowInsertTimeout(0.5f),
      mFastInsertTimeout(0.05f),
      mCurrentInsertCharFast(false),
      mIsFollowingTextUpdates(true),
      mInputController(nullptr)
{ }

UiMarkupTextEdit::UiMarkupTextEdit(const std::string &name, UiFrame *parent)
    : UiFrame(new UiMarkupTextEditPrivate(name, parent)),
      mSlowInsertTimeout(0.5f),
      mFastInsertTimeout(0.05f),
      mCurrentInsertCharFast(false),
      mIsFollowingTextUpdates(true),
      mInputController(nullptr)
{ }

UiMarkupTextEdit::~UiMarkupTextEdit()
{
    mInputController = nullptr;
}

bool UiMarkupTextEdit::keyPressed(const KeyboardEvent &event)
{
    auto dp = d->to<UiMarkupTextEditPrivate>();

    if (event.data.key == ve::KeyData::KEY_PAGE_UP)
    {

    }
    else if (event.data.key == ve::KeyData::KEY_PAGE_DOWN)
    {

    }
    else if (event.data.key == ve::KeyData::KEY_BACKSPACE)
    {
        dp->backspace();
    }
    else if (event.data.key == ve::KeyData::KEY_DELETE)
    {
        dp->del();
    }
    else if (event.data.key == ve::KeyData::KEY_HOME)
    {
        dp->home();
    }
    else if (event.data.key == ve::KeyData::KEY_END)
    {
        dp->end();
    }
    else if (event.data.isPrintable())
    {
        getCursor()->insert(event.data.text);
    }

    return true;
}

void UiMarkupTextEdit::update(float deltaTime, float realTime)
{
    d->to<UiMarkupTextEditPrivate>()->update(deltaTime, realTime);
}

ve::SizeF UiMarkupTextEdit::getMinimumSizeHint() const
{
    /// TODO: UiMarkupTextEdit::getMinimumSizeHint()
    return ve::SizeF(0.0f, 0.0f);
}

void UiMarkupTextEdit::setTextFontSize(uint32_t fontSize)
{
    d->to<UiMarkupTextEditPrivate>()->setFontSize(fontSize);
}

void UiMarkupTextEdit::setMaxLineCount(uint32_t count)
{
    d->to<UiMarkupTextEditPrivate>()->setMaxLineCount(count);
}

uint32_t UiMarkupTextEdit::getMaxLineCount() const
{
    return d->to<UiMarkupTextEditPrivate>()->getMaxLineCount();
}

//void UiMarkupTextEdit::setVisibleLineCount(uint32_t count)
//{
//    d->to<UiMarkupTextEditPrivate>()->setVisibleLineCount(count);
//}

uint32_t UiMarkupTextEdit::getVisiableLineCount() const
{
    return d->to<UiMarkupTextEditPrivate>()->getVisibleLineCount();
}

void UiMarkupTextEdit::setText(const std::string &text)
{
    d->to<UiMarkupTextEditPrivate>()->setText(text);
}

const std::string &UiMarkupTextEdit::getText() const
{
    return d->to<UiMarkupTextEditPrivate>()->getText();
}

void UiMarkupTextEdit::clear()
{
    setText(VE_STR_BLANK);
}

void UiMarkupTextEdit::setTextColor(const Ogre::ColourValue &color)
{
    d->to<UiMarkupTextEditPrivate>()->setTextColor(color);
}

const Ogre::ColourValue &UiMarkupTextEdit::getTextColor() const
{
    return d->to<UiMarkupTextEditPrivate>()->getTextColor();
}

void UiMarkupTextEdit::addText(const std::string &text)
{
    d->to<UiMarkupTextEditPrivate>()->addText(text);
}

void UiMarkupTextEdit::registredAsInputListener(bool registred, InputController *controller)
{
    if (registred)
    {
        mInputController = controller;
    }
    else
    {
        mInputController = nullptr;
    }
}

void UiMarkupTextEdit::_backspace()
{
    d->to<UiMarkupTextEditPrivate>()->backspace();
}

bool UiMarkupTextEdit::isValidChar(unsigned int c) const
{
    if (mInputController)
    {
        const char _c = c;
        return (std::isgraph(_c, mInputController->getLocale()) ||
                std::isspace(_c, mInputController->getLocale()));
    }
    return false;
}

bool UiMarkupTextEdit::Cursor::update(float deltaTime)
{
    if (isBlinking())
    {
        mBlinkTime += deltaTime;

        if (mBlinkTime >= mBlinkTimeInterval)
        {
            mBlinkTime = 0.0f;

            if (mState == Visiable)
            {
                mState = Hidden;
            }
            else
            {
                mState = Visiable;
            }

            return true;
        }
    }

    return false;
}

size_t UiMarkupTextEdit::Cursor::getPosition(MoveMode mode) const
{
    if (mode & MM_Char)
    {
        return mPosition;
    }
    else if (mode & MM_Word)
    {
        size_t wordCount = 0;
        bool wordNow = false;

        const std::string &text = mEdit->getText();
        for (size_t i = 0; i <= mPosition; i++)
        {
            if (std::isspace(text[i]))
            {
                if (wordNow)
                {
                    wordCount++;
                }
                wordNow = false;
            }
        }

        if (wordNow) wordCount++;
        return wordCount;
    }
    else if (mode & MM_Line)
    {
        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEdit::Cursor::getPosition(MM_Line), Logger::LOG_COMPONENT_UI);
    }

    return 0;
}

void UiMarkupTextEdit::Cursor::setPosition(int moveModeMask, int pos)
{
    if (mEdit->getText().empty())
    {
        mPosition = 0;
        return;
    }

    if (moveModeMask & MM_Char) // MM_Absolute is 0 bit
    {
        // if mode is MM_Relative convert it to MM_Absolute
        if (moveModeMask & MM_Relative)
        {
            pos = (int)mPosition + pos;
        }

        // now set absolute position
        mPosition = Ogre::Math::Clamp(pos, 0, (int)mEdit->getText().size());
    }
    else if (moveModeMask & MM_WholeText)
    {
        if (pos > 0)
        {
            if (pos != 1)
            {
                logger()->logClassMessage(VE_CLASS_NAME(UiMarkupTextEdit::Cursor),
                                          VE_SCOPE_NAME(setPosition(int moveModeMask, int pos)),
                                          "Parameter \"" + VE_AS_STR(int pos) + "\" not equals \"1\" while " +
                                          VE_AS_STR(moveModeMask) + " is set to \"" + VE_AS_STR(MM_WholeText) + ".",
                                          Logger::LOG_LEVEL_WARNING,
                                          Logger::LOG_COMPONENT_UI);
            }

            mPosition = mEdit->getText().size();
        }
        else if (pos < 0)
        {
            if (pos != -1)
            {
                logger()->logClassMessage(VE_CLASS_NAME(UiMarkupTextEdit::Cursor),
                                          VE_SCOPE_NAME(setPosition(int moveModeMask, int pos)),
                                          "Parameter \"" + VE_AS_STR(int pos) + "\" not equals \"-1\" while " +
                                          VE_AS_STR(moveModeMask) + " is set to \"" + VE_AS_STR(MM_WholeText) + ".",
                                          Logger::LOG_LEVEL_WARNING,
                                          Logger::LOG_COMPONENT_UI);
            }

            mPosition = 0;
        }
    }
    else
    {
        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEdit::Cursor::setPosition, Logger::LOG_COMPONENT_UI);
    }
}

std::string UiMarkupTextEdit::Cursor::getText(int moveModeMask, int count) const
{
    if (moveModeMask & MM_Char)
    {
        if (count >= 0)
        {
            return mEdit->getText().substr(getPosition(MM_Char), (size_t)count);
        }
        else
        {
            const size_t pos = getPosition(MM_Char);

            int begin = (int)pos + count;
            if (begin < 0)
            {
                begin = 0;
            }

            size_t realCount = pos - (size_t)begin;
            return mEdit->getText().substr((size_t)begin, realCount);
        }
    }
    else
    {
        /// TODO:
        //VE_NOT_IMPLEMENTED(UiMarkupTextEdit::Cursor::getText(!MM_Char), Logger::LOG_COMPONENT_UI);
    }

    return VE_STR_BLANK;
}

void UiMarkupTextEdit::Cursor::insert(unsigned int c)
{
    mEdit->_getText().insert(getPosition(MM_Char), 1, (char)c);
    mEdit->updateTextMetrics();
    ++mPosition;
}

void UiMarkupTextEdit::Cursor::insert(const std::string &str)
{
    mEdit->_getText().insert(getPosition(MM_Char), str);
    mEdit->updateTextMetrics();
    mPosition += str.size();
}

UiMarkupTextEdit::Cursor *UiMarkupTextEdit::getCursor()
{
    return &d->to<UiMarkupTextEditPrivate>()->getCursor();
}
