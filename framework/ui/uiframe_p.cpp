#include "uiframe_p.h"

#include <framework/ui/uimanager.h>
#include <framework/ui/uidebugcolors.h>
#include <framework/ui/3rdparty/Gorilla.h>

#include <framework/rect.h>

inline static Gorilla::Border convertBorder(UiBorder border)
{
    switch (border)
    {
    case UB_North: return Gorilla::Border_North;
    case UB_East:  return Gorilla::Border_East;
    case UB_South: return Gorilla::Border_South;
    case UB_West:  return Gorilla::Border_West;
    }

    veLogAndAssert(false,
                   VE_CLASS_NAME(UiFramePrivate),
                   VE_SCOPE_NAME(convertBorder(UiBorder border)),
                   VE_STR_BLANK,
                   "Undefined " + VE_ENUM_NAME(UiBorder).toString() + "!",
                   Logger::LOG_COMPONENT_UI);

    return Gorilla::Border_North;
}

inline static Gorilla::QuadCorner convertCorner(UiCorner corner)
{
    switch (corner)
    {
    case UC_TopLeft:     return Gorilla::TopLeft;
    case UC_TopRight:    return Gorilla::TopRight;
    case UC_BottomRight: return Gorilla::BottomRight;
    case UC_BottomLeft:  return Gorilla::BottomLeft;
    }

    veLogAndAssert(false,
                   VE_CLASS_NAME(UiFramePrivate),
                   VE_SCOPE_NAME(convertCorner(UiCorner corner)),
                   VE_STR_BLANK,
                   "Undefined " + VE_ENUM_NAME(UiCorner).toString() + "!",
                   Logger::LOG_COMPONENT_UI);

    return Gorilla::TopLeft;
}

inline static Gorilla::Gradient convertGradient(UiGradient gradient)
{
    switch (gradient)
    {
    case UG_Diagonal:   return Gorilla::Gradient_Diagonal;
    case UG_NorthSouth: return Gorilla::Gradient_NorthSouth;
    case UG_WestEast:   return Gorilla::Gradient_WestEast;
    }

    veLogAndAssert(false,
                   VE_CLASS_NAME(UiFramePrivate),
                   VE_SCOPE_NAME(convertGradient(UiGradient gradient)),
                   VE_STR_BLANK,
                   "Undefined " + VE_ENUM_NAME(UiGradient).toString() + "!",
                   Logger::LOG_COMPONENT_UI);

    return Gorilla::Gradient_Diagonal;
}

Ogre::NameGenerator UiFramePrivate::mNameGenerator("");

UiFramePrivate::UiFramePrivate(const std::string &name, Gorilla::Screen *screen)
    : mIsTopLevel(true),
      mParent(nullptr),
      mScreen(screen),
      mLayer(nullptr),
      mBackground(nullptr),
      mName(name),
      mDebugEnabled(false)
{
    if (!VE_CHECK_NULL_PTR(Gorilla::Screen, screen, Logger::LOG_COMPONENT_UI))
    {
        return;
    }

    mLayer = screen->createLayer();

    mBackground = mLayer->createRectangle(0, 0, 0, 0);

    // opaque background
    mBackground->background_colour(Ogre::ColourValue::ZERO);
}

UiFramePrivate::UiFramePrivate(const std::string &name, UiFrame *parent)
    : mIsTopLevel(false),
      mParent(parent),
      mScreen(nullptr),
      mLayer(nullptr),
      mBackground(nullptr),
      mName(name),
      mDebugEnabled(false)
{   
    // Non-top-level Frame
    if (VE_CHECK_NULL_PTR(VE_CLASS_NAME(UiFrame), parent, Logger::LOG_COMPONENT_UI))
    {
        mScreen = parent->d->getScreen();

        Gorilla::Layer *parentLayer = parent->d->getLayer();
        if (VE_CHECK_NULL_PTR(Gorilla::Layer, parentLayer, Logger::LOG_COMPONENT_UI))
        {
            mLayer = parentLayer->createSubLayer();

            mBackground = mLayer->createRectangle(0, 0, 0, 0);

            // transparent background
            mBackground->background_colour(Ogre::ColourValue::ZERO);
        }
    }
}

UiFramePrivate::UiFramePrivate(UiFrame *parent)
    : mIsTopLevel(false),
      mParent(parent),
      mScreen(nullptr),
      mLayer(nullptr),
      mBackground(nullptr),
      mName(parent->getName() + "_" + UiFramePrivate::mNameGenerator.generate()),
      mDebugEnabled(false)
{
    if (veAssert(parent,
                 VE_CLASS_NAME(UiFramePrivate),
                 VE_SCOPE_NAME(UiFramePrivate(UiFrame *parent)),
                 "Nullptr to parent in constructor of UiFrame",
                 Logger::LOG_COMPONENT_UI))
    {
        mScreen = parent->d->getScreen();
    }

    // Non-top-level Frame
    if (VE_CHECK_NULL_PTR(VE_CLASS_NAME(UiFrame), parent, Logger::LOG_COMPONENT_UI))
    {
        Gorilla::Layer *parentLayer = parent->d->getLayer();
        if (VE_CHECK_NULL_PTR(Gorilla::Layer, parentLayer, Logger::LOG_COMPONENT_UI))
        {
            mLayer = parentLayer->createSubLayer();

            mBackground = mLayer->createRectangle(0, 0, 0, 0);

            // transparent background
            mBackground->background_colour(Ogre::ColourValue::ZERO);
        }
    }
}

UiFramePrivate::~UiFramePrivate()
{
    if (ui(false) && mLayer)
    {
        mLayer->destroyRectangle(mBackground);
    }
    mBackground = nullptr;
    mLayer = nullptr;
}

ve::SizeF UiFramePrivate::getSize() const
{
    if (mLayer)
    {
        return { mBackground->width(), mBackground->height() };
    }
    return { 0.0f, 0.0f };
}

ve::PointF UiFramePrivate::getPosition() const
{
    if (mLayer)
    {
        return { mBackground->left(), mBackground->top() };
    }
    return { 0.0f, 0.0f };
}

void UiFramePrivate::setGeometry(const ve::PointF &position, const ve::SizeF &size)
{
    if (mLayer)
    {
        mBackground->left(position.x);
        mBackground->top(position.y);
        mBackground->width(size.width);
        mBackground->height(size.height);
    }
}

void UiFramePrivate::setBorderWidth(float width)
{
    if (mBackground)
    {
        mBackground->border_width(width);
    }
}

float UiFramePrivate::getBorderWidth() const
{
    if (mBackground)
    {
        return mBackground->border_width();
    }

    return 0.0f;
}

void UiFramePrivate::setVisible(bool visiable)
{
    if (mLayer)
    {
        /*
        if (mLayer && mLayer->isVisible() != visible)
        {
            mLayer->setVisible(visible);
            if (mListener)
            {
                mListener->visiblityChanged(visible);
            }
        }
        */
        mLayer->setVisible(visiable);
    }
}

bool UiFramePrivate::isVisible() const
{
    if (mLayer)
    {
        return mLayer->isVisible();
    }

    return false;
}

void UiFramePrivate::setBorderColor(UiBorder border, const Ogre::ColourValue &color)
{
    if (mBackground)
    {
        mBackground->border_colour(convertBorder(border), color);
    }
}

void UiFramePrivate::setBorderColor(const Ogre::ColourValue &color)
{
    if (mBackground)
    {
        mBackground->border_colour(convertBorder(UB_North), color);
        mBackground->border_colour(convertBorder(UB_East),  color);
        mBackground->border_colour(convertBorder(UB_South), color);
        mBackground->border_colour(convertBorder(UB_West),  color);
    }
}

const Ogre::ColourValue &UiFramePrivate::getBorderColor(UiBorder border) const
{
    if (mBackground)
    {
        mBackground->border_colour(convertBorder(border));
    }

    return Ogre::ColourValue::ZERO;
}

void UiFramePrivate::setBackgroundColor(UiCorner corner, const Ogre::ColourValue &color)
{
    if (mBackground)
    {
        if (isDebugEnabled())
        {
            setDebugEnabled(false);
            mBackground->background_colour(convertCorner(corner), color);
            setDebugEnabled(true);
        }
        else
        {
            mBackground->background_colour(convertCorner(corner), color);
        }
    }
}

void UiFramePrivate::setBackgroundColor(const Ogre::ColourValue &color)
{
    if (mBackground)
    {
        if (isDebugEnabled())
        {
            setDebugEnabled(false);
            mBackground->background_colour(color);
            setDebugEnabled(true);
        }
        else
        {
            mBackground->background_colour(color);
        }
    }
}

const Ogre::ColourValue &UiFramePrivate::getBackgroundColor(UiCorner corner) const
{
    if (mBackground)
    {
        return mBackground->background_colour(convertCorner(corner));
    }

    return Ogre::ColourValue::ZERO;
}

const Ogre::ColourValue &UiFramePrivate::getBackgroundColor() const
{
    return getBackgroundColor((UiCorner)0);
}

void UiFramePrivate::setBackgroundGradient(UiGradient gradient,
                                           const Ogre::ColourValue &from,
                                           const Ogre::ColourValue &to)
{
    if (mBackground)
    {
        return mBackground->background_gradient(convertGradient(gradient), from, to);
    }
}

Gorilla::Screen *UiFramePrivate::getScreen() const
{
    return mScreen;
}

Ogre::Viewport *UiFramePrivate::getViewport() const
{
    if (mScreen)
    {
        return mScreen->getViewport();
    }
    return nullptr;
}

/// ------ DEBUG ------ ///

void UiFramePrivate::setDebugEnabled(bool debug)
{
    if (mDebugEnabled != debug)
    {
        mDebugEnabled = debug;
        for (int i = 0; i < 4; i++)
        {
            Ogre::ColourValue tmp = getBackgroundColor((UiCorner)i);

            mBackground->background_colour((Gorilla::QuadCorner)i, mDebugBackgroundColour[i]);
            mDebugBackgroundColour[i] = tmp;
        }
    }
}

bool UiFramePrivate::isDebugEnabled() const
{
    return mDebugEnabled;
}

void UiFramePrivate::setDebugBackgroundColor(const Ogre::ColourValue &color)
{
    if (isDebugEnabled())
    {
        setDebugEnabled(false);
        for (int i = 0; i < 4; i++)
        {
            mDebugBackgroundColour[i] = color;
        }
        setDebugEnabled(true);
    }
    else
    {
        for (int i = 0; i < 4; i++)
        {
            mDebugBackgroundColour[i] = color;
        }
    }
}

void UiFramePrivate::setDebugBackgroundColorRecursive(const UiDebugColors &colors,
                                                      size_t depth, size_t index)
{ 
    setDebugBackgroundColor(colors.getDebugColor(depth, index));
}

Ogre::ColourValue UiFramePrivate::getDebugBackgroundColor()
{
    if (isDebugEnabled())
    {
        setDebugEnabled(false);
        Ogre::ColourValue result = mDebugBackgroundColour[0];
        setDebugEnabled(true);

        return result;
    }

    return mDebugBackgroundColour[0];
}
