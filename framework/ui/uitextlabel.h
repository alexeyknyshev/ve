#ifndef UITEXTLABEL_H
#define UITEXTLABEL_H

#include <framework/ui/uiframe.h>

class UiTextLabel : public UiFrame
{
public:
    UiTextLabel(const std::string &name, Gorilla::Screen *screen, const std::string &text = "");
    UiTextLabel(const std::string &name, UiFrame *parent, const std::string &text = "");
    UiTextLabel(UiFrame *parent);

    ve::SizeF getMinimumSizeHint() const override;

    void setText(const std::string &text);
    const std::string &getText() const;

    void clear();

    void setTextColor(const Ogre::ColourValue &color);
    const Ogre::ColourValue &getTextColor() const;

    void setTextFontSize(size_t size);
    size_t getTextFontSize() const;

    void setTextAlignment(UiAlignment align);
    UiAlignment getTextAlignment() const;
};

#endif // UITEXTLABEL_H
