#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <global.h>
//#include <OGRE/OgreResourceGroupManager.h>
#include <OGRE/OgreDataStream.h>

/** Platform independent set of basic file system functions */

class FileSystem
{
public:
    FileSystem();

    static bool isReservedDir(const char *path);
    static bool isAbsolutePath(const char *path);

    static bool isDirectory(const std::string &path);
    static bool isRegularFile(const std::string &path);

    static std::string concatenatePath(const std::string &dirname,
                                       const std::string &basename);

    static std::string dirname(const std::string &path);
    static std::string basename(const std::string &path);

    static void splitFileName(const std::string &path,
                              std::string &dirname,
                              std::string &basename);

    static const std::string &getCwd();

    static const std::string &getSharedLibExtensionStr();

    static std::string getFullpathFromCwd(const std::string &pathInCwd);

    static void removeResourceLocation(const std::string &pathAndName,
                                       const std::string &resGroup = RG_DEFAULT);

    static void addResourceLocation(const std::string &pathAndName,
                                    const std::string &locType,
                                    const std::string &resGroup = RG_DEFAULT,
                                    bool recursive = false);

    static bool hasResourceLocation(const std::string &pathAndName,
                                    const std::string &resGroup = RG_DEFAULT);


    /** Initializes resources in group and they can be loaded after.
     ** If group has been initialized yet then you must clearResourceGroup
     ** and initialize it again. */
    static void initialiseResourceGroup(const std::string &name);

    /** Unloads resources. After that you can addResourceLocation to this
     ** group and initialize it again. */
    static void clearResourceGroup(const std::string &name);

    static Ogre::DataStreamPtr openResource(
            const std::string &name,
            const std::string &groupName = RG_DEFAULT,
            bool searchGroupsIfNotFound = true,
            Ogre::Resource *resourceBeingLoaded = 0);

    const static std::string LOC_TYPE_FS;
    const static std::string LOC_TYPE_ZIP;

    /** Default resource group
     ** ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME equivalent */
    const static std::string RG_DEFAULT;
};

#endif // FILESYSTEM_H
