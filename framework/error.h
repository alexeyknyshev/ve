#ifndef ERROR_H
#define ERROR_H

#include <global.h>
#include <exception>

#include <framework/logger/logger.h>

class BaseException : public std::exception
{
public:
    BaseException()
        : mStackSize(0)
    { }

    BaseException(const std::string &backTrace,
              const std::string &comment)
        : mBackTrace(backTrace),
          mComment(comment),
          mStackSize(0)
    { }

    virtual ~BaseException() throw() { }

    inline const std::string &backTrace() const throw()
    {
        return mBackTrace;
    }

    inline uint32_t stackSize() const throw()
    {
        return mStackSize;
    }

    virtual const char* what() const throw()
    {
        return ("Backtrace: " + mBackTrace + "\n"
                "Comment: " + mComment + "\n").c_str();
    }

    inline BaseException &operator <<(const std::string &info) throw()
    {
        if (mStackSize == 0)
            mBackTrace += "\n\nStack:";

        mBackTrace += (info + "\n");
        return *this;
    }

    inline BaseException &operator <<(const char *info) throw()
    {
        return operator<<(std::string(info));
    }

protected:
    std::string mBackTrace;
    std::string mComment;
    uint32_t mStackSize;
};

class Error
{   
protected:
    void _raise(const std::string &backtrace,
                const std::string &message,
                const std::string &type)
    {
        veAssert(false, "[" + type + "] " + message + "\n\nTrace:" + backtrace,
                 Logger::LOG_COMPONENT_NONE);
    }

public:
    Error(const std::string &backtrace, const std::string &msg, bool raise = true)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw Exception(backtrace, msg);
#else
            _raise(backtrace, msg, VE_CLASS_NAME(Error).toString());
#endif
        }
    }
};

#endif // ERROR_H
