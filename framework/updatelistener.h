#ifndef UPDATELISTENER_H
#define UPDATELISTENER_H

#include <framework/updatable.h>

class UpdateListener : public Updatable
{
public:
    UpdateListener(bool supposedToBeDeleted)
    {
        setSupposedToBeDeleted(supposedToBeDeleted);
    }

    ~UpdateListener();

    inline bool isSupposedToBeDeleted() const { return mIsSupposedToBeDeleted; }

    inline void setSupposedToBeDeleted(bool supposed) { mIsSupposedToBeDeleted = supposed; }

    void setAutoUpdateEnabled(bool autoUpdate);

    bool isAutoUpdateEnabled() const;

private:
    bool mIsSupposedToBeDeleted;
};

#endif // UPDATELISTENER_H
