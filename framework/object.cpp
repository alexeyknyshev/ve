#include "object.h"

#include <game/game.h>

Object::Object()
    : mCallbacks(nullptr)
#ifndef NDEBUG
    , mDebugTraceEnabled(false)
#endif // NDEBUG
{ }

Object::~Object()
{
    /// WARNING: enable if event loop used
//    destroyed();
    delete mCallbacks;
}

void Object::setCallback(uint32_t eventType, CallbackFunc callback)
{
    if (!mCallbacks)
    {
        mCallbacks = new CallbackFuncMap;
    }

    auto *cb = new CallbackFunc(callback);
    auto it = mCallbacks->find(eventType);
    if (it != mCallbacks->cend())
    {
        delete it->second;
        mCallbacks->erase(it);
    }
    mCallbacks->insert({ eventType, cb });
}

const Object::CallbackFunc *Object::getCallback(uint32_t eventType) const
{
    if (mCallbacks)
    {
        auto it = mCallbacks->find(eventType);
        if (it != mCallbacks->cend())
        {
            return it->second;
        }
    }

    return nullptr;
}

void Object::destroyed()
{
    Game *game = Game::getSingletonPtr();
    game->receiverDestroyed(this);
}

void Object::assertNotImplemented(const ScopeName &scope, std::uint64_t component) const
{
    veNotImplementedClass(getClassName(), scope, component);
}

void Object::log(const std::string &msg, int level, std::uint64_t component) const
{
    logger()->logMessage(getClassName().toString() + " (" + logName() + "): " + msg, level, component);
}

void Object::logObjectTrace(const std::string &msg, int level, std::uint64_t component)
{
    if (_isDebugTraceEnabled())
    {
        log("<Trace> " + msg, level, component);
    }
}

void Object::logAndAssert(const ScopeName &scope,
                          const std::string &msg,
                          std::uint64_t component) const
{
    veLogAndAssert(false, getClassName(), scope, logName(), msg, component);
}

void Object::logAndSoftAssert(const ScopeName &scope,
                              const std::string &msg,
                              std::uint64_t component,
                              int level) const
{
    logAndSoftAssertStatic(getClassName(), logName(), scope, msg, component, level);
}

void Object::logAndSoftAssertStatic(const ClassName &className,
                                    const std::string &objectName,
                                    const ScopeName &scope,
                                    const std::string &msg,
                                    std::uint64_t component,
                                    int level)
{
    std::string text;

    text += className.toString().empty() ? "" : className.toString() + " ";
    text += objectName.empty()          ? "" : "(" + objectName + ") ";
    text += scope.toString().empty()     ? "" : "in [" + scope.toString() + "]";
    text += text.empty()                ? "" : ": ";

    text += msg;

    logger()->logMessage(text, level, component);
}
