#ifndef BITMATH_H
#define BITMATH_H

class BitMath
{
public:
    inline static bool isPowerOfTwo(size_t x)
    {
        return ((x != 0) && !(x & (x - 1)));
    }

    /** @remarks if x == 1 @returns 0
     **          if x == 0 @returns 0 */
    inline static uint32_t getPrevPowerOfTwo(uint32_t x)
    {
        return getNextPowerOfTwo(x) / 2;
    }

    /** @remarks if x == 0 @returns 0 */
    inline static uint32_t getNextPowerOfTwo(uint32_t x)
    {
        x--;
        x |= x >> 1;
        x |= x >> 2;
        x |= x >> 4;
        x |= x >> 8;
        x |= x >> 16;
        x++;
        return x;
    }
};

#define BIT(index) (std::uint64_t(1) << (index))

#endif // BITMATH_H
