#ifndef UPDATABLE_H
#define UPDATABLE_H

class Updatable
{
public:
    virtual ~Updatable()
    { }

    virtual void update(float deltaTime, float realDelta) = 0;
};

#endif // UPDATABLE_H
