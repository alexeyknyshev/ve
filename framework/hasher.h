#ifndef HASHER_H
#define HASHER_H

#include <cstddef> // size_t

/**
CRC is for slowpokes ;)

This works by casting the contents of the string pointer to "look like"
a size_t (int32 or int64 based on the optimal match for your hardware).
So the contents of the string are interpreted as a raw number, no worries
about characters anymore, and you then bit-shift this the precision needed
(you tweak this number to the best performance, I've found 2 works well for
hashing strings in set of a few thousands).

Also the really neat part is any decent compiler on modern hardware will hash
a string like this in 1 assembly instruction, hard to beat that ;)

(c) Robert Gould
*/
class Hasher
{
    static const int precision = 2; //change the precision with this

public:
    static size_t getHash(const char* str)
    {
        return (*(size_t*)str) >> precision;
    }
};

//----------------------------------------------------------------

#define VE_CLASS_HASH(clazz) \
    Hasher::getHash(""#clazz"");

#endif // HASHER_H
