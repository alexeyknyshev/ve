#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <global.h>

#include <framework/list.h>

class Token
{
    friend class Tokenizer;

public:
    Token()
        : mType(-1)
    { }

    inline void setText(const std::string &text) { mText = text; }
    inline const std::string &getText() const { return mText; }

    inline int getType() const { return mType; }
    inline void setType(int type) { mType = type; }

    inline void reset()
    {
        mType = -1;
        mText.clear();
    }

    inline bool isValid() const
    {
        return (mType > 0 && !mText.empty());
    }

private:
    std::string mText;
    int mType;
};

typedef List<Token> TokenList;

class Tokenizer
{
public:
    Tokenizer() = default;

    Tokenizer &setWhiteSpaces(const std::string &str);
    Tokenizer &getWhiteSpaces(std::string &str);

    Tokenizer &setInstructionSeparators(const std::string &str);
    Tokenizer &getInstructionSeparators(std::string &str);

    TokenList tokenize(std::string input);

protected:
    bool isWhiteSpace(char c) const;
    bool isInstructionSeparator(char c) const;

private:
    std::string mWhiteSpaces;
    std::string mInstructionSeparators;
};

#endif // TOKENIZER_H
