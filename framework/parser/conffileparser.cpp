#include "conffileparser.h"

#include <framework/conffile.h>
#include <framework/filesystem.h>
#include <framework/params.h>

ConfFileParser::ConfFileParser(ParamsMulti &result,
                               const std::string &confFileDirectPath,
                               bool *ok)
{
    /** Try to concatenate &path with cwd to obtain realpath.
      * If &path is absolute then FileSystem::concatenatePath will
      * return second argument without changes (i.e &path in this case). */
    ConfFile config;
    if (config.loadDirect(FileSystem::concatenatePath(FileSystem::getCwd(),
                                                      confFileDirectPath)))
    {
        writeToMultiMap(result, config);
        if (ok) *ok = true;
    }
    else
    {
        if (ok) *ok = false;
    }
}

ConfFileParser::ConfFileParser(ParamsMulti &result,
                               const std::string &resourceName,
                               const std::string &resourceGroup,
                               bool *ok)
{
    ConfFile config;
    if (config.loadFromResourceSystem(resourceName, resourceGroup))
    {
        writeToMultiMap(result, config);
        if (ok) *ok = true;
    }
    else
    {
        if (ok) *ok = false;
    }
}

ConfFileParser::ConfFileParser(ParamsMulti &result, const ConfFile &config)
{
    writeToMultiMap(result, config);
}

// ---------------------------------------------------------------

void ConfFileParser::writeToMultiMap(ParamsMulti &result, const ConfFile &config)
{
    const Ogre::ConfigFile::SettingsBySection &allSettings = config.getAllSettings();
    const auto end = allSettings.end();
    for (auto it = allSettings.begin(); it != end; ++it)
    {
        result.insert(it->second->begin(), it->second->end());
    }
}
