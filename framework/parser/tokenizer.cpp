#include "tokenizer.h"

Tokenizer &Tokenizer::setWhiteSpaces(const std::string &str)
{
    mWhiteSpaces = str;
    return *this;
}

Tokenizer &Tokenizer::getWhiteSpaces(std::string &str)
{
    str = mWhiteSpaces;
    return *this;
}

Tokenizer &Tokenizer::setInstructionSeparators(const std::string &str)
{
    mInstructionSeparators = str;
    return *this;
}

Tokenizer &Tokenizer::getInstructionSeparators(std::string &str)
{
    str = mInstructionSeparators;
    return *this;
}

TokenList Tokenizer::tokenize(std::string input)
{
    TokenList tokens;

    Token token;

    auto tokenEndIt = input.end();
    for (auto it = input.begin(); it != tokenEndIt; ++it)
    {
        char currentChar = *it;
        isWhiteSpace(currentChar);
        isInstructionSeparator(currentChar);
    }

    return tokens;
}

bool Tokenizer::isWhiteSpace(char c) const
{
    return (mWhiteSpaces.find(c) != std::string::npos);
}

bool Tokenizer::isInstructionSeparator(char c) const
{
    return (mInstructionSeparators.find(c) != std::string::npos);
}
