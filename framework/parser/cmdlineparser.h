#ifndef CMDLINEPARSER_H
#define CMDLINEPARSER_H

#include <global.h>

/** Command Line arguments parser (base imptementation)
 ** that sorts out argument-value pairs
 ** into multimap (ParamsMulti)
 **
 ** @example
 ** Application cmdline looks like this:
 **     supergame --no_bots --game scripts/startup.lua --script_engine Lua -f
 **
 ** @param @result contains (assumed that @result is empty before call):
 **     game            => scripts/startup.lua
 **     script_engine   => Lua
 **     "" (Empty string) => --no_bots, -f
 **/

class ParamsMulti;

class CmdLineParser
{
public:
    CmdLineParser(ParamsMulti &result, int argc, char **argv);
};

#endif // CMDLINEPARSER_H
