#ifndef CONFFILEPARSER_H
#define CONFFILEPARSER_H

#include <string>

class ParamsMulti;
class ConfFile;

/** Parses ini file key-value pairs info map ignoring sections */
class ConfFileParser
{
public:
    ConfFileParser(ParamsMulti &result, const std::string &confFileDirectPath,
                   bool *ok = nullptr);

    ConfFileParser(ParamsMulti &result, const std::string &resourceName,
                   const std::string &resourceGroup,
                   bool *ok = nullptr);

    ConfFileParser(ParamsMulti &result, const ConfFile &config);

protected:
    static void writeToMultiMap(ParamsMulti &result, const ConfFile &config);
};

#endif // CONFFILEPARSER_H
