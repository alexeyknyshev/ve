#include "cmdlineparser.h"

#include <framework/params.h>
#include <OGRE/OgreString.h>

// value
#define ARG_VALUE 0
// short option
#define ARG_SHORT 1
// long option
#define ARG_LONG 2

CmdLineParser::CmdLineParser(ParamsMulti &result, int argc, char **argv)
{
    std::string prevArg, curArg;
    int prevArgType, curArgType = ARG_VALUE;

    for (int i = 1; i < argc; ++i)
    {
        prevArgType = curArgType;
        prevArg = curArg;

        curArg = argv[i];

        if (curArg.find("--") == 0 && curArg.size() >= 4)
        {
            curArgType = ARG_LONG;

            /** Prev argument is option and cur argument
              * is option, so let's insert prev argument
              * in default ("") group
              */
            if (prevArgType)
            {
                result.insert(ParamsMultiValue(VE_STR_BLANK, prevArg));
            }

            /** @else Prev argument is NOT option and cur is option,
              * so cache cur and expect value
              */

        }
        else
        if (curArg.find("-") == 0 && curArg.size() == 2)
        {
            curArgType = ARG_SHORT;

            /** Prev argument is option and cur argument
              * is option, so let's insert prev argument
              * in default ("") group
              */
            if (prevArgType == ARG_SHORT)
            {
                result.insert(ParamsMultiValue(VE_STR_BLANK, prevArg.substr(1)));
            }
            else if (prevArgType == ARG_LONG)
            {
                result.insert(ParamsMultiValue(VE_STR_BLANK, prevArg.substr(2)));
            }

            /** @else Prev argument is NOT option and cur is option,
              * so cache cur and expect value
              */

        }
        else
        {
            curArgType = ARG_VALUE;

            /** Prev argument is option and cur argument
              * is value, so let's insert cur argument
              * in prev group
              */
            if (prevArgType == ARG_SHORT && prevArg.size() >= 2)
            {
                result.insert(ParamsMultiValue(prevArg.substr(1), curArg));
            }
            else
            if (prevArgType == ARG_LONG && prevArg.size() >= 4)
            {
                result.insert((ParamsMultiValue(prevArg.substr(2), curArg)));
            }

           /** @else Prev argument is value and cur argument
             * is value, so let's skip it
             */
        }
    }
}

#undef ARG_LONG
#undef ARG_SHORT
#undef ARG_VALUE
