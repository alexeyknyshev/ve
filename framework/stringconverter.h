#ifndef STRINGCONVERTER_H
#define STRINGCONVERTER_H

#include <OGRE/OgreStringConverter.h>

class StringConverter : public Ogre::StringConverter
{
public:
    static std::string toString(const Ogre::Vector3 &val, unsigned short precision = 6,
                                unsigned short width = 0, char fill = ' ',
                                std::ios::fmtflags flags = std::ios::fmtflags(0));

    static std::string toString(const Ogre::Vector4 &val, unsigned short precision = 6,
                                unsigned short width = 0, char fill = ' ',
                                std::ios::fmtflags flags = std::ios::fmtflags(0));

    static std::string toString(unsigned int val,
                                unsigned short width = 0, char fill = ' ',
                                std::ios::fmtflags flags = std::ios::fmtflags(0));

    static std::string toString(float val, unsigned short precision,
                                unsigned short width = 0, char fill = ' ',
                                std::ios::fmtflags flags = std::ios::fmtflags(0));
};

#endif // STRINGCONVERTER_H
