#ifndef EASING_H
#define EASING_H

//  easing.h
//
//  Copyright (c) 2011, Auerhaus Development, LLC
//                2015, Alexey Knyshev
//
//  This program is free software. It comes without any warranty, to
//  the extent permitted by applicable law. You can redistribute it
//  and/or modify it under the terms of the Do What The Fuck You Want
//  To Public License, Version 2, as published by Sam Hocevar. See
//  http://sam.zoy.org/wtfpl/COPYING for more details.

class Easing;

typedef float (*EasingFunction)(float);

class Easing
{
public:
    // Linear interpolation (no easing)
    /// @link http://www.wolframalpha.com/input/?i=plot+p+from+-1+to+1
    static float linearInterpolation(float p);

     /// ---------------------------------

    // Quadratic easing; p^2
    /// @link http://www.wolframalpha.com/input/?i=plot+sgn%28p%29*p^2+from+-1+to+1
    static float quadraticIn(float p);

    /// @link http://www.wolframalpha.com/input/?i=plot+-%28p+*+%28abs%28p%29+-+2%29%29+from+-1+to+1
    static float quadraticOut(float p);

    /// @link
    static float quadraticInOut(float p);

    /// ---------------------------------

    // Cubic easing; p^3
    /// @link http://www.wolframalpha.com/input/?i=plot+p^3+from+-1+to+1
    static float cubicIn(float p);    

    /// @link http://www.wolframalpha.com/input/?i=plot+%28p-sgn%28p%29%29^3+%2B+sgn%28p%29+from+-1+to+1
    static float cubicOut(float p);

    /// @link http://www.wolframalpha.com/input/?i=plot+%28%282+*+p%29+-+sgn%28p%29*2%29^3+*+0.5+%2B+sgn%28p%291+from+-1+to+-0.5
    static float cubicInOut(float p);

    // ---------------------------------

    // Quartic easing; p^4
    /// @link http://www.wolframalpha.com/input/?i=plot+p^4*sgn%28p%29+from+-1+to+1
    static float quarticIn(float p);


    static float quarticOut(float p);
    static float quarticInOut(float p);

    /// ---------------------------------

    // Quintic easing; p^5
    static float quinticIn(float p);
    static float quinticOut(float p);
    static float quinticInOut(float p);

    /// ---------------------------------

    // Sine wave easing; sin(p * PI/2)
    static float sineIn(float p);
    static float sineOut(float p);
    static float sineInOut(float p);

    /// ---------------------------------

    // Circular easing; sqrt(1 - p^2)
    static float circularIn(float p);
    static float circularOut(float p);
    static float circularInOut(float p);

    /// ---------------------------------

    // Exponential easing, base 2
    static float exponentialIn(float p);
    static float exponentialOut(float p);
    static float exponentialInOut(float p);

    // Exponentially-damped sine wave easing
    static float elasticIn(float p);
    static float elasticOut(float p);
    static float elasticInOut(float p);

    /// ---------------------------------

    // Overshooting cubic easing;
    static float backIn(float p);
    static float backOut(float p);
    static float backInOut(float p);

    /// ---------------------------------

    // Exponentially-decaying bounce easing
    static float bounceIn(float p);
    static float bounceOut(float p);
    static float bounceInOut(float p);

    /// ---------------------------------

    enum Type
    {
        // Linear interpolation (no easing)
        LinearInterpolation = 0,

        // Quadratic easing; p^2
        QuadraticIn,
        QuadraticOut,
        QuadraticInOut,

        // Cubic easing; p^3
        CubicIn,
        CubicOut,
        CubicInOut,

        // Quartic easing; p^4
        QuarticIn,
        QuarticOut,
        QuarticInOut,

        // Quintic easing; p^5
        QuinticIn,
        QuinticOut,
        QuinticInOut,

        // Sine wave easing; sin(p * PI/2)
        SineIn,
        SineOut,
        SineInOut,

        // Circular easing; sqrt(1 - p^2)
        CircularIn,
        CircularOut,
        CircularInOut,

        // Exponential easing, base 2
        ExponentialIn,
        ExponentialOut,
        ExponentialInOut,

        // Exponentially-damped sine wave easing
        ElasticIn,
        ElasticOut,
        ElasticInOut,

        // Overshooting cubic easing;
        BackIn,
        BackOut,
        BackInOut,

        // Exponentially-decaying bounce easing
        BounceIn,
        BounceOut,
        BounceInOut,
    };

    static EasingFunction getFunction(Type type)
    {
        switch (type)
        {
        case LinearInterpolation: return &Easing::linearInterpolation;
        case QuadraticIn:         return &Easing::quadraticIn;
        case QuadraticOut:        return &Easing::quadraticOut;
        case QuadraticInOut:      return &Easing::quadraticInOut;
        case CubicIn:             return &Easing::cubicIn;
        case CubicOut:            return &Easing::cubicOut;
        case CubicInOut:          return &Easing::cubicInOut;
        case QuarticIn:           return &Easing::quarticIn;
        case QuarticOut:          return &Easing::quarticInOut;
        case QuarticInOut:        return &Easing::quarticInOut;
        case QuinticIn:           return &Easing::quinticIn;
        case QuinticOut:          return &Easing::quinticOut;
        case QuinticInOut:        return &Easing::quinticInOut;
        case SineIn:              return &Easing::sineIn;
        case SineOut:             return &Easing::sineOut;
        case SineInOut:           return &Easing::sineInOut;
        case CircularIn:          return &Easing::circularIn;
        case CircularOut:         return &Easing::circularOut;
        case CircularInOut:       return &Easing::circularInOut;
        case ExponentialIn:       return &Easing::exponentialIn;
        case ExponentialOut:      return &Easing::exponentialOut;
        case ExponentialInOut:    return &Easing::exponentialInOut;
        case ElasticIn:           return &Easing::elasticIn;
        case ElasticOut:          return &Easing::elasticOut;
        case ElasticInOut:        return &Easing::elasticInOut;
        case BackIn:              return &Easing::backIn;
        case BackOut:             return &Easing::backOut;
        case BackInOut:           return &Easing::backInOut;
        case BounceIn:            return &Easing::bounceIn;
        case BounceOut:           return &Easing::bounceOut;
        case BounceInOut:         return &Easing::bounceInOut;
        }

        return &Easing::linearInterpolation;
    }
};

class Inverter
{
public:
    Inverter()
        : mEnabled(false)
    { }

    float operator()(float val)
    {
        return isEnabled() ? 1 - val : val;
    }

    inline void setEnabled(bool enabled)
    {
        mEnabled = enabled;
    }

    inline bool isEnabled()
    {
        return mEnabled;
    }

private:
    bool mEnabled;
};

#endif // EASING_H
