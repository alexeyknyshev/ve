#ifndef STRINGLIST_H
#define STRINGLIST_H

#include <string>
#include <functional>

#include <framework/list.h>

class StringList : public List<std::string>
{
public:
    const static StringList BLANK;

    StringList() = default;

    StringList(const std::string &text, const std::string &delims = "\n", bool preserveDelims = false);

    std::string join(const std::string &separator) const
    {
        return join([&](size_t, const std::string &str) { return str + separator; });
    }

    std::string join(std::function<std::string (size_t index, const std::string &str)> func) const;

    std::string join(StringList::const_iterator from,
                     StringList::const_iterator to,
                     const std::string &separator) const
    {
        std::string result;
        for (; from != to; ++from)
        {
            result += (*from) + separator;
        }
        return result;
    }

    const std::string &at(int index) const;
    std::string &at(int index);

    const std::string &operator[](int index) const { return at(index); }
    std::string &operator[](int index) { return at(index); }
};

#endif // STRINGLIST_H
