#include "params.h"

#include <framework/stringlist.h>

static std::string keyAndValuesToString(const std::string &key, const StringList &values)
{
    const size_t valuesSize = values.size();
    std::string str = "\"" + key + "\": ";
    if (valuesSize == 0)
    {
        str += "null";
    }
    else if (valuesSize == 1)
    {
        str += "\"" + values.front() + "\"";
    }
    else
    {
        str += "[ ";
        size_t currentIndex = 0;
        for (const std::string &val : values)
        {
            if (currentIndex < valuesSize - 1)
            {
                str += "\"" + val + "\", ";
            }
            else
            {
                str += "\"" + val + "\" ";
            }
            ++currentIndex;
        }
        str += "]";
    }
    return str;
}

std::string ParamsMulti::toString() const
{
    std::string str = "{";

    if (!empty())
    {
        str += "\n";

        std::string key = cbegin()->first;
        StringList valuesForKey;

        const const_iterator end = cend();
        for (const_iterator it = cbegin(); it != end; ++it)
        {
            const std::string &value = it->second;
            if (key != it->first)
            {
                str += keyAndValuesToString(key, valuesForKey) + ",\n";
                valuesForKey.clear();
                key = it->first;
            }
            valuesForKey += value;
        }
        str += keyAndValuesToString(key, valuesForKey) + "\n";
    }


    str += "}";
    return str;
}
