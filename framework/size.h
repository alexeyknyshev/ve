#ifndef SIZE_H
#define SIZE_H

#include <framework/stringconverter.h>

namespace ve
{
    template<typename Type> class SizeT
    {
    public:
        SizeT()
            : width(0),
              height(0)
        { }

        SizeT(Type w, Type h)
            : width(w),
              height(h)
        { }

        SizeT(const Ogre::Vector2 &vec)
            : width(vec.x),
              height(vec.y)
        { }

        SizeT(Ogre::Vector2 &&vec)
            : width(std::move(vec.x)),
              height(std::move(vec.y))
        { }

        /** @param string in [width]x[height] format
          * @example "100x200" */
        SizeT(const std::string &sizeStr, const SizeT<Type> &defaultSize = SizeT<Type>(1, 1))
        {
            const int maxSplits = 1;
            const bool preserveDelims = false;
            const Ogre::vector<Ogre::String>::type sizeSpilt =
                    Ogre::StringUtil::split(sizeStr, "x", maxSplits, preserveDelims);

            if (sizeSpilt.size() == 2)
            {
                width  = Ogre::StringConverter::parseReal(sizeSpilt.at(0), defaultSize.width);
                height = Ogre::StringConverter::parseReal(sizeSpilt.at(1), defaultSize.height);
            }
            else
            {
                width  = defaultSize.width;
                height = defaultSize.height;
            }
        }

        SizeT(const SizeT<Type> &other)
            : width(other.width),
              height(other.height)
        { }

        std::string toString() const
        {
            return StringConverter::toString(width) + "x" +
                   StringConverter::toString(height);
        }

        inline bool operator ==(const SizeT<Type> &other) const
        {
            return (width  == other.width &&
                    height == other.height);
        }

        inline bool operator !=(const SizeT<Type> &other) const
        {
            return !(*this == other);
        }

        inline bool isNull() const
        {
            return (width == 0 && height == 0);
        }

        Type width, height;
    };

    typedef SizeT<uint32_t> SizeU;
    typedef SizeT<float> SizeF;
} // namespace ve

#endif // SIZE_H
