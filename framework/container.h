#ifndef CONTAINER_H
#define CONTAINER_H

#include <algorithm> // std::find
#include <functional>
#include <set>
#include <map>

typedef std::set<std::string> StringSet;
typedef std::vector<std::string> StringVector;

/// ------ Algorithms ------

template<class Container>
void deleteAll(Container &container)
{
    for (auto *elementPtr : container)
    {
        delete elementPtr;
    }
}

template<class Container, class Value>
bool contains(const Container &container, const Value &value)
{
    return (container.find(value) != container.cend());
}

template<class Value>
using Map = std::map<std::string, Value>;

template<class Value>
bool containsMap(const Map<Value> &map, const Value &value)
{
    const auto end = map.cend();
    for (auto it = map.cbegin(); it != end; ++it)
    {
        if (it->second == value)
        {
            return true;
        }
    }
    return false;
}

template<class Map, class Value>
bool containsMap(const Map &map, const Value &value)
{
    const auto end = map.cend();
    for (auto it = map.cbegin(); it != end; ++it)
    {
        if (it->second == value)
        {
            return true;
        }
    }
    return false;
}

template<class Iterator, class Predicate>
bool contains_if(const Iterator &begin, const Iterator &end, const Predicate &pred)
{
    return std::find_if(begin, end, pred) != end;
}

template<class Generator, class Container, class Value>
Value unique(Generator &generator, const Container &container, const Value &value)
{
    if (contains(container, value))
    {
        return generator.generate();
    }
    return value;
}

template<class Generator, class Iterator, class Value>
Value unique(Generator &generator,
             const Iterator &begin,
             const Iterator &end,
             const Value &value)
{
    if (contains_if(begin, end, [&](const Value &v) -> bool { return value == v; } ))
    {
        return generator.generate();
    }
    return value;
}

template<class Generator, class Container, class Value, class Predicate>
Value unique_if(Generator &generator, const Container &container, const Value &value, const Predicate &pred)
{
    if (contains_if(container.cbegin(), container.cend(), pred))
    {
        return generator.generate();
    }
    return value;
}

#endif // CONTAINER_H
