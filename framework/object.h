#ifndef OBJECT_H
#define OBJECT_H

#include <framework/logger/logger.h>
#include <framework/hasher.h>

#include <string>
#include <functional>

class Event;

/** Base type that can receive events (as part of event loop
 ** or by direct call) and have a static meta information about
 ** its type (class) and individual name */

#ifndef SWIG
#define VE_DECLARE_STATIC_META_INFO(clazz) \
static const ClassName &getStaticClassName() \
{ \
    static const ClassName className(VE_AS_STR(clazz)); \
    return className; \
} \
\
static size_t getStaticClassHash() \
{ \
    static const size_t hash = VE_CLASS_HASH(clazz); \
    return hash; \
}
#else // SWIG
#define VE_DECLARE_STATIC_META_INFO(clazz)
#endif

#ifndef SWIG
#define VE_DECLARE_TYPE_INFO(clazz) \
    virtual const ClassName &getClassName() const \
    { \
        return clazz::getStaticClassName(); \
    } \
    \
    static const ClassName &getStaticClassName() \
    { \
        static const ClassName className(VE_AS_STR(clazz)); \
        return className; \
    } \
    \
    virtual size_t getClassHash() const \
    { \
        return clazz::getStaticClassHash(); \
    } \
    \
    static size_t getStaticClassHash() \
    { \
        static const size_t hash = VE_CLASS_HASH(clazz); \
        return hash; \
    }
#else // SWIG
#define VE_DECLARE_TYPE_INFO(clazz)
#endif // SWIG

#define VE_DECLARE_SINGLETON(Class) \
template<> Class *Ogre::Singleton<Class>::msSingleton = nullptr

class Object
{
public:
    Object();
    virtual ~Object();

#ifndef SWIG
    virtual bool event(const Event *event) = 0;
#endif // SWIG

    /** Callbacks */

    typedef std::function<void(const Params &)> CallbackFunc;
    void setCallback(uint32_t eventType, CallbackFunc callback);
    const CallbackFunc *getCallback(uint32_t eventType) const;

    /*
    class Hash
    {
    public:
        Hash(size_t hash_, Hash *parent)
            : hash(hash_)
        {
            // Check Hash has parent if non-root
            if (veAssert((parent != nullptr) != (this == &msInheritanceRoot),
                         VE_CLASS_NAME_STR(Hash),
                         VE_SCOPE_NAME_STR(Hash::Hash),
                         "Null parent for nonroot or not null parent for root condition detected!"))
            {
                parent->inheritors.insert(this);
            }
        }

        ~Hash()
        {
            for (auto it = inheritors.begin(); it != inheritors.end(); ++it)
            {
                delete (*it);
            }
        }

        Hash(const Hash &other)
            : hash(other.hash)
        {

        }

        size_t hash;
        std::vector<Hash *> inheritors;

        static Hash msInheritanceRoot;

        inline bool operator ==(const Hash &other)
        {
            return (hash == other.hash);
        }

        inline bool operator <(const Hash &other)
        {
            return hash < other.hash;
        }
    };
    */

    virtual const ClassName &getClassName() const = 0;
    virtual size_t getClassHash() const = 0;

    virtual const std::string &getName() const = 0;

    static void logAndSoftAssertStatic(const ClassName &className,
                                       const std::string &objectName,
                                       const ScopeName &scope,
                                       const std::string &msg,
                                       std::uint64_t component,
                                       int level);

protected:
    virtual void destroyed();

    inline bool checkNullPointer(const ScopeName &scope,
                                 const char *typeName,                                 
                                 const char *ptrName,
                                 const void *ptr,
                                 std::uint64_t component) const
    {
        if (!ptr)
        {
            logAndAssert(scope,
                         "Null pointer (" + std::string(ptrName) + ") of type " +
                         std::string(typeName) + "!",
                         component);
            return false;
        }

        return true;
    }

    virtual void assertNotImplemented(const ScopeName &scope, std::uint64_t component) const;

    virtual void log(const std::string &msg, int level, std::uint64_t component) const;

#ifndef NDEBUG
    virtual void logObjectTrace(const std::string &msg, int level, std::uint64_t component);
#endif // NDEBUG

    virtual void logAndAssert(const ScopeName &scope,
                              const std::string &msg,
                              std::uint64_t component) const;

    virtual void logAndSoftAssert(const ScopeName &scope,
                                  const std::string &msg,
                                  std::uint64_t component,
                                  int level) const;

    inline const std::string &logName() const
    {
        const std::string &name = getName();
        if (name.empty())
        {
            const static std::string Unnamed("UNNAMED_OBJECT");
            return Unnamed;
        }
        return name;
    }

#ifndef NDEBUG
public:
    void _setDebugTraceEnbled(bool enabled)
    {
        mDebugTraceEnabled = enabled;
    }

    bool _isDebugTraceEnabled() const
    {
        return mDebugTraceEnabled;
    }

private:
    bool mDebugTraceEnabled;
#endif // NDEBUG

private:
    typedef std::map<uint32_t, CallbackFunc *> CallbackFuncMap;
    CallbackFuncMap *mCallbacks;
};

#ifndef NDEBUG
#define VE_CLASS_CHECK_NULL_PTR(scope, typeName, ptr, component) \
    checkNullPointer(scope, \
                     #typeName, \
                     #ptr, \
                     ptr, \
                     component)
#else
#define VE_CLASS_CHECK_NULL_PTR(scope, typeName, ptr, component) true
#endif

#endif // OBJECT_H
