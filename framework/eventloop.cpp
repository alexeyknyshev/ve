#include "eventloop.h"

#include <framework/object.h>

#include <OGRE/OgreTimer.h>

#include <chrono>
#include <thread>

EventLoop::EventLoop()
    : mAboutToExit(false),
      mExitStatus(0),
      mCurrentBuffer(0)
{
}

int EventLoop::enter()
{
    static const std::chrono::nanoseconds sleepInterval(1);
    while (!isAboutToExit())
    {
//        std::lock_guard<std::mutex> lock(mEventsMutex);
        processEvents();
        std::this_thread::sleep_for(sleepInterval);
    }
    return mExitStatus;
}

void EventLoop::exit(int status) const
{
    if (status != 0)
    {
        mExitStatus = status;
    }
    mAboutToExit = true;
}

void EventLoop::processEvents(uint32_t mask)
{
    /* Get and lock buffer */
    EventBuffer &lockedBuffer = getCurrentEventBuffer();

    /* Process events */
    const auto end = lockedBuffer.end();
    for (auto it = lockedBuffer.begin(); it != end;)
    {
        Object *receiver = it->first;
        Event *event = it->second;

        if (event->getType() & mask)
        {
            /* Event successfully processed */
            if (notify(receiver, event))
            {
                /** @note
                 ** Increment the iteratior but returns the
                 ** original value for use by erase
                 ** @link http://stackoverflow.com/questions/263945/
                 **       what-happens-if-you-call-erase-on-a-map-element-while-iterating-from-begin-to
                 **/
                delete event;
                lockedBuffer.erase(it++);
            /* Event lifetime is over */
            }
            else
            if (!event->decreaseLifeTime())
            {
                delete event;
                lockedBuffer.erase(it++);
            }
            else
            {
                ++it;
            }
        }
        else
        {
            ++it;
        }
    }

    /* Now we can write to processed buffer */
    swapEventBuffers();
}

void EventLoop::processEvents(uint32_t mask, uint32_t maxmsec)
{
    static Ogre::Timer elapsedTime;
    elapsedTime.reset();

    processEvents(mask);

    /// TODO: processing event only by type
//    if (mask & RendererEvents) {
//        if (elapsedTime.getMilliseconds() > maxmsec)
//            break;
//    }
//    if (mask & UserInputEvents) {
//        if (elapsedTime.getMicroseconds() > maxmsec)
//            break;
//    }
}

void EventLoop::postEvent(Object *obj, Event *event)
{
//    std::lock_guard<std::mutex> lock(mEventsMutex);
    getBackEventBuffer().insert({ obj, event });
}

bool EventLoop::sendEvent(Object *obj, Event *event)
{
    return obj->event(event);
}

bool EventLoop::notify(Object *obj, Event *event)
{
    return sendEvent(obj, event);
}

void EventLoop::swapEventBuffers()
{
//    std::lock_guard<std::mutex> lock(mEventsMutex);
    if (mCurrentBuffer == 1)
    {
        mCurrentBuffer = 0;
    }
    else
    {
        mCurrentBuffer = 1;
    }
}

EventLoop::EventBuffer &EventLoop::getCurrentEventBuffer()
{
    return mEventBuffers[mCurrentBuffer];
}

EventLoop::EventBuffer &EventLoop::getBackEventBuffer()
{
    if (mCurrentBuffer == 1)
    {
        return mEventBuffers[0];
    }

    return mEventBuffers[1];
}

void EventLoop::receiverDestroyed(Object *reciever)
{
    removeEventFromBufferByReceiver(reciever, 0);
    removeEventFromBufferByReceiver(reciever, 1);
}

void EventLoop::removeEventFromBufferByReceiver(Object *receiver, uint32_t bufferIndex)
{
    if (bufferIndex > 1)
    {
        bufferIndex = 1;
    }

    std::pair<EventBuffer::iterator, EventBuffer::iterator> eq_range =
            mEventBuffers[bufferIndex].equal_range(receiver);

    for (auto it = eq_range.first; it != eq_range.second; ++it)
    {
        delete it->second;
        mEventBuffers[bufferIndex].erase(it);
    }
}
