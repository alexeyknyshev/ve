#include "abstractinputlistener.h"

#include <framework/stringconverter.h>

namespace ve {
    bool KeyData::isPrintable() const
    {
        return isAlpha() || isNum() || isWhiteSpace() || isPunct();
    }

    bool KeyData::isAlpha() const
    {
        return (key >= KEY_Q && key <= KEY_M);
    }

    bool KeyData::isNum() const
    {
        return (key >= KEY_1        && key <= KEY_0) ||
               (key >= KEY_NUMPAD_1 && key <= KEY_NUMPAD_0);
    }

    bool KeyData::isWhiteSpace() const
    {
        return (key == KEY_SPACE || key == KEY_TAB);
    }

    bool KeyData::isPunct() const
    {
        return (key >= KEY_GRAVE && key <= KEY_MULTIPLY) ||
               (key >= KEY_NUMPAD_MINUS && key <= KEY_NUMPAD_EQUALS);
    }
}

bool AbstractInputListener::event(const Event *event)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(AbstractInputListener::event(const Event *event)),
                                VE_CLASS_NAME(Event),
                                event,
                                Logger::LOG_COMPONENT_USERINPUT))
    {
        Event::EventType type = event->getType();
        switch (type)
        {
        case Event::ET_Keyboard:
        {
            const KeyboardEvent *keyEvent = static_cast<const KeyboardEvent *>(event);
            if (keyEvent->data.keyState == ve::KeyData::KBS_Pressed)
            {
                keyPressed(*keyEvent);
            }
            else if (keyEvent->data.keyState == ve::KeyData::KBS_Released)
            {
                keyReleased(*keyEvent);
            }
            break;
        }
        case Event::ET_Mouse:
        {
            const MouseEvent *mouseEvent = static_cast<const MouseEvent *>(event);
            if (mouseEvent->data.buttonState == ve::MouseData::MBS_Pressed)
            {
                mousePressed(*mouseEvent);
            }
            else if (mouseEvent->data.buttonState == ve::MouseData::MBS_Released)
            {
                mouseReleased(*mouseEvent);
            }
            break;
        }
        default:
            return false;
        }

        return true;
    }

    return false;
}

/*
bool AbstractInputListener::sliderMoved(const OIS::JoyStickEvent &event, int index)
{
    VE_UNUSED(event);
    VE_UNUSED(index);
    return true;
}

bool AbstractInputListener::povMoved(const OIS::JoyStickEvent &event, int index)
{
    VE_UNUSED(event);
    VE_UNUSED(index);
    return true;
}

bool AbstractInputListener::vector3Moved(const OIS::JoyStickEvent &event, int index)
{
    VE_UNUSED(event);
    VE_UNUSED(index);
    return true;
}
*/

/// =================================================================================

void KeyboardEvent::toStdMap(Params &output) const
{
    UserInputEvent::toStdMap(output);
    data.toStdMap(output);
}

void ve::KeyData::toStdMap(Params &output) const
{
    output["key"] = getKeyButtonName(key);
    if (keyState != KBS_UNASSIGNED)
    {
        output["state"] = keyState == KBS_Pressed ? "1" : "0";
    }
    output["interval"] = StringConverter::toString(interval, 4);
//    output["text"] =
}

/// =================================================================================

void MouseEvent::toStdMap(Params &output) const
{
    UserInputEvent::toStdMap(output);
    data.toStdMap(output);
}

void ve::MouseData::toStdMap(Params &output) const
{
    output["button"] = getMouseButtonName(button);
    if (buttonState != MBS_UNASSIGNED)
    {
        output["state"] = buttonState == MBS_Pressed ? "1" : "0";
    }
    output["xrelative"] = StringConverter::toString(xRelative);
    output["yrelative"] = StringConverter::toString(yRelative);
    output["x"] = StringConverter::toString(x);
    output["y"] = StringConverter::toString(y);
    /// TODO: wheel
}
