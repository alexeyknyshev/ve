#ifndef SDLINPUTBACKEND_H
#define SDLINPUTBACKEND_H

#include <framework/controller/inputcontrollerbackend.h>

class SDLInputBackend : public InputControllerBackend
{
public:
    SDLInputBackend(InputController *controller,
                    RenderView &view,
                    bool exclusiveInput);
    ~SDLInputBackend();

    bool capture() override;

    bool isCompatibleWithRenderViewBackend(const RenderViewBackend *backend) const override;

    void setTextEditingEnabled(bool enabled) override;
    bool isTextEditingEnabled() const override;

    static const std::string &getBackendNameStatic()
    {
        static const std::string NAME = "SDL";
        return NAME;
    }

    const std::string &getBackendName() const override
    {
        return getBackendNameStatic();
    }

    uint32_t sdlWindowId;
};

class SDLInputBackendFactory : public InputControllerBackendFactory
{
public:
    const std::string &getBackendName() const override
    {
        return SDLInputBackend::getBackendNameStatic();
    }

    InputControllerBackend *createInputControllerBackend(InputController *controller,
                                                         RenderView &view,
                                                         bool exclusiveInput) override;
};

#endif // SDLINPUTBACKEND_H
