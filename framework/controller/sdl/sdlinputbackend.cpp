#include "sdlinputbackend.h"

#include <framework/size.h>
#include <framework/controller/inputcontroller.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/renderviewbackend.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keyboard.h>

static ve::KeyData::KeyButton convertKeyCode(SDL_Scancode key)
{
    switch (key)
    {
    case SDL_SCANCODE_UNKNOWN:      return ve::KeyData::KEY_UNASSIGNED;

    case SDL_SCANCODE_A:            return ve::KeyData::KEY_A;
    case SDL_SCANCODE_B:            return ve::KeyData::KEY_B;
    case SDL_SCANCODE_C:            return ve::KeyData::KEY_C;
    case SDL_SCANCODE_D:            return ve::KeyData::KEY_D;
    case SDL_SCANCODE_E:            return ve::KeyData::KEY_E;
    case SDL_SCANCODE_F:            return ve::KeyData::KEY_F;
    case SDL_SCANCODE_G:            return ve::KeyData::KEY_G;
    case SDL_SCANCODE_H:            return ve::KeyData::KEY_H;
    case SDL_SCANCODE_I:            return ve::KeyData::KEY_I;
    case SDL_SCANCODE_J:            return ve::KeyData::KEY_J;
    case SDL_SCANCODE_K:            return ve::KeyData::KEY_K;
    case SDL_SCANCODE_L:            return ve::KeyData::KEY_L;
    case SDL_SCANCODE_M:            return ve::KeyData::KEY_M;
    case SDL_SCANCODE_N:            return ve::KeyData::KEY_N;
    case SDL_SCANCODE_O:            return ve::KeyData::KEY_O;
    case SDL_SCANCODE_P:            return ve::KeyData::KEY_P;
    case SDL_SCANCODE_Q:            return ve::KeyData::KEY_Q;
    case SDL_SCANCODE_R:            return ve::KeyData::KEY_R;
    case SDL_SCANCODE_S:            return ve::KeyData::KEY_S;
    case SDL_SCANCODE_T:            return ve::KeyData::KEY_T;
    case SDL_SCANCODE_U:            return ve::KeyData::KEY_U;
    case SDL_SCANCODE_V:            return ve::KeyData::KEY_V;
    case SDL_SCANCODE_W:            return ve::KeyData::KEY_W;
    case SDL_SCANCODE_X:            return ve::KeyData::KEY_X;
    case SDL_SCANCODE_Y:            return ve::KeyData::KEY_Y;
    case SDL_SCANCODE_Z:            return ve::KeyData::KEY_Z;

    case SDL_SCANCODE_1:            return ve::KeyData::KEY_1;
    case SDL_SCANCODE_2:            return ve::KeyData::KEY_2;
    case SDL_SCANCODE_3:            return ve::KeyData::KEY_3;
    case SDL_SCANCODE_4:            return ve::KeyData::KEY_4;
    case SDL_SCANCODE_5:            return ve::KeyData::KEY_5;
    case SDL_SCANCODE_6:            return ve::KeyData::KEY_6;
    case SDL_SCANCODE_7:            return ve::KeyData::KEY_7;
    case SDL_SCANCODE_8:            return ve::KeyData::KEY_8;
    case SDL_SCANCODE_9:            return ve::KeyData::KEY_9;
    case SDL_SCANCODE_0:            return ve::KeyData::KEY_0;

    case SDL_SCANCODE_RETURN:       return ve::KeyData::KEY_RETURN;
    case SDL_SCANCODE_ESCAPE:       return ve::KeyData::KEY_ESCAPE;
    case SDL_SCANCODE_BACKSPACE:    return ve::KeyData::KEY_BACKSPACE;
    case SDL_SCANCODE_TAB:          return ve::KeyData::KEY_TAB;
    case SDL_SCANCODE_SPACE:        return ve::KeyData::KEY_SPACE;

    case SDL_SCANCODE_MINUS:        return ve::KeyData::KEY_MINUS;
    case SDL_SCANCODE_EQUALS:       return ve::KeyData::KEY_EQUALS;
    case SDL_SCANCODE_LEFTBRACKET:  return ve::KeyData::KEY_BRACKET_LEFT;
    case SDL_SCANCODE_RIGHTBRACKET: return ve::KeyData::KEY_BRACKET_RIGHT;
    case SDL_SCANCODE_BACKSLASH:    return ve::KeyData::KEY_BACKSLASH;

    case SDL_SCANCODE_NONUSHASH:    return ve::KeyData::KEY_UNASSIGNED; /// TODO: investigate SDL_SCANCODE_NONUSHASH
    case SDL_SCANCODE_SEMICOLON:    return ve::KeyData::KEY_SEMICOLON;
    case SDL_SCANCODE_APOSTROPHE:   return ve::KeyData::KEY_APOSTROPHE;
    case SDL_SCANCODE_GRAVE:        return ve::KeyData::KEY_GRAVE;
    case SDL_SCANCODE_COMMA:        return ve::KeyData::KEY_COMMA;
    case SDL_SCANCODE_PERIOD:       return ve::KeyData::KEY_PERIOD;
    case SDL_SCANCODE_SLASH:        return ve::KeyData::KEY_SLASH;

    case SDL_SCANCODE_CAPSLOCK:     return ve::KeyData::KEY_CAPSLOCK;

    case SDL_SCANCODE_F1:           return ve::KeyData::KEY_F1;
    case SDL_SCANCODE_F2:           return ve::KeyData::KEY_F2;
    case SDL_SCANCODE_F3:           return ve::KeyData::KEY_F3;
    case SDL_SCANCODE_F4:           return ve::KeyData::KEY_F4;
    case SDL_SCANCODE_F5:           return ve::KeyData::KEY_F5;
    case SDL_SCANCODE_F6:           return ve::KeyData::KEY_F6;
    case SDL_SCANCODE_F7:           return ve::KeyData::KEY_F7;
    case SDL_SCANCODE_F8:           return ve::KeyData::KEY_F8;
    case SDL_SCANCODE_F9:           return ve::KeyData::KEY_F9;
    case SDL_SCANCODE_F10:          return ve::KeyData::KEY_F10;
    case SDL_SCANCODE_F11:          return ve::KeyData::KEY_F11;
    case SDL_SCANCODE_F12:          return ve::KeyData::KEY_F12;

    case SDL_SCANCODE_PRINTSCREEN:  return ve::KeyData::KEY_PRINT_SCREEN;
    case SDL_SCANCODE_SCROLLLOCK:   return ve::KeyData::KEY_SCRLOCK;
    case SDL_SCANCODE_PAUSE:        return ve::KeyData::KEY_PAUSE;
    case SDL_SCANCODE_INSERT:       return ve::KeyData::KEY_UNASSIGNED; /// TODO: INSERT key mapping

    case SDL_SCANCODE_HOME:         return ve::KeyData::KEY_HOME;
    case SDL_SCANCODE_PAGEUP:       return ve::KeyData::KEY_PAGE_UP;
    case SDL_SCANCODE_DELETE:       return ve::KeyData::KEY_DELETE;
    case SDL_SCANCODE_END:          return ve::KeyData::KEY_END;
    case SDL_SCANCODE_PAGEDOWN:     return ve::KeyData::KEY_PAGE_DOWN;
    case SDL_SCANCODE_RIGHT:        return ve::KeyData::KEY_RIGHT;
    case SDL_SCANCODE_LEFT:         return ve::KeyData::KEY_LEFT;
    case SDL_SCANCODE_DOWN:         return ve::KeyData::KEY_DOWN;
    case SDL_SCANCODE_UP:           return ve::KeyData::KEY_UP;

    case SDL_SCANCODE_KP_DIVIDE:    return ve::KeyData::KEY_SLASH;
    case SDL_SCANCODE_KP_MULTIPLY:  return ve::KeyData::KEY_MULTIPLY;
    case SDL_SCANCODE_KP_MINUS:     return ve::KeyData::KEY_NUMPAD_MINUS;
    case SDL_SCANCODE_KP_PLUS:      return ve::KeyData::KEY_NUMPAD_ADD;
    case SDL_SCANCODE_KP_ENTER:     return ve::KeyData::KEY_NUMPAD_RETURN;
    case SDL_SCANCODE_KP_1:         return ve::KeyData::KEY_NUMPAD_1;
    case SDL_SCANCODE_KP_2:         return ve::KeyData::KEY_NUMPAD_2;
    case SDL_SCANCODE_KP_3:         return ve::KeyData::KEY_NUMPAD_3;
    case SDL_SCANCODE_KP_4:         return ve::KeyData::KEY_NUMPAD_4;
    case SDL_SCANCODE_KP_5:         return ve::KeyData::KEY_NUMPAD_5;
    case SDL_SCANCODE_KP_6:         return ve::KeyData::KEY_NUMPAD_6;
    case SDL_SCANCODE_KP_7:         return ve::KeyData::KEY_NUMPAD_7;
    case SDL_SCANCODE_KP_8:         return ve::KeyData::KEY_NUMPAD_8;
    case SDL_SCANCODE_KP_9:         return ve::KeyData::KEY_NUMPAD_9;
    case SDL_SCANCODE_KP_0:         return ve::KeyData::KEY_NUMPAD_0;
    case SDL_SCANCODE_KP_PERIOD:    return ve::KeyData::KEY_NUMPAD_PERIOD;
    case SDL_SCANCODE_KP_EQUALS:    return ve::KeyData::KEY_EQUALS;
    case SDL_SCANCODE_KP_COMMA:     return ve::KeyData::KEY_COMMA;

    default:;
    }

    return ve::KeyData::KEY_UNASSIGNED;
}

static unsigned int convertToAsciiText(const SDL_KeyboardEvent &event,
                                       const std::locale &locale)
{
    switch (event.keysym.sym)
    {
    case SDLK_KP_DIVIDE:    return '/';
    case SDLK_KP_MULTIPLY:  return '*';
    case SDLK_KP_MINUS:     return '-';
    case SDLK_KP_PLUS:      return '+';
    case SDLK_KP_0:         return '0';
    case SDLK_KP_1:         return '1';
    case SDLK_KP_2:         return '2';
    case SDLK_KP_3:         return '3';
    case SDLK_KP_4:         return '4';
    case SDLK_KP_5:         return '5';
    case SDLK_KP_6:         return '6';
    case SDLK_KP_7:         return '7';
    case SDLK_KP_8:         return '8';
    case SDLK_KP_9:         return '9';
    case SDLK_KP_PERIOD:    return '.';
    }

    if (event.keysym.mod & KMOD_SHIFT)
    {
        if (std::isalpha((char)event.keysym.sym, locale))
        {
            return std::toupper((char)event.keysym.sym, locale);
        }

        if (event.keysym.sym == SDLK_SEMICOLON)
        {
            return SDLK_COLON;
        }

        if (event.keysym.sym == SDLK_QUOTE)
        {
            return SDLK_QUOTEDBL;
        }

        if (event.keysym.sym == SDLK_LEFTBRACKET)
        {
            return '{';
        }

        if (event.keysym.sym == SDLK_RIGHTBRACKET)
        {
            return '}';
        }

        if (event.keysym.sym == SDLK_BACKSLASH)
        {
            return '|';
        }

        if (event.keysym.sym == SDLK_COMMA)
        {
            return SDLK_LESS;
        }

        if (event.keysym.sym == SDLK_PERIOD)
        {
            return SDLK_GREATER;
        }

        if (event.keysym.sym == SDLK_SLASH)
        {
            return SDLK_QUESTION;
        }

        if (event.keysym.sym == SDLK_MINUS)
        {
            return SDLK_UNDERSCORE;
        }

        if (event.keysym.sym == SDLK_EQUALS)
        {
            return SDLK_PLUS;
        }

        if (event.keysym.sym == SDLK_1)
        {
            return SDLK_EXCLAIM;
        }

        if (event.keysym.sym == SDLK_2)
        {
            return SDLK_AT;
        }

        if (event.keysym.sym == SDLK_3)
        {
            return SDLK_HASH;
        }

        if (event.keysym.sym == SDLK_4)
        {
            return SDLK_DOLLAR;
        }

        if (event.keysym.sym == SDLK_5)
        {
            return SDLK_PERCENT;
        }

        if (event.keysym.sym == SDLK_6)
        {
            return SDLK_CARET;
        }

        if (event.keysym.sym == SDLK_7)
        {
            return SDLK_AMPERSAND;
        }

        if (event.keysym.sym == SDLK_8)
        {
            return SDLK_ASTERISK;
        }

        if (event.keysym.sym == SDLK_9)
        {
            return SDLK_LEFTPAREN;
        }

        if (event.keysym.sym == SDLK_0)
        {
            return SDLK_RIGHTPAREN;
        }

        if (event.keysym.sym == SDLK_BACKQUOTE)
        {
            return '~';
        }
    }

    return event.keysym.sym;
}

static ve::MouseData::MouseButton convertMouseButton(uint8_t button)
{
    switch (button)
    {
    case SDL_BUTTON_LEFT:   return ve::MouseData::MB_Left;
    case SDL_BUTTON_RIGHT:  return ve::MouseData::MB_Right;
    case SDL_BUTTON_MIDDLE: return ve::MouseData::MB_Middle;
    case SDL_BUTTON_X1:     return ve::MouseData::MB_Button3;
    case SDL_BUTTON_X2:     return ve::MouseData::MB_Button4;
    default:;
    }

    return ve::MouseData::MB_UNASSIGNED;
}

SDLInputBackend::SDLInputBackend(InputController *controller,
                                 RenderView &view,
                                 bool exclusiveInput)
    : InputControllerBackend(controller, view, exclusiveInput),
      sdlWindowId(-1)
{
    auto *renderViewBackend = view.getRenderViewBackend();
    if (!isCompatibleWithRenderViewBackend(renderViewBackend))
    {
        Object::logAndSoftAssertStatic(VE_CLASS_NAME(SDLInputBackend),
                                       VE_STR_BLANK,
                                       VE_SCOPE_NAME(SDLInputBackend::SDLInputBackend(InputController *, RenderView &, bool)),
                                       VE_CLASS_NAME(SDLInputBackend).toString() + " is incompatible with " +
                                       VE_CLASS_NAME(RenderViewBackend).toString() + " \"" + renderViewBackend->getBackendName() + "\"!",
                                       Logger::LOG_COMPONENT_USERINPUT,
                                       Logger::LOG_LEVEL_CRITICAL);
    }

    SDL_Window *window = static_cast<SDL_Window *>(view.getOptions().window);
    sdlWindowId = SDL_GetWindowID(window);

    if (exclusiveInput)
    {
        SDL_SetWindowGrab(window, SDL_bool(exclusiveInput));
    }
}

SDLInputBackend::~SDLInputBackend()
{
}

bool SDLInputBackend::capture()
{
    static SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        switch (event.type) {
        case SDL_KEYDOWN:
        {
            if (!event.key.repeat)
            {
                InputControllerBackend::keyPressed(convertKeyCode(event.key.keysym.scancode),
                                                   convertToAsciiText(event.key, mController->getLocale()));
            }
            break;
        }
        case SDL_KEYUP:
            if (!event.key.repeat)
            {
                InputControllerBackend::keyReleased(convertKeyCode(event.key.keysym.scancode),
                                                    convertToAsciiText(event.key, mController->getLocale()));
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
            InputControllerBackend::mousePressed(0, event.button.x, 0, event.button.y, 0, 0,
                                                 convertMouseButton(event.button.button));            
            break;
        case SDL_MOUSEBUTTONUP:
            InputControllerBackend::mouseReleased(0, event.button.x, 0, event.button.y, 0, 0,
                                                  convertMouseButton(event.button.button));
            break;
        case SDL_MOUSEMOTION:
        {
            static Sint32 lastX = 0;
            static Sint32 lastY = 0;

            InputControllerBackend::mouseMoved(event.motion.x - lastX, event.motion.x,
                                               event.motion.y - lastY, event.motion.y,
                                               0, 0, 0);
            lastX = event.motion.x;
            lastY = event.motion.y;

            break;
        }
        case SDL_MOUSEWHEEL:
        {
            InputControllerBackend::mouseWheelMoved(0, 0,
                                                    event.wheel.y * 120, 0,
                                                    event.wheel.x * 120, 0);

            break;
        }
        case SDL_WINDOWEVENT:
        {
            if (event.window.windowID != sdlWindowId)
            {
                break;
            }

            switch (event.window.event)
            {
            case SDL_WINDOWEVENT_SHOWN:
                break;
            case SDL_WINDOWEVENT_HIDDEN:
                break;
            case SDL_WINDOWEVENT_MOVED:
                break;
            case SDL_WINDOWEVENT_RESIZED:
                InputControllerBackend::resizeView(ve::SizeU((uint32_t)event.window.data1,
                                                             (uint32_t)event.window.data2));
                break;
            case SDL_WINDOWEVENT_CLOSE:
                /// We should force view closed event (unlike GLX)
                if (InputControllerBackend::closeView())
                {
                    InputControllerBackend::viewClosed();
                }
                return false;
            }
            break;
        }
        default:;
        }
    }

    return true;
}

bool SDLInputBackend::isCompatibleWithRenderViewBackend(const RenderViewBackend *backend) const
{
    return (backend->getBackendName() == getBackendNameStatic());
}

/** ------ Text editing ------ */

void SDLInputBackend::setTextEditingEnabled(bool enabled)
{
    if (enabled && !isTextEditingEnabled())
    {
        SDL_StartTextInput();
    }
    else if (!enabled && isTextEditingEnabled())
    {
        SDL_StopTextInput();
    }
}

bool SDLInputBackend::isTextEditingEnabled() const
{
    return SDL_IsTextInputActive();
}

/** ------ Factory ------ */

InputControllerBackend *SDLInputBackendFactory::createInputControllerBackend(InputController *controller,
                                                                             RenderView &view,
                                                                             bool exclusiveInput)
{
    return new SDLInputBackend(controller, view, exclusiveInput);
}
