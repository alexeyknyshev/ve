#ifndef ABSTRACTINPUTLISTENER_H
#define ABSTRACTINPUTLISTENER_H

#include <framework/object.h>

#include <framework/event.h>

namespace ve
{
    class KeyData
    {
    public:
        enum KeyButton
        {
            KEY_UNASSIGNED,

            KEY_1,
            KEY_2,
            KEY_3,
            KEY_4,
            KEY_5,
            KEY_6,
            KEY_7,
            KEY_8,
            KEY_9,
            KEY_0,

            KEY_Q,
            KEY_W,
            KEY_E,
            KEY_R,
            KEY_T,
            KEY_Y,
            KEY_U,
            KEY_I,
            KEY_O,
            KEY_P,
            KEY_A,
            KEY_S,
            KEY_D,
            KEY_F,
            KEY_G,
            KEY_H,
            KEY_J,
            KEY_K,
            KEY_L,
            KEY_Z,
            KEY_X,
            KEY_C,
            KEY_V,
            KEY_B,
            KEY_N,
            KEY_M,

            KEY_ESCAPE,
            KEY_F1,
            KEY_F2,
            KEY_F3,
            KEY_F4,
            KEY_F5,
            KEY_F6,
            KEY_F7,
            KEY_F8,
            KEY_F9,
            KEY_F10,
            KEY_F11,
            KEY_F12,

            KEY_GRAVE,

            KEY_BRACKET_LEFT,
            KEY_BRACKET_RIGHT,

            KEY_SEMICOLON,
            KEY_APOSTROPHE,

            KEY_SLASH,
            KEY_BACKSLASH,

            KEY_COMMA,
            KEY_PERIOD,

            KEY_MINUS,
            KEY_EQUALS,
            KEY_MULTIPLY,

            KEY_UP,
            KEY_DOWN,
            KEY_LEFT,
            KEY_RIGHT,

            KEY_CTRL_LEFT,
            KEY_CTRL_RIGHT,

            KEY_SHIFT_LEFT,
            KEY_SHIFT_RIGHT,

            KEY_ALT_LEFT,
            KEY_ALT_RIGHT,

            KEY_META_LEFT,
            KEY_META_RIGHT,

            KEY_TAB,
            KEY_CAPSLOCK,

            KEY_BACKSPACE,
            KEY_DELETE,

            KEY_SPACE,
            KEY_RETURN,

            KEY_PAUSE,
            KEY_PRINT_SCREEN,

            KEY_PAGE_UP,
            KEY_PAGE_DOWN,

            KEY_HOME,
            KEY_END,

            KEY_NUMLOCK,
            KEY_SCRLOCK,

            KEY_NUMPAD_1,
            KEY_NUMPAD_2,
            KEY_NUMPAD_3,
            KEY_NUMPAD_4,
            KEY_NUMPAD_5,
            KEY_NUMPAD_6,
            KEY_NUMPAD_7,
            KEY_NUMPAD_8,
            KEY_NUMPAD_9,
            KEY_NUMPAD_0,

            KEY_NUMPAD_MINUS,
            KEY_NUMPAD_ADD,
            KEY_NUMPAD_PERIOD,
            KEY_NUMPAD_EQUALS,
            KEY_NUMPAD_RETURN
        };

        static std::string getKeyButtonName(KeyButton button)
        {
            switch (button)
            {
            default:
            case KEY_UNASSIGNED: return "UNASSIGNED";

            case KEY_1: return "1";
            case KEY_2: return "2";
            case KEY_3: return "3";
            case KEY_4: return "4";
            case KEY_5: return "5";
            case KEY_6: return "6";
            case KEY_7: return "7";
            case KEY_8: return "8";
            case KEY_9: return "9";
            case KEY_0: return "0";

            case KEY_Q: return "Q";
            case KEY_W: return "W";
            case KEY_E: return "E";
            case KEY_R: return "R";
            case KEY_T: return "T";
            case KEY_Y: return "Y";
            case KEY_U: return "U";
            case KEY_I: return "I";

            case KEY_O: return "O";
            case KEY_P: return "P";
            case KEY_A: return "A";
            case KEY_S: return "S";
            case KEY_D: return "D";

            case KEY_F: return "F";
            case KEY_G: return "G";
            case KEY_H: return "H";
            case KEY_J: return "J";
            case KEY_K: return "K";
            case KEY_L: return "L";
            case KEY_Z: return "Z";
            case KEY_X: return "X";
            case KEY_C: return "C";
            case KEY_V: return "V";
            case KEY_B: return "B";
            case KEY_N: return "N";
            case KEY_M: return "M";

            case KEY_ESCAPE: return "ESCAPE";
            case KEY_F1: return "F1";
            case KEY_F2: return "F2";
            case KEY_F3: return "F3";
            case KEY_F4: return "F4";
            case KEY_F5: return "F5";
            case KEY_F6: return "F6";
            case KEY_F7: return "F7";
            case KEY_F8: return "F8";
            case KEY_F9: return "F9";
            case KEY_F10: return "F10";
            case KEY_F11: return "F11";
            case KEY_F12: return "F12";

            case KEY_GRAVE: return "`";

            case KEY_BRACKET_LEFT: return "[";
            case KEY_BRACKET_RIGHT: return "]";

            case KEY_SEMICOLON: return ";";
            case KEY_APOSTROPHE: return "'";

            case KEY_SLASH: return "/";
            case KEY_BACKSLASH: return "\\";

            case KEY_COMMA: return ",";
            case KEY_PERIOD: return ".";

            case KEY_MINUS: return "-";
            case KEY_EQUALS: return "=";
            case KEY_MULTIPLY: return "*";

            case KEY_UP: return "UP";
            case KEY_DOWN: return "DOWN";
            case KEY_LEFT: return "LEFT";
            case KEY_RIGHT: return "RIGHT";

            case KEY_CTRL_LEFT: return "CTRL_LEFT";
            case KEY_CTRL_RIGHT: return "CTRL_RIGHT";

            case KEY_SHIFT_LEFT: return "SHIFT_LEFT";
            case KEY_SHIFT_RIGHT: return "SHIFT_RIGHT";

            case KEY_ALT_LEFT: return "ALT_LEFT";
            case KEY_ALT_RIGHT: return "ALT_RIGHT";

            case KEY_META_LEFT: return "META_LEFT";
            case KEY_META_RIGHT: return "META_RIGHT";

            case KEY_TAB: return "TAB";
            case KEY_CAPSLOCK: return "CAPSLOCK";

            case KEY_BACKSPACE: return "BACKSPACE";
            case KEY_DELETE: return "DELETE";

            case KEY_SPACE: return "SPACE";
            case KEY_RETURN: return "RETURN";

            case KEY_PAUSE: return "PAUSE";
            case KEY_PRINT_SCREEN: return "PRINT_SCREEN";

            case KEY_PAGE_UP: return "PAGE_UP";
            case KEY_PAGE_DOWN: return "PAGE_DOWN";

            case KEY_HOME: return "HOME";
            case KEY_END: return "END";

            case KEY_NUMLOCK: return "NUMLOCK";
            case KEY_SCRLOCK: return "SCRLOCK";

            case KEY_NUMPAD_1: return "NUMPAD_1";
            case KEY_NUMPAD_2: return "NUMPAD_2";
            case KEY_NUMPAD_3: return "NUMPAD_3";
            case KEY_NUMPAD_4: return "NUMPAD_4";
            case KEY_NUMPAD_5: return "NUMPAD_5";
            case KEY_NUMPAD_6: return "NUMPAD_6";
            case KEY_NUMPAD_7: return "NUMPAD_7";
            case KEY_NUMPAD_8: return "NUMPAD_8";
            case KEY_NUMPAD_9: return "NUMPAD_9";
            case KEY_NUMPAD_0: return "NUMPAD_0";

            case KEY_NUMPAD_MINUS: return "NUMPAD_MINUS";
            case KEY_NUMPAD_ADD: return "NUMPAD_ADD";
            case KEY_NUMPAD_PERIOD: return "NUMPAD_PERIOD";
            case KEY_NUMPAD_EQUALS: return "NUMPAD_EQUALS";
            case KEY_NUMPAD_RETURN: return "NUMPAD_RETURN";
            }
        }

        enum KeyButtonState
        {
            KBS_Released = 0,
            KBS_Pressed,
            KBS_UNASSIGNED
        };

        KeyData(KeyButton key_,
                KeyButtonState keyState_,
                float interval_,
                unsigned int text_)
            : key(key_),
              keyState(keyState_),
              interval(interval_),
              text(text_)
        { }

        KeyData()
            : KeyData(KEY_UNASSIGNED, KBS_UNASSIGNED, 0.0f, 0)
        { }

        KeyButton key;
        KeyButtonState keyState;
        float interval;
        unsigned int text;

        inline bool operator ==(const KeyData &other)
        {
            return key == other.key;
        }

        bool isPrintable() const;
        bool isAlpha() const;
        bool isNum() const;
        bool isWhiteSpace() const;
        bool isPunct() const;

        inline std::string getKeyName() const { return getKeyButtonName(key); }

        void toStdMap(Params &output) const;
    };

    class MouseData
    {
    public:
        enum MouseButton
        {
            MB_Left = 0,
            MB_Right,
            MB_Middle,
            MB_Button3,
            MB_Button4,
            MB_Button5,
            MB_Button6,
            MB_Button7,
            MB_UNASSIGNED
        };

        static std::string getMouseButtonName(MouseButton button)
        {
            switch (button)
            {
            default:
            case MB_UNASSIGNED: return "UNASSIGNED";

            case MB_Left:    return "left";
            case MB_Right:   return "right";
            case MB_Middle:  return "middle";
            case MB_Button3: return "3";
            case MB_Button4: return "4";
            case MB_Button5: return "5";
            case MB_Button6: return "6";
            case MB_Button7: return "7";
            }
        }

        enum MouseButtonState
        {
            MBS_Released = 0,
            MBS_Pressed,
            MBS_UNASSIGNED
        };

        MouseData(MouseData::MouseButton button_,
                  MouseData::MouseButtonState buttonState_,
                  int32_t xRelative_, uint32_t x_,
                  int32_t yRelative_, uint32_t y_,
                  int32_t wheelVertRelative_, uint32_t wheelVert_,
                  int32_t wheelHorizRelative_, uint32_t wheelHoriz_)
            : button(button_),
              buttonState(buttonState_),
              xRelative(xRelative_), yRelative(yRelative_),
              wheelVertRelative(wheelVertRelative_), wheelHorizRelative(wheelHorizRelative_),
              x(x_), y(y_),
              wheelVert(wheelVert_), wheelHoriz(wheelHoriz_)
        { }

        MouseButton button;
        MouseButtonState buttonState;

        int32_t xRelative, yRelative, wheelVertRelative, wheelHorizRelative;

        uint32_t x, y, wheelVert, wheelHoriz;

        inline bool operator ==(const MouseData &other)
        {
            return (//buttonsStates == other.buttonsStates &&
                    (button      == other.button) &&
                    (buttonState == other.buttonState));
        }

        void toStdMap(Params &output) const;
    };
} // namespace ve

class InputController;

class UserInputEvent : public Event
{
public:
    UserInputEvent(const UserInputEvent &other)
        : sender(other.sender)
    { }

    UserInputEvent(EventType type, InputController *sender_)
        : Event(type),
          sender(sender_)
    { }

    InputController *sender;
};

class KeyboardEvent : public UserInputEvent
{
public:
    KeyboardEvent(const KeyboardEvent &other)
        : UserInputEvent(other),
          data(other.data)
    { }

    KeyboardEvent(ve::KeyData data_, InputController *sender)
        : UserInputEvent(ET_Keyboard, sender),
          data(data_)
    { }

    Event *clone() const override
    {
        return new KeyboardEvent(*this);
    }

    ve::KeyData data;

    void toStdMap(Params &output) const override;
};

class MouseEvent : public UserInputEvent
{
public:
    MouseEvent(const ve::MouseData &data_,
               InputController *sender)
        : UserInputEvent(ET_Mouse, sender),
          data(data_)
    { }

    Event *clone() const override
    {
        return new MouseEvent(*this);
    }

    ve::MouseData data;

    void toStdMap(Params &output) const override;
};

/// =================================================

class AbstractInputListener : public Object
{
    friend class InputController;

public:
    VE_DECLARE_TYPE_INFO(AbstractInputListener)

    bool event(const Event *event);

    /** ------ Keyboard ------ */
    virtual bool keyPressed(const KeyboardEvent &event) = 0;
    virtual bool keyReleased(const KeyboardEvent &event) = 0;

    /** ------ Mouse ------ */
    virtual bool mouseMoved(const MouseEvent &event) = 0;
    virtual bool mousePressed(const MouseEvent &event) = 0;
    virtual bool mouseReleased(const MouseEvent &event) = 0;

    /** ------ Joystick ------ */
    /*
    virtual bool buttonPressed(const OIS::JoyStickEvent &event, int button) = 0;
    virtual bool buttonReleased(const OIS::JoyStickEvent &event, int button) = 0;
    virtual bool axisMoved(const OIS::JoyStickEvent &event, int axis) = 0;

    // Not so common control events, so no override required
    virtual bool sliderMoved(const OIS::JoyStickEvent &event, int index);
    virtual bool povMoved(const OIS::JoyStickEvent &event, int index);
    virtual bool vector3Moved(const OIS::JoyStickEvent &event, int index);
    */

    /** ------ Gameloop ------ */
    virtual void update(float deltaTime, float realTime) = 0;

protected:
    /** ------ On attach/detach callback ------ */
    virtual void registredAsInputListener(bool registred, InputController *controller) = 0;
};

#endif // ABSTRACTINPUTLISTENER_H
