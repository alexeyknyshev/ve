#ifndef INPUTCONTROLLER_H
#define INPUTCONTROLLER_H

#include <framework/object.h>
#include <framework/updatelistener.h>

#include <set>

class RenderView;
namespace ve
{
    template<typename Type> class SizeT;
    typedef SizeT<uint32_t> SizeU;
}
class MouseEvent;
class KeyboardEvent;
class AbstractInputListener;
class InputControllerBackend;
class InputControllerBackendFactory;

class InputController : public Object,
                        public UpdateListener
{
public:
    VE_DECLARE_TYPE_INFO(InputController)

    friend class RenderView;
    friend class InputControllerBackend;

    InputController(const std::string &name, InputControllerBackendFactory *factory);

    virtual ~InputController();

    bool event(const Event *event);

    inline const std::string &getName() const
    {
        return mName;
    }

    inline RenderView *getControlledRenderView() const
    {
        return mControlledView;
    }

    bool capture();

    bool addInputListener(AbstractInputListener *listener, bool own = true);

    bool removeInputListener(AbstractInputListener *listener);

    void destroyAllInputListeners();

    inline void setModalInputListener(AbstractInputListener *listener)
    {
        mModalListener = listener;
    }

    inline AbstractInputListener *getModalInputListener() const
    {
        return mModalListener;
    }

    /** locale to convert chars */
    inline void setLocale(const std::locale &locale)
    {
        mLocale = locale;
    }

    inline const std::locale &getLocale() const
    {
        return mLocale;
    }

    void setBackendFactory(InputControllerBackendFactory *factory);

    void update(float deltaTime, float realTime) override;

protected:
    /** ------ Mouse ------ */

    void mouseMoved(const MouseEvent &evt);
    void mousePressed(const MouseEvent &evt);
    void mouseReleased(const MouseEvent &evt);

    /** ------ Keyboard ------ */

    void keyPressed(const KeyboardEvent &evt);
    void keyReleased(const KeyboardEvent &evt);


    virtual void init(RenderView &view, bool isExclusiveInput);

    virtual void shutdown();

    void setWindowExtents(const ve::SizeU &size);

private:
    const std::string mName;
    RenderView *mControlledView;

    typedef std::map<AbstractInputListener *, bool> InputListenerMap;

    InputListenerMap mInputListeners;
    AbstractInputListener *mModalListener;

    InputControllerBackendFactory *mBackendFactory;
    InputControllerBackend *mBackend;

    std::locale mLocale;
};

#endif // INPUTCONTROLLER_H
