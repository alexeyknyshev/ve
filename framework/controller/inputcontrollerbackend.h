#ifndef INPUTCONTROLLERBACKEND_H
#define INPUTCONTROLLERBACKEND_H

#include <framework/controller/abstractinputlistener.h>
#include <framework/updatable.h>

class InputController;

namespace ve
{
    template<typename Type> class SizeT;
    typedef SizeT<uint32_t> SizeU;
}

class RenderView;
class RenderViewBackend;

class InputControllerBackend : public Updatable
{
public:
    InputControllerBackend(InputController *controller,
                           RenderView &view,
                           bool exclusiveInput);

    virtual ~InputControllerBackend();

    virtual void update(float deltaTime, float realTime) override;

    /** on view resize callback */
    virtual void viewResized(const ve::SizeU &size);

    /** return true if you want to stop eventloop */
    virtual bool capture() = 0;

    virtual const std::string &getBackendName() const = 0;

    virtual bool isCompatibleWithRenderViewBackend(const RenderViewBackend *backend) const = 0;

    virtual void setTextEditingEnabled(bool enabled) = 0;
    virtual bool isTextEditingEnabled() const = 0;

protected:

    /** ------ Mouse ------ */

    void mouseMoved(int xRel, int x,
                    int yRel, int y,
                    int wheelRel, int wheel,
                    int button);

    void mousePressed(int xRel, int x,
                      int yRel, int y,
                      int wheelRel, int wheel,
                      int button);

    void mouseReleased(int xRel, int x,
                       int yRel, int y,
                       int wheelRel, int wheel,
                       int button);

    void mouseWheelMoved(int x, int y,
                         int wheelVertRel, int wheelVert,
                         int wheelHorizRel, int wheelHoriz);

    /** ------ Keyboard ------ */

    void keyPressed(ve::KeyData::KeyButton key, unsigned int text);
    void keyReleased(ve::KeyData::KeyButton key, unsigned int text);

    /** call this when input backend requested resize */
    void resizeView(const ve::SizeU &size);

    bool closeView();
    void viewClosed();

protected:
    InputController *mController;
};

class InputControllerBackendFactory
{
public:
    virtual ~InputControllerBackendFactory()
    { }

    virtual const std::string &getBackendName() const = 0;

    virtual InputControllerBackend *createInputControllerBackend(InputController *controller,
                                                                 RenderView &view,
                                                                 bool exclusiveInput) = 0;
};

#endif // INPUTCONTROLLERBACKEND_H
