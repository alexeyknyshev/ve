#ifndef DEFAULTINPUTLISTENER_H
#define DEFAULTINPUTLISTENER_H

#include <framework/controller/abstractinputlistener.h>

class DefaultInputListener : public AbstractInputListener
{
public:
    DefaultInputListener(const std::string &name);
    ~DefaultInputListener();

    const std::string &getName() const override
    {
        return mName;
    }

    /** ------ Keyboard ------ */
    bool keyPressed(const KeyboardEvent &event) override;
    bool keyReleased(const KeyboardEvent &event) override;

    /** ------ Mouse ------ */
    bool mouseMoved(const MouseEvent &event) override;
    bool mousePressed(const MouseEvent &event) override;
    bool mouseReleased(const MouseEvent &event) override;

    /** ------ Joystick ------ */
    /*
    virtual bool buttonPressed(const OIS::JoyStickEvent &event, int button);
    virtual bool buttonReleased(const OIS::JoyStickEvent &event, int button);
    virtual bool axisMoved(const OIS::JoyStickEvent &event, int axis);
    */

    /** ------ Updating ------ */

    void update(float deltaTime, float realTime) override;

protected:
    typedef std::map<ve::KeyData::KeyButton, ve::KeyData> PressedKeys;
    typedef std::map<ve::MouseData::MouseButton, bool> PressedButtons;

    inline const PressedKeys &getPressedKeys() const
    {
        return mPressedKeys;
    }

    inline const PressedButtons &getPressedButtons() const
    {
        return mPressedButtons;
    }

    const PressedKeys::value_type &getLastPressedKey() const;
    const PressedButtons::value_type &getLastPressedButton() const;

    bool isKeyPressed(ve::KeyData::KeyButton key) const;
    float getKeyPressedTime(ve::KeyData::KeyButton key) const;

    bool isButtonPressed(ve::MouseData::MouseButton button) const;
    float getButtonPressedTime(ve::MouseData::MouseButton button) const;

private:
    PressedKeys mPressedKeys;
    PressedButtons mPressedButtons;
    const std::string mName;
};

#endif // DEFAULTINPUTLISTENER_H
