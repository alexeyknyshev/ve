#include "defaultinputlistener.h"

DefaultInputListener::DefaultInputListener(const std::string &name)
    : mName(name)
{ }

DefaultInputListener::~DefaultInputListener()
{ }

/** ------ Keyboard ------ */

bool DefaultInputListener::keyPressed(const KeyboardEvent &event)
{
    if (event.data.key != ve::KeyData::KEY_UNASSIGNED)
    {
        mPressedKeys[event.data.key] = event.data;
    }
    return true;
}

bool DefaultInputListener::keyReleased(const KeyboardEvent &event)
{
    if (event.data.key != ve::KeyData::KEY_UNASSIGNED)
    {
        mPressedKeys.erase(event.data.key);
    }
    return true;
}

/** ------ Mouse ------ */

bool DefaultInputListener::mouseMoved(const MouseEvent &event)
{
    VE_UNUSED(event);
    return true;
}

bool DefaultInputListener::mousePressed(const MouseEvent &event)
{
    mPressedButtons[event.data.button] = 0.0f;
    return true;
}

bool DefaultInputListener::mouseReleased(const MouseEvent &event)
{
    mPressedButtons.erase(event.data.button);
    return true;
}

/** ------ Joystick ------ */

/*
bool DefaultInputListener::buttonPressed(const OIS::JoyStickEvent &event, int button)
{
    VE_UNUSED(event);
    VE_UNUSED(button);
    return true;
}

bool DefaultInputListener::buttonReleased(const OIS::JoyStickEvent &event, int button)
{
    VE_UNUSED(event);
    VE_UNUSED(button);
    return true;
}

bool DefaultInputListener::axisMoved(const OIS::JoyStickEvent &event, int axis)
{
    VE_UNUSED(event);
    VE_UNUSED(axis);
    return true;
}
*/

/** ------ Updating ------ */

void DefaultInputListener::update(float deltaTime, float realTime)
{
    const auto keysEnd = mPressedKeys.end();
    for (auto it = mPressedKeys.begin(); it != keysEnd; ++it)
    {
        it->second.interval += deltaTime;
    }

    const auto buttonsEnd = mPressedButtons.end();
    for (auto it = mPressedButtons.begin(); it != buttonsEnd; ++it)
    {
        it->second += deltaTime;
    }
}

/** ------ Key state info ------ */

const DefaultInputListener::PressedKeys::value_type &DefaultInputListener::getLastPressedKey() const
{
    if (mPressedKeys.empty())
    {
        const static PressedKeys::value_type failback(ve::KeyData::KEY_UNASSIGNED, ve::KeyData());
        return failback;
    }

    float minTime = std::numeric_limits<float>::max();
    ve::KeyData::KeyButton minTimeCode = ve::KeyData::KEY_UNASSIGNED;
    for (const PressedKeys::value_type &val : mPressedKeys)
    {
        if (val.second.interval < minTime)
        {
            minTimeCode = val.first;
            minTime = val.second.interval;
        }
    }

    return *mPressedKeys.find(minTimeCode);
}

bool DefaultInputListener::isKeyPressed(ve::KeyData::KeyButton key) const
{
    return (mPressedKeys.find(key) != mPressedKeys.end());
}

float DefaultInputListener::getKeyPressedTime(ve::KeyData::KeyButton key) const
{
    const auto it = mPressedKeys.find(key);
    if (it != mPressedKeys.end())
    {
        return it->second.interval;
    }
    return 0.0f;
}

/** ------ Button state info ------ */

const DefaultInputListener::PressedButtons::value_type &DefaultInputListener::getLastPressedButton() const
{
    if (mPressedButtons.empty())
    {
        const static PressedButtons::value_type failback(ve::MouseData::MB_UNASSIGNED, 0.0f);
        return failback;
    }

    float minTime = std::numeric_limits<float>::max();
    ve::MouseData::MouseButton minTimeButton = ve::MouseData::MB_UNASSIGNED;
    for (const PressedButtons::value_type &val : mPressedButtons)
    {
        if (val.second < minTime)
        {
            minTimeButton = val.first;
            minTime = val.second;
        }
    }

    return *mPressedButtons.find(minTimeButton);
}

bool DefaultInputListener::isButtonPressed(ve::MouseData::MouseButton button) const
{
    return (mPressedButtons.find(button) != mPressedButtons.end());
}

float DefaultInputListener::getButtonPressedTime(ve::MouseData::MouseButton button) const
{
    const auto it = mPressedButtons.find(button);
    if (it != mPressedButtons.end())
    {
        return it->second;
    }
    return 0.0f;
}
