#include "oisinputbackend.h"

#include <framework/controller/abstractinputlistener.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/renderviewbackend.h>

#include <OIS/OISInputManager.h>

#include <OGRE/OgreStringConverter.h>

static ve::KeyData::KeyButton convertKeyCode(OIS::KeyCode key)
{
    switch (key)
    {
    case OIS::KC_UNASSIGNED:    return ve::KeyData::KEY_UNASSIGNED;
    case OIS::KC_ESCAPE:        return ve::KeyData::KEY_ESCAPE;
    case OIS::KC_1:             return ve::KeyData::KEY_1;
    case OIS::KC_2:             return ve::KeyData::KEY_2;
    case OIS::KC_3:             return ve::KeyData::KEY_3;
    case OIS::KC_4:             return ve::KeyData::KEY_4;
    case OIS::KC_5:             return ve::KeyData::KEY_5;
    case OIS::KC_6:             return ve::KeyData::KEY_6;
    case OIS::KC_7:             return ve::KeyData::KEY_7;
    case OIS::KC_8:             return ve::KeyData::KEY_8;
    case OIS::KC_9:             return ve::KeyData::KEY_9;
    case OIS::KC_0:             return ve::KeyData::KEY_0;
    case OIS::KC_MINUS:         return ve::KeyData::KEY_MINUS;
    case OIS::KC_EQUALS:        return ve::KeyData::KEY_EQUALS;
    case OIS::KC_BACK:          return ve::KeyData::KEY_BACKSPACE;
    case OIS::KC_TAB:           return ve::KeyData::KEY_TAB;
    case OIS::KC_Q:             return ve::KeyData::KEY_Q;
    case OIS::KC_W:             return ve::KeyData::KEY_W;
    case OIS::KC_E:             return ve::KeyData::KEY_E;
    case OIS::KC_R:             return ve::KeyData::KEY_R;
    case OIS::KC_T:             return ve::KeyData::KEY_T;
    case OIS::KC_Y:             return ve::KeyData::KEY_Y;
    case OIS::KC_U:             return ve::KeyData::KEY_U;
    case OIS::KC_I:             return ve::KeyData::KEY_I;
    case OIS::KC_O:             return ve::KeyData::KEY_O;
    case OIS::KC_P:             return ve::KeyData::KEY_P;
    case OIS::KC_LBRACKET:      return ve::KeyData::KEY_BRACKET_LEFT;
    case OIS::KC_RBRACKET:      return ve::KeyData::KEY_BRACKET_RIGHT;
    case OIS::KC_RETURN:        return ve::KeyData::KEY_RETURN;
    case OIS::KC_LCONTROL:      return ve::KeyData::KEY_CTRL_LEFT;
    case OIS::KC_A:             return ve::KeyData::KEY_A;
    case OIS::KC_S:             return ve::KeyData::KEY_S;
    case OIS::KC_D:             return ve::KeyData::KEY_D;
    case OIS::KC_F:             return ve::KeyData::KEY_F;
    case OIS::KC_G:             return ve::KeyData::KEY_G;
    case OIS::KC_H:             return ve::KeyData::KEY_H;
    case OIS::KC_J:             return ve::KeyData::KEY_J;
    case OIS::KC_K:             return ve::KeyData::KEY_K;
    case OIS::KC_L:             return ve::KeyData::KEY_L;
    case OIS::KC_SEMICOLON:     return ve::KeyData::KEY_SEMICOLON;
    case OIS::KC_APOSTROPHE:    return ve::KeyData::KEY_APOSTROPHE;
    case OIS::KC_GRAVE:         return ve::KeyData::KEY_GRAVE;
    case OIS::KC_LSHIFT:        return ve::KeyData::KEY_SHIFT_LEFT;
    case OIS::KC_BACKSLASH:     return ve::KeyData::KEY_BACKSLASH;
    case OIS::KC_Z:             return ve::KeyData::KEY_Z;
    case OIS::KC_X:             return ve::KeyData::KEY_X;
    case OIS::KC_C:             return ve::KeyData::KEY_C;
    case OIS::KC_V:             return ve::KeyData::KEY_V;
    case OIS::KC_B:             return ve::KeyData::KEY_B;
    case OIS::KC_N:             return ve::KeyData::KEY_N;
    case OIS::KC_M:             return ve::KeyData::KEY_M;
    case OIS::KC_COMMA:         return ve::KeyData::KEY_COMMA;
    case OIS::KC_PERIOD:        return ve::KeyData::KEY_PERIOD;
    case OIS::KC_SLASH:         return ve::KeyData::KEY_SLASH;
    case OIS::KC_RSHIFT:        return ve::KeyData::KEY_SHIFT_RIGHT;
    case OIS::KC_MULTIPLY:      return ve::KeyData::KEY_MULTIPLY;
    case OIS::KC_LMENU:         return ve::KeyData::KEY_ALT_LEFT;
    case OIS::KC_SPACE:         return ve::KeyData::KEY_SPACE;
    case OIS::KC_CAPITAL:       return ve::KeyData::KEY_CAPSLOCK;
    case OIS::KC_F1:            return ve::KeyData::KEY_F1;
    case OIS::KC_F2:            return ve::KeyData::KEY_F2;
    case OIS::KC_F3:            return ve::KeyData::KEY_F3;
    case OIS::KC_F4:            return ve::KeyData::KEY_F4;
    case OIS::KC_F5:            return ve::KeyData::KEY_F5;
    case OIS::KC_F6:            return ve::KeyData::KEY_F6;
    case OIS::KC_F7:            return ve::KeyData::KEY_F7;
    case OIS::KC_F8:            return ve::KeyData::KEY_F8;
    case OIS::KC_F9:            return ve::KeyData::KEY_F9;
    case OIS::KC_F10:           return ve::KeyData::KEY_F10;
    case OIS::KC_NUMLOCK:       return ve::KeyData::KEY_NUMLOCK;
    case OIS::KC_SCROLL:        return ve::KeyData::KEY_SCRLOCK;
    case OIS::KC_NUMPAD7:       return ve::KeyData::KEY_NUMPAD_7;
    case OIS::KC_NUMPAD8:       return ve::KeyData::KEY_NUMPAD_8;
    case OIS::KC_NUMPAD9:       return ve::KeyData::KEY_NUMPAD_9;
    case OIS::KC_SUBTRACT:      return ve::KeyData::KEY_NUMPAD_MINUS;
    case OIS::KC_NUMPAD4:       return ve::KeyData::KEY_NUMPAD_4;
    case OIS::KC_NUMPAD5:       return ve::KeyData::KEY_NUMPAD_5;
    case OIS::KC_NUMPAD6:       return ve::KeyData::KEY_NUMPAD_6;
    case OIS::KC_ADD:           return ve::KeyData::KEY_NUMPAD_ADD;
    case OIS::KC_NUMPAD1:       return ve::KeyData::KEY_NUMPAD_1;
    case OIS::KC_NUMPAD2:       return ve::KeyData::KEY_NUMPAD_2;
    case OIS::KC_NUMPAD3:       return ve::KeyData::KEY_NUMPAD_3;
    case OIS::KC_NUMPAD0:       return ve::KeyData::KEY_NUMPAD_0;
    case OIS::KC_F11:           return ve::KeyData::KEY_F11;
    case OIS::KC_F12:           return ve::KeyData::KEY_F12;
    case OIS::KC_NUMPADENTER:   return ve::KeyData::KEY_NUMPAD_RETURN;
    case OIS::KC_UP:            return ve::KeyData::KEY_UP;
    case OIS::KC_DOWN:          return ve::KeyData::KEY_DOWN;
    case OIS::KC_LEFT:          return ve::KeyData::KEY_LEFT;
    case OIS::KC_RIGHT:         return ve::KeyData::KEY_RIGHT;
    case OIS::KC_HOME:          return ve::KeyData::KEY_HOME;
    case OIS::KC_END:           return ve::KeyData::KEY_END;
    case OIS::KC_PGUP:          return ve::KeyData::KEY_PAGE_UP;
    case OIS::KC_PGDOWN:        return ve::KeyData::KEY_PAGE_DOWN;
    case OIS::KC_DELETE:        return ve::KeyData::KEY_DELETE;
    default:;
    }

    return ve::KeyData::KEY_UNASSIGNED;
}

OISInputBackend::OISInputBackend(InputController *controller, RenderView &view, bool exclusiveInput)
    : InputControllerBackend(controller, view, exclusiveInput),
      mInputManager(nullptr),
      mKeyboard(nullptr),
      mMouse(nullptr),
      mIsTextEditingEnabled(false)
{
    auto *renderViewBackend = view.getRenderViewBackend();
    if (!isCompatibleWithRenderViewBackend(renderViewBackend))
    {
        Object::logAndSoftAssertStatic(VE_CLASS_NAME(OISInputBackend),
                                       VE_STR_BLANK,
                                       VE_SCOPE_NAME(OISInputBackend::OISInputBackend(InputController *, RenderView &, bool)),
                                       VE_CLASS_NAME(OISInputBackend).toString() + " is incompatible with " +
                                       VE_CLASS_NAME(RenderViewBackend).toString() + " \"" + renderViewBackend->getBackendName() + "\"!",
                                       Logger::LOG_COMPONENT_USERINPUT,
                                       Logger::LOG_LEVEL_CRITICAL);
    }

    OIS::ParamList paramList;

    // This is useful for non fullscreen mode, especially when debugging.
    if (!exclusiveInput)
    {
#if VE_PLATFORM == VE_PLATFORM_LINUX
        paramList.insert({ "x11_mouse_grab",     "false" });
        paramList.insert({ "x11_mouse_hide",     "false" });
        paramList.insert({ "x11_keyboard_grab",  "false" });
        paramList.insert({ "XAutoRepeatOn",      "true"  });
#elif VE_PLATFORM == VE_PLATFORM_WIN32
        paramList.insert({ "w32_mouse",    "DISCL_FOREGROUND"   });
        paramList.insert({ "w32_mouse",    "DISCL_NONEXCLUSIVE" });
        paramList.insert({ "w32_keyboard", "DISCL_FOREGROUND"   });
        paramList.insert({ "w32_keyboard", "DISCL_NONEXCLUSIVE" });
#endif
    }

    // Fill parameter list
    const std::string handlerStr = StringConverter::toString(view.getWindowHandler());
    paramList.insert({ "WINDOW", handlerStr });

    // Create input manager
    mInputManager = OIS::InputManager::createInputSystem(paramList);

    if (!mKeyboard && (mInputManager->getNumberOfDevices(OIS::OISKeyboard) > 0))
    {
        OIS::Object *obj = mInputManager->createInputObject(OIS::OISKeyboard, true);
        mKeyboard = static_cast<OIS::Keyboard *>(obj);
        mKeyboard->setEventCallback(this);
    }

    if (!mMouse && (mInputManager->getNumberOfDevices(OIS::OISMouse) > 0))
    {
        OIS::Object *obj = mInputManager->createInputObject(OIS::OISMouse, true);
        mMouse = static_cast<OIS::Mouse *>(obj);
        mMouse->setEventCallback(this);

        viewResized(view.getSize());
    }
}

OISInputBackend::~OISInputBackend()
{
    if (mMouse)
    {
        mInputManager->destroyInputObject(mMouse);
        mMouse = nullptr;
    }

    if (mKeyboard)
    {
        mInputManager->destroyInputObject(mKeyboard);
        mKeyboard = nullptr;
    }

    mInputManager->destroyInputSystem(mInputManager);
    mInputManager = nullptr;
}

bool OISInputBackend::capture()
{
    if (mInputManager)
    {
        if (mMouse)
        {
            mMouse->capture();
        }

        if (mKeyboard)
        {
            mKeyboard->capture();
        }

        return true;
    }

    return false;
}

bool OISInputBackend::isCompatibleWithRenderViewBackend(const RenderViewBackend *backend) const
{
    return (backend->getBackendName() == "OGRE");
}

void OISInputBackend::viewResized(const ve::SizeU &size)
{
    if (mMouse)
    {
        const OIS::MouseState &state = mMouse->getMouseState();
        state.width = size.width;
        state.height = size.height;
    }
}

/** ------ Mouse ------ */

bool OISInputBackend::mouseMoved(const OIS::MouseEvent &evt)
{
    InputControllerBackend::mouseMoved(evt.state.X.rel, evt.state.X.abs,
                                       evt.state.Y.rel, evt.state.Y.abs,
                                       0, 0,
                                       evt.state.buttons);

    if (evt.state.Z.rel != 0)
    {
        InputControllerBackend::mouseWheelMoved(evt.state.X.abs, evt.state.Y.abs,
                                                evt.state.Z.rel, evt.state.Z.abs,
                                                0, 0);
    }

    return true;
}

bool OISInputBackend::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    InputControllerBackend::mousePressed(evt.state.X.rel, evt.state.X.abs,
                                         evt.state.Y.rel, evt.state.Y.abs,
                                         evt.state.Z.rel, evt.state.Z.abs,
                                         (int)id);
    return true;
}

bool OISInputBackend::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    InputControllerBackend::mouseReleased(evt.state.X.rel, evt.state.X.abs,
                                          evt.state.Y.rel, evt.state.Y.abs,
                                          evt.state.Z.rel, evt.state.Z.abs,
                                          (int)id);
    return true;
}

/** ------ Keyboard ------ */

bool OISInputBackend::keyPressed(const OIS::KeyEvent &evt)
{
    InputControllerBackend::keyPressed(convertKeyCode(evt.key), evt.text);
    return true;
}

bool OISInputBackend::keyReleased(const OIS::KeyEvent &evt)
{
    InputControllerBackend::keyReleased(convertKeyCode(evt.key), evt.text);
    return true;
}

/** ------ Factory ------ */

InputControllerBackend *OISInputBackendFactory::createInputControllerBackend(InputController *controller,
                                                                             RenderView &view,
                                                                             bool exclusiveInput)
{
    return new OISInputBackend(controller, view, exclusiveInput);
}
