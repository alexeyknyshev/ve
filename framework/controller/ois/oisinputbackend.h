#ifndef OISINPUTBACKEND_H
#define OISINPUTBACKEND_H

#include <framework/controller/inputcontrollerbackend.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

class OISInputBackend : public InputControllerBackend,
                        public OIS::MouseListener,
                        public OIS::KeyListener
{
public:
    OISInputBackend(InputController *controller, RenderView &view, bool exclusiveInput);
    ~OISInputBackend();

    bool capture() override;

    bool isCompatibleWithRenderViewBackend(const RenderViewBackend *backend) const override;

    virtual void setTextEditingEnabled(bool enabled) override
    {
        mIsTextEditingEnabled = enabled;
    }

    virtual bool isTextEditingEnabled() const override
    {
        return mIsTextEditingEnabled;
    }

    static const std::string &getBackendNameStatic()
    {
        static const std::string NAME = "OIS";
        return NAME;
    }

    const std::string &getBackendName() const override
    {
        return getBackendNameStatic();
    }

    /** ------ Mouse ------ */

    bool mouseMoved(const OIS::MouseEvent &evt) override;
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id) override;
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id) override;

    /** ------ Keyboard ------ */

    bool keyPressed(const OIS::KeyEvent &evt) override;
    bool keyReleased(const OIS::KeyEvent &evt) override;

    /** ------ View ------ */

    void viewResized(const ve::SizeU &size) override;

private:
    OIS::InputManager *mInputManager;

    OIS::Keyboard *mKeyboard;
    OIS::Mouse *mMouse;

    bool mIsTextEditingEnabled;
};

/** ------ Factory ------ */

class OISInputBackendFactory : public InputControllerBackendFactory
{
public:
    const std::string &getBackendName() const override
    {
        const static std::string NAME = "OIS";
        return NAME;
    }

    InputControllerBackend *createInputControllerBackend(InputController *controller,
                                                         RenderView &view,
                                                         bool exclusiveInput) override;
};

#endif // OISINPUTBACKEND_H
