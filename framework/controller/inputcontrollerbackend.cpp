#include "inputcontrollerbackend.h"

#include <framework/controller/abstractinputlistener.h>
#include <framework/controller/inputcontroller.h>
#include <framework/frontend/renderview.h>

#include <global.h>

//#include <OGRE/OgreStringConverter.h>

InputControllerBackend::InputControllerBackend(InputController *controller, RenderView &view, bool exclusiveInput)
    : mController(controller)
{
    VE_UNUSED(view);
    VE_UNUSED(exclusiveInput);
}

InputControllerBackend::~InputControllerBackend()
{
    mController = nullptr;
}

void InputControllerBackend::update(float deltaTime, float realTime)
{
    VE_UNUSED(deltaTime); VE_UNUSED(realTime);
}

void InputControllerBackend::viewResized(const ve::SizeU &size)
{
    VE_UNUSED(size);
}

void InputControllerBackend::resizeView(const ve::SizeU &size)
{
    RenderView *view = mController->getControlledRenderView();
    if (VE_CHECK_NULL_PTR(RenderView, view, Logger::LOG_COMPONENT_USERINPUT))
    {
        view->setSize(size);
    }
}

bool InputControllerBackend::closeView()
{
    RenderView *view = mController->getControlledRenderView();
    return view->close();
}

void InputControllerBackend::viewClosed()
{
    RenderView *view = mController->getControlledRenderView();
    view->closed();
}

/** ------ Mouse ------ */

void InputControllerBackend::mouseMoved(int xRel, int x,
                                        int yRel, int y,
                                        int wheelRel, int wheel,
                                        int button)
{   
    /*
    const std::string xRelStr = StringConverter::toString(xRel);
    logger()->logMessage("Mouse move: xRel = " + xRelStr,
                         Logger::LOG_LEVEL_DEBUG,
                         Logger::LOG_COMPONENT_USERINPUT);*/

    ve::MouseData data((ve::MouseData::MouseButton)button,
                       ve::MouseData::MBS_UNASSIGNED,
                       xRel, x,
                       yRel, y,
                       wheelRel, wheel,
                       0, 0);
    MouseEvent e(data, mController);
    mController->mouseMoved(e);
}

void InputControllerBackend::mousePressed(int xRel, int x,
                                          int yRel, int y,
                                          int wheelRel, int wheel,
                                          int button)
{
    ve::MouseData data((ve::MouseData::MouseButton)button,
                       ve::MouseData::MBS_Pressed,
                       xRel, x,
                       yRel, y,
                       wheelRel, wheel,
                       0, 0);
    MouseEvent e(data, mController);
    mController->mousePressed(e);
}

void InputControllerBackend::mouseReleased(int xRel, int x,
                                           int yRel, int y,
                                           int wheelRel, int wheel,
                                           int button)
{
    ve::MouseData data((ve::MouseData::MouseButton)button,
                       ve::MouseData::MBS_Released,
                       xRel, x,
                       yRel, y,
                       wheelRel, wheel,
                       0, 0);
    MouseEvent e(data, mController);
    mController->mouseReleased(e);
}

void InputControllerBackend::mouseWheelMoved(int x, int y, int wheelVertRel, int wheelVert, int wheelHorizRel, int wheelHoriz)
{
    /*
    const std::string wheelVertRelStr = StringConverter::toString(wheelVertRel);
    const std::string wheelHorizRelStr = StringConverter::toString(wheelHorizRel);

    logger()->logMessage("Mouse wheel move: wheelVertRel = " + wheelVertRelStr +
                                        "; wheelHorizRel = " + wheelHorizRelStr,
                         Logger::LOG_LEVEL_DEBUG,
                         Logger::LOG_COMPONENT_USERINPUT);*/

    ve::MouseData data(ve::MouseData::MB_UNASSIGNED,
                       ve::MouseData::MBS_UNASSIGNED,
                       0, x,
                       0, y,
                       wheelVertRel, wheelVert,
                       wheelHorizRel, wheelHoriz);
    MouseEvent e(data, mController);
    mController->mouseMoved(e);
}

/** ------ Keyboard ------ */

void InputControllerBackend::keyPressed(ve::KeyData::KeyButton key, unsigned int text)
{
    KeyboardEvent e(ve::KeyData(key, ve::KeyData::KBS_Pressed, 0.0f, text), mController);
    mController->keyPressed(e);
}

void InputControllerBackend::keyReleased(ve::KeyData::KeyButton key, unsigned int text)
{
    KeyboardEvent e(ve::KeyData(key, ve::KeyData::KBS_Released, 0.0f, text), mController);
    mController->keyReleased(e);
}
