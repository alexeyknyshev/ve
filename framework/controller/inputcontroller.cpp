#include "inputcontroller.h"

#include <renderer/renderer.h>

#include <framework/frontend/renderview.h>

#include <framework/controller/abstractinputlistener.h>
#include <framework/controller/inputcontrollerbackend.h>

#include <framework/ui/uimanager.h>

#include <game/game.h>

InputController::InputController(const std::string &name,
                                 InputControllerBackendFactory *factory)
    : UpdateListener(false), // will not be auto collected
      mName(name),
      mControlledView(nullptr),
      mModalListener(nullptr),
      mBackendFactory(factory),
      mBackend(nullptr),
      mLocale(std::locale::classic())
{ }

InputController::~InputController()
{
    shutdown();
    setBackendFactory(nullptr);
}

bool InputController::event(const Event *event)
{
    assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                         Logger::LOG_COMPONENT_USERINPUT);
    VE_UNUSED(event);
    return false;
}

bool InputController::capture()
{
    if (mBackend)
    {
        return mBackend->capture();
    }

    return true;
}

bool InputController::addInputListener(AbstractInputListener *listener, bool own)
{
    if (listener && mInputListeners.insert({ listener, own }).second)
    {
        listener->registredAsInputListener(true, this);
        return true;
    }
    return false;
}

bool InputController::removeInputListener(AbstractInputListener *listener)
{
    if (mModalListener == listener)
    {
        mModalListener = nullptr;
    }

    if (listener && mInputListeners.erase(listener) > 0)
    {
        listener->registredAsInputListener(false, this);
        return true;
    }
    return false;
}

void InputController::destroyAllInputListeners()
{
    mModalListener = nullptr;

    InputListenerMap workingCopy(mInputListeners.begin(), mInputListeners.end());
    mInputListeners.clear();

    const auto end = workingCopy.end();
    for (auto it = workingCopy.begin(); it != end; ++it)
    {
        const AbstractInputListener *listener = it->first;
        const bool own = it->second;
        if (own)
        {
            delete listener;
        }
    }
}

void InputController::init(RenderView &view, bool isExclusiveInput)
{
    mControlledView = &view;

    delete mBackend;
    mBackend = nullptr;

    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(InputController::init(RenderView &view, bool isExclusiveInput)),
                                InputControllerBackendFactory,
                                mBackendFactory,
                                Logger::LOG_COMPONENT_USERINPUT))
    {
        mBackend = mBackendFactory->createInputControllerBackend(this,
                                                                 view,
                                                                 isExclusiveInput);
    }

    UiManager *ui = renderer()->getUiManager();
    if (ui)
    {
        ui->inputControllerRegistred(true, this);
    }
    setAutoUpdateEnabled(true);
}

void InputController::shutdown()
{
    delete mBackend;
    mBackend = nullptr;

    destroyAllInputListeners();

    UiManager *ui = renderer()->getUiManager();
    if (ui)
    {
       ui->inputControllerRegistred(false, this);
    }

    mControlledView = nullptr;
}

void InputController::setWindowExtents(const ve::SizeU &size)
{
    if (mBackend)
    {
        mBackend->viewResized(size);
    }
}

/** ------ Mouse ------ */

void InputController::mouseMoved(const MouseEvent &evt)
{
    if (mModalListener && mModalListener->mouseMoved(evt))
    {
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->mouseMoved(evt);
    }
}

void InputController::mousePressed(const MouseEvent &evt)
{
    if (mModalListener && mModalListener->mousePressed(evt))
    {
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->mousePressed(evt);
    }
}

void InputController::mouseReleased(const MouseEvent &evt)
{
    if (mModalListener && mModalListener->mouseReleased(evt))
    {
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->mouseReleased(evt);
    }
}

/** ------ Keyboard ------ */

void InputController::keyPressed(const KeyboardEvent &evt)
{
    if (mModalListener && mModalListener->keyPressed(evt))
    {
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->keyPressed(evt);
    }
}

void InputController::keyReleased(const KeyboardEvent &evt)
{
    if (mModalListener && mModalListener->keyReleased(evt))
    {
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->keyReleased(evt);
    }
}

void InputController::setBackendFactory(InputControllerBackendFactory *factory)
{
    if (mBackendFactory == factory)
    {
        return;
    }

    delete mBackendFactory;
    mBackendFactory = factory;
}

/** ------ Renderer ------ */

void InputController::update(float deltaTime, float realTime)
{   
    const bool continue_ = capture();
    if (!continue_)
    {
        game()->exit(0);
        return;
    }

    const auto end = mInputListeners.end();
    for (auto it = mInputListeners.begin(); it != end; ++it)
    {
        AbstractInputListener *listener = it->first;
        listener->update(deltaTime, realTime);
    }

    if (mBackend)
    {
        mBackend->update(deltaTime, realTime);
    }
}
