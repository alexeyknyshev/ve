#ifndef EVENTLOOP_H
#define EVENTLOOP_H

//#include <mutex>
#include <queue>
#include <global.h>
#include <framework/event.h>

/** Base (non-multitheaded) implementation of event loop */

class Object;

class EventLoop
{
    friend class Object;

public:
    EventLoop();
    virtual ~EventLoop() { }

    virtual int enter();
    virtual void exit(int code = 0) const;

    virtual void processEvents(uint32_t mask = Event::ET_All); /// \todo
    virtual void processEvents(uint32_t mask, uint32_t maxmsec); /// \todo

    inline bool isAboutToExit() const
    {
        return mAboutToExit;
    }

    inline int getExitStatus() const
    {
        return mExitStatus;
    }

    typedef std::multimap<Object *, Event *> EventBuffer;

    virtual void postEvent(Object *obj, Event *event);

    static bool sendEvent(Object *obj, Event *event);

protected:
    virtual bool notify(Object *obj, Event *event);

    void swapEventBuffers();

    EventBuffer &getCurrentEventBuffer();
    EventBuffer &getBackEventBuffer();

    virtual void receiverDestroyed(Object *reciever);

    void removeEventFromBufferByReceiver(Object *receiver, uint32_t bufferIndex);

private:
    mutable bool mAboutToExit;
    mutable int  mExitStatus;

//    std::mutex mEventsMutex;

    EventBuffer mEventBuffers[2];
    uint32_t mCurrentBuffer;
};

#endif // EVENTLOOP_H
