#include "filesystem.h"

#include <OGRE/OgreString.h>
#include <OGRE/OgreResourceGroupManager.h>

#include <framework/logger/logger.h>

#include <sys/stat.h>

const std::string FileSystem::LOC_TYPE_FS  = "FileSystem";
const std::string FileSystem::LOC_TYPE_ZIP = "Zip";

const std::string FileSystem::RG_DEFAULT =
        Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;

bool FileSystem::isDirectory(const std::string &path)
{
    struct stat st;
    if (stat(path.c_str(), &st) != 0)
    {
        return false;
    }

    return S_ISDIR(st.st_mode);
}

bool FileSystem::isRegularFile(const std::string &path)
{
    struct stat st;
    if (stat(path.c_str(), &st) != 0)
    {
        return false;
    }

    return S_ISREG(st.st_mode);
}

bool FileSystem::isReservedDir(const char *path)
{
    return (path[0] == '.' && (path[1] == 0 || (path[1] == '.' && path[2] == 0)));
}

bool FileSystem::isAbsolutePath(const char *path)
{
#if VE_PLATFORM == VE_PLATFORM_WIN32
    if (!isalpha(uchar(path[0])) && path[1] == ':')
        return true;
#endif
    return (path[0] == '/' || path[0] == '\\');    
}

std::string FileSystem::concatenatePath(const std::string &dirname,
                                        const std::string &basename)
{
    if (dirname.empty() || isAbsolutePath(basename.c_str()))
        return basename;
    else
        return dirname + '/' + basename;
}

std::string FileSystem::dirname(const std::string &path)
{
    std::string dname, bname;
    splitFileName(path, dname, bname);

    return dname;
}

std::string FileSystem::basename(const std::string &path)
{
    std::string dname, bname;
    splitFileName(path, dname, bname);

    return bname;
}

void FileSystem::splitFileName(const std::string &path,
                               std::string &dirname,
                               std::string &basename)
{
    return Ogre::StringUtil::splitFilename(path, basename, dirname);
}

// -------------------------

const std::string &FileSystem::getCwd()
{
    static std::string cwd = "";
    if (cwd.empty())
    {
#if VE_PLATFORM == VE_PLATFORM_WIN32
            char *scwd = _getcwd(NULL, 0);
#else
            char *scwd = getcwd(NULL, 0);
#endif
            if (scwd)
            {
                cwd = scwd;
                free(scwd);
            }
#ifndef VE_NO_EXCEPTIONS
            else
            {
                throw std::bad_alloc();
            }
#endif
    }
    return cwd;
}

std::string FileSystem::getFullpathFromCwd(const std::string &pathInCwd)
{
    return concatenatePath(getCwd(), pathInCwd);
}

// -------------------------

const std::string &FileSystem::getSharedLibExtensionStr()
{
    const static std::string SharedLibExt =
#if VE_PLATFORM == VE_PLATFORM_WIN32
    "dll";
#elif VE_PLATFORM == VE_PLATFORM_APPLE
    "dylib";
#else // VE_PLATFORM_LINUX and other unix
    "so";
#endif

    return SharedLibExt;
}

// -------------------------

void FileSystem::removeResourceLocation(const std::string &pathAndName,
                                        const std::string &resGroup)
{
    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();

    resourceMgr->removeResourceLocation(pathAndName, resGroup);
}

void FileSystem::addResourceLocation(const std::string &pathAndName,
                                     const std::string &locType,
                                     const std::string &resGroup,
                                     bool recursive)
{

    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();
    resourceMgr->addResourceLocation(pathAndName, locType, resGroup, recursive);
}

bool FileSystem::hasResourceLocation(const std::string &pathAndName,
                                     const std::string &resGroup)
{
    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();

    try
    {
        const Ogre::ResourceGroupManager::LocationList &list =
                resourceMgr->getResourceLocationList(resGroup);

        for (Ogre::ResourceGroupManager::LocationList::const_iterator it = list.begin();
             it != list.end(); it++)
        {
            Ogre::ResourceGroupManager::ResourceLocation *loc = *it;

            if (loc->archive->getName() == pathAndName)
            {
                return true;
            }
        }
    }
    catch (Ogre::Exception &e)
    {
        if (e.getNumber() == Ogre::Exception::ERR_ITEM_NOT_FOUND)
        {
            veAssert(false, "ResourceGroupManager has not ResourceGroup named \"" +
                     resGroup + "\" (" +
                     VE_SCOPE_NAME(FileSystem::hasResourceLocation(const std::string &pathAndName,
                                                                   const std::string &resGroup)).toString()
                     + ")!",
                     Logger::LOG_COMPONENT_FILESYSTEM);
        }

#ifndef VE_NO_EXCEPTIONS
        throw;
#endif

    }

    return false;
}

// -------------------------

void FileSystem::initialiseResourceGroup(const std::string &name)
{
    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();
    resourceMgr->initialiseResourceGroup(name);
}

void FileSystem::clearResourceGroup(const std::string &name)
{
    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();
    resourceMgr->clearResourceGroup(name);
}

Ogre::DataStreamPtr FileSystem::openResource(const std::string &name,
                                             const std::string &groupName,
                                             bool searchGroupsIfNotFound,
                                             Ogre::Resource *resourceBeingLoaded)
{
    auto *resourceMgr = Ogre::ResourceGroupManager::getSingletonPtr();
    return resourceMgr->openResource(name, groupName, searchGroupsIfNotFound,
                                     resourceBeingLoaded);
}
