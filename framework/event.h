#ifndef EVENT_H
#define EVENT_H

#include <global.h>
#include <framework/object.h>

#include <framework/bitmath.h>

class EventLoop;

/** Base type of communication process in event loop.
 ** It allways has a reciever (object, which will receive event),
 ** life time (how many times event loop tries to send event to
 ** the receiver) and priority. */
/*
enum EventGroupMask {
    EG_User      = BIT(0),
    EG_Renderer  = BIT(1),
    EG_Physics   = BIT(2),
    EG_System    = BIT(3),

    EG_All = (EG_User | EG_Renderer |
              EG_Physics | EG_System)
};*/

class Object;

class Event
{
    friend class EventLoop;

public:
    enum {
        MaxPriority = 0,
        NorPriority = 255,
        MinPriority = 65535
    };

    enum EventType
    {
        ET_Unknown  = BIT(0),
        ET_Keyboard = BIT(1),
        ET_Mouse    = BIT(2),
        ET_Renderer = BIT(3),
        ET_Physics  = BIT(4),
        ET_System   = BIT(5),

        ET_All = ET_Keyboard | ET_Mouse |
                 ET_Mouse    | ET_Renderer |
                 ET_Physics  | ET_System
    };

    static const std::string &eventTypeToString(EventType type)
    {
        static const std::string Unknown  = "unknown";
        static const std::string Keyboard = "keyboard";
        static const std::string Mouse    = "mouse";
        static const std::string Renderer = "renderer";
        static const std::string Physics  = "physics";
        static const std::string System   = "system";

        switch(type)
        {
        case ET_All:
        case ET_Unknown:  return Unknown;
        case ET_Keyboard: return Keyboard;
        case ET_Mouse:    return Mouse;
        case ET_Renderer: return Renderer;
        case ET_Physics:  return Physics;
        case ET_System:   return System;
        }

        return Unknown;
    }

    virtual Event *clone() const = 0;

    inline EventType getType() const
    {
        return mType;
    }

    inline const std::string &getEventTypeName() const
    {
        return eventTypeToString(getType());
    }

    enum {
        DefLifeTime = 1
    };

    Event() = default;

    Event(const Event &other)
        : mType(other.mType),
          mPriority(other.mPriority),
          mLifeTime(other.mLifeTime)
    { }

    Event(EventType type,
          uint32_t priority = NorPriority,
          uint32_t lifeTime = DefLifeTime);

    virtual ~Event() { }

//    virtual const std::string &getClassName() const = 0;
//    virtual size_t getClassHash() const = 0;

    inline bool operator<(const Event &other) const
    {
        return mPriority < other.mPriority;
    }

//    virtual EventGroupMask getGroup() const = 0;

    /*
    inline Object *getReceiver() const
    {
        return mReceiver;
    }
    */

    inline int getPriority() const
    {
        return mPriority;
    }

    inline uint32_t getLifeTime() const
    {
        return mLifeTime;
    }

    virtual void toStdMap(Params &output) const;

protected:
    inline bool decreaseLifeTime()
    {
        if (mLifeTime > 0) mLifeTime--;
        return (mLifeTime > 0);
    }

private:
    EventType mType;
    uint32_t mPriority;
    uint32_t mLifeTime;
    //Object   *mReceiver;
};

#endif // EVENT_H
