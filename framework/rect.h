#ifndef RECT_H
#define RECT_H

#include <framework/point.h>
#include <framework/size.h>

namespace ve
{
    template<class Type>
    class RectT
    {
    public:
        PointT<Type> bottomLeft, topRight;

        RectT()
        { }

        RectT(const PointT<Type> &bottomLeft_,
              const PointT<Type> &topRight_)
            : bottomLeft(bottomLeft_),
              topRight(topRight_)
        { }

        RectT(Type width, Type height)
            : bottomLeft(Type(0), Type(0)),
              topRight(width, height)
        { }

        bool operator ==(const RectT<Type> &other) const
        {
            return (bottomLeft == other.bottomLeft &&
                    topRight == other.topRight);
        }

        inline bool operator !=(const PointT<Type> &other) const
        {
            return !(*this == other);
        }

        inline void setBottomLeft(const PointT<Type> &bottomLeft_)
        {
            bottomLeft = bottomLeft_;
        }

        inline const PointT<Type> &getBottomLeft() const
        {
            return bottomLeft;
        }

        inline void setTopRight(const PointT<Type> &topRight_)
        {
            topRight = topRight_;
        }

        inline const PointT<Type> &getTopRight() const
        {
            return topRight;
        }

        inline void setWidth(Type width)
        {
            topRight.x = bottomLeft.x + width;
        }

        inline Type getWidth() const
        {
            return (topRight.x - bottomLeft.x);
        }

        inline void setHeight(Type height)
        {
            topRight.y = bottomLeft.y + height;
        }

        inline Type getHeight() const
        {
            return (topRight.y - bottomLeft.y);
        }

        inline void setSize(const ve::SizeT<Type> &size)
        {
            setWidth(size.width);
            setHeight(size.height);
        }

        inline ve::SizeT<Type> getSize() const
        {
            return ve::SizeT<Type>(getWidth(), getHeight());
        }

        inline Type getX() const
        {
            return bottomLeft.x;
        }

        inline Type getY() const
        {
            return bottomLeft.y;
        }
    };

    typedef RectT<int32_t> Rect;
    typedef RectT<float> RectF;
} // namespace ve

#endif // RECT_H
