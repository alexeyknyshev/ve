#ifndef POINT_H
#define POINT_H

#include <stdint.h>

namespace ve
{
    template<class Type>
    class PointT
    {
    public:
        Type x, y;

        PointT() : x(Type(0)), y(Type(0)) { }
        PointT(Type x_, Type y_) : x(x_), y(y_) { }
        PointT(const PointT &other) : x(other.x), y(other.y) { }

        inline bool operator ==(const PointT &other) const
        {
            return (x == other.x && y == other.y);
        }

        inline bool operator !=(const PointT &other) const
        {
            return !(*this == other);
        }

        inline const PointT operator +(const PointT &other) const
        {
            return PointT(x + other.x, y + other.y);
        }

        inline void operator +=(const PointT &other)
        {
            x += other.x;
            y += other.y;
        }

        inline const PointT operator -(const PointT &other) const
        {
            return PointT(x - other.x, y - other.y);
        }

        inline void operator -=(const PointT &other)
        {
            x -= other.x;
            y -= other.y;
        }
    };

    typedef PointT<int32_t> Point;
    typedef PointT<float> PointF;
}

#endif // POINT_H
