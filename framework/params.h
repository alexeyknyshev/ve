#ifndef PARAMS_H
#define PARAMS_H

#include <OGRE/OgrePrerequisites.h>

class Params : public Ogre::map<Ogre::String, Ogre::String>::type
{
public:
    inline const std::string &get(const std::string &key, const std::string &defaultValue) const
    {
        const_iterator it = find(key);
        if (it != end())
        {
            return it->second;
        }
        return defaultValue;
    }

    std::string toString() const
    {
        std::string str = "{";
        str += empty() ? "" : "\n";
        for (const auto &pair : *this)
        {
            str += "\"" + pair.first + "\"" + ":" + "\"" + pair.second + "\"" + "\n";
        }
        str += "}";
        return str;
    }
};

typedef Params::value_type ParamsValue;

class ParamsMulti : public std::multimap<std::string, std::string>
{
public:
    inline const std::string &get(const std::string &key, const std::string &defaultValue) const
    {
        const_iterator it = find(key);
        if (it != end())
        {
            return it->second;
        }
        return defaultValue;
    }

    std::string toString() const;
};

typedef ParamsMulti::value_type ParamsMultiValue;
typedef std::pair<ParamsMulti::const_iterator, ParamsMulti::const_iterator> ParamsMultiEqualRange;

#endif // PARAMS_H
