#include "conffile.h"

#include <OgreException.h>
#include <framework/logger/logger.h>

static void logConfFileCritical(const ScopeName &scope, const std::string &text)
{
    logger()->logClassMessage(VE_CLASS_NAME(ConfFile),
                              scope,
                              text,
                              Logger::LOG_LEVEL_CRITICAL,
                              Logger::LOG_COMPONENT_FILESYSTEM);
}

ConfFile::ConfFile()
    : mIsLoaded(false)
{ }

ConfFile::ConfFile(const std::string &resourceName,
                   const std::string &resourceGroup,
                   const std::string &separators,
                   bool trimWhitespace)
    : mIsLoaded(false)
{
    loadFromResourceSystem(resourceName, resourceGroup, separators, trimWhitespace);
}

bool ConfFile::loadDirect(const std::string &filename,
                          const std::string &separators, bool trimWhitespace)
{
    mPath = filename;

    try
    {
        ConfigFile::loadDirect(filename, separators, trimWhitespace);
    }
    catch (Ogre::Exception &e)
    {
        const ScopeName scope = VE_SCOPE_NAME(loadDirect(const std::string &filename,
                                                         const std::string &separators, bool trimWhitespace));

        if (e.getNumber() == Ogre::Exception::ERR_FILE_NOT_FOUND)
        {
            logConfFileCritical(scope, "Couldn't locate config file!");
            logConfFileCritical(scope, filename);
            logConfFileCritical(scope, "^ Check config filepath");
        }
        else
        {
            logConfFileCritical(scope, e.getFullDescription());
        }
#ifndef VE_NO_EXCEPTIONS
        throw;
#endif

        return false;
    }

    mIsLoaded = true;
    return true;
}

bool ConfFile::loadFromResourceSystem(const std::string &resourceName,
                                      const std::string &resourceGroup,
                                      const std::string &separators,
                                      bool trimWhitespace)
{
    mPath = resourceName;

    try
    {
        ConfigFile::loadFromResourceSystem(resourceName,
                                           resourceGroup,
                                           separators,
                                           trimWhitespace);
    }
    catch (Ogre::Exception &e)
    {
        if (e.getNumber() == Ogre::Exception::ERR_FILE_NOT_FOUND)
        {
            const ScopeName scope = VE_SCOPE_NAME(loadFromResourceSystem(const std::string &resourceName,
                                                                         const std::string &resourceGroup,
                                                                         const std::string &separators,
                                                                         bool trimWhitespace));

            logConfFileCritical(scope, "Couldn't locate config file!");
            logConfFileCritical(scope, "resource name:  " + resourceName);
            logConfFileCritical(scope, "resource group: " + resourceGroup);
        }
#ifndef VE_NO_EXCEPTIONS
        throw;
#endif

        return false;
    }

    mIsLoaded = true;
    return true;
}

const std::string &ConfFile::getPath() const
{
    return mPath;
}
