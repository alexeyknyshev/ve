#ifndef LIST_H
#define LIST_H

#include <list>

template<typename T>
class List : public std::list<T>
{
public:
    List() = default;

    List(std::initializer_list<T> init_list)
        : std::list<T>(init_list)
    { }

    void operator+=(const T &val) { std::list<T>::push_back(val); }

    bool removeAt(std::size_t index)
    {
        if (index >= std::list<T>::size())
        {
            return false;
        }

        auto it = std::list<T>::cbegin();
        std::advance(it, index);
        std::list<T>::erase(it);
        return true;
    }
};

#endif // LIST_H
