#include "updatelistener.h"

#include <game/game.h>

void UpdateListener::setAutoUpdateEnabled(bool autoUpdate)
{
    if (autoUpdate)
    {
        game()->addUpdateListener(this);
    }
    else
    {
        const bool listenerAlive = true;
        game()->removeUpdateListener(this, listenerAlive);
    }
}

UpdateListener::~UpdateListener()
{
    const bool listenerAlive = false;
    game()->removeUpdateListener(this, listenerAlive);
}

bool UpdateListener::isAutoUpdateEnabled() const
{
    return game()->isUpdateListenerAdded(this);
}
