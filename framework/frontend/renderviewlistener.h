#ifndef RENDERVIEWLISTENER_H
#define RENDERVIEWLISTENER_H

#include <framework/object.h>

class RenderView;

class RenderViewListener : public Object
{
public:
    VE_DECLARE_TYPE_INFO(RenderViewListener)

    RenderViewListener(const std::string &name);

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_RENDERER);

        VE_UNUSED(event);
        return false;
    }

    virtual bool viewClosing(RenderView *view) = 0;
    virtual void viewClosed(RenderView *view) = 0;
    virtual void viewFocusChange(RenderView *view) = 0;
    virtual void viewResized(RenderView *view) = 0;

    const std::string &getName() const override
    {
        return mName;
    }

private:
    const std::string mName;
};

typedef std::set<RenderViewListener *> RenderViewListenerSet;

#endif // RENDERVIEWLISTENER_H
