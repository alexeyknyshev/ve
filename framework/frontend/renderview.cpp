#include "renderview.h"

#include <global.h>
#include <game/game.h>
#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <framework/frontend/renderviewoptions.h>
#include <framework/frontend/renderviewbackend.h>
#include <framework/controller/inputcontroller.h>

#include <OGRE/OgreRenderWindow.h>

RenderView::RenderView(const std::string &name)
    : mInputContoller(nullptr)
{
    /* First of all set name, because we need to create
     * RenderWindow with apropriate name. */
    mOptions.name = name;

    if (!hasRenderWindow())
    {
        logAndAssert(VE_SCOPE_NAME(RenderView(const std::string &name)),
                     "Couldn't locate RenderWindow named \"" + name + "\"!",
                     Logger::LOG_COMPONENT_RENDERER);
    }
}

RenderView::RenderView(RenderViewOptions &opt)
    : mInputContoller(nullptr)
{
    /* First of all set name, because we need to create
     * RenderWindow with apropriate name. */
    mOptions.name = opt.name;

    /* Check if we already have RenderWindow with
     * the same name. If we don't then create one. */
    Ogre::RenderWindow *win = nullptr;
    if (!hasRenderWindow())
    {
        win = renderer()->createRenderWindow(opt);
    }
    else
    {
        const bool checkWindowPtr = false;
        win = getRenderWindow(checkWindowPtr);
    }

    /* Now we are sure that we have RenderWindow instance
     * and can setup it. */
    setOptions(opt, win);

    Ogre::WindowEventUtilities::addWindowEventListener(win, this);
}

RenderView::~RenderView()
{
    removeAllListeners();

    delete mInputContoller;
    mInputContoller = nullptr;

    if (hasRenderWindow())
    {
        const bool checkWindowPtr = false;
        Ogre::RenderWindow *win = getRenderWindow(checkWindowPtr);
        Ogre::WindowEventUtilities::removeWindowEventListener(win, this);
        destroy();
    }
}

void RenderView::update(float deltaTime, float realDelta)
{
    VE_UNUSED(deltaTime);
    renderer()->getRenderViewBackend()->update(this, realDelta);
}

bool RenderView::isValid() const
{
    return hasRenderWindow();
}

void RenderView::setInputController(InputController *contoller)
{   
    if (!contoller)
    {
        logAndSoftAssert(VE_SCOPE_NAME(setInputController(InputController *controller)),
                         "Invalid (nullptr) controller!",
                         Logger::LOG_COMPONENT_USERINPUT,
                         Logger::LOG_LEVEL_CRITICAL);

        return;
    }

    if (mInputContoller != contoller)
    {
        mInputContoller = contoller;
        contoller->init(*this, isFullScreen());

        log(VE_CLASS_NAME(InputController).toString() +
            " (" + contoller->getName() + ") has been registred "
            "and initialized!",
            Logger::LOG_LEVEL_DEBUG,
            Logger::LOG_COMPONENT_USERINPUT);
    }
}

InputController *RenderView::getInputContoller(bool checkPoiner) const
{
    if (checkPoiner)
    {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getInputContoller(bool checkPoiner) const),
                                InputController,
                                mInputContoller,
                                Logger::LOG_COMPONENT_USERINPUT);
    }
    return mInputContoller;
}

const RenderViewBackend *RenderView::getRenderViewBackend()
{
    return renderer()->getRenderViewBackend();
}

void RenderView::setSize(const ve::SizeU &size)
{
    Ogre::RenderWindow *win = getRenderWindow();
    win->resize(size.width, size.height);
    windowResized(win);
    log("resized: " + StringConverter::toString(size.width) + "x" +
        StringConverter::toString(size.height),
        Logger::LOG_LEVEL_DEBUG,
        Logger::LOG_COMPONENT_RENDERER);
}

ve::SizeU RenderView::getSize() const
{
    uint32_t w, h, c;
    int l, t;
    getRenderWindow()->getMetrics(w, h, c, l ,t);
    return ve::SizeU(w, h);
}

unsigned long RenderView::getWindowHandler()
{
    return renderer()->getRenderViewBackend()->getRenderWindowHandler(this);
}

void RenderView::setFullscreen(bool fullScreen, ve::SizeU size)
{
    bool wasFullScreen = isFullScreen();
    getRenderWindow()->setFullscreen(fullScreen,
                                     size.width,
                                     size.height);

    // If screen state changed reinitialize inputContoller
    if ((wasFullScreen != fullScreen) && mInputContoller)
    {
        mInputContoller->init(*this, true);
    }

    std::string text;
    text += fullScreen ? "is in fullscreen mode now;" :
                         "is in windowed mode now;";
    text += " size: " + StringConverter::toString(size.width) + "x" +
                        StringConverter::toString(size.height);

    log(text, Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_RENDERER);
}

bool RenderView::isFullScreen() const
{
    return getRenderWindow()->isFullScreen();
}

void RenderView::setAutoDestroy(bool autodestroy)
{
    if (autodestroy)
    {
        log("controlled RenderWindow will be auto destroyed",
            Logger::LOG_LEVEL_DEBUG,
            Logger::LOG_COMPONENT_RENDERER);
    }
    else
    {
        log("controlled RenderWindow will not be longer auto destroyed",
            Logger::LOG_LEVEL_DEBUG,
            Logger::LOG_COMPONENT_RENDERER);
    }

    mOptions.autoDestroy = autodestroy;
}

bool RenderView::addListener(RenderViewListener* listener)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(addListener(RenderViewListener* listener)),
                                RenderViewListener,
                                listener,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        if (mOptions.listeners.insert(listener).second)
        {
            log(VE_CLASS_NAME(RenderViewListener).toString() +
                " (\"" + listener->getName() +
                "\") registred.",
                Logger::LOG_LEVEL_INFO,
                Logger::LOG_COMPONENT_RENDERER);
        }
        else
        {
            log(VE_CLASS_NAME(RenderViewListener).toString() +
                " (\"" + listener->getName() +
                "\") already registred and nothing has been changed.",
                Logger::LOG_LEVEL_INFO,
                Logger::LOG_COMPONENT_RENDERER);
        }
        return true;
    }
    return false;
}

bool RenderView::removeListener(RenderViewListener *listener)
{
    auto it = mOptions.listeners.find(listener);
    if (it != mOptions.listeners.end())
    {
        log(VE_CLASS_NAME(RenderViewListener).toString() +
            " (\"" + listener->getName() +
            "\") destroyed.",
            Logger::LOG_LEVEL_INFO,
            Logger::LOG_COMPONENT_RENDERER);

        delete listener;
        mOptions.listeners.erase(it);
        return true;
    }

    return false;
}

void RenderView::removeAllListeners()
{
    for (RenderViewListener *listener : mOptions.listeners)
    {
        log(VE_CLASS_NAME(RenderViewListener).toString() +
            " (\"" + listener->getName() +
            "\") destroyed.",
            Logger::LOG_LEVEL_INFO,
            Logger::LOG_COMPONENT_RENDERER);

        delete listener;
    }

    mOptions.listeners.clear();
}


void RenderView::setOptions(const RenderViewOptions &opt)
{
    setOptions(opt, getRenderWindow());
}

void RenderView::setOptions(const RenderViewOptions &opt, Ogre::RenderWindow *win)
{
    if (!opt.name.empty() && win->getName() != opt.name)
    {
        log("Parameter name and RenderView name does not match (RenderView::setOptions). "
            "Did you mean \'" + getName() + "\'?",
            Logger::LOG_LEVEL_WARNING,
            Logger::LOG_COMPONENT_RENDERER);
    }

    if (opt.fullScreen == win->isFullScreen())
    {
        setSize(opt.size);
    }
    else
    {
        setFullscreen(opt.fullScreen, opt.size);
    }

    for (RenderViewListener *listener : opt.listeners)
    {
        addListener(listener);
    }

    mOptions.window = opt.window;
    mOptions.context = opt.context;
    mOptions._mainContext = opt._mainContext;

    setAutoDestroy(opt.autoDestroy);

    windowResized(getRenderWindow(true));
}

void RenderView::setViewports(const ViewportOptionsMap &viewports)
{
    mViewportOptions = viewports;

    Ogre::RenderWindow *win = getRenderWindow();
    win->removeAllViewports();

    int viewportIndex = 0;

    auto end = viewports.end();
    for (auto it = viewports.begin(); it != end; ++it, ++viewportIndex)
    {
        const int zOrder = it->first;
        const ViewportOptions &vOpt = it->second;

        win->addViewport(vOpt.camera.getCameraImpl().get(),
                         zOrder,
                         vOpt.position.left, vOpt.position.top,
                         vOpt.position.width, vOpt.position.height);
    }

    windowResized(win);
}

Ogre::Viewport *RenderView::setViewport(const ViewportOptions &viewport)
{
    const int zOrder = 0;

    mViewportOptions.clear();
    mViewportOptions.insert({zOrder, viewport});

    Ogre::RenderWindow *win = getRenderWindow();
    win->removeAllViewports();

    Ogre::Viewport *vp = win->addViewport(viewport.camera.getCameraImpl().get(), zOrder,
                                          viewport.position.left, viewport.position.top,
                                          viewport.position.width, viewport.position.height);

    windowResized(win);

    return vp;
}

//----------------------------------------------------------------

void RenderView::setTitle(const std::string &title)
{
    renderer()->getRenderViewBackend()->setWindowTitle(getOptions(), title);
}

std::string RenderView::getTitle() const
{
    return renderer()->getRenderViewBackend()->getWindowTitle(getOptions());
}

//----------------------------------------------------------------

bool RenderView::destroy()
{
    if (mOptions.autoDestroy)
    {
        renderer()->destroyRenderWindow(mOptions);

        log("Controlled RenderWindow destroyed.",
            Logger::LOG_LEVEL_INFO,
            Logger::LOG_COMPONENT_RENDERER);
        return true;
    }
    return false;
}

//----------------------------------------------------------------

ViewportList RenderView::getViewports() const
{
    Ogre::RenderWindow *win = getRenderWindow();

    ViewportList list;
    for (unsigned short i = 0; i < win->getNumViewports(); ++i)
    {
        list.push_back(win->getViewport(i));
    }
    return list;
}

uint16_t RenderView::getViewportsCount() const
{
    return getRenderWindow()->getNumViewports();
}

BaseCamera RenderView::getCamera(int viewportZOrder) const
{
    const Ogre::Viewport *viewport = getViewportByZOrder(viewportZOrder);
    if (viewport)
    {
        Ogre::Camera *camera = viewport->getCamera();

        return BaseCamera(camera);
    }
    return BaseCamera();
}

Ogre::Viewport *RenderView::getViewportByZOrder(int zOrder) const
{    
#if OGRE_VERSION_MAJOR >= 1 && OGRE_VERSION_MINOR >= 8 /* Ogre 1.8.0+ */
    Ogre::Viewport *viewport = nullptr;
    try
    {
        viewport = getRenderWindow()->getViewportByZOrder(zOrder);
    }
    catch (Ogre::Exception &e)
    {
        if (e.getNumber() == Ogre::Exception::ERR_ITEM_NOT_FOUND)
        {
            logAndAssert(VE_SCOPE_NAME(getViewportByZOrder(int zOrder)),
                         "RenderView has not Viewport with zOrder " +
                         StringConverter::toString(zOrder) + "!",
                         Logger::LOG_COMPONENT_RENDERER);
        }
#ifndef VE_NO_EXCEPTIONS
        throw;
#endif
    }

    return viewport;
#else
    ViewportList viewports = getViewports();
    for (Viewport *vp : viewports)
    {
        if (vp->getZOrder() == zOrder)
        {
            return vp;
        }
    }
    return nullptr;
#endif
}

Ogre::Viewport *RenderView::getViewportByIndex(uint16_t index) const
{
    return getRenderWindow()->getViewport(index);
}

//----------------------------------------------------------------

Ogre::RenderWindow *RenderView::getRenderWindow(bool checkPointer) const
{
    Ogre::RenderWindow *win = renderer()->getRenderWindow(getName());
    if (checkPointer) {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getRenderWindow(bool checkPointer) const),
                                RenderWindow,
                                win,
                                Logger::LOG_COMPONENT_RENDERER);
    }
    return win;
}

bool RenderView::hasRenderWindow() const
{
    return renderer()->hasRenderWindow(getName());
}

/** -------------------------- **/

void RenderView::windowFocusChange(Ogre::RenderWindow *window)
{
    if (window->getName() != getName())
    {
        return;
    }

    for (RenderViewListener *listener : mOptions.listeners)
    {
        listener->viewFocusChange(this);
    }
}

void RenderView::windowResized(Ogre::RenderWindow *window)
{
    if (window->getName() != getName())
    {
        return;
    }

    /* Get registred cameras */
    std::list<Ogre::Camera *> cameraList;

    const auto end = mViewportOptions.end();
    for (auto it = mViewportOptions.begin(); it != end; ++it)
    {
        const ViewportOptions &vOpt = it->second;
        auto ogreCamera = vOpt.camera.getCameraImpl();
        if (ogreCamera)
        {
            cameraList.push_back(ogreCamera.get());
        }
    }

    /* Go through window viewports and update their dimensions */
    for (unsigned short i = 0; i < window->getNumViewports(); ++i)
    {
        Ogre::Viewport *viewport = window->getViewport(i);
        Ogre::Camera *camera = viewport->getCamera();

        const auto end = cameraList.end();
        const auto it = std::find(cameraList.begin(), end, camera);

        if (it != end)
        {
            const float aspect =
                    (float)viewport->getActualWidth() /
                    (float)viewport->getActualHeight();

            camera->setAspectRatio(aspect);
        }
    }
}

bool RenderView::windowClosing(Ogre::RenderWindow *window)
{
    if (window->getName() != getName())
    {
        return false;
    }

    bool isClosing = false;

    for (RenderViewListener *listener : mOptions.listeners)
    {
        isClosing |= listener->viewClosing(this);
    }

    if (isClosing)
    {
        mInputContoller->setAutoUpdateEnabled(false);
        return true;
    }
    return false;
}

void RenderView::windowClosed(Ogre::RenderWindow *window)
{
    if (window->getName() != getName())
    {
        return;
    }

    for (RenderViewListener *listener : mOptions.listeners)
    {
        listener->viewClosed(this);
    }
}

bool RenderView::close()
{
    return windowClosing(renderer()->getRenderWindow(getName()));
}

void RenderView::closed()
{
    return windowClosed(renderer()->getRenderWindow(getName()));
}
