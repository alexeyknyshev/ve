#ifndef RENDERVIEWBACKEND_H
#define RENDERVIEWBACKEND_H

#include <string>

class RenderWindowOptions;
class RenderView;
class Params;

namespace ve
{
    template<typename Type> class SizeT;
    typedef SizeT<uint32_t> SizeU;
}

namespace Ogre
{
    class RenderWindow;
}

class RenderViewBackend
{
public:
    virtual ~RenderViewBackend()
    { }

    virtual Ogre::RenderWindow *createRenderWindow(RenderWindowOptions &opt) const = 0;
    virtual void destroyRenderWindow(const RenderWindowOptions &opt) const = 0;

    virtual void setWindowTitle(const RenderWindowOptions &opt, const std::string &title) const = 0;
    virtual std::string getWindowTitle(const RenderWindowOptions &opt) const = 0;

    virtual const std::string &getBackendName() const = 0;

    virtual bool getRenderViewSizeHint(ve::SizeU &sizeHint);

    virtual unsigned long getRenderWindowHandler(const RenderView *view) const = 0;

    virtual void update(RenderView *view, float realDelta) const = 0;

protected:
    static Ogre::RenderWindow *_createRenderWindow(const std::string &name, unsigned int width, unsigned int height,
                                                   bool fullScreen, const Params *miscParams = 0);

    static Ogre::RenderWindow *getViewRenderWindow(const RenderView *view);
    static Ogre::RenderWindow *getRenderWindow(const RenderWindowOptions &opt);
};

#endif // RENDERVIEWBACKEND_H
