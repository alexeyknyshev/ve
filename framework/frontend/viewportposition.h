#ifndef VIEWPORTOPTIONS_H
#define VIEWPORTOPTIONS_H

#include <global.h>
#include <renderer/basecamera.h>

class ViewportPosition
{
public:
    ViewportPosition(float left_ = 0.0f,
                     float top_ = 0.0f,
                     float width_ = 1.0f,
                     float height_ = 1.0f)
        : left(left_),
          top(top_),
          width(width_),
          height(height_)
    { }
    
    float left;
    float top;
    float width;
    float height;
    
    bool operator==(const ViewportPosition &o) const
    {
        return (left == o.left && top == o.top &&
                width == o.width && height == o.height);
    }
    
    const static ViewportPosition FULLSCREEN;
};

class ViewportOptions
{
public:
    ViewportOptions(const BaseCamera &cam = BaseCamera(),
                    const ViewportPosition &pos = ViewportPosition::FULLSCREEN)
        : camera(cam),
          position(pos)
    { }

    BaseCamera camera;
    ViewportPosition position;
};

/** key : zOrder;  value : vOpt */
typedef std::map<int, ViewportOptions> ViewportOptionsMap;

#endif // VIEWPORTOPTIONS_H
