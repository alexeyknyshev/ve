#include "renderviewbackend.h"

#include <renderer/renderer.h>

#include <framework/frontend/renderview.h>

#include <OGRE/OgreRenderWindow.h>

bool RenderViewBackend::getRenderViewSizeHint(ve::SizeU &sizeHint)
{
    VE_UNUSED(sizeHint);
    return false;
}

Ogre::RenderWindow *RenderViewBackend::_createRenderWindow(const std::string &name,
                                                           unsigned int width,
                                                           unsigned int height,
                                                           bool fullScreen,
                                                           const Params *miscParams)
{
    return renderer()->createRenderWindow(name, width, height, fullScreen, miscParams);
}

Ogre::RenderWindow *RenderViewBackend::getViewRenderWindow(const RenderView *view)
{
    return view->getRenderWindow(true);
}

Ogre::RenderWindow *RenderViewBackend::getRenderWindow(const RenderWindowOptions &opt)
{
    return renderer()->getRenderWindow(opt.name);
}
