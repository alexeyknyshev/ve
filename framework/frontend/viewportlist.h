#ifndef VIEWPORTLIST_H
#define VIEWPORTLIST_H

#include <list>

namespace Ogre
{
    class Viewport;
}

typedef std::list<Ogre::Viewport *> ViewportList;

#endif // VIEWPORTLIST_H
