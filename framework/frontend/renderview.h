#ifndef RENDERVIEW_H
#define RENDERVIEW_H

#include <global.h>

#include <framework/object.h>
#include <framework/frontend/renderviewoptions.h>
#include <framework/frontend/viewportlist.h>
#include <framework/frontend/viewportposition.h>
#include <framework/frontend/renderviewlistener.h>
#include <framework/updatable.h>

#include <OGRE/OgreWindowEventUtilities.h>

class RenderViewOptions;
class RenderViewBackend;
class InputController;
class Game;

class RenderView : public Object,
                   public Ogre::WindowEventListener,
                   public Updatable
{
    friend class InputController;
    friend class RenderViewBackend;

public:
    VE_DECLARE_TYPE_INFO(RenderView)

    RenderView(const std::string &name);
    RenderView(RenderViewOptions &opt);
    virtual ~RenderView();

    void update(float deltaTime, float realDelta) override;

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_RENDERER);
        VE_UNUSED(event);
        return false;
    }

    bool isValid() const;

    /** @remarks takes ownership of @controller */
    void setInputController(InputController *contoller);

    InputController *getInputContoller(bool checkPoiner = true) const;

    static const RenderViewBackend *getRenderViewBackend();

    void setOptions(const RenderViewOptions &opt);

    /** Setups split screen or normal screen (if cameraMap has one cameara)
      * @example
      *     @see tests/renderviewtest.cpp
      */
    void setViewports(const ViewportOptionsMap &viewports);

    Ogre::Viewport *setViewport(const ViewportOptions &viewport);

    const RenderViewOptions &getOptions() const
    {
        return mOptions;
    }

    void setTitle(const std::string &title);
    std::string getTitle() const;

    void setSize(const ve::SizeU &size);
    ve::SizeU getSize() const;

    unsigned long getWindowHandler();
    
    /** RenderView fullsreen mode on/off
      * @size new size of RenderView (optional)
      */
    void setFullscreen(bool fullScreen, ve::SizeU size = ve::SizeU());

    bool isFullScreen() const;

    void setAutoDestroy(bool autodestroy);

    inline bool isAutoDestroy() const
    {
        return mOptions.autoDestroy;
    }
    
    /** Completely frees internal resources (deletes mWindow)
     ** if has permissions
     **/
    bool destroy();

    bool addListener(RenderViewListener *listener);
    bool removeListener(RenderViewListener *listener);
    void removeAllListeners();

    /** @returns ViewportList of RenderView
      * @remarks Only for manipulating viewports
      * @throws if RenderView isValid == false (mWindow == nullptr)
      * Don't delete or detach them
      */
    ViewportList getViewports() const;
    uint16_t getViewportsCount() const;

    BaseCamera getCamera(int viewportZOrder) const;

    Ogre::Viewport *getViewportByZOrder(int zOrder) const;
    Ogre::Viewport *getViewportByIndex(uint16_t index) const;
    
    inline const std::string &getName() const
    {
        return mOptions.name;
    }

    /** -------------------- **/

    void windowFocusChange(Ogre::RenderWindow *window) override;
    void windowResized(Ogre::RenderWindow *window) override;
    bool windowClosing(Ogre::RenderWindow *window) override;
    void windowClosed(Ogre::RenderWindow *window) override;

    bool close();
    void closed();
    
protected:
    /** @remarks safe to call (VE_CLASS_CHECK_NULL_PTR inside) */
    Ogre::RenderWindow *getRenderWindow(bool checkPointer = true) const;

    void setOptions(const RenderViewOptions &opt, Ogre::RenderWindow *win);

    bool hasRenderWindow() const;

    ViewportOptionsMap mViewportOptions;
    RenderViewOptions mOptions;

    InputController *mInputContoller;
    RenderViewBackend *mBackend;
};

#endif // RENDERVIEW_H
