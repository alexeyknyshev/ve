#ifndef SDLRENDERVIEWBACKEND_H
#define SDLRENDERVIEWBACKEND_H

#include <framework/frontend/renderviewbackend.h>

class SDLRenderViewBackend : public RenderViewBackend
{
public:
    SDLRenderViewBackend();
    ~SDLRenderViewBackend();

    Ogre::RenderWindow *createRenderWindow(RenderWindowOptions &opt) const override;
    void destroyRenderWindow(const RenderWindowOptions &opt) const override;

    void setWindowTitle(const RenderWindowOptions &opt, const std::string &title) const override;
    std::string getWindowTitle(const RenderWindowOptions &opt) const override;

    bool getRenderViewSizeHint(ve::SizeU &sizeHint) override;

    unsigned long getRenderWindowHandler(const RenderView *view) const override;

    const std::string &getBackendName() const override
    {
        const static std::string ImplName = "SDL";
        return ImplName;
    }

    void update(RenderView *view, float) const override;
};

#endif // SDLRENDERVIEWBACKEND_H
