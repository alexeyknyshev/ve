#include "sdlrenderviewbackend.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

#include <OGRE/OgreRenderWindow.h>
#include <OGRE/OgreStringConverter.h>
#include <OGRE/OgrePlugin.h>

#include <framework/frontend/renderviewoptions.h>
#include <framework/frontend/renderview.h>

#include <game/game.h>

#include <renderer/renderer.h>

#include <GL/gl.h>

inline static SDL_Window *toSDLWindow(void *window)
{
    return static_cast<SDL_Window *>(window);
}


SDLRenderViewBackend::SDLRenderViewBackend()
{
    SDL_Init(SDL_INIT_EVERYTHING);
}

SDLRenderViewBackend::~SDLRenderViewBackend()
{
    SDL_Quit();
}

static SDL_GLContext createSDLGLContext(const RenderWindowOptions &opt)
{
    SDL_GLContext context = SDL_GL_CreateContext(toSDLWindow(opt.window));
    if (veAssert(context, TypeName(VE_AS_STR(SDL_GLContext)), VE_SCOPE_NAME(createSDLGLContext),
                 "failed to create SDL OpenGL context", Logger::LOG_COMPONENT_RENDERER))
    {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    }
    return (SDL_GLContext)context;
}

static void deleteSDLGLContext(SDL_GLContext context)
{
    SDL_GL_DeleteContext(context);
}

Ogre::RenderWindow *SDLRenderViewBackend::createRenderWindow(RenderWindowOptions &opt) const
{
    std::string title;
    const auto titleIt = opt.parameters.find("title");
    if (titleIt != opt.parameters.end())
    {
        title = titleIt->second;
    }
    else
    {
        title = opt.name;
    }

    uint32_t sdlWindowFlags = SDL_WINDOW_SHOWN;
    if (opt.fullScreen)
    {
        sdlWindowFlags |= SDL_WINDOW_FULLSCREEN;
    }
    else
    {
        sdlWindowFlags |= SDL_WINDOW_RESIZABLE;
    }

    const auto windowBackendContextIt = opt.parameters.find(VE_OPTION_WINDOW_BACKEND_CONTEXT);
    if (windowBackendContextIt != opt.parameters.end())
    {
        if (Ogre::StringConverter::parseBool(windowBackendContextIt->second, false))
        {
            sdlWindowFlags |= SDL_WINDOW_OPENGL;
        }
    }

    SDL_Window *window = SDL_CreateWindow(title.c_str(),
                                          SDL_WINDOWPOS_CENTERED,
                                          SDL_WINDOWPOS_CENTERED,
                                          (int)opt.size.width,
                                          (int)opt.size.height,
                                          sdlWindowFlags);

    opt.window = window;
        if (!veAssert(window,
                  VE_CLASS_NAME(SDLRenderViewBackend).toString() + " in [" +
                  VE_SCOPE_NAME(createRenderWindow(RenderWindowOptions &opt)).toString() + "]: " +
                  "Cannot create SDL Window (" + VE_TO_STR(SDL_GetError()) + ")!",
                  Logger::LOG_COMPONENT_USERINPUT))
    {
        return nullptr;
    }


#if VE_PLATFORM == VE_PLATFORM_LINUX
    if (sdlWindowFlags & SDL_WINDOW_OPENGL)
    {
        opt.context = createSDLGLContext(opt);
        opt.parameters["currentGLContext"] = "Yes";
    }
    else
    {
        SDL_SysWMinfo sysInfo;
        SDL_VERSION(&sysInfo.version);

        if (SDL_GetWindowWMInfo(window, &sysInfo) <= 0)
        {
            veAssert(false,
                     VE_CLASS_NAME(SDLRenderViewBackend).toString() + " in [" +
                     VE_SCOPE_NAME(createRenderWindow(RenderWindowOptions &opt)).toString() + "]: " +
                     "Cannot get SDL Window info (" + VE_TO_STR(SDL_GetError()) + ")!",
                     Logger::LOG_COMPONENT_USERINPUT);
        }

        opt.parameters["parentWindowHandle"] =
                Ogre::StringConverter::toString((unsigned long)sysInfo.info.x11.window);
    }
#elif VE_PLATFORM == VE_PLATFORM_WIN32
    opt.parameters["externalGLContext"] = Ogre::StringConverter::toString((unsigned long)glContext);
    opt.parameters["externalWindowHandle"] =
            Ogre::StringConverter::toString((unsigned long)sysWMinfo.info.win.window);
#elif VE_PLATFORM == VE_PLATFORM_APPLE
    opt.parameters["externalWindowHandle"] = OSX_cocoa_view( syswm_info );
    opt.parameters["macAPI"] = "cocoa";
    opt.parameters["macAPICocoaUseNSView"] = "true";
#endif

    Ogre::RenderWindow *ogreWindow = _createRenderWindow(opt.name,
                                                         opt.size.width,
                                                         opt.size.height,
                                                         opt.fullScreen,
                                                         &opt.parameters);

#if VE_PLATFORM != VE_PLATFORM_APPLE
//        SDL_GL_SetSwapInterval(1);
#endif

    ogreWindow->setVisible(true);

#if VE_PLATFORM == VE_PLATFORM_APPLE
    OSX_GL_clear_current(window);
#else
//        SDL_GL_MakeCurrent(window, nullptr);
#endif

    return ogreWindow;
}

void SDLRenderViewBackend::destroyRenderWindow(const RenderWindowOptions &opt) const
{
    if (opt.context)
    {
        deleteSDLGLContext(opt.context);
    }

    if (/*renderer()->getRenderTargetNames().empty() && */opt.window)
    {
        SDL_DestroyWindow(static_cast<SDL_Window *>(opt.window));
    }
}

void SDLRenderViewBackend::setWindowTitle(const RenderWindowOptions &opt, const std::string &title) const
{
    if (opt.window)
    {
        SDL_SetWindowTitle(toSDLWindow(opt.window), title.c_str());
    }
}

std::string SDLRenderViewBackend::getWindowTitle(const RenderWindowOptions &opt) const
{
    if (opt.window)
    {
        return SDL_GetWindowTitle(toSDLWindow(opt.window));
    }

    return Ogre::StringUtil::BLANK;
}

bool SDLRenderViewBackend::getRenderViewSizeHint(ve::SizeU &sizeHint)
{
    if (SDL_GetNumVideoDisplays() > 0)
    {
        SDL_DisplayMode displayMode;
        if (SDL_GetDesktopDisplayMode(0, &displayMode) == 0)
        {
            sizeHint.width  = displayMode.w;
            sizeHint.height = displayMode.h;
            return true;
        }
    }
    return false;
}

unsigned long SDLRenderViewBackend::getRenderWindowHandler(const RenderView *view) const
{
    Ogre::RenderWindow *win = getViewRenderWindow(view);
    if (!win)
    {
        return 0;
    }

    SDL_SysWMinfo sysInfo;
    SDL_VERSION(&sysInfo.version);

    if (SDL_GetWindowWMInfo(static_cast<SDL_Window *>(view->getOptions().window), &sysInfo) <= 0)
    {
        veLogAndAssert(false,
                       VE_CLASS_NAME(SDLRenderViewBackend),
                       VE_SCOPE_NAME(getRenderWindowHandler(const RenderView *view)),
                       Ogre::StringUtil::BLANK,
                       "Cannot get SDL Window info (" + VE_TO_STR(SDL_GetError()) + ")!",
                       Logger::LOG_COMPONENT_USERINPUT);
    }
    return sysInfo.info.x11.window;
}

void SDLRenderViewBackend::update(RenderView *view, float) const
{
    if (view && view->getOptions().window)
    {
        SDL_GL_SwapWindow(toSDLWindow(view->getOptions().window));
    }
}
