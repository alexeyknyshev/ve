#ifndef RENDERVIEWOPTIONS_H
#define RENDERVIEWOPTIONS_H

#include <global.h>
#include <renderer/renderwindowoptions.h>
#include <framework/frontend/renderviewlistener.h>

/** RenderViewOptions extended RenderWindowOptions.
 ** Adds list of listeners and auto destruction option */ 

class RenderViewOptions : public RenderWindowOptions
{
    friend class RenderView;

public:
    RenderViewOptions();

    bool autoDestroy;
    RenderViewListenerSet listeners;
};

#endif // RENDERVIEWOPTIONS_H
