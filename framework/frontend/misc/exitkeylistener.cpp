#include "exitkeylistener.h"

#include <game/game.h>

ExitKeyListener::ExitKeyListener(const std::string &name, ve::KeyData::KeyButton exitKey)
    : DefaultInputListener(name),
      mExitKey(exitKey)
{ }

bool ExitKeyListener::keyPressed(const KeyboardEvent &event)
{
    if (event.data.key == mExitKey)
    {
        Game::getSingletonPtr()->exit();
        return true;
    }
    return false;
}
