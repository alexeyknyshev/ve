#include "renderviewcloselistener.h"

#include <game/game.h>

void RenderViewCloseListener::viewClosed(RenderView *view)
{
    VE_UNUSED(view);
    Game::getSingletonPtr()->exit();
}
