#ifndef RENDERVIEWCLOSELISTENER_H
#define RENDERVIEWCLOSELISTENER_H

#include <framework/frontend/renderviewlistener.h>

/** Stops game loop when RenderView closes */
class RenderViewCloseListener : public RenderViewListener
{
public:
    VE_DECLARE_TYPE_INFO(RenderViewCloseListener)

    RenderViewCloseListener(const std::string &name)
        : RenderViewListener(name)
    { }

    void viewClosed(RenderView *view) override;
    bool viewClosing(RenderView *view) override { VE_UNUSED(view); return true; }
    void viewFocusChange(RenderView *view) override { VE_UNUSED(view); }
    void viewResized(RenderView *view) override { VE_UNUSED(view); }
};

#endif // RENDERVIEWCLOSELISTENER_H
