#ifndef EXITKEYLISTENER_H
#define EXITKEYLISTENER_H

#include <framework/controller/defaultinputlistener.h>

class ExitKeyListener : public DefaultInputListener
{
public:
    ExitKeyListener(const std::string &name, ve::KeyData::KeyButton exitKey);

    bool keyPressed(const KeyboardEvent &event) override;

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_EVENTLOOP);
        VE_UNUSED(event);
        return false;
    }

protected:
    void registredAsInputListener(bool registred, InputController *controller) override
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

private:
    const ve::KeyData::KeyButton mExitKey;
};

#endif // EXITKEYLISTENER_H
