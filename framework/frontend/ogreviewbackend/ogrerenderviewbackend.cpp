#include "ogrerenderviewbackend.h"

#include <framework/frontend/renderview.h>

#include <renderer/renderer.h>

#include <OGRE/OgreRenderWindow.h>

#if VE_PLATFORM == VE_PLATFORM_LINUX
#include <X11/Xlib.h>
#endif

OGRERenderViewBackend::OGRERenderViewBackend()
{ }

OGRERenderViewBackend::~OGRERenderViewBackend()
{ }

Ogre::RenderWindow *OGRERenderViewBackend::createRenderWindow(RenderWindowOptions &opt) const
{
    return _createRenderWindow(opt.name,
                               opt.size.width,
                               opt.size.height,
                               opt.fullScreen,
                               &opt.parameters);
}

void OGRERenderViewBackend::setWindowTitle(const RenderWindowOptions &opt, const std::string &title) const
{
    Ogre::RenderWindow *win = getRenderWindow(opt);
    if (win)
    {
#if VE_PLATFORM == VE_PLATFORM_LINUX
        Display *display = nullptr;
        win->getCustomAttribute("XDISPLAY", &display);

        Window window = 0;
        win->getCustomAttribute("WINDOW", &window);

        XStoreName(display, window, title.c_str());
#endif
    }
}

std::string OGRERenderViewBackend::getWindowTitle(const RenderWindowOptions &opt) const
{
    Ogre::RenderWindow *win = getRenderWindow(opt);
    if (win)
    {
        Display *display = nullptr;
        win->getCustomAttribute("XDISPLAY", &display);

        Window window = 0;
        win->getCustomAttribute("WINDOW", &window);

        char *winName = 0;
        if (XFetchName(display, window, &winName))
        {
            std::string result(winName);
            XFree(winName);
            return result;
        }
    }

    return VE_STR_BLANK;
}

bool OGRERenderViewBackend::getRenderViewSizeHint(ve::SizeU &sizeHint)
{
    VE_UNUSED(sizeHint);
    return false;
}

unsigned long OGRERenderViewBackend::getRenderWindowHandler(const RenderView *view) const
{
    unsigned long windowHandler = 0;
    getViewRenderWindow(view)->getCustomAttribute("WINDOW", &windowHandler);
    return windowHandler;
}
