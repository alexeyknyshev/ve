#ifndef OGRERENDERVIEWBACKEND_H
#define OGRERENDERVIEWBACKEND_H

#include <framework/frontend/renderviewbackend.h>

class OGRERenderViewBackend : public RenderViewBackend
{
public:
    OGRERenderViewBackend();
    ~OGRERenderViewBackend();

    Ogre::RenderWindow *createRenderWindow(RenderWindowOptions &opt) const override;
    void destroyRenderWindow(const RenderWindowOptions &) const override
    { }

    void setWindowTitle(const RenderWindowOptions &opt, const std::string &title) const override;
    std::string getWindowTitle(const RenderWindowOptions &opt) const override;

    bool getRenderViewSizeHint(ve::SizeU &sizeHint) override;

    unsigned long getRenderWindowHandler(const RenderView *view) const override;

    const std::string &getBackendName() const override
    {
        const static std::string ImplName = "OGRE";
        return ImplName;
    }

    void update(RenderView *, float) const override
    { }
};

#endif // OGRERENDERVIEWBACKEND_H
