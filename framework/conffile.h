#ifndef CONFFILE_H
#define CONFFILE_H

#include <OGRE/OgreConfigFile.h>
#include <global.h>

class ConfFile : public Ogre::ConfigFile
{
public:
    ConfFile();
    ConfFile(const std::string &resourceName,
             const std::string &resourceGroup,
             const std::string &separators = "=",
             bool trimWhitespace = true);

    bool loadDirect(const std::string &filename,
                    const std::string &separators = "=",
                    bool trimWhitespace = true);

    bool loadFromResourceSystem(const std::string &resourceName,
                                const std::string &resourceGroup,
                                const std::string &separators = "=",
                                bool trimWhitespace = true);

    const std::string &getPath() const;

    inline bool isLoaded() const
    {
        return mIsLoaded;
    }

    inline const Ogre::ConfigFile::SettingsBySection &getAllSettings() const
    {
        return mSettings;
    }

private:
    std::string mPath;
    bool mIsLoaded;
};

#endif // CONFFILE_H
