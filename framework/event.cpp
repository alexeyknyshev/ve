#include "event.h"

#include <framework/stringconverter.h>

Event::Event(Event::EventType type,
             uint32_t priority,
             uint32_t lifeTime)
    : mType(type),
      mPriority(priority),
      mLifeTime(lifeTime)
//     mReceiver(receiver)
{
    //VE_CHECK_NULL_PTR(Object, receiver, Logger::LOG_COMPONENT_EVENTLOOP);
}

void Event::toStdMap(Params &output) const
{
    output["type"] = getEventTypeName();
    output["priority"] = StringConverter::toString(getPriority());
    output["lifetime"] = StringConverter::toString(getLifeTime());
}
