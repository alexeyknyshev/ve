#ifndef LOGGER_H
#define LOGGER_H

#include <global.h>

#include <OGRE/OgreLogManager.h>
#include <framework/params.h>

class Logger
#ifndef SWIG
        : public Ogre::LogManager
#endif // SWIG
{

public:
    Logger();
    virtual ~Logger();

    static const int LOG_LEVEL_DEBUG;
    static const int LOG_LEVEL_INFO;
    static const int LOG_LEVEL_WARNING;
    static const int LOG_LEVEL_CRITICAL;

    /// Logging component bits
    /// Use NONE to mask out message

    static const std::uint64_t LOG_COMPONENT_NONE;

    static const std::uint64_t LOG_COMPONENT_OTHER;

    static const std::uint64_t LOG_COMPONENT_RENDERER;
    static const std::uint64_t LOG_COMPONENT_GAME;
    static const std::uint64_t LOG_COMPONENT_EVENTLOOP;
    static const std::uint64_t LOG_COMPONENT_PHYSICS;
    static const std::uint64_t LOG_COMPONENT_UI;
    static const std::uint64_t LOG_COMPONENT_USERINPUT;
    static const std::uint64_t LOG_COMPONENT_SCRIPT;
    static const std::uint64_t LOG_COMPONENT_LOGGER;
    static const std::uint64_t LOG_COMPONENT_FILESYSTEM;
    static const std::uint64_t LOG_COMPONENT_TESTING;

    static const int _LOG_COMPONENTS_COUNT;

    static const std::uint64_t LOG_COMPONENT_ALL_MASK;

    inline void setLogDetail(int level)
    {
        Ogre::LogManager::setLogDetail(getOgreLoggingLevel(level));
    }

    inline void setLogComponentMask(std::uint64_t mask)
    {
        mLogComponentEnabledMask = mask;
    }

    inline std::uint64_t getLogComponentMask() const
    {
        return mLogComponentEnabledMask;
    }

    inline void setLogComponentEnabled(std::uint64_t component, bool enabled)
    {
        if (enabled)
        {
            mLogComponentEnabledMask |= component;
        }
        else
        {
            mLogComponentEnabledMask &= ~component;
        }
    }

    inline bool isLogComponentEnabled(std::uint64_t component)
    {
        return (mLogComponentEnabledMask & component);
    }

    void scriptLog(const std::string &msg, int logLevel);

    inline void logMessage(const std::string &msg, int level,
                           std::uint64_t component = LOG_COMPONENT_NONE)
    {
        log(msg, level, component);
    }

    inline void logClassMessage(const TypeName &typeName,
                                const std::string &msg,
                                int level,
                                std::uint64_t component)
    {
        log(VE_PRINTABLE("[" + typeName.toString() + "]: " + msg), level, component);
    }

    inline void logClassMessage(const TypeName &typeName,
                                const ScopeName &scopeName,
                                const std::string &msg,
                                int level,
                                std::uint64_t component)
    {
        log(VE_PRINTABLE("[" + typeName.toString() + "::" + scopeName.toString() + "]: " + msg),
            level, component);
    }

    static Logger *getSingletonPtr();

    //------------------------------

    void setLogMessageLevelName(int level, const std::string &name);
    void setLogMessageComponentName(std::uint64_t component, const std::string &name);

    const std::string &getLogMessageLevelName(int level);
    const std::string &getLogMessageComponentName(std::uint64_t component);

protected:
    virtual void log(const std::string &message, int level, std::uint64_t component);
    static Ogre::LoggingLevel getOgreLoggingLevel(int level);
    static Ogre::LogMessageLevel getOgreLogMessageLevel(int level);

private:
    typedef std::map<int, std::string> LogLevelNames;
    typedef std::map<std::uint64_t, std::string> LogComponentNames;

    LogLevelNames mLogLevelNames;
    LogComponentNames mLogComponentNames;

    std::uint64_t mLogComponentEnabledMask;
};

Logger *logger();

#endif // LOGGER_H
