#include "logger.h"

#include <limits>
#include <framework/bitmath.h>

const int Logger::LOG_LEVEL_DEBUG    = 0;
const int Logger::LOG_LEVEL_INFO     = Ogre::LML_TRIVIAL;
const int Logger::LOG_LEVEL_WARNING  = Ogre::LML_NORMAL;
const int Logger::LOG_LEVEL_CRITICAL = Ogre::LML_CRITICAL;

const std::uint64_t Logger::LOG_COMPONENT_NONE =        0;

const std::uint64_t Logger::LOG_COMPONENT_OTHER =       BIT(0);

const std::uint64_t Logger::LOG_COMPONENT_RENDERER =    BIT(1);
const std::uint64_t Logger::LOG_COMPONENT_GAME =        BIT(2);
const std::uint64_t Logger::LOG_COMPONENT_EVENTLOOP =   BIT(3);
const std::uint64_t Logger::LOG_COMPONENT_PHYSICS =     BIT(4);
const std::uint64_t Logger::LOG_COMPONENT_UI =          BIT(5);
const std::uint64_t Logger::LOG_COMPONENT_USERINPUT =   BIT(6);
const std::uint64_t Logger::LOG_COMPONENT_SCRIPT =      BIT(7);
const std::uint64_t Logger::LOG_COMPONENT_LOGGER =      BIT(8);
const std::uint64_t Logger::LOG_COMPONENT_FILESYSTEM =  BIT(9);
const std::uint64_t Logger::LOG_COMPONENT_TESTING =     BIT(10);

///  !!@attention!!! increment me after adding new component
const int Logger::_LOG_COMPONENTS_COUNT = 12;

const std::uint64_t Logger::LOG_COMPONENT_ALL_MASK = std::numeric_limits<std::uint64_t>::max();

Logger::Logger()
    : mLogComponentEnabledMask(LOG_COMPONENT_ALL_MASK)
{
    setLogMessageLevelName(LOG_LEVEL_DEBUG,     "Debug");
    setLogMessageLevelName(LOG_LEVEL_INFO,      "Info");
    setLogMessageLevelName(LOG_LEVEL_WARNING,   "Warning");
    setLogMessageLevelName(LOG_LEVEL_CRITICAL,  "Critical");

    /// -----------------------------------------------------------

    setLogMessageComponentName(LOG_COMPONENT_NONE,      "None");
    setLogMessageComponentName(LOG_COMPONENT_OTHER,     "Other");
    setLogMessageComponentName(LOG_COMPONENT_RENDERER,  "Renderer");
    setLogMessageComponentName(LOG_COMPONENT_GAME,      "Game");
    setLogMessageComponentName(LOG_COMPONENT_EVENTLOOP, "EventLoop");
    setLogMessageComponentName(LOG_COMPONENT_PHYSICS,   "Physics");
    setLogMessageComponentName(LOG_COMPONENT_UI,        "UserInterface");
    setLogMessageComponentName(LOG_COMPONENT_USERINPUT, "UserInput");
    setLogMessageComponentName(LOG_COMPONENT_SCRIPT,    "Script");
    setLogMessageComponentName(LOG_COMPONENT_LOGGER,    "Logger");
    setLogMessageComponentName(LOG_COMPONENT_FILESYSTEM,"FileSystem");
    setLogMessageComponentName(LOG_COMPONENT_TESTING,   "Testing");

    if (mLogComponentNames.size() != _LOG_COMPONENTS_COUNT)
    {   
        std::cerr << "Critical: "
                  << VE_SCOPE_NAME(Logger::Logger()).toString() << " "
                  << "DEVEL: Check that you registred all Logger components "
                  << "(see " << VE_SCOPE_NAME(Logger::Logger()).toString() << " constructor for details)."
                  << std::endl;
    }
}

Logger::~Logger()
{
    log("Logger destroyed", Ogre::LML_TRIVIAL, LOG_COMPONENT_LOGGER);
}

void Logger::scriptLog(const std::string &msg, int logLevel)
{
    log(msg, logLevel, LOG_COMPONENT_SCRIPT);
}

void Logger::setLogMessageLevelName(int level, const std::string &name)
{
    mLogLevelNames[level] = name;
}

void Logger::setLogMessageComponentName(std::uint64_t component, const std::string &name)
{
    mLogComponentNames[component] = name;
}

const std::string &Logger::getLogMessageLevelName(int level)
{
    auto it = mLogLevelNames.find(level);
    if (it != mLogLevelNames.end())
    {
        return it->second;
    }

    std::stringstream stream;
    stream << level;

    static const std::string Unknown = "Unknown";
    return Unknown;
}

const std::string &Logger::getLogMessageComponentName(std::uint64_t component)
{
    auto it = mLogComponentNames.find(component);
    if (it != mLogComponentNames.end())
    {
        return it->second;
    }

    std::stringstream stream;
    stream << component;

    static const std::string Unknown = "Unknown";
    return Unknown;
}

//----------------------------------------------------------------

Logger *Logger::getSingletonPtr()
{
    return static_cast<Logger *>(LogManager::getSingletonPtr());
}

void Logger::log(const std::string &message, int level, std::uint64_t component)
{
    if (isLogComponentEnabled(component))
    {
        std::string msg = "[" + getLogMessageLevelName(level) + "]";
        if (component != LOG_COMPONENT_OTHER)
        {
            msg += "{" + getLogMessageComponentName(component) + "}";
        }
        msg += " " + message;

        LogManager::logMessage(getOgreLogMessageLevel(level), msg, false);
    }
}

//----------------------------------------------------------------

Ogre::LoggingLevel Logger::getOgreLoggingLevel(int level)
{
    Ogre::LoggingLevel result;
    switch(level)
    {
    case LOG_LEVEL_DEBUG:
        result = Ogre::LL_BOREME;
        break;
    case LOG_LEVEL_INFO:
    case LOG_LEVEL_WARNING:
        result = Ogre::LL_NORMAL;
        break;
    case LOG_LEVEL_CRITICAL:
    default:
        result = Ogre::LL_LOW;
    }
    return result;
}

Ogre::LogMessageLevel Logger::getOgreLogMessageLevel(int level)
{
    Ogre::LogMessageLevel result;
    switch(level)
    {
    case LOG_LEVEL_DEBUG:
        result = Ogre::LML_TRIVIAL;
        break;
    case LOG_LEVEL_INFO:
    case LOG_LEVEL_WARNING:
        result = Ogre::LML_NORMAL;
        break;
    case LOG_LEVEL_CRITICAL:
    default:
        result = Ogre::LML_CRITICAL;
    }
    return result;
}

//----------------------------------------------------------------

Logger *logger()
{
    return Logger::getSingletonPtr();
}
