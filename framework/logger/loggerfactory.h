#ifndef LOGGERFACTORY_H
#define LOGGERFACTORY_H

class Logger;

class LoggerFactory
{
public:
    virtual ~LoggerFactory() { }

    virtual Logger *createLogger() const;
};

#endif // LOGGERFACTORY_H
