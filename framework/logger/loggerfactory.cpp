#include "loggerfactory.h"

#include <framework/logger/logger.h>

Logger *LoggerFactory::createLogger() const
{
    return new Logger;
}
