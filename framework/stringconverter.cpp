#include "stringconverter.h"

std::string StringConverter::toString(const Ogre::Vector3 &val, unsigned short precision,
                                      unsigned short width, char fill, std::ios::fmtflags flags)
{
    return Ogre::StringConverter::toString(val.x, precision, width, fill, flags) + fill +
           Ogre::StringConverter::toString(val.y, precision, width, fill, flags) + fill +
           Ogre::StringConverter::toString(val.z, precision, width, fill, flags);
}

std::string StringConverter::toString(const Ogre::Vector4 &val, unsigned short precision,
                                      unsigned short width, char fill, std::ios::fmtflags flags)
{
    return Ogre::StringConverter::toString(val.x, precision, width, fill, flags) + fill +
           Ogre::StringConverter::toString(val.y, precision, width, fill, flags) + fill +
           Ogre::StringConverter::toString(val.z, precision, width, fill, flags) + fill +
           Ogre::StringConverter::toString(val.w, precision, width, fill, flags);
}


std::string StringConverter::toString(unsigned int val, unsigned short width,
                                      char fill, std::ios::fmtflags flags)
{
    return Ogre::StringConverter::toString(val, width, fill, flags);
}

std::string StringConverter::toString(float val, unsigned short precision,
                                      unsigned short width, char fill,
                                      std::ios::fmtflags flags)
{
    return Ogre::StringConverter::toString(val, precision, width, fill, flags);
}
