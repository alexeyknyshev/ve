#include "stringlist.h"

#include <OGRE/OgreStringVector.h>
#include <OGRE/OgreStringConverter.h>

#include <global.h>

const StringList StringList::BLANK;

StringList::StringList(const std::string &text, const std::string &delims, bool preserveDelims)
{
    auto vector = Ogre::StringUtil::split(text, delims, 0, preserveDelims);

    for (const std::string &str : vector)
    {
        push_back(str);
    }
}

std::string StringList::join(std::function<std::string (size_t, const std::string &)> func) const
{
    std::string result;

    size_t index = 0;
    for (const std::string &str : *this)
    {
        result += func(index, str);
        ++index;
    }

    return result;
}

const std::string &StringList::at(int index) const
{
    auto it = begin();
    std::advance(it, index);
    if (it != end())
    {
        return *it;
    }
    return VE_STR_BLANK;
}

std::string &StringList::at(int index)
{
    static std::string STUB = VE_STR_BLANK;

    auto it = begin();
    std::advance(it, index);
    if (it != end())
    {
        return *it;
    }

    STUB.clear();
    return STUB;
}
