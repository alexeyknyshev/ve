#ifndef GLOBAL_H
#define GLOBAL_H

#include <OGRE/OgrePlatform.h>
#include <OGRE/OgrePrerequisites.h>
#include <OGRE/OgreCommon.h>

#include <cstddef>
#include <string>
#include <algorithm>

// Default keys for settings values in gameOptions
///    @see Game::getSettingsValue @ game/game.h

#define VE_OPTION_WINDOW_BACKEND_CONTEXT "currentGLContext"

#define VE_OPTION_PLUGIN_RENDER_SYSTEM "render_system_plugin"
#define VE_OPTION_PLUGIN_PHYSICS_SYSTEM "physics_system_plugin"
#define VE_OPTION_PLUGIN_SCENE_MANAGER "scene_manager_plugin"

#define VE_OPTION_PHYSICS_WORLD_HALF_EXTENTS "physics_world_half_extents"
#define VE_OPTION_PHYSICS_WORLD_GRAVITY "physics_world_gravity"

#define VE_OPTION_PHYSICS_WORLD_HALF_EXTENTS_DEFAULT { 1000.0f, 1000.0f, 1000.0f }
#define VE_OPTION_PHYSICS_WORLD_GRAVITY_DEFAULT { 0.0f, -9.81f, 0.0f }

//#define VE_USE_LUABIND 1
#define VE_NO_EXCEPTIONS

/** ------ Platforms ------ */

#define VE_PLATFORM_LINUX OGRE_PLATFORM_LINUX
#define VE_PLATFORM_APPLE OGRE_PLATFORM_APPLE
#define VE_PLATFORM_WIN32 OGRE_PLATFORM_WIN32

#define VE_PLATFORM OGRE_PLATFORM

/** ------ Compilers ------ */

#define VE_COMPILER_GNUC OGRE_COMPILER_GNUC
#define VE_COMPILER_CLANG OGRE_COMPILER_CLANG
#define VE_COMPILER_MSVC OGRE_COMPILER_MSVC

#define VE_COMPILER OGRE_COMPILER

#define VE_COMPILER_VER OGRE_COMP_VER

/** ----------------------------- */

// Condition that can be used for nullptr asserting
/// \example
#define VE_CHECK_NULL_PTR(clazz, ptr, component) \
    veAssert(ptr != nullptr, "Null pointer "#ptr" of class "#clazz"!", (component))

#define VE_AS_STR(what) std::string(""#what"")

#define VE_TO_STR(smth) std::string(smth)

#if OGRE_VERSION_MAJOR == 2
#define VE_STR_BLANK Ogre::BLANKSTRING
#else
#define VE_STR_BLANK Ogre::StringUtil::BLANK
#endif

/// @see ScopeName class
/// @example VE_SCOPE_NAME(Game::getSettings())
#define VE_SCOPE_NAME(scope) ScopeName(VE_AS_STR(scope))

/// @see ClassName class
/// @example VE_CLASS_NAME(Game)
#define VE_CLASS_NAME(Class) classNameHelper<Class>()
#define VE_ENUM_NAME(Enum) enumNameHelper<Enum>()

#define VE_PRINTABLE(string) ((string)).c_str()

#define VE_UNUSED(unused) (void) (unused)

#define VE_FUNCTION VE_PRINTABLE(std::string("\n") + __PRETTY_FUNCTION__)
#define VE_FUNCTION_STR std::string(__PRETTY_FUNCTION__)

//----------------------------------------------------------------


bool veIsUIntNumber(const std::string &str);
bool veIsUIntNumber(std::string::const_iterator begin,
                    std::string::const_iterator end);

bool veIsNumber(const std::string &str);
bool veIsNumber(std::string::const_iterator begin,
                std::string::const_iterator end);

template<typename T>
int veSgn(T val)
{
    return (int)(T(0) < val) - (int)(val < T(0));
}

//----------------------------------------------------------------

#include <typeinfo>

class TypeName
{
public:
    explicit TypeName(const std::string &name)
        : mName(name)
    { }

    std::string toString() const;

    bool operator==(const TypeName &other) const
    { return toString() == other.toString(); }

    bool operator <(const TypeName &other) const
    { return toString() < other.toString(); }

private:
    const std::string mName;
};

class ClassName : public TypeName
{
public:
    explicit ClassName(const std::string &name)
        : TypeName(name)
    { }
};

class EnumName : public TypeName
{
public:
    explicit EnumName(const std::string &name)
        : TypeName(name)
    { }
};

class ScopeName : public TypeName
{
public:
    explicit ScopeName(const std::string &name)
        : TypeName(name)
    { }
};

template<class T>
ClassName classNameHelper()
{
    static_assert(std::is_class<T>::value, "ClassName parameter is not a class");
    return ClassName(typeid(T).name());
}

template<typename T>
EnumName enumNameHelper()
{
    static_assert(std::is_enum<T>::value, "EnumName parameter is not an enum");
    return EnumName(typeid(T).name());
}

//----------------------------------------------------------------

bool veAssert(bool condition,
              const std::string &msgOnFail,
              std::uint64_t component);

bool veAssert(bool condition,
              const TypeName &className,
              const ScopeName &scopeName,
              const std::string &msgOnFail,
              std::uint64_t component);

void veLogAndAssert(bool condition,
                    const TypeName &className,
                    const ScopeName &scopeName,
                    const std::string &objectName,
                    const std::string &msgOnFail,
                    std::uint64_t component);

bool veWarning(bool condition,
               const std::string &msgOnFail);

void veSignalsHandler(int signal) throw();

//----------------------------------------------------------------
/*
#define VE_NOT_IMPLEMENTED(scope, component) \
    veAssert(false, VE_SCOPE_NAME_STR(scope).getName() + " is not implemented yet!", (component))
*/

void veNotImplemented(const ScopeName &scopeName, std::uint64_t component);

void veNotImplementedClass(const ClassName &className, const ScopeName &scopeName, std::uint64_t component);

//----------------------------------------------------------------

#endif // GLOBAL_H
