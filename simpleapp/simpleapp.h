#ifndef SIMPLEAPP_H
#define SIMPLEAPP_H

/** Example of simpliest ve-based app setup */
class SimpleApp
{
public:
    int run(int argc, char *argv[]);
};

#endif // SIMPLEAPP_H
