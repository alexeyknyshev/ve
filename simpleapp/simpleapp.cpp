#include "simpleapp.h"

#include <framework/params.h>
#include <framework/parser/cmdlineparser.h>
#include <framework/parser/conffileparser.h>
#include <framework/logger/logger.h>

#include <renderer/renderer.h>
#include <framework/frontend/renderviewoptions.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
#include <framework/frontend/ogreviewbackend/ogrerenderviewbackend.h>
#include <game/scriptedgame.h>

#include <framework/controller/inputcontroller.h>
#include <framework/controller/sdl/sdlinputbackend.h>
#include <framework/controller/ois/oisinputbackend.h>
#include <framework/frontend/misc/exitkeylistener.h>
#include <framework/frontend/misc/renderviewcloselistener.h>

int SimpleApp::run(int argc, char *argv[])
{
    ParamsMulti gameOptions;
    CmdLineParser(gameOptions, argc, argv);

    Logger logger_;
    Ogre::Log *deflog = logger_.getDefaultLog();
    if (!deflog)
    {
        const std::string defaultLogPath = "ve.log";
        const std::string &logFilePath = gameOptions.get("log", defaultLogPath);
        logger_.createLog(logFilePath, true);
    }
    logger_.setLogDetail(Logger::LOG_LEVEL_DEBUG);

    /* Parse config file (path passed from command line) */
    const auto it = gameOptions.find("config");
    if (it != gameOptions.end())
    {
        bool ok = false;
        ConfFileParser(gameOptions, it->second, &ok);
        if (!ok)
        {
            return 255;
        }
    }

    Renderer renderer_(gameOptions);
    renderer_.setRenderViewBackend(new SDLRenderViewBackend
                                   //new OGRERenderViewBackend
                                   );

    ve::SizeU viewSize(800, 600);
    renderer_.getRenderViewSizeHint(viewSize);

    RenderViewOptions opt;
    opt.name = "renderWindow";
    opt.size = viewSize;
    opt.fullScreen = false;
    opt.parameters["FSAA"] = "2";
    opt.parameters["VSync"] = "Yes";
    opt.listeners.insert(new RenderViewCloseListener("closeListener"));


    logger_.logMessage("Creating RenderView...", Logger::LOG_LEVEL_DEBUG);
    RenderView *view = new RenderView(opt);

    InputController *input = new InputController("input",
                                                 new SDLInputBackendFactory
                                                 //new OISInputBackendFactory
                                                 );
    input->addInputListener(new ExitKeyListener(VE_CLASS_NAME(SimpleApp).toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

    view->setInputController(input);

    ScriptedGame game("scriptedGame", &logger_,
                      &renderer_, gameOptions, *view);

    view->setTitle(VE_CLASS_NAME(SimpleApp).toString());

    /* Enter event loop */
    int status = game.enter();

    delete view;

    return status;
}

int main(int argc, char *argv[])
{
    SimpleApp app;

    return app.run(argc, argv);
}
