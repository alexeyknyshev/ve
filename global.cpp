#include "global.h"
#include <framework/logger/logger.h>

#include <execinfo.h>
#include <string>

bool veIsUIntNumber(std::string::const_iterator begin,
                    std::string::const_iterator end)
{
    return begin != end &&
           std::find_if(begin, end, [](char c) { return !std::isdigit(c); }) == end;
}

bool veIsUIntNumber(const std::string &str)
{
    return veIsUIntNumber(str.cbegin(), str.cend());
}

bool veIsNumber(std::string::const_iterator begin,
                std::string::const_iterator end)
{
    if (begin == end)
    {
        return false;
    }

    if (*begin == '-')
    {
        ++begin;
    }

    return veIsUIntNumber(begin, end);
}

bool veIsNumber(const std::string &str)
{
    return veIsNumber(str.cbegin(), str.cend());
}

// --------------------------------------------------------------------------------

bool veAssert(bool condition, const std::string &msgOnFail, std::uint64_t component)
{
    if (!condition)
    {
        if (!msgOnFail.empty())
        {
            logger()->logMessage(msgOnFail, Logger::LOG_LEVEL_CRITICAL, component);
        }
        assert(false);
        return false;
    }
    return true;
}

bool veAssert(bool condition,
              const TypeName &className,
              const ScopeName &scopeName,
              const std::string &msgOnFail,
              std::uint64_t component)
{
    if (!condition)
    {
        if (!msgOnFail.empty())
        {
            logger()->logClassMessage(className,
                                      scopeName,
                                      msgOnFail,
                                      Logger::LOG_LEVEL_CRITICAL,
                                      component);
        }
        assert(false);
        return false;
    }
    return true;
}

void veLogAndAssert(bool condition,
                    const TypeName &className,
                    const ScopeName &scopeName,
                    const std::string &objectName,
                    const std::string &msgOnFail,
                    std::uint64_t component)
{
    std::string msg = className.toString() + " ";
    if (!objectName.empty())
    {
        msg += "(" + objectName + ") ";
    }
    msg += "in [" + scopeName.toString() + "]: " + msgOnFail;

    veAssert(condition, msg, component);
}

bool veWarning(bool condition, const std::string &msgOnFail)
{
    if (!condition)
    {
        if (!msgOnFail.empty())
        {
            logger()->logMessage(msgOnFail, Logger::LOG_LEVEL_WARNING);
        }
        return false;
    }
    return true;
}

void veSignalsHandler(int signal) throw()
{
    fprintf(stderr, "Application has recieved signal %d (%s) from system.\n", signal, strsignal(signal));
    static uint32_t maxStackSize;

    void *stackEntries[maxStackSize];
    int stackEntriesSize = backtrace(stackEntries, maxStackSize);
    
    backtrace_symbols_fd(stackEntries, stackEntriesSize, 2);
    
    exit(signal);
}

// ---------------------------------------------------------------------------------

void veNotImplemented(const ScopeName &scopeName, std::uint64_t component)
{
    veAssert(false, scopeName.toString() + " is not implemented yet!", component);
}

void veNotImplementedClass(const ClassName &className, const ScopeName &scopeName, std::uint64_t component)
{
    veAssert(false, className.toString() + "::" + scopeName.toString() + " is not implemented yet!", component);
}

/// ================================================================================

std::string TypeName::toString() const
{
#if VE_COMPILER == VE_COMPILER_GNUC
    /// gcc adds numeric prefix to typename
    size_t i = 0;
    const std::locale &loc = std::locale::classic();
    while (i < mName.size() && std::isdigit(mName[i], loc))
    {
        ++i;
    }
    return mName.substr(i);
#else
    return mName;
#endif
}
