#ifndef SCRIPTEDGAME_H
#define SCRIPTEDGAME_H

#include <game/game.h>
#include <framework/filesystem.h>

#include <script/scripthandler.h>

/** Game with scripting support */

class ScriptedGame : public Game,
                     public ScriptHandler
{
public:
    VE_DECLARE_TYPE_INFO(ScriptedGame)

    /**
     * @brief ScriptedGame
     * @param name     - game name
     * @param logger   - ptr to Logger (you guarantee the existence of the @logger
     *                   during the life of the Game class obj)
     * @param renderer - ptr to Renderer (you guarantee the existence of the @renderer
     *                   during the life of the Game class obj)
     * @param options  - key-value pairs
     *                   "--game"          --> relative path to game script (Lua for example)
     *                   "--script_engine" --> script engine type (Lua bt default)
     * @param view
     */
    ScriptedGame(const std::string &name, Logger *logger,
                 Renderer *renderer, const ParamsMulti &options, RenderView &view);

    void init(const ParamsMulti &options, RenderView &view) override;
    void shutdown() override;

    void registerScriptFactory(ScriptFactory *factory);

    /** Create Script and load it from DataStream */
    Script *createScript(const Ogre::DataStreamPtr &stream) const;

    /** Create Script and load it from resource with given name */
    Script *createScript(const std::string &resourceName,
                         const std::string &resourceGroupName =
                            FileSystem::RG_DEFAULT) const;
                            
    Script *createScriptFromSource(const std::string &src) const;

    void setDefaultScriptFactoryType(const std::string &type);

    inline const std::string &getDefaultScriptFactoryType() const
    {
        return mDefaultScriptType;
    }

protected:
    void _init(const ParamsMulti &options);
    void _shutdown();

private:
    const std::string mDefaultScriptType;
};

#endif // SCRIPTEDGAME_H
