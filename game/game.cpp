#include "game.h"

#include <thread>

#include <renderer/renderer.h>

#include <game/modelcontroller.h>

#include <framework/filesystem.h>
#include <framework/updatelistener.h>
#include <framework/parser/cmdlineparser.h>
#include <framework/logger/logger.h>
#include <framework/logger/loggerfactory.h>
#include <framework/frontend/renderview.h>

#if OGRE_VERSION_MAJOR == 2
#include <OGRE/OgreTimer.h>
#endif

/// Event system
#include <framework/event.h>

/// Physics system
#include <physics/physicsmanager.h>

VE_DECLARE_SINGLETON(Game);

Game::Game(const std::string &name, Logger *logger,
           Renderer *renderer, const ParamsMulti &options,
           RenderView &view)
    : mRenderer(renderer),
      mLogger(logger),
      mPhysicsManager(nullptr),
      mName(name),
      mModelController(nullptr)
{
    veAssert(renderer, getClassName(),
             VE_SCOPE_NAME(Game(const std::string &name, Logger *logger,
                                Renderer *renderer, const ParamsMulti &options,
                                RenderView &view)),
             "Invalid (nullptr) renderer!",
             Logger::LOG_COMPONENT_GAME);

    veAssert(logger, getClassName(),
             VE_SCOPE_NAME(Game(const std::string &name, Logger *logger,
                                Renderer *renderer, const ParamsMulti &options,
                                RenderView &view)),
             "Invalid (nullptr) logger!",
             Logger::LOG_COMPONENT_GAME);

    parseAditionalOptions(options);
//    parseGameConfigFile(options);

    setModelController(new ModelController(VE_CLASS_NAME(ModelController).toString()));

    Game::_init(mSettings, view);
}

Game::~Game()
{
    removeModelController();

    Game::_shutdown();

    mRenderer = nullptr;
    mLogger = nullptr;
}

//----------------------------------------------------------------

void Game::init(const ParamsMulti &options, RenderView &view)
{
    Game::_init(options, view);
}

void Game::_init(const ParamsMulti &options, RenderView &view)
{
    initPhysics(options, view);

#ifndef NDEBUG
    printArgs();
#endif
}

void Game::shutdown()
{
    Game::_shutdown();
}

void Game::_shutdown()
{
    mUpdateListenersForRemove.clear();

    // Iteartor safe list
    // if we will try to delete UpdateListeners while
    // iteration mUpdateListener it cause crash because some
    // update listeners can self-remove from mUpdateListeners list
    // inside the loop which breaks itearators
    std::list<UpdateListener *> deletionList;

    for (UpdateListener *listener : mUpdateListeners)
    {
        // listener may be already deleted (not alive)
        auto it = mUpdateListenersForRemove.find(listener);
        if (it != mUpdateListenersForRemove.end())
        {
            const bool alive = it->second;
            if (alive)
            {
                if (listener->isSupposedToBeDeleted())
                {
                    deletionList.push_back(listener);
                }
            }
        }
    }
    mUpdateListeners.clear();

    // flush mUpdateListenersForAdd
    for (UpdateListener *listener : mUpdateListenersForAdd)
    {
        // listener may be already deleted (not alive)
        auto it = mUpdateListenersForRemove.find(listener);
        if (it != mUpdateListenersForRemove.end())
        {
            const bool alive = it->second;
            if (alive)
            {
                if (listener->isSupposedToBeDeleted())
                {
                    deletionList.push_back(listener);
                }
            }
        }
    }
    mUpdateListenersForAdd.clear();
    mUpdateListenersForRemove.clear();

    for (UpdateListener *listener : deletionList)
    {
        delete listener;
    }

    mLogger->logClassMessage(VE_CLASS_NAME(Game),
                             "is now halt",
                             Logger::LOG_LEVEL_INFO,
                             Logger::LOG_COMPONENT_LOGGER);
}

bool Game::addUpdateListener(UpdateListener *listener)
{
    if (!contains(mUpdateListeners, listener))
    {
        mUpdateListenersForAdd.insert(listener);
        return true;
    }
    return false;
}

bool Game::isUpdateListenerAdded(const UpdateListener *listener) const
{
    return contains_if(mUpdateListeners.cbegin(), mUpdateListeners.cend(),
                       [&](const UpdateListener *l) -> bool { return listener == l; })
           ||
           contains_if(mUpdateListenersForAdd.cbegin(), mUpdateListenersForAdd.cend(),
                       [&](const UpdateListener *l) -> bool { return listener == l; });
}

bool Game::removeUpdateListener(UpdateListener *listener, bool listenerAlive)
{
    if (contains(mUpdateListeners, listener))
    {
        mUpdateListenersForRemove.insert({ listener, listenerAlive });
        return true;
    }
    return false;
}

/*
bool Game::removeUpdateListener(const std::string &name)
{
    const auto it = mUpdateListeners.find(name);
    if (it != mUpdateListeners.end())
    {
        mUpdateListenersForRemove.insert(it->first);
        return true;
    }

    return false;
}
*/

// --------------------------------------------------------

void Game::setModelController(ModelController *controller)
{
    if (mModelController == controller)
    {
        return;
    }

    if (mModelController)
    {
        removeModelController();
    }

    mModelController = controller;
    mModelController->setAutoUpdateEnabled(true);
}

bool Game::removeModelController()
{
    if (mModelController)
    {
        delete mModelController;
        mModelController = nullptr;

        return true;
    }

    return false;
}

ModelController *Game::getModelController(bool checkPointer) const
{
    if (checkPointer)
    {
        VE_CHECK_NULL_PTR(ModelController, mModelController, Logger::LOG_COMPONENT_GAME);
    }
    return mModelController;
}

// --------------------------------------------------------

int Game::enter()
{
    static bool alreadyRunning = false;

    if (alreadyRunning)
    {
        return -1;
    }

    if (isAboutToExit())
    {
        return getExitStatus();
    }

    alreadyRunning = true;

    if (!mRenderer->reset())
    {
        /// \todo \exception There isn't any RenderSystem registred
        return -1;
    }

    Ogre::Timer *timer = mRenderer->getTimer();

    unsigned long currentTime = timer->getMilliseconds();
    unsigned long timeSinceLastRenderedFrame = 0;

    while (!isAboutToExit())
    {
        const unsigned long newTime = timer->getMilliseconds();
        const unsigned long frameTime = newTime - currentTime;
        currentTime = newTime;

        /* Make sure the game doesn't freeze in case the computer isn't fast
           enough to run the logic as fast as we want it to. Note that this
           will skip logic frames so all time in the game will be stretched.
           Note also that this is very unlikely to happen. */
        const unsigned long fixedFrameTime = std::min((unsigned long)33, frameTime);
        timeSinceLastRenderedFrame += fixedFrameTime;

        for (UpdateListener *listener : mUpdateListeners)
        {
            listener->update(fixedFrameTime * 0.001f, frameTime * 0.001f);
        }

        const auto end = mUpdateListenersForRemove.cend();
        for (auto it = mUpdateListenersForRemove.begin(); it != end; ++it)
        {
            UpdateListener *listener = it->first;
            bool listenerAlive = it->second;
            mUpdateListeners.erase(listener);
            if (listenerAlive)
            {
                if (listener->isSupposedToBeDeleted())
                {
                    delete listener;
                }
            }
        }
        mUpdateListenersForRemove.clear();

        mUpdateListeners.insert(mUpdateListenersForAdd.begin(),
                                mUpdateListenersForAdd.end());
        mUpdateListenersForAdd.clear();

        static const unsigned long expectedRenderTime = 16;
        static const unsigned long criticalRenderTime = expectedRenderTime * 2;

        bool freeCpu = true;
        if (timeSinceLastRenderedFrame > expectedRenderTime)
        {
            if (timeSinceLastRenderedFrame > criticalRenderTime)
            {
                freeCpu = false;
            }

            timeSinceLastRenderedFrame -= expectedRenderTime;
        }

        if (!mRenderer->renderOneFrame())
        {
            exit();
        }

        /** now disabled because there isn't any point to use it */
        //processEvents();

        if (freeCpu)
        {
            static const std::chrono::nanoseconds sleepInterval(1);
            std::this_thread::sleep_for(sleepInterval);
        }
    }

    alreadyRunning = false;

    return getExitStatus();
}

//----------------------------------------------------------------

void Game::initPhysics(const ParamsMulti &options, RenderView &view)
{
    shutdownPhysics();

    if (!view.isValid())
    {
        logger()->logClassMessage(VE_CLASS_NAME(Game),
                                  VE_SCOPE_NAME(initPhysics(const ParamsMulti &options, RenderView &view)),
                                  "Game physics requires initialized " +
                                  VE_CLASS_NAME(RenderView).toString() + "!",
                                  Logger::LOG_LEVEL_CRITICAL,
                                  Logger::LOG_COMPONENT_GAME);

        veAssert(false,
                 VE_CLASS_NAME(Game),
                 VE_SCOPE_NAME(initPhysics(const ParamsMulti &options, RenderView &view)),
                 "No valid rendering context!",
                 Logger::LOG_COMPONENT_GAME);
    }

    mPhysicsManager = new PhysicsManager(getRenderer()->getSceneManager(),
                                         options);
}

void Game::shutdownPhysics()
{
    if (mPhysicsManager)
    {
        const bool physicsManagerAlive = false;
        removeUpdateListener(mPhysicsManager, physicsManagerAlive);
        delete mPhysicsManager;
        mPhysicsManager = nullptr;
    }
}

//----------------------------------------------------------------

void Game::parseAditionalOptions(const ParamsMulti &options)
{
    mSettings.insert(options.begin(), options.end());
}

/*
void Game::parseGameConfigFile(const ParamsMulti &options)
{
    const auto end = options.end();
    const std::string configStr = "config";

    const auto it = options.find(configStr);
    if (it != end)
    {
        if (loadConfig(it->second))
        {
            return;
        }
    }

    const std::string configArg = getSettingsValue(configStr);
    if (!configArg.empty())
    {
        loadConfig(configArg);
    }
}
*/

#ifndef NDEBUG
void Game::printArgs()
{
    logger()->logClassMessage(VE_CLASS_NAME(Game),
                              VE_SCOPE_NAME(printArgs()),
                              "Game parameters: ",
                              Logger::LOG_LEVEL_DEBUG,
                              Logger::LOG_COMPONENT_GAME);

    auto end = getSettings().end();
    for (auto it = getSettings().begin(); it != end; ++it)
    {
        logger()->logClassMessage(VE_CLASS_NAME(Game),
                                  VE_SCOPE_NAME(printArgs()),
                                  it->first + ": " + it->second,
                                  Logger::LOG_LEVEL_DEBUG,
                                  Logger::LOG_COMPONENT_GAME);
    }
}
#endif

bool Game::hasSettingsValue(const std::string &key)
{
    return (getSettings().count(key) > 0);
}

std::string Game::getSettingsValue(const std::string &arg,
                                   const std::string &defValue) const
{
    ParamsMultiEqualRange optEqualRange = getSettings().equal_range(arg);

    if (optEqualRange.first != optEqualRange.second)
    {
        return optEqualRange.first->second;
    }
    return defValue;
}

//----------------------------------------------------------------

void Game::processEvents(uint32_t mask)
{
    EventLoop::processEvents(mask);
}

void Game::processEvents(uint32_t mask, uint32_t maxmsec)
{
    EventLoop::processEvents(mask, maxmsec);
}

void Game::postEvent(Object *obj, Event *event)
{
    VE_UNUSED(obj); VE_UNUSED(event);
    mLogger->logClassMessage(VE_CLASS_NAME(Game),
                             "Event system support is disabled!",
                             Logger::LOG_LEVEL_CRITICAL,
                             Logger::LOG_COMPONENT_EVENTLOOP);

    //VE_NOT_IMPLEMENTED(Game::postEvent, Logger::LOG_COMPONENT_GAME);
}

Game *game(bool checkPoiner)
{
    Game *game = Game::getSingletonPtr();
    if (checkPoiner)
    {
        veAssert(game != nullptr, VE_CLASS_NAME(Game), VE_SCOPE_NAME(Game *game(bool)),
                 "Game instance has not found! Please make sure that you have created Game object.",
                 Logger::LOG_COMPONENT_GAME);
    }
    return game;
}
