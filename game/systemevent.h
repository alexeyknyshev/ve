#ifndef SYSTEMEVENT_H
#define SYSTEMEVENT_H

#include <framework/event.h>

class SystemEvent : public Event
{
public:
    VE_DECLARE_TYPE_INFO(SystemEvent)

    SystemEvent(uint32_t priority = NorPriority, uint32_t lifeTime = DefLifeTime);
};

#endif // SYSTEMEVENT_H
