#ifndef MODELCONTROLLER_H
#define MODELCONTROLLER_H

#include <framework/object.h>
#include <framework/updatelistener.h>

#include <renderer/model.h>

class ModelController : public Object,
                        public UpdateListener
{
public:
    VE_DECLARE_TYPE_INFO(ModelController)

    ModelController(const std::string &name);
    virtual ~ModelController() { }

    const std::string &getName() const override
    {
       return mName;
    }

    bool event(const Event *event) override;

    //----------------------

    bool enableControll(Model *model);
    bool disableControll(Model *model);

    //----------------------

    void update(float deltaTime, float realTime);

private:
    const std::string mName;

    ModelMap mModels;
};

#endif // MODELCONTROLLER_H
