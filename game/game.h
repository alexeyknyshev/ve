#ifndef GAME_H
#define GAME_H

#include <functional>

#include <framework/eventloop.h>
#include <framework/container.h>

#include <OGRE/OgreSingleton.h>

class Renderer;
class RenderView;
class RenderViewOptions;
class RenderViewFactory;
class Event;
class Logger;
class LoggerFactory;
class PhysicsManager;
class UpdateListener;
class ModelController;

/** Core game object which plays coordination role
 ** of all system components. Includes eventloop
 ** implementation, command line arguments handling
 ** and configurable sys objects creation (for example
 ** rendering views (windows), loggers and so on). */

class Game : public EventLoop,
             public Ogre::Singleton<Game>
{
public:
    VE_DECLARE_TYPE_INFO(Game)

    Game(const std::string &name,
         Logger *logger,
         Renderer *renderer,
         const ParamsMulti &options,
         RenderView &view);

    ~Game();

    int enter() override;

    /* Game options splitted in argument-value pairs */
    inline const ParamsMulti &getSettings() const
    {
        return mSettings;
    }

    inline Renderer *getRenderer() const
    {
        return mRenderer;
    }

    inline PhysicsManager *getPhysics() const
    {
        return mPhysicsManager;
    }

    inline bool hasPhysicsSupport() const
    {
        return (bool)getPhysics();
    }

    bool hasSettingsValue(const std::string &key);

    std::string getSettingsValue(const std::string &arg,
                                 const std::string &defValue = VE_STR_BLANK) const;

    void initPhysics(const ParamsMulti &options, RenderView &view);
    void shutdownPhysics();

    virtual const std::string &getName() const
    {
        return mName;
    }

    void processEvents(uint32_t mask = Event::ET_All) override;
    void processEvents(uint32_t mask, uint32_t maxmsec) override;

    void postEvent(Object *obj, Event *event);

    virtual void init(const ParamsMulti &options, RenderView &view);
    virtual void shutdown();

    bool addUpdateListener(UpdateListener *listener);
    bool isUpdateListenerAdded(const UpdateListener *listener) const;
    bool removeUpdateListener(UpdateListener *listener, bool listenerAlive);

    /// TODO: add update listener functor
///    void addUpdateListener(std::function<void(float)> func);


    void setModelController(ModelController *controller);
    bool removeModelController();
    ModelController *getModelController(bool checkPointer = true) const;

#ifndef NDEBUG
    void printArgs();
#endif

protected:
    void _init(const ParamsMulti &options, RenderView &view);
    void _shutdown();

private:
    Renderer *mRenderer;
    Logger *mLogger;
    PhysicsManager *mPhysicsManager;
    ParamsMulti mSettings;
    const std::string mName;

    ModelController *mModelController;

    typedef std::set<UpdateListener *> UpdateListenerSet;
    typedef std::map<UpdateListener *, bool> UpdateListenerAndAliveMap;
    UpdateListenerSet mUpdateListeners;

    UpdateListenerSet mUpdateListenersForAdd;
    UpdateListenerAndAliveMap mUpdateListenersForRemove;

    /// TODO update functors list
///    typedef std::set<std::function<void(float)>> mUpdateFuncListeners;

    void parseAditionalOptions(const ParamsMulti &options);
//    void parseGameConfigFile(const ParamsMulti &options);

/*
    inline bool isValidShortSetting(const std::string &opt)
    {
        return (opt.find("-") == 0 && opt.size() == 2);
    }

    inline bool isValidLongArg(const std::string &opt)
    {
        return (opt.find("--") == 0 && opt.size() >= 4);
    }
*/
};

Game *game(bool checkPoiner = true);

#endif // GAME_H
