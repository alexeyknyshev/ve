#include "scriptedgame.h"

#include <global.h>
#include <framework/filesystem.h>
#include <framework/logger/logger.h>

#include <script/lua/luascriptfactory.h>

ScriptedGame::ScriptedGame(const std::string &name, Logger *logger,
                           Renderer *renderer, const ParamsMulti &options,
                           RenderView &view)
    : Game(name, logger, renderer, options, view),
      mDefaultScriptType("Lua")
{
    ScriptedGame::_init(options);
}

void ScriptedGame::init(const ParamsMulti &options, RenderView &view)
{
    Game::init(options, view);
    ScriptedGame::_init(options);
}

void ScriptedGame::_init(const ParamsMulti &options)
{
    VE_UNUSED(options);

    std::string scriptPath = getSettingsValue("--game");
    if (!scriptPath.empty())
    {
        std::string scriptLocation = FileSystem::dirname(scriptPath);

        if (scriptLocation.empty())
        {
            scriptLocation = FileSystem::getCwd();
        }

        if (!FileSystem::hasResourceLocation(scriptLocation))
        {
            FileSystem::addResourceLocation(scriptLocation,
                                            FileSystem::LOC_TYPE_FS,
                                            FileSystem::RG_DEFAULT,
                                            false);
        }
    }
}

void ScriptedGame::shutdown()
{
    ScriptedGame::_shutdown();
    Game::shutdown();
}

void ScriptedGame::_shutdown()
{
    deleteRegistredScriptFactory(getDefaultScriptFactoryType());

    std::string scriptPath = getSettingsValue("--game");
    if (!scriptPath.empty())
    {
        std::string scriptLocation = FileSystem::dirname(scriptPath);

        if (scriptLocation.empty())
        {
            scriptLocation = FileSystem::getCwd();
        }

        if (FileSystem::hasResourceLocation(scriptLocation, FileSystem::RG_DEFAULT))
        {
            FileSystem::removeResourceLocation(scriptLocation, FileSystem::RG_DEFAULT);
        }
    }
}

void ScriptedGame::registerScriptFactory(ScriptFactory *factory)
{
    if (!factory)
    {
        return;
    }

    if (getScriptFactoryMap().empty())
    {
        setDefaultScriptFactoryType(factory->getScriptTypeName());
    }

    ScriptHandler::registerScriptFactory(factory);
}

void ScriptedGame::setDefaultScriptFactoryType(const std::string &type)
{
    logger()->logClassMessage(getClassName(), "Default ScriptFactory is now \"" + type + "\"",
                              Logger::LOG_LEVEL_DEBUG,
                              Logger::LOG_COMPONENT_SCRIPT);
}

Script *ScriptedGame::createScript(const Ogre::DataStreamPtr &stream) const
{
    return ScriptHandler::createScript(
                getSettingsValue("--script_engine", getDefaultScriptFactoryType()),
                stream);
}

Script *ScriptedGame::createScript(const std::string &resourceName,
                                   const std::string &resourceGroupName) const
{
    return ScriptHandler::createScript(
                getSettingsValue("--script_engine", getDefaultScriptFactoryType()),
                resourceName, resourceGroupName);
}

Script* ScriptedGame::createScriptFromSource(const std::string& src) const
{
    return ScriptHandler::createScriptFromSource(
                getSettingsValue("--script_engine", getDefaultScriptFactoryType()), src);
}

