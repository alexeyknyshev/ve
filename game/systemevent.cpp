#include "systemevent.h"

SystemEvent::SystemEvent(uint32_t priority, uint32_t lifeTime)
    : Event(ET_System, priority, lifeTime)
{
}
