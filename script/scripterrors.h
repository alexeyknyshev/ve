#ifndef SCRIPTERRORS_H
#define SCRIPTERRORS_H

#include <global.h>
#include <framework/error.h>

class ScriptExeception : public BaseException
{
public:
    ScriptExeception(const std::string &backTrace, const std::string &comment)
        : BaseException(backTrace, comment)
    { }
};

class ScriptError : public Error
{
public:
    explicit ScriptError(const std::string &backTrace,
                         const std::string &comment,
                         bool raise = true)
        : Error(backTrace, "Script error", false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw ScriptExeception(backTrace, comment);
#else
            _raise(backTrace, comment, VE_CLASS_NAME(ScriptError).toString());
#endif
        }
    }
};

#endif // SCRIPTERRORS_H
