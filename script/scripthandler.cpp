#include "scripthandler.h"

#include <script/script.h>
#include <script/scriptfactory.h>
#include <framework/logger/logger.h>

ScriptHandler::ScriptHandler()
    : mScriptNameGenerator("script_")
{ }

ScriptHandler::~ScriptHandler()
{
    deleteRegistredScriptFactories();
}

bool ScriptHandler::hasRegistredScriptFactory(const std::string &name) const
{
    return contains(mScriptFactoryMap, name);
}

void ScriptHandler::registerScriptFactory(ScriptFactory *factory)
{
    if (!factory)
    {
        return;
    }

    const std::string &type = factory->getScriptTypeName();

    if (!mScriptFactoryMap.insert(ScriptFactoryMapValue(type, factory)).second)
    {
        deleteRegistredScriptFactory(type);
        mScriptFactoryMap.insert(ScriptFactoryMapValue(type, factory));
    }
    logger()->logMessage("ScriptFactory of type \"" + type + "\" registred.",
                         Logger::LOG_LEVEL_DEBUG);
}

bool ScriptHandler::deleteRegistredScriptFactory(const std::string &type)
{
    const auto it = mScriptFactoryMap.find(type);
    if (it != mScriptFactoryMap.end())
    {
        delete it->second;
        mScriptFactoryMap.erase(it);
        logger()->logMessage("ScriptFactory of type \"" + type + "\" unregistred.",
                             Logger::LOG_LEVEL_DEBUG);
        return true;
    }
    return false;
}

void ScriptHandler::deleteRegistredScriptFactories()
{
    const auto end = mScriptFactoryMap.end();
    for (auto it = mScriptFactoryMap.begin(); it != end; ++it)
    {
        delete it->second;
        logger()->logMessage("ScriptFactory of type \"" + it->first + "\" unregisterd.",
                             Logger::LOG_LEVEL_DEBUG);
    }
    mScriptFactoryMap.clear();
}

//----------------------------------------------------------------

Script *ScriptHandler::createScript(const std::string &type, const std::string &name,
                                    const Ogre::DataStreamPtr &stream) const
{
    const auto it = mScriptFactoryMap.find(type);
    if (it == mScriptFactoryMap.end())
    {
        const std::string text = "ScriptFactory of type \"" + type + "\" has not found!";
        //logger()->logMessage(text, Logger::LOG_LEVEL_CRITICAL);
        veAssert(false, text, Logger::LOG_COMPONENT_SCRIPT);
        return nullptr;
    }

    logger()->logClassMessage(VE_CLASS_NAME(ScriptHandler),
                              VE_SCOPE_NAME((const std::string &type, const DataStreamPtr &stream) const),
                              "[" + type + "] Loading script \"" + name + "\"",
                              Logger::LOG_LEVEL_INFO,
                              Logger::LOG_COMPONENT_SCRIPT);

    ScriptFactory *factory = it->second;
    return factory->createScript(name, stream);
}

Script *ScriptHandler::createScript(const std::string &type,
                                    const std::string &resourceName,
                                    const std::string &resourceGroupName) const
{
    const auto it = mScriptFactoryMap.find(type);
    if (it == mScriptFactoryMap.end())
    {
        const std::string text = "ScriptFactory of type \"" + type + "\" has not found!";
        //logger()->logMessage(text, Logger::LOG_LEVEL_CRITICAL);
        veAssert(false, text, Logger::LOG_COMPONENT_SCRIPT);
        return nullptr;
    }

    logger()->logClassMessage(VE_CLASS_NAME(ScriptHandler),
                              VE_SCOPE_NAME(createScript(const std::string &type, const DataStreamPtr &stream) const),
                              "[" + type + "] Loading script \"" + resourceName + "\" from resource group \"" +
                              resourceGroupName + "\"",
                              Logger::LOG_LEVEL_INFO,
                              Logger::LOG_COMPONENT_SCRIPT);

    ScriptFactory *factory = it->second;
    return factory->createScript(resourceName, resourceGroupName);
}

Script* ScriptHandler::createScriptFromSource(const std::string& type,
                                              const std::string& src) const
{
    const auto it = mScriptFactoryMap.find(type);
    if (it == mScriptFactoryMap.end())
    {
        const std::string text = "ScriptFactory of type \"" + type + "\" has not found!";
        veAssert(false, text, Logger::LOG_COMPONENT_SCRIPT);
        return nullptr;
    }

    logger()->logClassMessage(VE_CLASS_NAME(ScriptHandler),
                              VE_SCOPE_NAME((const std::string &type, const DataStreamPtr &stream) const),
                              "[" + type + "] Loading script from string",
                              Logger::LOG_LEVEL_INFO,
                              Logger::LOG_COMPONENT_SCRIPT);

    ScriptFactory *factory = it->second;
    return factory->createScriptFromSource(mScriptNameGenerator.generate(), src);
}


//----------------------------------------------------------------

/*
bool ScriptHandler::deleteScript(Script *script)
{
    ScriptFactoryMap::const_iterator it = mScriptFactoryMap.find(script->type());
    if (it != mScriptFactoryMap.end())
        return false;

    it->second->deleteScript(script);
    return true;
}
*/

//----------------------------------------------------------------
