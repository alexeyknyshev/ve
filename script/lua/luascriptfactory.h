#ifndef LUASCRIPTFACTORY_H
#define LUASCRIPTFACTORY_H

#include <script/scriptfactory.h>

class LuaScript;

class LuaScriptFactory : public ScriptFactory
{
public:
    Script *createScript(const std::string &resourceName,
                         const std::string &resourceGroupName = FileSystem::RG_DEFAULT) override;

    Script *createScript(const std::string &scriptName,
                         const Ogre::DataStreamPtr &stream) override;

    Script *createScriptFromSource(const std::string &name,
                                   const std::string &src) override;

    const std::string &getScriptTypeName() const override;
};

#endif // LUASCRIPTFACTORY_H
