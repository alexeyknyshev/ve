#include "luascript.h"

#include <iostream>
#include <limits>
#include <functional>

#include <script/lua/luaerrors.h>
#include <lua.hpp>
#include <script/lua/bindings/ve.cc>

static const char *SELF = "self";

static const int LUA_OWNED_YES = 1;
static const int LUA_OWNED_NO = 0;

const std::string &LuaScript::type() const
{
    static const std::string TypeName = "Lua";
    return TypeName;
}

//----------------------------------------------------------------

LuaScript::LuaScript(const std::string &name, const std::string &src)
    : Script(name, src),
      state(nullptr),
      mName(name)
{
    init();
    loadString(src);
}

LuaScript::LuaScript(const std::string &name, const LuaScript &script)
    : Script(script),
      state(nullptr),
      mName(name)
{
    init();
    loadString(script.getSource());
}

LuaScript::~LuaScript()
{
    shutdown();
}

//----------------------------------------------------------------

void LuaScript::init()
{
    /* Create new lua state. */
    state = luaL_newstate();

    /* Open all lua base libs */
    luaL_openlibs(state);

    /* Load bindings for ve */
    luaopen_ve(state);

    resetModulePath();
    pushScriptObject();

    lua_sethook(state, [](lua_State *l, lua_Debug *ar)
                       {
                            LuaScript *_this = LuaScript::getScriptObject(l);
                            if (_this && _this->getDebugCallback())
                            {
                                _this->getDebugCallback()(l, ar);
                            }
                       },
                LUA_MASKLINE, 0);
}

void LuaScript::shutdown()
{
    if (state)
    {
        lua_close(state);
    }
}

StringList LuaScript::trace() const
{
    StringList result;
    if (!state)
    {
        return result;
    }
    LuaStackGuard g(state);

    if (lua_isstring(state, -1))
    {
        result += lua_tostring(state, -1);
    }

    return result;
}

bool LuaScript::call(int argc, int resc) const
{
    if (!state)
    {
        return false;
    }
    LuaStackGuard g(state);

    int exitCode = lua_pcall(state, argc, resc, 0);
    try
    {
        return checkErr(exitCode);
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::call(int argc, int resc)).toString();
        throw;
    }
}

void LuaScript::pop(int count) const
{
    lua_pop(state, count);
}

std::string LuaScript::_getTypeName(int idx) const
{
    return SWIG_Lua_typename(state, idx);
}

//----------------------------------------------------------------

void LuaScript::resetModulePath()
{
    if (!state)
    {
        return;
    }

    LuaStackGuard g(state);
    lua_getglobal(state, "package");
    if (lua_type(state, -1) != LUA_TTABLE)
    {
        return;
    }

    lua_pushstring(state, "path");
    lua_pushstring(state, "");
    lua_settable(state, -3);
}

void LuaScript::pushScriptObject()
{
    if (!state)
    {
        return;
    }

    LuaStackGuard g(state);
    SWIG_NewPointerObj(state, this, SWIGTYPE_p_Script, LUA_OWNED_NO);
    lua_setglobal(state, SELF);

    // duplicate this into registry index
    // to prevent loss of contex due some code like
    // self = nil
    lua_pushstring(state, SELF);
    lua_pushlightuserdata(state, this);
    lua_settable(state, LUA_REGISTRYINDEX);
}

LuaScript *LuaScript::getScriptObject(lua_State *state)
{
    LuaStackGuard g(state);
    lua_pushstring(state, SELF);
    lua_gettable(state, LUA_REGISTRYINDEX);
    if (lua_isuserdata(state, -1))
    {
        void *scriptPtr = lua_touserdata(state, -1);
        return reinterpret_cast<LuaScript *>(scriptPtr);
    }
    return nullptr;
}

bool LuaScript::event(const Event *event)
{
    if (!state)
    {
        return false;
    }

    lua_getglobal(state, "event");
    if (lua_type(state, -1) != LUA_TFUNCTION)
    {
        // remove global from stack
        lua_pop(state, 1);
        return false;
    }

    SWIG_NewPointerObj(state, event, SWIGTYPE_p_Event, LUA_OWNED_NO);
    if (call(1, 1))
    {
        bool result = toBoolean(1);
        lua_pop(state, 1);
        return result;
    }

    return false;
}

//----------------------------------------------------------------

float LuaScript::toFloat(int idx) const
{
    return lua_tonumber(state, idx);
}

std::string LuaScript::toFloatStr(int idx) const
{
    std::ostringstream stream;
    stream << toFloat(idx);
    return stream.str();
}

bool LuaScript::toBoolean(int idx) const
{
    return (bool)lua_toboolean(state, idx);
}

std::string LuaScript::toBooleanStr(int idx) const
{
    if (toBoolean(idx))
    {
        return "true";
    }
    else
    {
        return "false";
    }
}

std::string LuaScript::toString(int idx) const
{
    return lua_tostring(state, idx);
}

//----------------------------------------------------------------

bool LuaScript::loadString(const std::string &src) const
{
    bool ret = false;

    try
    {
        ret = checkErr(luaL_loadstring(state, src.c_str()));
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::loadString(const std::string &src)).toString();
        throw;
    }

    return ret;
}

bool LuaScript::doString(const std::string &src) const
{
    bool ret = false;

    try
    {
        ret = loadString(src);
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::doString(const std::string &src)).toString();
        lua_pop(state, -1);
        throw;
    }

    try
    {
        run();
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::doString(const std::string &src)).toString();
        throw;
    }

    return ret;
}

//----------------------------------------------------------------

void LuaScript::run() const
{
    try
    {
        call(0, 0);
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::run()).toString();
        throw;
    }
}

bool LuaScript::compile()
{
    try
    {
        return loadString(getSource());
    }
    catch (LuaException &e)
    {
        e << VE_SCOPE_NAME(LuaScript::compile()).toString();
        throw;
    }
    return false;
}

//----------------------------------------------------------------

/*
LuaScript::Type LuaScript::getGlobal(const std::string &name)
{
    return LuaScript::Type(getGlobal<std::string>(name));
}

std::string LuaScript::getGlobal(const std::string &name)
{
    lua_getglobal(state, name.c_str());
    int type = lua_type(state, -1);

    std::string result;

    switch (type) {
    case LUA_TNIL:
        result = "nil";
        break;
    case LUA_TBOOLEAN:
        if (lua_toboolean(state, -1))
            result = "true";
        else
            result = "false";
        break;
    case LUA_TLIGHTUSERDATA:

    }
}
*/

Script::ScriptVar LuaScript::getGlobal(const std::string &globalName) const
{
    if (!state)
    {
        return ScriptVar();
    }
    LuaStackGuard g(state);

    lua_getglobal(state, globalName.c_str());
    const int stackTop = -1;
    const int type = lua_type(state, stackTop);

    /// TODO: err handling
    switch (type)
    {
    case LUA_TNONE:
        return ScriptVar(Script::ScriptVar::SVT_None); /// we needn't to pop
    case LUA_TNIL:
        return ScriptVar(Script::ScriptVar::SVT_Null);
        break;
    case LUA_TBOOLEAN:
        return ScriptVar(toBoolean(stackTop));
        break;
    case LUA_TLIGHTUSERDATA:
        /// TODO: raw pointer
        return ScriptVar();
        break;
    case LUA_TNUMBER:
        return ScriptVar(toFloat(stackTop));
        break;
    case LUA_TSTRING:
        return ScriptVar(toString(stackTop));
        break;
    case LUA_TTABLE:
    {
        std::string result;
        /** Push nil as zero index of table contents.
         ** We will start itertion from this. */
        lua_pushnil(state);

        /** Get value assigned to current key
         ** Stack after call:
         **     -1: value
         **     -2: key
         **     -3: iteratedTable */
        while (lua_next(state, stackTop) != 0)
        {
            /// Get type of value
            const int valIndex = -1;
            const int valType = lua_type(state, valIndex);

            switch (valType)
            {
            case LUA_TNONE:
            case LUA_TNIL:
                result += "nil;";
                break;
            case LUA_TBOOLEAN:
                result += toBooleanStr(valIndex) + ";";
                break;
            case LUA_TLIGHTUSERDATA:
                break;
            case LUA_TNUMBER:
                result += toFloatStr(valIndex) + ";";
                break;
            case LUA_TSTRING:
                result += toString(valIndex) + ";";
                break;
            case LUA_TTABLE:
                break;
            case LUA_TFUNCTION:
                break;
            case LUA_TTHREAD:
                break;
            }

            lua_pop(state, 1);
        }

        return result;
    }
    case LUA_TFUNCTION:
        /// TODO: function formating
        break;
    case LUA_TUSERDATA:
        /// TODO: userdata formating
        break;
    case LUA_TTHREAD:
        /// TODO: thread formating
        break;
    }

    lua_pop(state, 1);

    return ScriptVar();
}

int LuaScript::getGlobalType(const std::string &globalName) const
{
    if (!state)
    {
        return LUA_TNONE;
    }
    LuaStackGuard g(state);
    lua_getglobal(state, globalName.c_str());
    const int ret = lua_type(state, -1);
    pop();
    return ret;
}

//----------------------------------------------------------------

const StringSet &LuaScript::getKeyWords() const
{
    static const StringSet KeyWords =
    {
        "and",      "break",    "do",       "else",
        "elseif",   "end",      "false",    "for",
        "function", "if",       "in",       "local",
        "nil",      "not",      "or",       "repeat",
        "return",   "then",     "true",     "until",
        "while"
    };

    return KeyWords;
}

ScriptCompletions LuaScript::getCompletion(const std::string &cmd, bool caseSensetive) const
{    
    ScriptCompletions output = Script::getCompletion(cmd, caseSensetive);
    if (!state)
    {
        return output;
    }

    LuaStackGuard g(state);

    const size_t dotPos = cmd.find_last_of(".:");
    const std::string tableName = cmd.substr(0, dotPos);
    const std::string keyName = cmd.substr(dotPos + 1);

    if (dotPos != std::string::npos)
    {
        if (getGlobalType(cmd) == LUA_TFUNCTION)
        {
            output.getAll().insert({ ScriptCompletions::Brackets, cmd + "()" });
        }

        if (dotPos != 0)
        {
            lua_getglobal(state, tableName.c_str());

            const char separator = cmd[dotPos];

            switch (lua_type(state, -1))
            {
            case LUA_TTABLE:
                output += getTableCompletion(cmd, tableName, '.', keyName, caseSensetive);
                break;
            case LUA_TUSERDATA:
                if (lua_getmetatable(state, -1))
                {
                    // get setters table
                    if (separator == '.')
                    {
                        lua_pushstring(state, ".set");
                        lua_rawget(state, -2);
                        if (lua_istable(state, -1))
                        {
                            output += getTableCompletion(cmd, tableName, separator, keyName, caseSensetive);
                        }
                        pop();

                        // get getters table
                        lua_pushstring(state, ".get");
                        lua_rawget(state, -2);
                        if (lua_istable(state, -1))
                        {
                            output += getTableCompletion(cmd, tableName, separator, keyName, caseSensetive);
                        }
                        pop();
                    }
                    else
                    {
                        // get function table
                        lua_pushstring(state, ".fn");
                        lua_rawget(state, -2);
                        if (lua_istable(state, -1))
                        {
                            output += getTableCompletion(cmd, tableName, separator, keyName, caseSensetive);
                        }
                        pop();
                    }
                }

                std::string tName = _getTypeName(-1);
                tName += " ";
            }
            pop();
        }
    }
    // try to get completion for global symbol
    else
    {
        lua_pushglobaltable(state);

        // push nil (initial key)
        lua_pushnil(state);

        while (lua_next(state, -2))
        {
            // lua_next pushes key & value on stack
            // key (index -2) & value (index -1)

            // we need only string-typed keys
            if (lua_type(state, -2) == LUA_TSTRING)
            {
                const std::string completionCandidat = toString(-2);
                if (completionCandidat.size() > keyName.size() &&
                    Ogre::StringUtil::startsWith(completionCandidat, tableName, false))
                {
                    output.getAll().insert({ ScriptCompletions::Variables, completionCandidat });
                }
            }

            // remove value
            // lua_next expects key at -1
            pop();
        }

        // remove globaltable
        pop();
    }

    StringSet usedStrings;

    auto it = output.getAll().begin();

    while (it != output.getAll().end())
    {
        const std::string &completionCandidat = it->second;
        if (!usedStrings.insert(completionCandidat).second)
        {
            it = output.getAll().erase(it);
        }
        else
        {
            ++it;
        }
    }
    return output;
}

ScriptCompletions LuaScript::getTableCompletion(const std::string &cmd,
                                                const std::string &tableName,
                                                const char separator,
                                                const std::string &keyName,
                                                bool caseSensetive) const
{
    VE_UNUSED(caseSensetive);

    ScriptCompletions output;

    if (!state)
    {
        return output;
    }

    LuaStackGuard g(state);

    // first key
    lua_pushnil(state);

    bool foundCompletion = false;
    // table is now below nil (index -2)
    while (lua_next(state, -2))
    {
        // lua_next pushes key & value on stack
        // key (index -2) & value (index -1)

        // we need only string-typed keys
        if (lua_type(state, -2) == LUA_TSTRING)
        {
            const std::string completionCandidat = toString(-2);
            if (keyName.empty() ||
                (completionCandidat.size() > keyName.size() &&
                 Ogre::StringUtil::startsWith(completionCandidat, keyName, false)))
            {
                output.getAll().insert({ ScriptCompletions::Variables,
                                         tableName + separator + completionCandidat });
                foundCompletion = true;
            }
        }

        // remove value to make key on top for next iteration
        pop();
    }

    if (!foundCompletion)
    {
        lua_pushstring(state, keyName.c_str());
        lua_gettable(state, -2);
        if (lua_type(state, -1) == LUA_TFUNCTION)
        {
            output.getAll().insert({ ScriptCompletions::Brackets, cmd + "()" });
        }
    }

    return output;
}

//----------------------------------------------------------------

std::string &LuaScript::output()
{
    return mOutput;
}

//----------------------------------------------------------------

bool LuaScript::checkErr(int code) const
{
    static const std::string comment = "A script error occurred";
    const std::string trace_ =
            VE_SCOPE_NAME(LuaScript::checkErr(int code)).toString() + "\n" + trace().join("\n");

    switch (code)
    {
    case 0:
        return true;
    case LUA_ERRRUN:
        LuaRuntimeError(state, trace_, comment);
        return false;
    case LUA_ERRSYNTAX:
        LuaSyntaxError(state, trace_, comment);
        return false;
    case LUA_ERRMEM:
        LuaMemoryError(state, trace_, comment);
        return false;
    case LUA_ERRERR:
        LuaCallbackError(state, trace_, comment);
        return false;
    default:
        LuaUnknownError(state, trace_, comment);
    }

    return false;
}

/// =================================================================================

LuaScript::LuaStackGuard::LuaStackGuard(lua_State *state_)
    : state(state_),
      savedStackSize(-1)
{
    if (state)
    {
        savedStackSize = lua_gettop(state);
    }
}

LuaScript::LuaStackGuard::~LuaStackGuard()
{
    if (state)
    {
        const int currentStackSize = lua_gettop(state);
        const int stackSizeDelta = currentStackSize - savedStackSize;
        if (stackSizeDelta > 0)
        {
            lua_pop(state, stackSizeDelta);
        }
    }
}
