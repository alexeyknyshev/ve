#include "luascriptfactory.h"

#include <script/lua/luascript.h>

Script *LuaScriptFactory::createScript(const std::string &resourceName,
                                       const std::string &resourceGroupName)
{
    return createScript(resourceName,
                        FileSystem::openResource(resourceName, resourceGroupName));
}

Script *LuaScriptFactory::createScript(const std::string &scriptName,
                                       const Ogre::DataStreamPtr &stream)
{
    LuaScript *script = nullptr;
    try
    {
       script = new LuaScript(scriptName, stream->getAsString());
    }
    catch (ScriptExeception &e)
    {
        e << VE_SCOPE_NAME(LuaScriptFactory::createScript(const std::string &scriptName,
                                                          const Ogre::DataStreamPtr &stream)).toString();
        throw;
    }
    return script;
}

Script *LuaScriptFactory::createScriptFromSource(const std::string &name,
                                                 const std::string &src)
{
    LuaScript *script = nullptr;
    try
    {
        script = new LuaScript(name, src);
    }
    catch (ScriptExeception &e)
    {
        e << VE_SCOPE_NAME(LuaScriptFactory::createScriptFromSource(const std::string &src)).toString();
        throw;
    }

    return script;
}

const std::string &LuaScriptFactory::getScriptTypeName() const
{
    const static std::string Name = "Lua";
    return Name;
}
