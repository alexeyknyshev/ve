table.clone = function (source)
    if not source or not next(source) then
        return {}
    end

    local result = {}
    for k, v in pairs(source) do
        result[k] = v
    end

    return result
end
