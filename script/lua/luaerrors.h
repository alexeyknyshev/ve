#ifndef LUAERRORS_H
#define LUAERRORS_H

#include <global.h>
#include <script/scripterrors.h>
#include <lua.hpp>

#include <framework/logger/logger.h>

class LuaException : public ScriptExeception
{
public:
    LuaException(lua_State *state, const std::string &backTrace,
               const std::string &comment)
        : ScriptExeception(backTrace, comment),
          mState(state)
    { }

    lua_State* state() const
    {
        return mState;
    }

private:
    lua_State* mState;
};

class LuaError : public ScriptError
{
public:
    void _raise(lua_State *state, const std::string &backtrace,
                const std::string &message, const std::string &type)
    {
        std::stringstream stream;
        stream << state;

        logger()->logClassMessage(VE_CLASS_NAME(LuaError),
                                  VE_SCOPE_NAME(_raise(lua_State *state, const std::string &backtrace,
                                                       const std::string &message, const std::string &type)),
                                  "[" + type + "(state: " + stream.str() + ")] " +
                                  message + "\nTrace:\n" + backtrace,
                                  Logger::LOG_LEVEL_CRITICAL,
                                  Logger::LOG_COMPONENT_SCRIPT);
    }

    explicit LuaError(lua_State *state, const std::string &backTrace,
                      const std::string &comment, bool raise = true)
        : ScriptError(backTrace, comment, false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaError).toString());
#endif
        }
    }
};

class LuaSyntaxException : public LuaException
{
public:
    LuaSyntaxException(lua_State *state, const std::string &backTrace,
                       const std::string &comment)
        : LuaException(state, backTrace, comment)
    { }
};

class LuaSyntaxError : public LuaError
{
public:
    explicit LuaSyntaxError(lua_State* state, const std::string &backTrace,
                            const std::string &comment, bool raise = true)
        : LuaError(state, backTrace, comment, false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaSyntaxException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaError).toString());
#endif
        }
    }
};

class LuaMemoryException : LuaException
{
public:
    LuaMemoryException(lua_State *state, const std::string &backTrace,
                       const std::string &comment)
        : LuaException(state, backTrace, comment)
    { }
};

class LuaMemoryError : public LuaError
{
public:
    explicit LuaMemoryError(lua_State *state, const std::string &backTrace,
                            const std::string &comment, bool raise = true)
        : LuaError(state, backTrace, comment, false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaMemoryException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaMemoryError).toString());
#endif
        }
    }
};

class LuaRuntimeException : public LuaException
{
public:
    LuaRuntimeException(lua_State *state, const std::string &backTrace,
                        const std::string &comment)
        : LuaException(state, backTrace, comment)
    { }
};

class LuaRuntimeError : public LuaError
{
public:
    explicit LuaRuntimeError(lua_State *state, const std::string &backTrace,
                             const std::string &comment, bool raise = true)
        : LuaError(state, backTrace, comment, false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaRuntimeException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaRuntimeError).toString());
#endif
        }
    }
};

class LuaCallbackException : public LuaException
{
public:
    LuaCallbackException(lua_State *state, const std::string &backTrace,
                         const std::string &comment)
        : LuaException(state, backTrace, comment)
    { }
};

class LuaCallbackError : public LuaError
{
public:
    explicit LuaCallbackError(lua_State *state, const std::string &backTrace,
                              const std::string &comment, bool raise = true)
        : LuaError(state, backTrace, comment, raise)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaCallbackException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaCallbackError).toString());
#endif
        }
    }
};

class LuaUnknownException : public LuaException
{
public:
    LuaUnknownException(lua_State *state, const std::string &backTrace,
                        const std::string &comment)
        : LuaException(state, backTrace, comment)
    { }
};

class LuaUnknownError : public LuaError
{
public:
    explicit LuaUnknownError(lua_State *state, const std::string &backTrace,
                             const std::string &comment, bool raise = true)
        : LuaError(state, backTrace, comment, false)
    {
        if (raise) {
#ifndef VE_NO_EXCEPTIONS
            throw LuaUnknownException(state, backTrace, comment);
#else
            _raise(state, backTrace, comment, VE_CLASS_NAME(LuaUnknownError).toString());
#endif
        }
    }
};

#endif // LUAERRORS_H
