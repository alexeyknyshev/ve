#ifndef LUASCRIPT_H
#define LUASCRIPT_H

#include <script/script.h>
#include <script/lua/luaerrors.h>

#include <functional>

/** Lua scripting support */

class LuaScript : public Script
{
public:
    VE_DECLARE_TYPE_INFO(LuaScript)

    const std::string &getName() const
    {
        return mName;
    }

    bool event(const Event *event);

    LuaScript(const std::string &name, const std::string &src);
    LuaScript(const std::string &name, const LuaScript &script);

    virtual ~LuaScript();

    /// @throws
    bool checkErr(int code) const;

    Script::ScriptVar getGlobal(const std::string &globalName) const override;

    int getGlobalType(const std::string &globalName) const;


    const StringSet &getKeyWords() const override;
    ScriptCompletions getCompletion(const std::string &cmd,
                                    bool caseSensetive) const override;

    std::string &output() override;

    /** Loads source from @src
      * @throws LuaError
      */
    bool loadString(const std::string &src) const;

    /** Executes code from @src
      * @throws LuaError
      */
    bool doString(const std::string &src) const;

    /** Runs script (global chunck)
      * @throws LuaError
      */
    void run() const override;

    bool compile() override;

    const std::string &type() const override;

    lua_State *state;

protected:
    /** Setups lua state and loads libs */
    void init();

    /** Destroys lua state, unloads libs and frees heap */
    void shutdown();

    virtual StringList trace() const override;

    float toFloat(int idx) const;
    std::string toFloatStr(int idx) const;

    bool toBoolean(int idx) const;
    std::string toBooleanStr(int idx) const;

    std::string toString(int idx) const;

    // table on top of the stack
    ScriptCompletions getTableCompletion(const std::string &cmd,
                                         const std::string &tableName,
                                         const char separator,
                                         const std::string &keyName,
                                         bool caseSensetive) const;

    /// debugging callbacks
    typedef std::function<void(lua_State *l, lua_Debug *ar)> LuaDebugCallback;

    inline void setDebugCallback(LuaDebugCallback callback) { mDebugCallback = callback; }
    const LuaDebugCallback &getDebugCallback() const { return mDebugCallback; }

private:
    /** lua stack guard class
     *  @class
     *      Saves lua stack state while construction
     *      and restores after destruction
     *  @example
     *      {
     *      ... some code ...
     *      LuaStackGuard g(state) // save stack of lua_State state
     *      ... stack manipulation such as:
     *      lua_pushstring(state, "myString")
     *      lua_getglobal(state, "luaTable");
     *      ...
     *      } // destruction of g (stack guard) -> stack restored
     * */
    class LuaStackGuard
    {
    public:
        LuaStackGuard(lua_State *state);

        ~LuaStackGuard();

    private:
        lua_State *const state;
        int savedStackSize;
    };

    bool call(int argc, int resc) const;
    void pop(int count = 1) const;
    std::string _getTypeName(int idx) const;

    void resetModulePath();
    void pushScriptObject();

    static LuaScript *getScriptObject(lua_State *state);

    std::string mName;
    std::string mOutput;

    std::function<void(lua_State *state, lua_Debug *ar)> mDebugCallback;
};

#endif // LUASCRIPT_H
