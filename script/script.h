#ifndef SCRIPT_H
#define SCRIPT_H

#include <framework/object.h>
#include <framework/container.h>
#include <framework/stringlist.h>
#include <OGRE/OgreSharedPtr.h>

class Script;

class ScriptCompletions
{
    friend class Script;

public:
    ScriptCompletions() = default;

    /*
    ScriptCompletions &operator=(ScriptCompletions &&o)
    {
        mCompletions = std::move(o.mCompletions);
        return *this;
    }

    ScriptCompletions(ScriptCompletions &&o)
        : mCompletions(std::move(o.mCompletions))
    { }
    */

    enum CompletionType
    {
        KeyWords = 0,
        Brackets,
        Variables
    };

    typedef std::multimap<CompletionType, std::string> Completions;

    inline StringSet get(CompletionType type) const
    {
        StringSet result;
        const auto range = mCompletions.equal_range(type);
        for (auto it = range.first; it != range.second; ++it)
        {
            result.insert(it->second);
        }
        return result;
    }

    inline Completions &getAll()
    {
        return mCompletions;
    }

    inline const Completions &getAll() const
    {
        return mCompletions;
    }

    ScriptCompletions &operator+=(const ScriptCompletions &other)
    {
        mCompletions.insert(other.mCompletions.begin(), other.mCompletions.end());
        return *this;
    }

protected:
    Completions mCompletions;
};

/** Script is abstracton layer for low level scripting
 ** implementations like Lua */

class Script : public Object
{
public:    
    VE_DECLARE_TYPE_INFO(Script)

    explicit Script(const std::string &scriptName, const std::string &src = VE_STR_BLANK);
    explicit Script(const Script &script);

    /** API for logging from scripts */
    void log(const std::string &msg, int level) const;

    virtual ~Script();

    /** In the derived class must execute global chunk or
      * start executing from any enty point (like main)
      */
    virtual void run() const = 0;

    /** @returns The name representing type of script
      * @note Name must match the name of ScriptFactory registered in
      * ScriptHandler
      * @see scripthandler.h
      */
    virtual const std::string &type() const = 0;

    class ScriptVar
    {
    public:
        ScriptVar();
        ~ScriptVar();

        ScriptVar(const ScriptVar &other);

        enum Type
        {
            SVT_String = 0,
            SVT_Null,
            SVT_Bool,
            SVT_Number,
            SVT_Address,
            SVT_None
        };

        ScriptVar(Type type); // type without data
        ScriptVar(bool boolean);
        ScriptVar(int number);
        ScriptVar(float number);
        ScriptVar(const std::string &str);
        ScriptVar(std::string &&str);

        inline Type getType() const
        {
            return mType;
        }

        std::string toString() const;
        float toNumber(bool *ok) const;
        uint64_t toAddress() const;

    private:
        const Type mType;

        union Data
        {
            std::string *str;
            float num;
            uint64_t addr;
            bool b;
        };

        Data mData;
    };

    virtual Script::ScriptVar getGlobal(const std::string &name) const = 0;

    virtual std::string &output() = 0;

    virtual const std::string &getSource() const;
    void setSource(const std::string &src);

    virtual const StringSet &getKeyWords() const = 0;

    virtual ScriptCompletions getCompletion(const std::string &cmd,
                                            bool caseSensitive = false) const;

    virtual bool compile() = 0;

protected:
    /** Debug information in case of failure */
    virtual StringList trace() const = 0;

    void dirty();

private:
    std::string mSrc;
    bool mDirty;
};

#endif // SCRIPT_H
