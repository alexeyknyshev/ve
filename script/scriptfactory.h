#ifndef SCRIPTFACTORY_H
#define SCRIPTFACTORY_H

#include <global.h>

#include <framework/filesystem.h>

class Script;

class ScriptFactory
{
public:
    virtual ~ScriptFactory() { }

    virtual Script *createScript(const std::string &resourceName,
                                 const std::string &resourceGroupName = FileSystem::RG_DEFAULT) = 0;

    virtual Script *createScript(const std::string &scriptName,
                                 const Ogre::DataStreamPtr &stream) = 0;

    virtual Script *createScriptFromSource(const std::string &scriptName,
                                           const std::string &src) = 0;

    virtual const std::string &getScriptTypeName() const = 0;
};

#endif // SCRIPTFACTORY_H
