#include "script.h"

#include <framework/stringconverter.h>

Script::Script(const std::string &scriptName, const std::string &src)
{
    (void)scriptName;
    setSource(src);
}

Script::Script(const Script &script)
{
    setSource(script.getSource());
}

Script::~Script()
{ }

void Script::log(const std::string &msg, int level) const
{
    Object::log(msg, level, Logger::LOG_COMPONENT_SCRIPT);
}

const std::string &Script::getSource() const
{
    return mSrc;
}

void Script::setSource(const std::string &src)
{
    mSrc = src;
    dirty();
}

void Script::dirty()
{
    mDirty = true;
}

ScriptCompletions Script::getCompletion(const std::string &cmd,
                                        bool caseSensitive) const
{
    ScriptCompletions result;

    for (const std::string &keyWord : getKeyWords())
    {
        if (Ogre::StringUtil::startsWith(keyWord, cmd, caseSensitive))
        {
            result.mCompletions.insert({ ScriptCompletions::KeyWords, keyWord });
        }
    }

    return result;
}

/// ====== ScriptVar ======

Script::ScriptVar::ScriptVar(const ScriptVar &other)
    : mType(other.getType()),
      mData(other.mData)
{
}

Script::ScriptVar::ScriptVar(Type type)
    : mType(type)
{
    switch (type) {
    case SVT_String:
        mData.str = new std::string;
        break;
    case SVT_Null:
    case SVT_None:
        mData.str = nullptr;
        break;
    case SVT_Bool:
        mData.b = false;
        break;
    case SVT_Number:
        mData.num = 0.0f;
        break;
    case SVT_Address:
        mData.addr = 0;
        break;
    }
}

Script::ScriptVar::ScriptVar(bool boolean)
    : mType(SVT_Bool)
{
    mData.b = boolean;
}

Script::ScriptVar::ScriptVar(int number)
    : mType(SVT_Number)
{
    mData.num = (float)number;
}

Script::ScriptVar::ScriptVar(float number)
    : mType(SVT_Number)
{
    mData.num = number;
}

Script::ScriptVar::ScriptVar(const std::string &str)
    : mType(SVT_String)
{
    mData.str = new std::string(str);
}

Script::ScriptVar::ScriptVar(std::string &&str)
    : mType(SVT_String)
{
    mData.str = new std::string(std::move(str));
}

Script::ScriptVar::ScriptVar()
    : mType(SVT_None)
{
    mData.str = nullptr;
}

Script::ScriptVar::~ScriptVar()
{
    if (getType() == SVT_String)
    {
        delete mData.str;
    }
}

std::string Script::ScriptVar::toString() const
{
    switch (getType())
    {
    case SVT_String:
    {
        if (mData.str)
        {
            return *mData.str;
        }
        break;
    }
    case SVT_Null:
        return "null";
    case SVT_Bool:
        return mData.b ? "true" : "false";
    case SVT_Number:
        return StringConverter::toString(mData.num);
    case SVT_Address:
    {
        std::ios_base::fmtflags ff = std::cout.flags();
        ff |= std::cout.hex;
        return StringConverter::toString((unsigned int)mData.addr, 0, ' ', ff);
    }
    case SVT_None:
        break;
    }

    return VE_STR_BLANK;
}
