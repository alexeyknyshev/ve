@std_string
@std_map

%include "veStringList.swg"

%module ve
%{
#include <framework/logger/logger.h>
#include <framework/object.h>
#include <renderer/model.h>
#include <renderer/modeloptions.h>
#include <renderer/renderer.h>
#include <game/game.h>

void exit()
{
    game()->exit();
}

const int LOG_LEVEL_DEBUG    = Logger::LOG_LEVEL_DEBUG;
const int LOG_LEVEL_INFO     = Logger::LOG_LEVEL_INFO;
const int LOG_LEVEL_WARNING  = Logger::LOG_LEVEL_WARNING;
const int LOG_LEVEL_CRITICAL = Logger::LOG_LEVEL_CRITICAL;
%}

%include <../../framework/object.h>
%include <../../framework/logger/logger.h>
%include <../../framework/updatable.h>
%include <../../renderer/model.h>
%include <../../renderer/modeloptions.h>

%feature("notabstract") Renderer;
%include <../../renderer/renderer.h>

%include <OgreMath.swg>
%include <OgreVector3.swg>
%include <OgreVector4.swg>
%include <OgreQuaternion.swg>
%include <OgrePlane.swg>

%include <veSizeU.swg>
%include <veSizeF.swg>

%include <Script.swg>

void exit();

const int LOG_LEVEL_DEBUG;
const int LOG_LEVEL_INFO;
const int LOG_LEVEL_WARNING;
const int LOG_LEVEL_CRITICAL;

%include <models/PhysicsModel.swg>
%include <models/ProceduralModel.swg>
%include <models/PlaneModel.swg>
