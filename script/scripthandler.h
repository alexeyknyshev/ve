#ifndef SCRIPTHANDLER_H
#define SCRIPTHANDLER_H

#include <OGRE/OgreNameGenerator.h>
#include <framework/filesystem.h>

class Script;
class ScriptFactory;

class ScriptHandler
{
public:
    ScriptHandler();
    virtual ~ScriptHandler();

    /** Creates Script from given code source.
      * @returns script of @type if code source is valid,
      * nullptr otherwise
      */
    Script *createScript(const std::string &type,
                         const std::string &resourceName,
                         const std::string &resourceGroupName =
                               FileSystem::RG_DEFAULT) const;

    Script *createScript(const std::string &type,
                         const std::string &name,
                         const Ogre::DataStreamPtr &stream) const;
                         
    Script *createScriptFromSource(const std::string &type,
                                   const std::string &src) const;

    /** Deletes @script
      * @returns false if ScriptFactory with type == script->type()
      * hasn't been registed
      */
//    bool deleteScript(Script *script);

    typedef std::map<std::string, ScriptFactory *> ScriptFactoryMap;
    typedef std::pair<std::string, ScriptFactory *> ScriptFactoryMapValue;

    bool hasRegistredScriptFactory(const std::string &name) const;

    void registerScriptFactory(ScriptFactory *factory);
    bool deleteRegistredScriptFactory(const std::string &type);
    void deleteRegistredScriptFactories();

protected:
    inline const ScriptFactoryMap &getScriptFactoryMap() const
    {
        return mScriptFactoryMap;
    }

private:
    mutable Ogre::NameGenerator mScriptNameGenerator;
    ScriptFactoryMap mScriptFactoryMap;
};

#endif // SCRIPTHANDLER_H
