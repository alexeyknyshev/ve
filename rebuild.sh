#!/bin/bash -efu

rootdir=$(cd $(dirname "$0"); pwd)
SELF=$(basename "$0")

echo "$SELF: --- ve rebuilding ---"

echo "$SELF: $rootdir/distclean.sh"
$rootdir/distclean.sh

echo "$SELF: $rootdir/swig_all.sh"
$rootdir/swig_all.sh

echo "$SELF: $rootdir/build.sh"
$rootdir/build.sh

echo "$SELF: --- ve rebuilding completed ---"
