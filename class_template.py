#!/usr/bin/python

import sys, getopt

def printHelp():
	print "usage: class_template.py CLASSNAME\n"
	print "\t-h, --help\t display this help and exit"
	print "\t--header-only\t generate only header without *.cpp file"
	print "\t--cpp-only\t genearate only cpp without declaration in *.h file"
	print ""
	print "Generates C++ header with include guard and (optional) cpp files in current dir with template for C++ class named CLASSNAME."
	print "\tNote: generated files' names are downcased"

def main(argv):
	try:
		opts, args = getopt.getopt(argv, 'h', [ "help", "header-only", "cpp-only" ])
	except getopt.GetoptError as err:
		print str(err) #will print something like "option -a not recognized"
		printHelp()
		sys.exit(2)
	
	if not args:	
		printHelp()
		sys.exit(0)

	generateHeaderFile = True
	generateCppFile    = True

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			printHelp()
			sys.exit(0)
		if opt == "--cpp-only":
			generateHeaderFile = False
		if opt == "--header-only":
			generateCppFile = False
	
	className = args[0]
	headerName = className.lower() + ".h"
	sourceName = className.lower() + ".cpp"

	if generateHeaderFile:
		try:
			header = open(headerName, "w")
			try:
				header.write("#ifndef " + className.upper() + "_H\n")
				header.write("#define " + className.upper() + "_H\n")
				header.write("\n")
				header.write("class " + className + "\n")
				header.write("{\n")
				header.write("\n")
				header.write("};\n")
				header.write("\n")
				header.write("#endif // " + className.upper() + "_H\n")
			finally:
				header.close()
		except IOError:
			print "Cannot open " + headerName + " for writing"
			sys.exit(1)

	if generateCppFile:
		try:
			source = open(sourceName, "w")
			try:
				source.write("#include \"" + headerName + "\"\n")
			finally:
				source.close()
		except IOError:
			print "Cannot open " + className.lower() + ".cpp for writing"
			sys.exit(1)
	

if __name__ == "__main__":
	main(sys.argv[1:])
