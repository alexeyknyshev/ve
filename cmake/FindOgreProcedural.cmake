# - Find OgreProcedural
# Find the native OgreBullet includes and library
#
#   OGREPROCEDURAL_FOUND        - True if OgreProcedural found.
#   OGREPROCEDURAL_INCLUDE_DIRS - where to find includes
#   OGREPROCEDURAL_LIBRARIES    - List of libraries when using OgreProcedural.
#

if(OGREPROCEDURAL_INCLUDE_DIR)
    # Already in cache, be silent
    set(OgreProcedural_FIND_QUIETLY TRUE)
endif(OGREPROCEDURAL_INCLUDE_DIR)

find_path(OGREPROCEDURAL_INCLUDE_DIR Procedural.h
          PATH_SUFFIXES OgreProcedural)

find_library(OGREPROCEDURAL_LIB OgreProcedural)

set(OGREPROCEDURAL_INCLUDE_DIRS ${OGREPROCEDURAL_INCLUDE_DIR})
set(OGREPROCEDURAL_LIBRARIES ${OGREPROCEDURAL_LIB})

# handle the QUIETLY and REQUIRED arguments and set OGREBULLET_FOUND to TRUE if
# all listed variables are TRUE
include("FindPackageHandleStandardArgs")
find_package_handle_standard_args(OgreProcedural DEFAULT_MSG OGREPROCEDURAL_INCLUDE_DIR OGREPROCEDURAL_LIBRARIES)

mark_as_advanced(OGREPROCEDURAL_INCLUDE_DIR OGREPROCEDURAL_LIBRARIES)

