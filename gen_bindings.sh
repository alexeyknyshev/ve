#!/bin/bash

SELF=$(basename "$0")
project_root=`dirname $(readlink -e $0)`

langs=(lua)
lang_default='lua'

function usage()
{
    echo "usage:"
    echo -e "\t$SELF [-i first.i ...] [-l lang1 ...] \tgenerate bindings for files first.i ... and languages lang1 ...\n"
    echo -e "\t$SELF -p|--preview\t\t\t\tlist files which will be used in after geneartion"
    echo -e "\t$SELF -h|--help\t\t\t\tshow this help\n"
    echo "avalible langs:"
    echo -e "\t$langs\n"
    echo "note:"
    echo -e "\tif no option -i provided bindings will be generated for all *.i files below project root dir: $project_root. wrap_* files are ignored."
    echo -e "\tif no option -l provided bindings will be generated for default lang: $lang_default"
}

function find_interface_files()
{
    result=()
    find . -name '*.i' -not -name 'wrap_*' -type f -print 2>/dev/null |
    while read path
    do
        echo $path
        result+=($path)
    done
    echo $result
}

function preview()
{
    find_interface_files
    #echo $interface_files
}

lang='lua'

if [[ $# == 1 ]]
then
    case "$1" in
        -h|--help)
            usage
            exit
            ;;
        -p|--preview)
            preview
            exit
            ;;
        *)
            echo "$SELF: unknown option \"$1\""
            usage
            exit 1
            ;;
    esac
fi

echo "$SELF: --- ve generating bindings (swig) ---"

sw=$(which swig2.0)

if [ $? -eq 0 ]; then
    echo -e "$SELF: swig excutable found:\t\t$sw"
else
    echo "$SELF: swig excutable has not found!"
    echo "$SELF: swig required but not installed... Aborting"
    echo "$SELF: --- ve generating bindings (swig) FAILED! ---"
    exit 1
fi

sw_version=`$sw -version | grep -i 'version' | awk '{print $3}'`
#echo $sw_version

sw_version_major=`echo $sw_version | awk 'BEGIN { FS = "." } ; { print $1 }'`
#echo $sw_version_major

string_res=''
map_res=''

swig_tempalte_dir=`$sw -swiglib`
#echo $swig_tempalte_dir

if grep -q "darwin" <<< "$OSTYPE"; then
    string_res=$(find /opt/local/share/swig* -name 'std_string.i' -print 2>/dev/null | grep lua)
    map_res=$(find /opt/local/share/swig* -name 'std_map.i' -print 2>/dev/null | grep lua)
else
    string_res=$(find $swig_tempalte_dir/$lang -name 'std_string.i' -type f -print 2>/dev/null)
    map_res=$(find $swig_tempalte_dir/$lang -name 'std_map.i' -type f -print 2>/dev/null)
fi

string_i=${string_res[0]}
map_i=${map_res[0]}

echo -e "$SELF: string template found:\t\t$string_i"
echo -e "$SELF: map template found:\t\t$map_i"

stdstring="%include <$string_i>"
stdmap="%include <$map_i>"

std_dir="$swig_tempalte_dir/std"
echo "$SELF: std_dir: $std_dir"

find . -name '*.i' -not -name 'wrap_*' -type f -print 2>/dev/null |
while read path
do
        echo -e "$SELF: parsing swig interface file:\t\t$path"

        workpath=$path.work
        rm -f $workpath

        cp $path $workpath
        sed -e "s|@std_string|$stdstring|" -i $workpath
        sed -e "s|@std_map|$stdmap|" -i $workpath

        bname=`basename $path`
        gen="script/$lang/bindings/${bname%.i}.cc"
        $sw -I$std_dir -nodefaultctor -c++ -Wall -$lang -o $gen $workpath
        if [ $? -eq 0 ]
        then
                echo -e "$SELF: cxx generated file:\t\t$gen"
        else
                echo -e "$SELF: cxx file generation failed:\t\t$gen"
                echo -e "$SELF: \tinput file: \t$workpath"
                echo -e "$SELF: \torig file: \t$path"
                exit 1
        fi

        rm -f $workpath
done

echo "$SELF: --- ve generating bindings (swig) completed ---"
