#!/bin/bash

rootdir=$(cd $(dirname "$0"); pwd)
SELF=$(basename "$0")

cd $rootdir
echo "cd $(pwd)"

echo "$SELF: --- ve building ---"
cmake . && VERBOSE=1 make -j2
echo "$SELF: --- ve building completed ---"
