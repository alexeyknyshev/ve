#!/bin/bash -efu

rootdir=$(cd $(dirname "$0"); pwd)
SELF=$(basename "$0")

echo "$SELF: --- ve distcleaning ---"

cd $rootdir
echo "$SELF: cd $(pwd)"

echo "$SELF: ./rm_bindings.sh"
./rm_bindings.sh

echo "$SELF: rm -f ve.cbp"
rm -f ve.cbp

echo "$SELF: rm -f CMakeLists.txt.user*"
rm -f CMakeLists.txt.user*

echo "$SELF: rm -f CMakeCache.txt"
rm -f CMakeCache.txt

echo "$SELF: rm -rf CMakeFiles/"
rm -rf CMakeFiles/

echo "$SELF: rm -f cmake_install.cmake"
rm -f cmake_install.cmake

echo "$SELF: rm -f Makefile"
rm -f Makefile

echo "$SELF: --- ve distcleaning completed ---"
