#ifndef MODELOPTIONS_H
#define MODELOPTIONS_H

#include <string>

namespace Ogre
{
    class SceneNode;
}

class ModelOptions
{
    friend class RigidBody;

public:
    ModelOptions();

    std::string getFallbackModelNodeName() const
    {
        return modelName + "_node";
    }

    std::string getFallbackMeshName() const
    {
        return modelName + "_mesh";
    }

    enum ModelType
    {
        MT_Static = 0,
        MT_Dynamic
    };

    ModelType modelType;

    std::string modelName;
    std::string meshName;
    Ogre::SceneNode *modelNode;
    Ogre::SceneNode *parentNode;
    std::string resourceGroup;
    bool addToScene;
    bool nodeBased;
};

#endif // MODELOPTIONS_H
