#ifndef RENDERER_H
#define RENDERER_H

#include <global.h>

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreRoot.h>

#include <framework/object.h>
#include <framework/params.h>

#include <framework/container.h>
#include <framework/stringlist.h>

class RenderWindowOptions;
class BaseCamera;

class RenderView;
class RenderViewBackend;

class UiManager;

namespace ve
{
    template <typename Type> class SizeT;
    typedef SizeT<uint32_t> SizeU;
}

/** Core rendering object that handles all rendering
 ** process and other rendering subsystems, responsible
 ** for creation low level components like viewports,
 ** rendering views (windows), scene
 ** managers and so on */

class Renderer :
#ifndef SWIG
    public Ogre::Root,
#endif
    public Object
{
    friend class RenderView;
    friend class RenderViewBackend;
    friend class BaseCamera;

public:
#ifndef SWIG
    VE_DECLARE_TYPE_INFO(Renderer)

    Renderer(const ParamsMulti &options);
    virtual ~Renderer();

    const std::string &getName() const override
    {
        static const std::string Name = "renderer";
        return Name;
    }

    void log(const std::string &msg, int level) const;

    /** Resets renderer (do it before renderOneFrame)
      * @returns true if RenderSystem registred, false otherwise
      */
    bool reset();

    /** Renders one frame and updates all RenderWindows
      */
    bool renderOneFrame();

    /** Overriden to do nothing
      * Use Game::exit to interrupt event loop
      */
    void queueEndRendering() { }

    /** Overriden to do nothing.
      * Use reset --> renderOneFrame instead
      */
    void startRendering() { }

    Ogre::RenderSystem *createRenderSystem(const ParamsMulti &options);
    void destroyRenderSystem(const std::string &name);
    void destroyRenderSystem();

    Ogre::SceneManager *createSceneManager(const ParamsMulti &options);
    void destroySceneManager();

    BaseCamera createCamera(const std::string &name);
    BaseCamera createCamera(Ogre::SceneManager *mgr, const std::string &name);
#endif // SWIG

    bool hasRenderWindow(const std::string &name);

#ifndef SWIG
    /** Creates new fullsized (match to renderWindow) Viewport instance
      * @def ColourValue backgroundColor = BLACK
      */
    Ogre::Viewport *createViewport(const std::string &cameraName,
                                   const std::string &renderWindowName,
                                   int zOrder = 0, float left = .0f, float top = .0f,
                                   float width = 1.0f, float height = 1.0f);

    static Ogre::Viewport *createViewport(Ogre::Camera *camera,
                                          Ogre::RenderWindow *renderWindow,
                                          int zOrder = 0, float left = .0f,
                                          float top = .0f, float width = 1.0f,
                                          float height = 1.0f);

    Ogre::SceneManager *getSceneManager() const;
    Ogre::SceneManager *getSceneManager(const std::string &name) const;

    void setRenderViewBackend(RenderViewBackend *backend);
    inline const RenderViewBackend *getRenderViewBackend() const
    {
        return mRenderViewBackend;
    }

    bool getRenderViewSizeHint(ve::SizeU &sizeHint) const;
#endif // SWIG

    bool hasCamera(const std::string &name);

    StringList getCameraNames() const;

    inline const StringList &getRenderTargetNames() const
    {
        return mRenderTargetNames;
    }

    const std::string &getDefaultSceneManagerName() const;

    StringList getSceneManagerNames();

    inline UiManager* getUiManager() const
    {
        return mUiManager;
    }

#ifndef SWIG
    static Renderer *getSingletonPtr();

    /// Event system
    bool event(const Event *event) override;

    bool loadPlugin(const std::string &path);

protected:
    /** Creates new RenderWindow instance
      * @param opt:
      *         name Name of instatnce
      *         size Size of window
      *         fullScreen FullScreen mode
      *         parameters Other parameters specifed by Root::createRenderWindow
      * @remarks opt passed by non-const ref, so they can be changed.
      *         final options will be stored in opt. (Now only name would be changed
      *         if it is not unique.
      */
    Ogre::RenderWindow *createRenderWindow(RenderWindowOptions &opt);

    /** Default impl */
    Ogre::RenderWindow *createRenderWindow(const std::string &name, unsigned int width, unsigned int height,
                                           bool fullScreen, const Params *miscParams = 0);

    void destroyRenderWindow(const RenderWindowOptions &opt);

    Ogre::RenderWindow *getRenderWindow(const std::string &name);

    Ogre::Camera *getOgreCamera(const std::string &name);

    inline RenderViewBackend *gerRenderViewBackend() const
    {
        return mRenderViewBackend;
    }

    bool hasCamera(Ogre::Camera *camera) const;

private:
    StringList mRenderTargetNames;

    RenderViewBackend *mRenderViewBackend;

    Ogre::NameGenerator mNameGenerator;
    StringSet mRegistredSceneMgrs;

    const std::string mGenericSceneManagerName;
    const std::string mOpenGLRenderingSubsystemName;

    std::map<std::string, std::string> mRendereringSubsystemToPluginName;

    UiManager *mUiManager;
#endif // SWIG
};

Renderer *renderer(bool checkPoiner = true);

#endif // RENDERER_H
