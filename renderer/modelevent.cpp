#include "modelevent.h"

#include <framework/stringconverter.h>

ModelEvent::ModelEvent(float deltaTime,
                       uint32_t priority,
                       uint32_t lifeTime)
    : RendererEvent(priority, lifeTime),
      mDeltaTime(deltaTime)
{

}

void ModelEvent::toStdMap(Params &output) const
{
    output["deltatime"] = StringConverter::toString(getDeltaTime());

    RendererEvent::toStdMap(output);
}
