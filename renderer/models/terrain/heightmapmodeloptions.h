#ifndef HEIGHTMAPMODELOPTIONS_H
#define HEIGHTMAPMODELOPTIONS_H

#include <renderer/models/physics/physicsmodeloptions.h>
#include <global.h>

class ParamsMulti;

namespace Ogre {
    class Image;
}

class HeightMapModelOptions : public PhysicsModelOptions
{
public:
    HeightMapModelOptions();
    HeightMapModelOptions(const ParamsMulti &options);

//    Terrain::ImportData importData;
    Ogre::Image *heightMapImage;
    bool deleteHeightMapImage;

    float maxHeight;
    float maxPixelError;
    float worldSize;
    uint16_t terrainSize; /// @attention n ^ 2 + 1
    float inputScale;
    uint16_t minBatchSize; /// @attention n ^ 2 + 1 && <= 65
    uint16_t maxBatchSize; /// @attention n ^ 2 + 1 && <= 65
//    Size pageSize;
    uint32_t heightMapImageStep;
    float compositeMapDistance;
};

#endif // HEIGHTMAPMODELOPTIONS_H
