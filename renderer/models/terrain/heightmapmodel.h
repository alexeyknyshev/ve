#ifndef HEIGHTMAPMODEL_H
#define HEIGHTMAPMODEL_H

#include <renderer/models/physics/physicsmodel.h>

class HeightMapModelOptions;

class HeightMapModel : public PhysicsModel
{
public:
    VE_DECLARE_TYPE_INFO(HeightMapModel)

    HeightMapModel(const HeightMapModelOptions &options);

    ~HeightMapModel();

protected:
    HeightMapModel(const HeightMapModelOptions &options, bool init);

    void _init(const HeightMapModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const HeightMapModelOptions &options);

    class TerrainPrivate;

    TerrainPrivate *mTerrain;
};

#endif // HEIGHTMAPMODEL_H
