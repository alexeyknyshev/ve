#include "heightmapmodeloptions.h"

#include <framework/logger/logger.h>
#include <framework/filesystem.h>
#include <framework/params.h>
#include <framework/stringconverter.h>

#include <OGRE/OgreImage.h>

class HeightMapModel;

HeightMapModelOptions::HeightMapModelOptions()
    : heightMapImage(nullptr),
      deleteHeightMapImage(false),
      maxHeight(100.0f),
      maxPixelError(8.0f),
      worldSize(513.0f),
      terrainSize(513),
      inputScale(1.0f),
      minBatchSize(33),
      maxBatchSize(65),
//          pageSize(10.0f, 10.0f),
      heightMapImageStep(10.0f),
      compositeMapDistance(3000.0f)
{
    /// override due to special case
    nodeBased = false;
}

HeightMapModelOptions::HeightMapModelOptions(const ParamsMulti &options)
    : HeightMapModelOptions()
{
    const auto end = options.end();

    auto it = options.find(VE_AS_STR(worldSize));
    if (it != end)
    {
        worldSize = StringConverter::parseReal(it->second, 513.0f);
    }

    it = options.find(VE_AS_STR(terrainSize));
    if (it != end)
    {
        terrainSize = StringConverter::parseUnsignedInt(it->second, 513);
    }

    it = options.find(VE_AS_STR(maxHeight));
    if (it != end)
    {
        maxHeight = StringConverter::parseReal(it->second, 100.0f);
    }

    it = options.find(VE_AS_STR(heightMapImageStep));
    if (it != end)
    {
        heightMapImageStep = StringConverter::parseUnsignedInt(it->second, 1);
    }

    std::string heightMapImagePath = VE_STR_BLANK;
    it = options.find(VE_AS_STR(heightMapImage));
    if (it != end)
    {
        heightMapImagePath = it->second;
    }


/*
    const std::string pageSizeStr =
            config.getSetting(VE_AS_STR(pageSize), VE_STR_BLANK, "10x10");

    uint32_t pageSide = StringConverter::parseUnsignedInt(pageSizeStr, 10);

    pageSize = Size(pageSide, pageSide);
*/



    deleteHeightMapImage = true;
    heightMapImage = new Ogre::Image;

    if (heightMapImagePath.empty())
    {
        logger()->logClassMessage(VE_CLASS_NAME(HeightMapModelOptions),
                                  "Loading from configuration file failed! No \"" +
                                  VE_AS_STR(heightMapImage) +
                                  "\" setting specified! " +
                                  VE_AS_STR(HeightMapModel) +
                                  " based terrain will be flat.",
                                  Logger::LOG_LEVEL_WARNING,
                                  Logger::LOG_COMPONENT_RENDERER);
    }
    else
    {
        if (resourceGroup.empty())
        {
            logger()->logClassMessage(VE_CLASS_NAME(HeightMapModelOptions),
                                      "No \"" + VE_AS_STR(resourceGroup) + "\" setting "
                                      "specified! Default resource group will be used instead.",
                                      Logger::LOG_LEVEL_WARNING,
                                      Logger::LOG_COMPONENT_RENDERER);
            resourceGroup = FileSystem::RG_DEFAULT;
        }

        heightMapImage->load(heightMapImagePath, resourceGroup);
    }
}
