#include "heightmapmodel.h"

#include <renderer/models/terrain/heightmapmodeloptions.h>

#include <OGRE/OgreStringConverter.h>

#include <OGRE/OgreImage.h>
#if VE_PLATFORM == VE_PLATFORM_APPLE
#include <OGRE/OgreTerrain.h>
#include <OGRE/OgreTerrainGroup.h>
#else
#include <OGRE/Terrain/OgreTerrain.h>
#include <OGRE/Terrain/OgreTerrainGroup.h>
#endif

#include <renderer/renderer.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

#include <physics/rigidbody.h>

class HeightMapModel::TerrainPrivate
{
public:
    TerrainPrivate(HeightMapModelOptions &options);

    virtual ~TerrainPrivate();

    void loadAllTerrians();

    void freeTempResources();

    static void createTerrainOptions(const HeightMapModelOptions &options);

    Ogre::TerrainGroup *createTerrain(HeightMapModelOptions &options);

protected:
    bool checkImageIsSquare(Ogre::Image *image);
    bool checkImageSizeIsPowerOf2Plus1(Ogre::Image *image);

private:
    static Ogre::TerrainGlobalOptions *msTerrainGlobalOptions;
    Ogre::TerrainGroup *mTerrainGroup;
};

Ogre::TerrainGlobalOptions *HeightMapModel::TerrainPrivate::msTerrainGlobalOptions = nullptr;

HeightMapModel::TerrainPrivate::TerrainPrivate(HeightMapModelOptions &options)
    : mTerrainGroup(nullptr)
{
    createTerrainOptions(options);
    mTerrainGroup = createTerrain(options);
}

HeightMapModel::TerrainPrivate::~TerrainPrivate()
{
    delete mTerrainGroup;
    mTerrainGroup = nullptr;
}

void HeightMapModel::TerrainPrivate::loadAllTerrians()
{
    mTerrainGroup->loadAllTerrains(true);
}

void HeightMapModel::TerrainPrivate::freeTempResources()
{
    mTerrainGroup->freeTemporaryResources();
}

void HeightMapModel::TerrainPrivate::createTerrainOptions(const HeightMapModelOptions &options)
{
    if (msTerrainGlobalOptions)
    {
        return;
    }

    Ogre::SceneManager *sceneMgr = renderer()->getSceneManager();

    msTerrainGlobalOptions = new Ogre::TerrainGlobalOptions;

    msTerrainGlobalOptions->setMaxPixelError(options.maxPixelError);
    msTerrainGlobalOptions->setCompositeMapDistance(options.compositeMapDistance);

    /// TODO: remove stub!!
    Ogre::Vector3 lightMapDir(Ogre::Vector3::NEGATIVE_UNIT_Y + Ogre::Vector3::NEGATIVE_UNIT_X);
    lightMapDir.normalise();

    msTerrainGlobalOptions->setLightMapDirection(lightMapDir);

    msTerrainGlobalOptions->setCompositeMapAmbient(sceneMgr->getAmbientLight());
}

Ogre::TerrainGroup *HeightMapModel::TerrainPrivate::createTerrain(HeightMapModelOptions &options)
{
    Ogre::SceneManager *sceneMgr = renderer()->getSceneManager();

    Ogre::TerrainGroup *terrainGroup = new Ogre::TerrainGroup(sceneMgr,
                                                              Ogre::Terrain::ALIGN_X_Z,
                                                              513,
                                                              513.0f);

    /// TODO: remove stub!
    terrainGroup->setFilenameConvention(options.modelName, std::string("dat"));
    terrainGroup->setOrigin(Ogre::Vector3::ZERO);

    Ogre::Terrain::ImportData &defaultimp = terrainGroup->getDefaultImportSettings();

    const uint32_t imgWidth  = options.heightMapImage->getWidth();
    const uint32_t imgHeight = options.heightMapImage->getHeight();

    /// Image is not square
    if (imgWidth != imgHeight)
    {
        logger()->logClassMessage(VE_CLASS_NAME(HeightMapModel::TerrainPrivate),
                                  VE_SCOPE_NAME(createTerrain(HeightMapModelOptions &options)),
                                  "Terrain heightMapImage is not square. "
                                  "It will be resized to prev n ^ 2 + 1 square.",
                                  Logger::LOG_LEVEL_WARNING,
                                  Logger::LOG_COMPONENT_RENDERER);

        uint32_t minSide = static_cast<uint32_t>
                (std::min(options.heightMapImage->getHeight(),options.heightMapImage->getWidth()));

        minSide = BitMath::getPrevPowerOfTwo(minSide) + 1;
        options.heightMapImage->resize(minSide, minSide, Ogre::Image::FILTER_BILINEAR);

    /// Image is square but is not n ^ 2 + 1 dim
    }
    else
    if (!BitMath::isPowerOfTwo(imgWidth - 1))
    {
        logger()->logClassMessage(VE_CLASS_NAME(HeightMapModel::TerrainPrivate),
                                  VE_SCOPE_NAME(createTerrain(HeightMapModelOptions &options)),
                                  "Terrain heightMapImage size is not eqals n ^ 2 + 1. "
                                  "It will be resized to prev n ^ 2 + 1 square.",
                                  Logger::LOG_LEVEL_WARNING,
                                  Logger::LOG_COMPONENT_RENDERER);

        if (imgWidth < 2) /// Prevent resizing to zero width/height
        {
            veAssert(false,
                     VE_CLASS_NAME(HeightMapModel::TerrainPrivate),
                     VE_SCOPE_NAME(createTerrain(HeightMapModelOptions &options)),
                     "Terrain heightMapImage size is <= 1!",
                     Logger::LOG_COMPONENT_RENDERER);
        }
        else
        {
            const uint32_t pow2side = BitMath::getPrevPowerOfTwo(imgWidth);
            options.heightMapImage->resize(pow2side, pow2side, Ogre::Image::FILTER_BILINEAR);
        }
    }

    defaultimp.inputImage       = options.heightMapImage;
    defaultimp.deleteInputData  = options.deleteHeightMapImage;

    defaultimp.worldSize        = options.worldSize;

    defaultimp.terrainSize      = options.terrainSize;
    defaultimp.inputScale       = options.inputScale;
    defaultimp.minBatchSize     = options.minBatchSize;
    defaultimp.maxBatchSize     = options.maxBatchSize;

    defaultimp.layerList.resize(3);
    defaultimp.layerList[0].worldSize = 100;
    defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");
    defaultimp.layerList[0].textureNames.push_back("dirt_grayrocky_normalheight.dds");
    defaultimp.layerList[1].worldSize = 30;
    defaultimp.layerList[1].textureNames.push_back("grass_green-01_diffusespecular.dds");
    defaultimp.layerList[1].textureNames.push_back("grass_green-01_normalheight.dds");
    defaultimp.layerList[2].worldSize = 200;
    defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");
    defaultimp.layerList[2].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");

    /// TODO: remove stub!
    std::string filename = terrainGroup->generateFilename(0, 0);
    if (Ogre::ResourceGroupManager::getSingleton()
            .resourceExists(terrainGroup->getResourceGroup(), filename))
    {
        terrainGroup->defineTerrain(0, 0);
    }
    else
    {
        terrainGroup->defineTerrain(0, 0, options.heightMapImage);
    }

    return terrainGroup;
}

// ------------------------------------------------------

HeightMapModel::HeightMapModel(const HeightMapModelOptions &options)
    : PhysicsModel(options, false)
{
    _init(options, true);
}

HeightMapModel::HeightMapModel(const HeightMapModelOptions &options, bool init)
    : PhysicsModel(options, false)
{
    _init(options, init);
}

void HeightMapModel::_init(const HeightMapModelOptions &options, bool meshInit)
{
    mTerrain = nullptr;

    HeightMapModelOptions opt(options);

    // Override option due to static nature of heightmap
    opt.physicsType = PhysicsModelOptions::PMT_Static;

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const HeightMapModelOptions &options, bool meshInit)),
                       opt.modelName);

        mTerrain = new TerrainPrivate(opt);
        mTerrain->loadAllTerrians();
        mTerrain->freeTempResources();

        mRigidBody = createRigidBody(opt);
        if (!mRigidBody)
        {
            /// *STUB* failed to create rigidBody by PhysicsManager
        }
    }
}

HeightMapModel::~HeightMapModel()
{
    delete mTerrain;
    mTerrain = nullptr;
}

RigidBody *HeightMapModel::createRigidBody(const HeightMapModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(HeightMapRigidBody)),
                                      options,
                                      nullptr);
}

