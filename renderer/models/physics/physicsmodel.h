#ifndef PHYSICSMODEL_H
#define PHYSICSMODEL_H

#include <renderer/model.h>

class RigidBody;
class PhysicsModelOptions;

class PhysicsModel : public Model
{
public:
    VE_DECLARE_STATIC_META_INFO(PhysicsModel)

    ~PhysicsModel();

    enum { PhysicsModelHash = BIT(1) };
    size_t getClassHash() const override
    {
        return PhysicsModelHash | Model::getClassHash();
    }

    bool isDynamic() const;

    RigidBody *getRigidBody() const;

    void setPosition(const Ogre::Vector3 &position) override;
    void setOrientation(const Ogre::Quaternion &orient) override;

    void shutdown(size_t componentHash) override;

protected:
    PhysicsModel(const PhysicsModelOptions &options, bool init);

    /** Overload it in derived class and do RigidBody related stuff there */
    RigidBody *createRigidBody(const PhysicsModelOptions &options) const
    {
        assertNotImplemented(VE_SCOPE_NAME(createRigidBody(const PhysicsModelOptions &options) const),
                             Logger::LOG_COMPONENT_PHYSICS);
        VE_UNUSED(options);
        return nullptr;
    }

    void shutdownPhysics();

    RigidBody *mRigidBody;
};

#endif // PHYSICSMODEL_H
