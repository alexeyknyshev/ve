#include "physicsmodel.h"

#include <renderer/models/physics/physicsmodeloptions.h>

#include <OGRE/OgreSceneNode.h>

#include <physics/physicsmanager.h>
#include <physics/rigidbody.h>

PhysicsModel::PhysicsModel(const PhysicsModelOptions &options, bool init)
    : Model(options, init),
      mRigidBody(nullptr)
{ }

PhysicsModel::~PhysicsModel()
{
    shutdownPhysics();
}

bool PhysicsModel::isDynamic() const
{
    if (mRigidBody)
    {
        return mRigidBody->isDynamic();
    }
    return false;
}

RigidBody *PhysicsModel::getRigidBody() const
{
    return mRigidBody;
}

void PhysicsModel::setPosition(const Ogre::Vector3 &position)
{
    Model::setPosition(position);
    if (mRigidBody)
    {
        mRigidBody->setWorldPosition(getWorldPosition());
    }
}

void PhysicsModel::setOrientation(const Ogre::Quaternion &orient)
{
    Model::setOrientation(orient);
    if (mRigidBody)
    {
        mRigidBody->setWorldOrientation(orient);
    }
}

void PhysicsModel::shutdown(size_t componentHash)
{
    if (componentHash == physics()->getClassHash())
    {
        shutdownPhysics();
    }
    Model::shutdown(componentHash);
}

void PhysicsModel::shutdownPhysics()
{
    delete mRigidBody;
    mRigidBody = nullptr;
}
