#ifndef PHYSICSMODELOPTIONS_H
#define PHYSICSMODELOPTIONS_H

#include <renderer/modeloptions.h>
#include <OGRE/OgreVector3.h>

class PhysicsModel;

class PhysicsModelOptions : public ModelOptions
{
public:
    PhysicsModelOptions()
        : physicsType(PMT_Dynamic),
          segments({ -1, -1, -1 }),
          restitution(0.0f),
          friction(0.5f),
          mass(1.0f),
          linearDamping(0.2f),
          angularDamping(0.2f)
    { }

    enum PhysicsModelType
    {
        PMT_None = 0,
        PMT_Static,
        PMT_Dynamic
    };

    PhysicsModelType physicsType;

    Ogre::Vector3 segments;

    float restitution;
    float friction;
    float mass;

    float linearDamping;
    float angularDamping;
};

#endif // PHYSICSMODELOPTIONS_H
