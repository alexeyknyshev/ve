#ifndef VEHICLEMODEL_H
#define VEHICLEMODEL_H

#include <renderer/models/physics/physicsmodel.h>
#include <renderer/models/vehicle/vehiclewheelmodel.h>

class RayCastVehicle;
class VehicleModelOptions;

class VehicleModel : public PhysicsModel
{
public:
    VE_DECLARE_STATIC_META_INFO(VehicleModel)

    virtual const ClassName &getClassName() const override
    {
        return getStaticClassName();
    }

    enum { VehicleModelHash = std::uint64_t(2 << 3) };
    size_t getClassHash() const override
    {
        return VehicleModelHash | PhysicsModel::getClassHash();
    }

    VehicleModel(VehicleModelOptions &options);

    ~VehicleModel();

    void update(float deltaTime, float realDelta) override;

    inline RayCastVehicle *getRayCastVehicle() const
    {
        return mRayCastVehicle;
    }

    void resetOptions(VehicleModelOptions &options);

    void shutdown(size_t componentHash);

protected:
    VehicleModel(VehicleModelOptions &options, bool init);

    void _init(VehicleModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const VehicleModelOptions &options);

    void shutdownVehicle();

    VehicleWheelModelSet mWheels;

private:
    RayCastVehicle *mRayCastVehicle;
};

#endif // VEHICLEMODEL_H
