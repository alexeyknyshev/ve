#ifndef VEHICLEMODELOPTIONS_H
#define VEHICLEMODELOPTIONS_H

#include <OGRE/OgreVector3.h>

#include <renderer/models/physics/physicsmodeloptions.h>
#include <renderer/models/vehicle/vehiclewheelmodeloptions.h>

class VehicleModelOptions : public PhysicsModelOptions
{
public:
    VehicleModelOptions()
        : chassisShift(0.0f, 1.0f, 0.0f),
          suspensionStiffness(5.88f),
          suspensionCompression(0.83f),
          suspensionDamping(0.88f),
          maxSuspensionTravelCm(500.0f),
          frictionSlip(10.5f),
          maxSuspensionForce(6000.0f),
          maxEngineForce(3000.0f),
          maxSteeringAngle(Ogre::Math::HALF_PI * 0.5f)
    { }

    Ogre::Vector3 chassisShift;
    float suspensionStiffness;
    float suspensionCompression;
    float suspensionDamping;
    float maxSuspensionTravelCm;
    float frictionSlip;
    float maxSuspensionForce;
    float maxEngineForce;
    float maxSteeringAngle;

    std::vector<VehicleWheelModelOptions> wheelOpts;
};

#endif // VEHICLEMODELOPTIONS_H
