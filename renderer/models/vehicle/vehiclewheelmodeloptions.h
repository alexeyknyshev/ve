#ifndef VEHICLEWHEELMODELOPTIONS_H
#define VEHICLEWHEELMODELOPTIONS_H

#include <renderer/models/physics/physicsmodeloptions.h>

#include <OGRE/OgreVector3.h>

class VehicleWheelModelOptions : public PhysicsModelOptions
{
public:
    VehicleWheelModelOptions()
        : rollInfluence(1.0f),
          suspensionRestLength(0.6f),
          radius(0.0f),
          orientation(FrontRight),
          connectionPoint(Ogre::Vector3::ZERO)
    {}

    enum Orientation {
        FrontRight = 0,
        FrontLeft,
        BackRight,
        BackLeft
    };

    inline bool isFront() const
    {
        if (orientation == FrontLeft || orientation == FrontRight)
        {
            return true;
        }
        return false;
    }

    float rollInfluence;
    float suspensionRestLength;

    float radius; // will be initalized from Mesh

    Orientation orientation;
    Ogre::Vector3 connectionPoint;
};

#endif // VEHICLEWHEELMODELOPTIONS_H
