#ifndef VEHICLEWHEELMODEL_H
#define VEHICLEWHEELMODEL_H

#include <OGRE/OgreVector3.h>

#include <renderer/models/physics/physicsmodel.h>

class VehicleWheelModelOptions;
class RayCastVehicle;

class VehicleWheelModel : public Model
{
public:
    VehicleWheelModel(VehicleWheelModelOptions &options, RayCastVehicle *vehicle);

    ~VehicleWheelModel();

    bool isFrontWheel() const;

    float getSuspensionLength() const;
    void setSuspensionLength(float length);

    float getSuspensionRestLength() const;
    void setSuspensionRestLength(float length);

protected:
    VehicleWheelModel(VehicleWheelModelOptions &options, RayCastVehicle *vehicle, bool init);

    void _init(VehicleWheelModelOptions &options, bool meshInit);

    int mIndex;

    RayCastVehicle *mVehicle;
};

typedef std::vector<VehicleWheelModel *> VehicleWheelModelVector;
typedef std::set<VehicleWheelModel *> VehicleWheelModelSet;

#endif // VEHICLEWHEELMODEL_H
