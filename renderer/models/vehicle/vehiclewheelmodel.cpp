#include "vehiclewheelmodel.h"

#include <renderer/models/vehicle/vehiclewheelmodeloptions.h>
#include <physics/vehicle/raycastvehicle.h>

#include <OGRE/OgreEntity.h>

VehicleWheelModel::VehicleWheelModel(VehicleWheelModelOptions &options, RayCastVehicle *vehicle)
    : Model(options, true),
      mIndex(options.orientation),
      mVehicle(vehicle)
{
    _init(options, true);
}

VehicleWheelModel::VehicleWheelModel(VehicleWheelModelOptions &options, RayCastVehicle *vehicle, bool init)
    : Model(options, init), /// accept loading from mesh
      mIndex(options.orientation),
      mVehicle(vehicle)
{
//    if (init) {
//        mRigidBody = createRigidBody(options);
//        if (!mRigidBody) {
//            /// *STUB* failed to create rigidBody by PhysicsManager
//        }
//    }

    _init(options, init);
}

void VehicleWheelModel::_init(VehicleWheelModelOptions &options, bool meshInit)
{
    VE_UNUSED(meshInit);

    mIndex = (int)options.orientation;

    options.radius = getBoundingBox().getSize().z * 0.5f;
    options.modelNode = _getModelNode();
}

VehicleWheelModel::~VehicleWheelModel()
{
}

bool VehicleWheelModel::isFrontWheel() const
{
    return mVehicle->isFrontWheel(mIndex);
}

float VehicleWheelModel::getSuspensionLength() const
{
    return mVehicle->getSuspensionLength(mIndex);
}

void VehicleWheelModel::setSuspensionLength(float length)
{
    mVehicle->setSuspensionLength(mIndex, length);
}

float VehicleWheelModel::getSuspensionRestLength() const
{
    return mVehicle->getSuspensionLength(mIndex);
}

void VehicleWheelModel::setSuspensionRestLength(float length)
{
    mVehicle->setSuspensionRestLength(mIndex, length);
}

/*
RigidBody *VehicleWheelModel::createRigidBody(const VehicleWheelModelOptions &options)
{
    CylinderModelOptions cylinderOpt(options);
    cylinderOpt.orientation = CylinderModelOptions::OrientationX;

    const AxisAlignedBox &aabb = options._model->_getEntity()->getBoundingBox();
    cylinderOpt.halfExtents = aabb.getSize() * 0.5;

    mWheelRaduis = cylinderOpt.halfExtents.x;

    return physics()->createRigidBody(VE_CLASS_NAME_STR(CylinderRigidBody), cylinderOpt);
}
*/
