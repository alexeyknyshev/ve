#include "vehiclemodel.h"

#include <renderer/models/vehicle/vehiclemodeloptions.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

#include <physics/vehicle/raycastvehicle.h>

VehicleModel::VehicleModel(VehicleModelOptions &options)
    : VehicleModel(options, true)
{ }

VehicleModel::VehicleModel(VehicleModelOptions &options, bool init)
    : PhysicsModel(options, init),
      mRayCastVehicle(nullptr)
{
    _init(options, init);
}

void VehicleModel::_init(VehicleModelOptions &options, bool meshInit)
{
    if (meshInit)
    {
        mRigidBody = createRigidBody(options);
        if (!mRigidBody)
        {
            logAndAssert(VE_SCOPE_NAME(_init(VehicleModelOptions &options, bool meshInit)),
                         "Failed to create " +
                         VE_AS_STR(WheeledRigidBody) + "!",
                         Logger::LOG_COMPONENT_PHYSICS);
            return;
        }

        mRayCastVehicle = physics()->createRayCastVehicle(options, mRigidBody);
        if (!mRayCastVehicle)
        {
            logAndAssert(VE_SCOPE_NAME(_init(VehicleModelOptions &options, bool meshInit)),
                         "Failed to create " +
                         VE_AS_STR(RayCastVehicle) + "!",
                         Logger::LOG_COMPONENT_PHYSICS);
            return;
        }

        for (VehicleWheelModelOptions &wheelOpt : options.wheelOpts)
        {
            VehicleWheelModel *wheel = new VehicleWheelModel(wheelOpt, mRayCastVehicle);

            mWheels.insert(wheel);
            mRayCastVehicle->addWheel(wheelOpt);
        }
    }
}

VehicleModel::~VehicleModel()
{
    void shutdownVehicle();
}

RigidBody *VehicleModel::createRigidBody(const VehicleModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(WheeledRigidBody)),
                                      options,
                                      _getEntity());
}

void VehicleModel::update(float deltaTime, float realDelta)
{
    VE_UNUSED(realDelta);

    PhysicsModel::update(deltaTime, realDelta);

    for (VehicleWheelModel *wheel : mWheels)
    {
        if (wheel->getSuspensionLength() >= 1.2f * wheel->getSuspensionRestLength())
        {
            wheel->setSuspensionLength(1.2f * wheel->getSuspensionRestLength());
        }
    }
}

void VehicleModel::shutdown(size_t componentHash)
{
    if (componentHash == VehicleModel::getStaticClassHash())
    {
        shutdownVehicle();
    }
    PhysicsModel::shutdown(componentHash);
}

void VehicleModel::shutdownVehicle()
{
    delete mRayCastVehicle;
    mRayCastVehicle = nullptr;
}
