#include "proceduralmodel.h"

#include <OGRE/OgreMeshManager.h>

ProceduralModel::ProceduralModel(const PhysicsModelOptions &options)
    : PhysicsModel(options, false)
{
}

Ogre::MeshPtr ProceduralModel::createMesh(const std::string &meshName,
                                          const std::string &resourceGroup)
{
    auto &mgr = Ogre::MeshManager::getSingleton();
    Ogre::MeshPtr mesh = mgr.getByName(meshName, resourceGroup);
    if (!mesh.isNull())
    {
        return mgr.load(meshName, resourceGroup);
    }
    return mesh;
}
