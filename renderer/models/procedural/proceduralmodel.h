#ifndef PROCEDURALMODEL_H
#define PROCEDURALMODEL_H

#include <renderer/models/physics/physicsmodel.h>

#include <OGRE/OgreMesh.h>

class ProceduralModel : public PhysicsModel
{
public:
    enum { ProceduralModelHash = BIT(2) };
    size_t getClassHash() const
    {
        return ProceduralModelHash | PhysicsModel::getClassHash();
    }

protected:
    ProceduralModel(const PhysicsModelOptions &options);

    /** Overload it in derived class and do Procedural mesh generation there */
    Ogre::MeshPtr createMesh(const PhysicsModelOptions &options) const
    {
        assertNotImplemented(VE_SCOPE_NAME(ProceduralModel::createMesh),
                             Logger::LOG_COMPONENT_RENDERER);
        VE_UNUSED(options);
        return Ogre::MeshPtr();
    }

    static Ogre::MeshPtr createMesh(const std::string &meshName,
                                    const std::string &resourceGroup);
};

#endif // PROCEDURALMODEL_H
