#ifndef TRIMESHMODEL_H
#define TRIMESHMODEL_H

#include <renderer/models/physics/physicsmodel.h>

class TriMeshModelOptions;

class TriMeshModel : public PhysicsModel
{
public:
    VE_DECLARE_STATIC_META_INFO(TriMeshModel)

    virtual const ClassName &getClassName() const
    {
        return getStaticClassName();
    }

    enum { TriMeshModelHash = std::uint64_t(1 << 3) };
    size_t getClassHash() const
    {
        return TriMeshModelHash | PhysicsModel::getClassHash();
    }

    TriMeshModel(const TriMeshModelOptions &options);

protected:
    TriMeshModel(const TriMeshModelOptions &options, bool init);

    void _init(const TriMeshModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const TriMeshModelOptions &options);
};

#endif // TRIMESHMODEL_H
