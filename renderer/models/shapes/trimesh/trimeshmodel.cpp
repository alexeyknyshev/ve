#include "trimeshmodel.h"

#include <renderer/models/shapes/trimesh/trimeshmodeloptions.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

TriMeshModel::TriMeshModel(const TriMeshModelOptions &options)
    : TriMeshModel(options, true)
{ }

TriMeshModel::TriMeshModel(const TriMeshModelOptions &options, bool init)
    : PhysicsModel(options, init)
{
    _init(options, init);
}

void TriMeshModel::_init(const TriMeshModelOptions &options, bool meshInit)
{
    if (meshInit)
    {
        mRigidBody = createRigidBody(options);
        if (!mRigidBody)
        {
            /// *STUB* failed to create rigidBody by PhysicsManager
        }
    }

    setModelNodeParent(options.parentNode);
}

RigidBody *TriMeshModel::createRigidBody(const TriMeshModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(TriMeshRigidBody)),
                                      options,
                                      _getEntity());
}
