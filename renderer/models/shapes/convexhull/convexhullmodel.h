#ifndef CONVEXHULLMODEL_H
#define CONVEXHULLMODEL_H

#include <renderer/models/physics/physicsmodel.h>

class ConvexHullModelOptions;

class ConvexHullModel : public PhysicsModel
{
public:
    VE_DECLARE_TYPE_INFO(ConvexHullModel)

    ConvexHullModel(const ConvexHullModelOptions &options);

protected:
    ConvexHullModel(const ConvexHullModelOptions &options, bool meshInit);

    void _init(const ConvexHullModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const ConvexHullModelOptions &options);
};

#endif // CONVEXHULLMODEL_H
