#include "convexhullmodel.h"

#include <renderer/models/shapes/convexhull/convexhullmodeloptions.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

ConvexHullModel::ConvexHullModel(const ConvexHullModelOptions &options)
    : ConvexHullModel(options, true)
{ }

ConvexHullModel::ConvexHullModel(const ConvexHullModelOptions &options, bool meshInit)
    : PhysicsModel(options, meshInit)
{
    _init(options, meshInit);
}

void ConvexHullModel::_init(const ConvexHullModelOptions &options, bool meshInit)
{
    if (meshInit)
    {
        mRigidBody = createRigidBody(options);
        if (!mRigidBody)
        {
            /// *STUB* failed to create rigidBody by PhysicsManager
        }
    }
}

RigidBody *ConvexHullModel::createRigidBody(const ConvexHullModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(ConvexHullRigidBody)),
                                      options,
                                      _getEntity());
}


