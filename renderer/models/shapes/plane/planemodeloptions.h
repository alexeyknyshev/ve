#ifndef PLANEMODELOPTIONS_H
#define PLANEMODELOPTIONS_H

#include <renderer/models/physics/physicsmodeloptions.h>

#include <OGRE/OgrePlane.h>
#include <framework/size.h>
#include <framework/point.h>

class PlaneModelOptions : public PhysicsModelOptions
{
public:
    PlaneModelOptions()
        : plane(Ogre::Vector3::UNIT_Y, 0), // Normal up
          position(Ogre::Vector3::ZERO),
          size(1, 1),
          uTile(1),
          vTile(1)
    {
        segments = { 1, 1, -1 };
    }

public:
    Ogre::Plane plane;
    Ogre::Vector3 position;
    ve::SizeF size;
    int uTile;
    int vTile;
};

#endif // PLANEMODELOPTIONS_H
