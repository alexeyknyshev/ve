#ifndef PLANEMODEL_H
#define PLANEMODEL_H

#include <renderer/models/procedural/proceduralmodel.h>

class PlaneModelOptions;

class PlaneModel : public ProceduralModel
{
public:
    VE_DECLARE_STATIC_META_INFO(PlaneModel)

    virtual const ClassName &getClassName() const
    {
        return getStaticClassName();
    }

    enum { PlaneModelHash = std::uint64_t(3 << 3) };
    size_t getClassHash() const
    {
        return PlaneModelHash | ProceduralModel::getClassHash();
    }

    PlaneModel(const PlaneModelOptions &options);

protected:
    PlaneModel(const PlaneModelOptions &options, bool meshInit);

    void _init(const PlaneModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const PlaneModelOptions &options);
    Ogre::MeshPtr createMesh(const PlaneModelOptions &options) const;

    static std::string getProceduralMeshName(const PlaneModelOptions &options);
};

#endif // PLANEMODEL_H
