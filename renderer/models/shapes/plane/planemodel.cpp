#include "planemodel.h"

#include <renderer/models/shapes/plane/planemodeloptions.h>

#include <renderer/renderer.h>
#include <OgreProcedural/ProceduralPlaneGenerator.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

#include <framework/stringconverter.h>

PlaneModel::PlaneModel(const PlaneModelOptions &options)
    : PlaneModel(options, true)
{ }

PlaneModel::PlaneModel(const PlaneModelOptions &options, bool meshInit)
    : ProceduralModel(options)
{
    _init(options, meshInit);
}

void PlaneModel::_init(const PlaneModelOptions &options, bool meshInit)
{
    PlaneModelOptions opt(options);

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const PlaneModelOptions &options, bool meshInit)),
                       opt.modelName);

        if (opt.meshName.empty())
        {
            opt.meshName = opt.getFallbackMeshName();
        }

        initFromMesh(createMesh(opt));

        mRigidBody = createRigidBody(opt);
        if (!mRigidBody) {
            /// *STUB* failed to create rigidBody by PhysicsManager
        }
    }
}

RigidBody *PlaneModel::createRigidBody(const PlaneModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(PlaneRigidBody)),
                                      options,
                                      _getEntity());
}

Ogre::MeshPtr PlaneModel::createMesh(const PlaneModelOptions &options) const
{
    const std::string meshName = getProceduralMeshName(options);
    Ogre::MeshPtr mesh = ProceduralModel::createMesh(meshName,
                                                     options.resourceGroup);
    if (!mesh.isNull())
    {
        return mesh;
    }

    Procedural::PlaneGenerator generator;
    generator.setNormal(options.plane.normal)
            .setNumSegX(options.segments.x)
            .setNumSegY(options.segments.y)
            .setSizeX(options.size.width)
            .setSizeY(options.size.height)
            .setUTile(options.uTile)
            .setVTile(options.vTile)
            .setPosition(options.position);

    return generator.realizeMesh(meshName, options.resourceGroup);

}

std::string PlaneModel::getProceduralMeshName(const PlaneModelOptions &options)
{
    Ogre::Vector4 data(options.plane.normal);
    data.w = options.plane.d;
    return std::string("plane" + StringConverter::toString(data));
}
