#ifndef SPHERERMODEL_H
#define SPHERERMODEL_H

#include <renderer/models/procedural/proceduralmodel.h>

class SphereModelOptions;

class SphereModel : public ProceduralModel
{
public:
    VE_DECLARE_STATIC_META_INFO(SphereModel)

    virtual const ClassName &getClassName() const
    {
        return getStaticClassName();
    }

    enum { SphereModelHash = std::uint64_t(4 << 3) };
    size_t getClassHash() const
    {
        return SphereModelHash | ProceduralModel::getClassHash();
    }

    SphereModel(const SphereModelOptions &options);

protected:
    SphereModel(const SphereModelOptions &options, bool init);

    void _init(const SphereModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const SphereModelOptions &options);
    Ogre::MeshPtr createMesh(const SphereModelOptions &options) const;

    static std::string getProceduralMeshName(const SphereModelOptions &options);
};

#endif // SPHERERMODEL_H

