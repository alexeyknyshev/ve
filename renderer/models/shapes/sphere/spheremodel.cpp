#include "spheremodel.h"

#include <renderer/models/shapes/sphere/spheremodeloptions.h>

#include <framework/stringconverter.h>

#include <OGRE/OgreRoot.h>

#include <OgreProcedural/ProceduralSphereGenerator.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

SphereModel::SphereModel(const SphereModelOptions &options)
    : SphereModel(options, true)
{ }

SphereModel::SphereModel(const SphereModelOptions &options, bool init)
    : ProceduralModel(options)
{
    _init(options, init);
}

void SphereModel::_init(const SphereModelOptions &options, bool meshInit)
{
    SphereModelOptions opt(options);

    if (opt.radius == 0.0f)
    {
        log("Sphere radius is zero. It will be 0.5f instead.",
            Logger::LOG_LEVEL_WARNING);

        opt.radius = 0.5f;
    }
    else
    if (opt.radius < 0.0f)
    {
        log("Sphere radius is negative. It will be absolute instead.",
            Logger::LOG_LEVEL_WARNING);

        opt.radius = Ogre::Math::Abs(opt.radius);
    }

    opt.meshName = getProceduralMeshName(options);

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const SphereModelOptions &options, bool meshInit)),
                       opt.modelName);

        initFromMesh(createMesh(opt));

        mRigidBody = createRigidBody(opt);
    }

    setModelNodeParent(opt.parentNode);
}

RigidBody *SphereModel::createRigidBody(const SphereModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(SphereRigidBody)),
                                      options,
                                      _getEntity());
}

Ogre::MeshPtr SphereModel::createMesh(const SphereModelOptions &options) const
{
    const std::string meshName = getProceduralMeshName(options);
    Ogre::MeshPtr mesh = ProceduralModel::createMesh(meshName,
                                                     options.resourceGroup);
    if (!mesh.isNull())
    {
        return mesh;
    }

    Procedural::SphereGenerator generator;
    generator.setRadius(options.radius);

    return generator.realizeMesh(meshName, options.resourceGroup);
}

std::string SphereModel::getProceduralMeshName(const SphereModelOptions &options)
{
    return std::string("sphere" + StringConverter::toString(options.radius));
}
