#ifndef SPHEREMODELOPTIONS_H
#define SPHEREMODELOPTIONS_H

#include <renderer/models/physics/physicsmodeloptions.h>

class SphereModelOptions : public PhysicsModelOptions
{
public:
    SphereModelOptions()
        : radius(0.5f)
    { }

    float radius;
};

#endif // SPHEREMODELOPTIONS_H
