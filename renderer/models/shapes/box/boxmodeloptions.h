#ifndef BOXMODELOPTIONS_H
#define BOXMODELOPTIONS_H

#include <renderer/models/physics/physicsmodeloptions.h>

class BoxModelOptions : public PhysicsModelOptions
{
public:
    BoxModelOptions()
        : dimensions(Ogre::Vector3(1.0f, 1.0f, 1.0f))
    { }

    Ogre::Vector3 dimensions;
};

#endif // BOXMODELOPTIONS_H
