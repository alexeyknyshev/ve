#ifndef BOXMODEL_H
#define BOXMODEL_H

#include <renderer/models/procedural/proceduralmodel.h>

class BoxModelOptions;

class BoxModel : public ProceduralModel
{
public:
    VE_DECLARE_STATIC_META_INFO(BoxModel)

    virtual const ClassName &getClassName() const override
    {
        return getStaticClassName();
    }

    enum { BoxModelHash = std::uint64_t(1 << 3) };
    size_t getClassHash() const override
    {
        return BoxModelHash | ProceduralModel::getClassHash();
    }

    BoxModel(const BoxModelOptions &options);

protected:
    BoxModel(const BoxModelOptions &options, bool meshInit);

    void _init(const BoxModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const BoxModelOptions &options);
    Ogre::MeshPtr createMesh(const BoxModelOptions &options) const;

    static std::string getProceduralMeshName(const BoxModelOptions &options);
};

#endif // BOXMODEL_H
