#include "boxmodel.h"

#include <renderer/models/shapes/box/boxmodeloptions.h>

#include <framework/stringconverter.h>

#include <OgreProcedural/ProceduralBoxGenerator.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

BoxModel::BoxModel(const BoxModelOptions &options)
    : BoxModel(options, true)
{ }

BoxModel::BoxModel(const BoxModelOptions &options, bool meshInit)
    : ProceduralModel(options)
{
    _init(options, meshInit);
}

void BoxModel::_init(const BoxModelOptions &options, bool meshInit)
{
    BoxModelOptions opt(options);

    if (opt.dimensions == Ogre::Vector3::ZERO)
    {
        log("Box size is zero. It will be 1x1x1 instead.",
            Logger::LOG_LEVEL_WARNING);

        opt.dimensions = Ogre::Vector3(1.0f, 1.0f, 1.0f);
    }
    else
    {
        if (opt.dimensions.x < 0.0f)
        {
            log("Box x-axiz size is negative. It will be absolute instead.",
                Logger::LOG_LEVEL_WARNING);

            opt.dimensions.x = Ogre::Math::Abs(opt.dimensions.x);
        }

        if (opt.dimensions.y < 0.0f)
        {
            log("Box y-axiz size is negative. It will be absolute instead.",
                Logger::LOG_LEVEL_WARNING);

            opt.dimensions.y = Ogre::Math::Abs(opt.dimensions.x);
        }

        if (opt.dimensions.z < 0.0f)
        {
            log("Box z-axiz size is negative. It will be absolute instead.",
                Logger::LOG_LEVEL_WARNING);
            opt.dimensions.z = Ogre::Math::Abs(opt.dimensions.x);
        }
    }

    // -------------------------------------

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const BoxModelOptions &options, bool meshInit)),
                       opt.modelName);

        if (opt.meshName.empty())
        {
            opt.meshName = opt.getFallbackMeshName();
        }

        initFromMesh(createMesh(opt));

        mRigidBody = createRigidBody(opt);
    }
}

RigidBody *BoxModel::createRigidBody(const BoxModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(BoxRigidBody)),
                                      options,
                                      _getEntity());
}

Ogre::MeshPtr BoxModel::createMesh(const BoxModelOptions &options) const
{
    const std::string meshName = getProceduralMeshName(options);
    Ogre::MeshPtr mesh = ProceduralModel::createMesh(meshName,
                                                     options.resourceGroup);
    if (!mesh.isNull())
    {
        return mesh;
    }

    const int segX = options.segments.x == -1 ? Ogre::Math::Floor(options.dimensions.x) + 1 : options.segments.x;
    const int segY = options.segments.y == -1 ? Ogre::Math::Floor(options.dimensions.y) + 1 : options.segments.y;
    const int segZ = options.segments.z == -1 ? Ogre::Math::Floor(options.dimensions.z) + 1 : options.segments.z;

    Procedural::BoxGenerator generator;
    generator.setSize(options.dimensions);
    generator.setNumSegX(segX);
    generator.setNumSegY(segY);
    generator.setNumSegZ(segZ);

    return generator.realizeMesh(meshName, options.resourceGroup);
}

std::string BoxModel::getProceduralMeshName(const BoxModelOptions &options)
{
    return std::string("box" + StringConverter::toString(options.dimensions));
}
