#ifndef CYLINDERMODELOPTIONS_H
#define CYLINDERMODELOPTIONS_H

#include <OGRE/OgreVector3.h>

#include <renderer/models/physics/physicsmodeloptions.h>

class CylinderModelOptions : public PhysicsModelOptions
{
public:
    CylinderModelOptions()
        : dimensions(Ogre::Vector3::ZERO)
    { }

    CylinderModelOptions(const PhysicsModelOptions &other)
        : PhysicsModelOptions(other),
          dimensions(Ogre::Vector3::ZERO)
    {
    }

    Ogre::Vector3 dimensions;
};

#endif // CYLINDERMODELOPTIONS_H
