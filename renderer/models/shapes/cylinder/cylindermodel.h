#ifndef CYLINDERMODEL_H
#define CYLINDERMODEL_H

#include <renderer/models/procedural/proceduralmodel.h>

class CylinderModelOptions;

class CylinderModel : public ProceduralModel
{
public:
    VE_DECLARE_STATIC_META_INFO(CylinderModel)

    virtual const ClassName &getClassName() const
    {
        return getStaticClassName();
    }

    enum { CylinderModelHash = std::uint64_t(2 << 3) };
    size_t getClassHash() const
    {
        return CylinderModelHash | ProceduralModel::getClassHash();
    }

    CylinderModel(const CylinderModelOptions &options);

protected:
    CylinderModel(const CylinderModelOptions &options, bool meshInit);

    void _init(const CylinderModelOptions &options, bool meshInit);

    RigidBody *createRigidBody(const CylinderModelOptions &options);
    Ogre::MeshPtr createMesh(const CylinderModelOptions &options) const;

    static std::string getProceduralMeshName(const CylinderModelOptions &options);
};

#endif // CYLINDERMODEL_H
