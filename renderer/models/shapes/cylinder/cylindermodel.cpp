#include "cylindermodel.h"

#include <renderer/models/shapes/cylinder/cylindermodeloptions.h>

#include <OGRE/OgreRoot.h>

#include <OgreProcedural/ProceduralCylinderGenerator.h>

#include <physics/physicsmanager.h>
#include <physics/shapes/physicsprerequisites.h>

#include <framework/stringconverter.h>

CylinderModel::CylinderModel(const CylinderModelOptions &options)
    : CylinderModel(options, true)
{ }

CylinderModel::CylinderModel(const CylinderModelOptions &options, bool meshInit)
    : ProceduralModel(options)
{
    _init(options, meshInit);
}

void CylinderModel::_init(const CylinderModelOptions &options, bool meshInit)
{
    CylinderModelOptions opt(options);

    if (opt.dimensions == Ogre::Vector3::ZERO)
    {
        log(VE_AS_STR(CylinderModelOptions::dimensions) + " is missing. "
            "Default configuration will be used instead (1.0; 1.0; 1.0).",
            Logger::LOG_LEVEL_WARNING);
        opt.dimensions = 1.0f; // each component
    }

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const CylinderModelOptions &options, bool meshInit)),
                       opt.modelName);

        if (opt.meshName.empty())
        {
            opt.meshName = opt.getFallbackMeshName();
        }

        initFromMesh(createMesh(opt));

        mRigidBody = createRigidBody(opt);
    }
}

RigidBody *CylinderModel::createRigidBody(const CylinderModelOptions &options)
{
    return physics()->createRigidBody(ClassName(VE_AS_STR(CylinderRigidBody)),
                                      options,
                                      _getEntity());
}

Ogre::MeshPtr CylinderModel::createMesh(const CylinderModelOptions &options) const
{
    const std::string meshName = getProceduralMeshName(options);
    Ogre::MeshPtr mesh = ProceduralModel::createMesh(meshName,
                                                     options.resourceGroup);
    if (!mesh.isNull())
    {
        return mesh;
    }

    Procedural::CylinderGenerator generator;
    generator.setRadius(options.dimensions.x * 0.5f)
            .setHeight(options.dimensions.y)
            .setPosition(Ogre::Vector3(0, -options.dimensions.y * 0.5f, 0));

    const float zScaleFactor = options.dimensions.z / options.dimensions.x;
    if (!Ogre::Math::RealEqual(zScaleFactor, 1.0f))
    {
        generator.setScale(Ogre::Vector3(1, 1, zScaleFactor));
    }

    return generator.realizeMesh(meshName, options.resourceGroup);
}

std::string CylinderModel::getProceduralMeshName(const CylinderModelOptions &options)
{
    return std::string("cylinder" + StringConverter::toString(options.dimensions));
}
