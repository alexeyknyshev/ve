#ifndef BASECAMERA_H
#define BASECAMERA_H

#include <memory>

#include <global.h>

#include <OGRE/OgreCamera.h>

#include <framework/object.h>

class CameraController;

class BaseCamera : public Object,
                   public Ogre::Camera::Listener
{
    friend class RenderView;
    friend class Renderer;

public:
    VE_DECLARE_TYPE_INFO(BaseCamera)

#ifndef SWIG
    BaseCamera();
#endif // SWIG

    BaseCamera(const std::string &name);

    bool event(const Event *event) override
    {
        VE_UNUSED(event);
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_RENDERER);
        return false;
    }

    virtual ~BaseCamera();

    const std::string &getName() const override;

    /** Check if internal Camera Impl exists */
    bool isInitialized() const;

    Ogre::Viewport *getLastViewport() const;

    void setPosition(const Ogre::Vector3 &pos);
    const Ogre::Vector3 &getPosition() const;

    void setNearClipDistance(float nearDist);
    float getNearClipDistance() const;

    void setFarClipDistance(float farDist);
    float getFarClipDistance() const;

    void setLookAt(const Ogre::Vector3 &target);

    void setDirection(const Ogre::Vector3 &dir);
    Ogre::Vector3 getDirection() const;

    void setOrientation(const Ogre::Quaternion &orient);
    const Ogre::Quaternion &getOrientation() const;

    /** --------------------------- **/

    void roll(const Ogre::Radian &angle);

    void yaw(const Ogre::Radian &angle);

    void pitch(const Ogre::Radian &angle);

    /** --------------------------- **/

    void moveRelative(const Ogre::Vector3 &direction);

    void move(const Ogre::Vector3 &direction);

    /** --------------------------- **/

    void setAutoTracking(bool enabled,
                         Ogre::SceneNode *trackingTarget = 0,
                         const Ogre::Vector3 &lookAtOffset =
                            Ogre::Vector3::ZERO);

    Ogre::SceneNode *getAutoTrackingTarget() const;

    const Ogre::Vector3 &getAutoTrackingLookAtOffset() const;

    bool isAutoTrackingEnabled() const;


    /** --------------------------- **/

    void setAspectRatio(float ratio);

    /** --------------------------- **/

    enum PolygonMode
    {
        PM_Points,      /// Only points are rendered.
        PM_Wireframe,   /// Wireframe models are rendered.
        PM_Solid        /// Solid polygons are rendered.
    };

    void setPolygonMode(PolygonMode mode);
    PolygonMode getPolygonMode() const;

    static PolygonMode getNextPolygonMode(PolygonMode mode);

    /** --------------------------- **/

    void setCameraController(CameraController *controller);

    CameraController *getCameraController() const
    {
        return mCameraController;
    }

    /** ------ Listener ------ */
    void cameraDestroyed(Ogre::Camera *cam) override;

protected:
    BaseCamera(Ogre::Camera *camera);

    std::shared_ptr<Ogre::Camera> getCameraImpl(bool checkPtr = true) const;

    virtual Ogre::Camera *createCameraImpl(const std::string &name);

    Ogre::Camera *detachOgreCamera();
    Ogre::Camera *attachOgreCamera(Ogre::Camera *camera);

private:
    std::shared_ptr<Ogre::Camera> mCamera;
    CameraController *mCameraController;
};

typedef std::list<BaseCamera *> BaseCameraList;

#endif // BASECAMERA_H
