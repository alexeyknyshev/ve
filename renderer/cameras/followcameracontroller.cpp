#include "followcameracontroller.h"

#include <OGRE/OgreViewport.h>

#include <OGRE/OgreStringConverter.h>

FollowCameraController::FollowCameraController(const std::string &name,
                                               const FollowCamera &followCamera,
                                               float zoomSpeed,
                                               float rotationSpeed)
    : CameraController(name),
      mCamera(followCamera),
      mDeltaZoom(1.0f),
      mZoomSpeed(zoomSpeed),
      mRotationSpeed(rotationSpeed),
      mRotateMode(false)
{ }

void FollowCameraController::update(float deltaTime, float realTime)
{
    mCamera.update(deltaTime, realTime);

    if (mDeltaZoom != 1.0f)
    {
        const float newZoom = deltaTime * mDeltaZoom * mZoomSpeed;
        mCamera.setFollowOffset(newZoom + mCamera.getFollowOffset());

        const float zoomDelta = 0.5f * deltaTime;

        if (mDeltaZoom > 1.0f + zoomDelta)
        {
            mDeltaZoom -= zoomDelta;
        }
        else
        if (mDeltaZoom < -1.0f - zoomDelta)
        {
            mDeltaZoom += zoomDelta;
        }
        else
        {
            mDeltaZoom = 1.0f;
        }

//        std::string zoom = StringConverter::toString(mDeltaZoom, 6, 2);
//        log("Zoom: " + zoom, Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_RENDERER);
    }


    if (mDeltaRotation != Ogre::Radian())
    {
        const Ogre::Radian newRotation = deltaTime * mDeltaRotation * mRotationSpeed;

        const float rotationDelta = 0.06f * deltaTime;

        if (mDeltaRotation.valueRadians() > rotationDelta)
        {
            mDeltaRotation -= Ogre::Radian(rotationDelta);
        }
        else
        if (mDeltaRotation.valueRadians() < -rotationDelta)
        {
            mDeltaRotation += Ogre::Radian(rotationDelta);
        }
        else
        {
            mDeltaRotation = Ogre::Radian();
        }

        mCamera.setFollowOffset(Ogre::Quaternion(newRotation, Ogre::Vector3::UNIT_Y) *
                                mCamera.getFollowOffset());
//        mDeltaRotation = Ogre::Radian();
    }
}

void FollowCameraController::registredAsInputListener(bool registred, InputController *controller)
{
    mCamera.controllStateChanged(registred);
}

bool FollowCameraController::mouseMoved(const MouseEvent &event)
{
    if (event.data.wheelVertRelative != 0)
    {
        mDeltaZoom = -0.01f * event.data.wheelVertRelative;
    }

    if (isButtonPressed(ve::MouseData::MB_Right))
    {
        const float width = mCamera.getLastViewport()->getActualWidth();
        mDeltaRotation = (float)-event.data.xRelative / width * Ogre::Math::PI;
    }

    return true;
}
