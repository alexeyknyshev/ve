#ifndef CAMERAMODECONTROLLER_H
#define CAMERAMODECONTROLLER_H

#include <renderer/cameras/cameracontroller.h>
#include <renderer/cameras/controlledcamera.h>

class CameraModeController : public CameraController
{
public:
    CameraModeController(const std::string &name,
                         const ControlledCamera &camera,
                         ve::KeyData::KeyButton modeSwitchKey);

    void update(float deltaTime, float realTime) override;

protected:
    void registredAsInputListener(bool registred, InputController *controller) override
    { VE_UNUSED(registred); VE_UNUSED(controller); }

private:
    ControlledCamera mCamera;
    const ve::KeyData::KeyButton mModeSwitchKey;
};

#endif // CAMERAMODECONTROLLER_H
