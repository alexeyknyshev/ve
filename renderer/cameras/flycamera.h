#ifndef FLYCAMERA_H
#define FLYCAMERA_H

#include <renderer/cameras/controlledcamera.h>
//#include <renderer/model.h>

class FlyCamera : public ControlledCamera
{
public:
    FlyCamera();
    FlyCamera(const std::string &name);
    FlyCamera(const BaseCamera &base);

    void controllStateChanged(bool isControlled);

protected:
    FlyCamera(Ogre::Camera *camera);
/*
private:
    ModelPtr mModel;
*/
};

#endif // FLYCAMERA_H
