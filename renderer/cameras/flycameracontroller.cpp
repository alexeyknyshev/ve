#include "flycameracontroller.h"

#include <OGRE/OgreViewport.h>

#include <framework/stringconverter.h>
#include <framework/container.h>

static float movingHelper(bool positive, bool negative, float deltaTime, float &movingTime,
                          float movingSpeed, float dampingTime, EasingFunction func)
{
    if (positive ^ negative)
    {
        const float directionMultiplier = (positive ? 1.0f : -1.0f);

        Inverter inverter;

        if (positive)
        {
            if (movingTime < 0)
            {
                inverter.setEnabled(true);
            }
        }
        else // negative
        {
            if (movingTime > 0)
            {
                inverter.setEnabled(true);
            }
        }

        if (std::fabs(movingTime) + deltaTime < dampingTime)
        {
            movingTime += directionMultiplier * deltaTime;
        }
        else
        {
            movingTime = directionMultiplier * dampingTime;
        }
        return directionMultiplier * movingSpeed * inverter(func(std::fabs(movingTime) / dampingTime));
    }
    else
    {
        const float directionMultiplier = (movingTime > 0 ? -1.0f : 1.0f);
        if (std::fabs(movingTime) - deltaTime > 0.0f)
        {
            movingTime += directionMultiplier * deltaTime;
        }
        else
        {
            movingTime = 0.0f;
        }
        return -directionMultiplier * movingSpeed * func(std::fabs(movingTime) / dampingTime);
    }
}

static float rotationHelper(Ogre::Radian deltaRotation, float deltaTime, float rotationTime,
                            float rotationSpeed, float dampingTime, EasingFunction func)
{

}

FlyCameraController::FlyCameraController(const std::string &name,
                                         const FlyCamera &camera,
                                         float movingSpeed,
                                         float rotationSpeed,
                                         float dampingFactor)
    : CameraController(name),
      mMovingSpeed(movingSpeed),
      mRotationSpeed(rotationSpeed),
      mDamptingTime(1.0f),

      mMovingVector(Ogre::Vector3::ZERO),
      mMovingTimeXYZ(Ogre::Vector3::ZERO),

      mRotation(Ogre::Vector2::ZERO),
      mRotationTimeXY(Ogre::Vector2::ZERO),

      mCamera(camera)
{
    mEasingFunc = Easing::getFunction(Easing::LinearInterpolation);
    mDampingFactor = Ogre::Math::Clamp(dampingFactor, 0.0f, 1.0f);
}

bool FlyCameraController::mouseMoved(const MouseEvent &event)
{
    const float height = mCamera.getLastViewport()->getActualHeight();
    const float width  = mCamera.getLastViewport()->getActualWidth();

    mRotation.x = (float)-event.data.xRelative / width  * Ogre::Math::PI * 24;
    mRotation.y = (float)-event.data.yRelative / height * Ogre::Math::PI * 24;

    return true;
}

void FlyCameraController::update(float deltaTime, float realTime)
{
    VE_UNUSED(realTime);


    /// ------ Moving forward while holding right & left mouse buttons

    bool forward = false;

    {
        const PressedButtons &pressedButtons = getPressedButtons();

        if (contains(pressedButtons, ve::MouseData::MB_Left) &&
            contains(pressedButtons, ve::MouseData::MB_Right))
        {
            forward = true;
        }
    }

    /// ------ Keyboard moving ------

    const PressedKeys &pressedKeys = getPressedKeys();

    forward |= contains(pressedKeys, ve::KeyData::KEY_W);
    const bool backward = contains(pressedKeys, ve::KeyData::KEY_S);

    const bool left  = contains(pressedKeys, ve::KeyData::KEY_A);
    const bool right = contains(pressedKeys, ve::KeyData::KEY_D);

    const bool down = contains(pressedKeys, ve::KeyData::KEY_X);
    const bool up   = contains(pressedKeys, ve::KeyData::KEY_SPACE);

    /// ------ Moving ------

    mMovingVector.z = movingHelper(backward, forward, deltaTime, mMovingTimeXYZ.z, getMovingSpeed(), getDamptingTime(), mEasingFunc);
    mMovingVector.x = movingHelper(right,     left,   deltaTime, mMovingTimeXYZ.x, getMovingSpeed(), getDamptingTime(), mEasingFunc);
    mMovingVector.y = movingHelper(up,        down,   deltaTime, mMovingTimeXYZ.y, getMovingSpeed(), getDamptingTime(), mEasingFunc);

    if (!mMovingVector.positionEquals(Ogre::Vector3::ZERO))
    {
        logObjectTrace("MV " + StringConverter::toString(mMovingVector),  Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_USERINPUT);
        logObjectTrace("MT " + StringConverter::toString(mMovingTimeXYZ), Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_USERINPUT);
    }
    mCamera.moveRelative(mMovingVector * deltaTime);

    /// ------ Rotation ------

    const float rotationDampingFactor = 1.0f - (0.97f * deltaTime);

    if (mRotation.x != 0.0f)
    {
        mRotation.x *= rotationDampingFactor;

//        mCamera.yaw(mRotaionX * mRotationSpeed * deltaTime * 1000);

        if (Ogre::Math::Abs(mRotation.x) < 0.01f) mRotation.x = 0.0f;
    }

    if (mRotation.y != 0.0f)
    {
        mRotation.y *= rotationDampingFactor;

//        mCamera.pitch(mRotaionY * mRotationSpeed * deltaTime * 1000);

        if (Ogre::Math::Abs(mRotation.y) < 0.01f) mRotation.y = 0.0f;
    }
}

//void FlyCameraController::setDampingEasingFunction(Easing::Type type)
//{


//Easing::Type FlyCameraController::getDampingEasingFunction() const
//{

//}
