#include "flycamera.h"

FlyCamera::FlyCamera()
//    : mModel()
{ }

FlyCamera::FlyCamera(const std::string &name)
    : ControlledCamera(name)/*,
      mModel()*/
{ }

FlyCamera::FlyCamera(const BaseCamera &base)
    : ControlledCamera(base)
{
}

FlyCamera::FlyCamera(Ogre::Camera *camera)
    : ControlledCamera(camera)
{
}

void FlyCamera::controllStateChanged(bool isControlled)
{
    VE_UNUSED(isControlled);
}
