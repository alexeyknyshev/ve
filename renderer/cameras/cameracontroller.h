#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <framework/controller/defaultinputlistener.h>

class CameraController : public DefaultInputListener
{
public:
    CameraController(const std::string &name);
};

#endif // CAMERACONTROLLER_H
