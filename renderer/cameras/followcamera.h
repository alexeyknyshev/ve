#ifndef FOLLOWCAMERA_H
#define FOLLOWCAMERA_H

#include <renderer/cameras/controlledcamera.h>

class FollowCamera : public ControlledCamera
{
public:
    FollowCamera();

    FollowCamera(const std::string &name,
                 Ogre::SceneNode *followNode = nullptr,
                 const Ogre::Vector3 &offset = Ogre::Vector3::ZERO);

    FollowCamera(const BaseCamera &base,
                 Ogre::SceneNode *followNode = nullptr,
                 const Ogre::Vector3 &offset = Ogre::Vector3::ZERO);

    void update(float deltaTime, float realTime);

    void setFollowNode(Ogre::SceneNode *node)
    {
        mFollowNode = node;
    }

    void setFollowOffset(const Ogre::Vector3 &offset)
    {
        mFollowOffset = offset;
    }

    Ogre::SceneNode *getFollowNode() const
    {
        return mFollowNode;
    }

    const Ogre::Vector3 &getFollowOffset() const
    {
        return mFollowOffset;
    }

    void controllStateChanged(bool isControlled);

private:
    Ogre::Vector3 mFollowOffset;
    Ogre::SceneNode *mFollowNode;
    bool mIsFollowing;
};

#endif // FOLLOWCAMERA_H
