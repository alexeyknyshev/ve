#ifndef CONTROLLEDCAMERA_H
#define CONTROLLEDCAMERA_H

#include <renderer/basecamera.h>

class ControlledCamera : public BaseCamera
{
public:
    ControlledCamera();
    ControlledCamera(const std::string &name);
    ControlledCamera(const BaseCamera &other);

    virtual void controllStateChanged(bool isControlled) { VE_UNUSED(isControlled); }

protected:
    ControlledCamera(Ogre::Camera *camera);
};

#endif // CONTROLLEDCAMERA_H
