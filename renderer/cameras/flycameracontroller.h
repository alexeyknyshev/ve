#ifndef FLYCAMERACONTROLLER_H
#define FLYCAMERACONTROLLER_H

#include <renderer/cameras/cameracontroller.h>
#include <renderer/cameras/flycamera.h>

#include <framework/easing/easing.h>

class FlyCameraController : public CameraController
{
public:
    VE_DECLARE_TYPE_INFO(FlyCameraController)

    FlyCameraController(const std::string &name,
                        const FlyCamera &camera,
                        float movingSpeed,
                        float rotationSpeed,
                        float dampingFactor = 0.5f);

    /** ------ Mouse ------ */

    bool mouseMoved(const MouseEvent &event);

    /** ------ Updating ------ */

    void update(float deltaTime, float realTime);

    bool event(const Event *event) override
    {
        VE_UNUSED(event);
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_USERINPUT);
        return false;
    }

    void registredAsInputListener(bool registred, InputController *controller)
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

    void setMovingSpeed(float speed)
    {
        mMovingSpeed = speed;
    }

    float getMovingSpeed() const
    {
        return mMovingSpeed;
    }

    void setDamptingTime(float time)
    {
        mDamptingTime = time;
    }

    float getDamptingTime() const
    {
        return mDamptingTime;
    }

    void setDampingEasingFunction(Easing::Type type)
    {
        mEasingFunc = Easing::getFunction(type);
        mEasingFuncType = type;
    }

    Easing::Type getDampingEasingFunction() const
    {
        return mEasingFuncType;
    }

    const Ogre::Vector3 &getMovingVector() const
    {
        return mMovingVector;
    }

    const Ogre::Vector3 &getMovingTimeXYZ() const
    {
        return mMovingTimeXYZ;
    }

private:
    float mMovingSpeed;
    float mRotationSpeed;
    float mDampingFactor;
    float mDamptingTime;

    Ogre::Vector3 mMovingVector;
    Ogre::Vector3 mMovingTimeXYZ;

    Ogre::Vector2 mRotation;
    Ogre::Vector2 mRotationTimeXY;

    FlyCamera mCamera;

    EasingFunction mEasingFunc;
    Easing::Type mEasingFuncType;
};

#endif // FLYCAMERACONTROLLER_H
