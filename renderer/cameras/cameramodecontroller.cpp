#include <renderer/cameras/cameramodecontroller.h>

CameraModeController::CameraModeController(const std::string &name,
                                           const ControlledCamera &camera,
                                           ve::KeyData::KeyButton modeSwitchKey)
    : CameraController(name),
      mCamera(camera),
      mModeSwitchKey(modeSwitchKey)
{ }

void CameraModeController::update(float deltaTime, float realTime)
{
    static float timeOut = 0.0f;

    DefaultInputListener::update(deltaTime, realTime);

    timeOut += realTime;

    if (isKeyPressed(mModeSwitchKey) && timeOut > 0.5f)
    {
        mCamera.setPolygonMode(BaseCamera::getNextPolygonMode(mCamera.getPolygonMode()));
        timeOut = 0.0f;
    }
}
