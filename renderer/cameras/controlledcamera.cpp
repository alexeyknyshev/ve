#include "controlledcamera.h"

ControlledCamera::ControlledCamera()
{ }

ControlledCamera::ControlledCamera(const std::string &name)
    : BaseCamera(name)
{ }

ControlledCamera::ControlledCamera(const BaseCamera &other)
    : BaseCamera(other)
{ }

ControlledCamera::ControlledCamera(Ogre::Camera *camera)
    : BaseCamera(camera)
{ }
