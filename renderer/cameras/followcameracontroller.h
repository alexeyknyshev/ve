#ifndef FOLLOWCAMERACONTROLLER_H
#define FOLLOWCAMERACONTROLLER_H

#include <renderer/cameras/cameracontroller.h>
#include <renderer/cameras/followcamera.h>

class FollowCameraController : public CameraController
{
public:
    VE_DECLARE_TYPE_INFO(FollowCameraController)

    FollowCameraController(const std::string &name, const FollowCamera &followCamera,
                           float zoomSpeed, float rotationSpeed);

    bool event(const Event *event)
    {
        VE_UNUSED(event);
        return false;
    }

    void update(float deltaTime, float realTime);

    bool mouseMoved(const MouseEvent &event);

    void registredAsInputListener(bool registred, InputController *controller);

protected:
    FollowCamera mCamera;
    float mDeltaZoom;

    float mZoomSpeed;
    float mRotationSpeed;

    bool mRotateMode;
    Ogre::Radian mDeltaRotation;
};

#endif // FOLLOWCAMERACONTROLLER_H
