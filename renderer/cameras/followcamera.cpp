#include "followcamera.h"

#include <OGRE/OgreSceneNode.h>

FollowCamera::FollowCamera()
    : ControlledCamera(),
      mFollowOffset(Ogre::Vector3::ZERO),
      mFollowNode(nullptr),
      mIsFollowing(false)
{
}

FollowCamera::FollowCamera(const std::string &name,
                           Ogre::SceneNode *followNode,
                           const Ogre::Vector3 &offset)
    : ControlledCamera(name),
      mFollowOffset(offset),
      mFollowNode(followNode),
      mIsFollowing(false)
{
}

FollowCamera::FollowCamera(const BaseCamera &base,
                           Ogre::SceneNode *followNode,
                           const Ogre::Vector3 &offset)
    : ControlledCamera(base),
      mFollowOffset(offset),
      mFollowNode(followNode),
      mIsFollowing(false)
{
}

void FollowCamera::update(float deltaTime, float realTime)
{
    VE_UNUSED(deltaTime);
    VE_UNUSED(realTime);

    if (mFollowNode && mIsFollowing)
    {
        const Ogre::Vector3 &targetPos = mFollowNode->_getDerivedPosition();
        getCameraImpl()->setPosition(targetPos + mFollowOffset);

        setLookAt(targetPos);
    }
}

void FollowCamera::controllStateChanged(bool isControlled)
{
    mIsFollowing = isControlled;
}
