#include "renderer.h"

#include <OGRE/OgreRenderWindow.h>
#include <OGRE/OgreWindowEventUtilities.h>
#include <OGRE/OgreCommon.h>

#include <framework/filesystem.h>
#include <framework/event.h>
#include <framework/frontend/renderviewbackend.h>

#include <renderer/renderwindowoptions.h>
#include <renderer/basecamera.h>

#include <framework/ui/uimanager.h>

Renderer::Renderer(const ParamsMulti &options)
    : Root("", "", ""),
      mRenderViewBackend(nullptr),
      mNameGenerator("ve_"),
      mGenericSceneManagerName("GenericSceneManager"),
      mOpenGLRenderingSubsystemName("OpenGL Rendering Subsystem"),
      mUiManager(nullptr)
{
    log("Renderer created", Logger::LOG_LEVEL_INFO);

    const auto *renderSystem = createRenderSystem(options);
    if (!renderSystem)
    {
        logAndAssert(VE_SCOPE_NAME(Renderer(const ParamsMulti &options)),
                     "Failed to create renderSystem!",
                     Logger::LOG_COMPONENT_RENDERER);
    }

    const auto *sceneMgr = createSceneManager(options);
    if (!sceneMgr)
    {
        logAndAssert(VE_SCOPE_NAME(Renderer(const ParamsMulti &options)),
                     "Failed to create sceneManager!",
                     Logger::LOG_COMPONENT_RENDERER);
    }

    if (renderSystem && sceneMgr)
    {
        mUiManager = new UiManager;
    }
}

Renderer::~Renderer()
{
    delete mUiManager;
    mUiManager = nullptr;

    setRenderViewBackend(nullptr);

    log("Renderer destroyed", Logger::LOG_LEVEL_INFO);
}

void Renderer::log(const std::string &msg, int level) const
{
    Object::log(msg, level, Logger::LOG_COMPONENT_RENDERER);
}

//----------------------------------------------------------------

Renderer *Renderer::getSingletonPtr()
{
    return static_cast<Renderer *>(Ogre::Root::getSingletonPtr());
}

Renderer *renderer(bool checkPoiner)
{
    Renderer *renderer = Renderer::getSingletonPtr();
    if (checkPoiner)
    {
        VE_CHECK_NULL_PTR(Renderer, renderer,
                          Logger::LOG_COMPONENT_RENDERER);
    }
    return renderer;
}

//----------------------------------------------------------------

Ogre::RenderSystem *Renderer::createRenderSystem(const ParamsMulti &options)
{
    std::string renderSystemPath;
    std::string renderSystemName;

    Ogre::RenderSystem *system = nullptr;

    const auto it = options.find(VE_OPTION_PLUGIN_RENDER_SYSTEM);
    if (it != options.end())
    {
        renderSystemPath = it->second;

        std::string renderSystemPathLower = renderSystemPath;
        Ogre::StringUtil::toLowerCase(renderSystemPathLower);
        if (renderSystemPathLower.find("gl") != std::string::npos)
        {
            renderSystemName = mOpenGLRenderingSubsystemName;
        }
        /// TODO: gl3 and dx
        //else if () { }
    }
    else
    {
        renderSystemPath = "./RenderSystem_GL." + FileSystem::getSharedLibExtensionStr();
        renderSystemName = mOpenGLRenderingSubsystemName;
    }

    const size_t nameBegin = renderSystemName.find("OpenGL");
    if (nameBegin != std::string::npos)
    {
        std::string defaultPath = "";

        if (renderSystemName.find("OpenGL3", nameBegin) != std::string::npos)
        {
            defaultPath = "./RenderSystem_GL3Plus." + FileSystem::getSharedLibExtensionStr();
        }
        else
        {
            defaultPath = "./RenderSystem_GL." + FileSystem::getSharedLibExtensionStr();
        }

        const auto rspIt = options.find(VE_OPTION_PLUGIN_RENDER_SYSTEM);
        if (rspIt != options.end())
        {
            renderSystemPath = rspIt->second;
        }
        else
        {
            renderSystemPath = defaultPath;
        }

        if (loadPlugin(renderSystemPath))
        {
            mRendereringSubsystemToPluginName.insert({ renderSystemName, renderSystemPath });
            log(renderSystemName + " plugin loaded", Logger::LOG_LEVEL_INFO);

            /*
            for (Ogre::RenderSystem *renderSystem : getAvailableRenderers())
            {
                logger()->logClassMessage(VE_CLASS_NAME_STR(Renderer),
                                          VE_SCOPE_NAME_STR(Renderer::createRenderSystem()),
                                          renderSystem->getName(),
                                          Logger::LOG_LEVEL_INFO,
                                          Logger::LOG_COMPONENT_RENDERER);
            }
            */

            system = getRenderSystemByName(renderSystemName);
            if (system)
            {
                setRenderSystem(system);
                initialise(false);
                log(renderSystemName + " initialized", Logger::LOG_LEVEL_INFO);
            }
            else
            {
                log("Failed to initialize \"" + renderSystemName + "\" rendering subsystem\n" +
                    "^ No such rendering subsystem \"" + renderSystemName + "\"",
                    Logger::LOG_LEVEL_CRITICAL);
            }
        }
        else
        {
            log("Failed to load \"" + renderSystemName + "\" rendering subsystem plugin!",
                Logger::LOG_LEVEL_CRITICAL);
            log(renderSystemPath, Logger::LOG_LEVEL_CRITICAL);
            log("^ Check plugin filepath (option: " VE_OPTION_PLUGIN_RENDER_SYSTEM ")", Logger::LOG_LEVEL_CRITICAL);
        }
    }

    return system;
}

void Renderer::destroyRenderSystem(const std::string &name)
{
    auto activeRenderSystem = getRenderSystem();
    auto renderSystem = getRenderSystemByName(name);
    if (renderSystem)
    {
        if (activeRenderSystem == renderSystem)
        {
            setRenderSystem(nullptr);
            /// ^ renderSystem->shutdown() is called inside
        }

        auto it = mRendereringSubsystemToPluginName.find(name);
        if (it != mRendereringSubsystemToPluginName.end())
        {
            unloadPlugin(it->second);
        }
    }

    log(name + " unloaded", Logger::LOG_LEVEL_INFO);
}

void Renderer::destroyRenderSystem()
{
    if (getRenderSystem())
    {
        delete mUiManager;
        mUiManager = nullptr;

        destroyRenderSystem(getRenderSystem()->getName());
    }
}

//----------------------------------------------------------------

Ogre::SceneManager *Renderer::createSceneManager(const ParamsMulti &options)
{
    Ogre::SceneManager *sceneMgr = nullptr;

    log("Creating SceneManager...", Logger::LOG_LEVEL_DEBUG);

    const std::string defaultPath =
            "./Plugin_OctreeSceneManager." + FileSystem::getSharedLibExtensionStr();

    const std::string sceneMgrPath = options.get(VE_OPTION_PLUGIN_SCENE_MANAGER, defaultPath);

    if (loadPlugin(sceneMgrPath))
    {
        mRegistredSceneMgrs.insert("Generic");
        sceneMgr = Ogre::Root::createSceneManager(Ogre::ST_GENERIC, mGenericSceneManagerName);
        log(mGenericSceneManagerName + " created", Logger::LOG_LEVEL_INFO);
    }
    else
    {
        log("Failed to load \"" + mGenericSceneManagerName + "\" plugin!",
            Logger::LOG_LEVEL_CRITICAL);
        log(sceneMgrPath, Logger::LOG_LEVEL_CRITICAL);
        log("^ Check plugin filepath (option: " VE_OPTION_PLUGIN_SCENE_MANAGER ")", Logger::LOG_LEVEL_CRITICAL);
    }

    return sceneMgr;
}

void Renderer::destroySceneManager()
{
    auto sceneManager = getSceneManager();
    if (sceneManager)
    {
        std::string name = sceneManager->getName();
        Ogre::Root::destroySceneManager(sceneManager);
        log(name + " destroyed", Logger::LOG_LEVEL_INFO);
    }
}

Ogre::SceneManager *Renderer::getSceneManager() const
{
    if (Ogre::Root::hasSceneManager(mGenericSceneManagerName))
    {
        return Ogre::Root::getSceneManager(mGenericSceneManagerName);
    }
    return nullptr;
}

Ogre::SceneManager *Renderer::getSceneManager(const std::string &name) const
{
    return Ogre::Root::getSceneManager(name);
}

//----------------------------------------------------------------

void Renderer::setRenderViewBackend(RenderViewBackend *backend)
{
    if (mRenderViewBackend == backend)
    {
        return;
    }

    if (mRenderViewBackend)
    {
        if (backend && mRenderViewBackend->getBackendName() == backend->getBackendName())
        {
            log(VE_CLASS_NAME(RenderViewBackend).toString() + " with name \"" + backend->getBackendName() +
                "\" already has been registred, so ignored.", Logger::LOG_LEVEL_WARNING);
            return;
        }

        log("RenderWindow backend \"" + mRenderViewBackend->getBackendName() + "\" unregistered",
            Logger::LOG_LEVEL_INFO);
    }

    delete mRenderViewBackend;
    mRenderViewBackend = backend;

    if (backend)
    {
        log("RenderWindow backend \"" + backend->getBackendName() + "\" installed",
            Logger::LOG_LEVEL_INFO);
    }
}

//----------------------------------------------------------------

bool Renderer::getRenderViewSizeHint(ve::SizeU &sizeHint) const
{
    if (mRenderViewBackend)
    {
        return mRenderViewBackend->getRenderViewSizeHint(sizeHint);
    }
    return false;
}

//----------------------------------------------------------------

BaseCamera Renderer::createCamera(const std::string &name)
{
    return createCamera(getSceneManager(), name);
}

BaseCamera Renderer::createCamera(Ogre::SceneManager *mgr, const std::string &name)
{
    bool hasAnyRenderTarget = !getRenderTargetNames().empty();
    if (!hasAnyRenderTarget)
    {
        logAndAssert(VE_SCOPE_NAME(createCamera(Ogre::SceneManager *mgr, const std::string &name)),
                     "Cannot create camera named \"" + name + "\"! "
                     "There is no any RenderWindow (RenderTarget)",
                     Logger::LOG_COMPONENT_RENDERER);
    }

    const std::string _name = unique(mNameGenerator, mgr->getCameras().cbegin(), mgr->getCameras().cend(), name);

    BaseCamera camera(mgr->createCamera(_name));

    camera.setPosition(Ogre::Vector3(100, 100, 100));
    camera.setLookAt(Ogre::Vector3::ZERO);
    camera.setNearClipDistance(1);

    return camera;
}

bool Renderer::hasCamera(const std::string &name)
{
#if OGRE_VERSION_MAJOR == 2
    auto cameras = getSceneManager()->getCameras();
    std::find_if(cameras.begin(), cameras.end(),
                 [&](const Ogre::Camera *cam) -> bool { return name == cam->getName(); })
            != cameras.end();
#else
    return getSceneManager()->hasCamera(name);
#endif
}

bool Renderer::hasCamera(Ogre::Camera *camera) const
{
    return containsMap(getSceneManager()->getCameras(), camera);
}

StringList Renderer::getCameraNames() const
{
    StringList result;

    const auto &list = getSceneManager()->getCameras();
#if OGRE_VERSION_MAJOR == 2
    for (Ogre::Camera *cam : list)
    {
        result += cam->getName();
    }
#else
    const auto end = list.cend();
    for (auto it = list.cbegin(); it != end; ++it)
    {
        const std::string &cameraName = it->first;
        result += cameraName;
    }
#endif
    return result;
}

Ogre::Camera *Renderer::getOgreCamera(const std::string &name)
{
#if OGRE_VERSION_MAJOR == 1
    return getSceneManager()->getCamera(name);
#else
    for (Ogre::Camera *cam : getSceneManager()->getCameras())
    {
        if (cam->getName() == name)
        {
            return cam;
        }
    }
    return nullptr;
#endif
}

//----------------------------------------------------------------

Ogre::RenderWindow *Renderer::createRenderWindow(RenderWindowOptions &opt)
{
    /// first time window creating
    /// main context will be there
    if (mRenderTargetNames.empty())
    {
        opt._mainContext = true;
    }
    /// only if there is no any RenderTargets
    /// name can collide with another
    else
    {
        opt.name = unique(mNameGenerator,
                          mRenderTargetNames.cbegin(),
                          mRenderTargetNames.cend(),
                          opt.name);
    }

    Ogre::RenderWindow *ogreWindow = nullptr;
    if (mRenderViewBackend)
    {
        ogreWindow = mRenderViewBackend->createRenderWindow(opt);
        log("RenderWindow backend: " + mRenderViewBackend->getBackendName(),
            Logger::LOG_LEVEL_INFO);
        mRenderTargetNames.push_back(opt.name);
    }
    else
    {
        logAndAssert(VE_SCOPE_NAME(createRenderWindow(RenderWindowOptions &opt)),
                     "Failed to create RenderWindow. No " +
                     VE_CLASS_NAME(RenderViewBackend).toString() + " registred!",
                     Logger::LOG_COMPONENT_RENDERER);
    }

    return ogreWindow;
}

Ogre::RenderWindow *Renderer::createRenderWindow(const std::string &name,
                                                 unsigned int width,
                                                 unsigned int height,
                                                 bool fullScreen,
                                                 const Params *miscParams)
{
    return Root::createRenderWindow(name, width, height, fullScreen, miscParams);
}

void Renderer::destroyRenderWindow(const RenderWindowOptions &opt)
{
    const auto it = std::find(mRenderTargetNames.begin(),
                              mRenderTargetNames.end(),
                              opt.name);
    if (mRenderViewBackend && it != mRenderTargetNames.end())
    {
        if (mRenderTargetNames.size() == 1)
        {
            destroySceneManager();
            Ogre::ResourceGroupManager::getSingletonPtr()->shutdownAll();
        }

        Ogre::RenderWindow *window = getRenderWindow(opt.name);
        if (window)
        {
            window->destroy();
        }

        /// destroy RenderSystem in case of
        /// current RenderWindow handles main context
        if (opt._mainContext)
        {
            mRenderTargetNames.erase(it);
            if (mRenderTargetNames.empty())
            {
                renderer()->destroyRenderSystem();
            }
        }

        mRenderViewBackend->destroyRenderWindow(opt);
    }
}

bool Renderer::hasRenderWindow(const std::string &name)
{
    return (bool)getRenderWindow(name);
}

Ogre::RenderWindow *Renderer::getRenderWindow(const std::string &name)
{
    if (!contains_if(mRenderTargetNames.cbegin(), mRenderTargetNames.cend(),
                     [&](const std::string &renderTargetName) -> bool { return name == renderTargetName; }))
    {
        return nullptr;
    }

    return static_cast<Ogre::RenderWindow *>(getRenderTarget(name));
}

//----------------------------------------------------------------

Ogre::Viewport *Renderer::createViewport(const std::string &cameraName,
                                         const std::string &renderWindowName, int zOrder,
                                         float left, float top, float width, float height)
{
    Ogre::RenderWindow *renderWin = getRenderWindow(renderWindowName);
    Ogre::Camera *curCamera = getOgreCamera(cameraName);
    if (!renderWin || !curCamera)
    {
        return nullptr;
    }

    return createViewport(curCamera, renderWin, zOrder, left, top, width, height);
}

Ogre::Viewport *Renderer::createViewport(Ogre::Camera *camera, Ogre::RenderWindow *renderWindow, int zOrder,
                                         float left, float top, float width, float height)
{
    Ogre::Viewport *viewport = nullptr;

    if (VE_CHECK_NULL_PTR(Ogre::Camera, camera,
                          Logger::LOG_COMPONENT_RENDERER))
    {
        viewport = renderWindow->addViewport(camera, zOrder, left, top, width, height);
        viewport->setBackgroundColour(Ogre::ColourValue::Black);

        camera->setAspectRatio(float(viewport->getActualWidth()) / float(viewport->getActualHeight()));
    }
    return viewport;
}

//----------------------------------------------------------------

const std::string &Renderer::getDefaultSceneManagerName() const
{
    return mGenericSceneManagerName;
}

StringList Renderer::getSceneManagerNames()
{
    StringList result;

    auto it_wrap = getSceneManagerIterator();

    const auto end = it_wrap.end();
    for (auto it = it_wrap.begin(); it != end; ++it)
    {
        result += it->first;
    }

    return result;
}

//----------------------------------------------------------------

bool Renderer::event(const Event *event)
{
    if (event->getType() == Event::ET_Renderer)
    {
        return true;
    }
    return false;
}

//----------------------------------------------------------------

bool Renderer::renderOneFrame()
{
    Ogre::WindowEventUtilities::messagePump();
    return Ogre::Root::renderOneFrame();
}

//----------------------------------------------------------------

bool Renderer::reset()
{
    if (!VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(reset()),
                                 Ogre::RenderSystem,
                                 mActiveRenderer,
                                 Logger::LOG_COMPONENT_RENDERER))
    {
        return false;
    }

    mActiveRenderer->_initRenderTargets();
    clearEventTimes();
    mQueuedEnd = false;

    return true;
}

//----------------------------------------------------------------

bool Renderer::loadPlugin(const std::string &path)
{
//    if (!FileSystem::isRegularFile(path))
//    {
//        return false;
//    }

    try
    {
        Ogre::Root::loadPlugin(path);
    }
    catch (Ogre::InternalErrorException &e)
    {
        logAndSoftAssert(VE_SCOPE_NAME(Renderer::loadPlugin(const std::string &path)),
                         e.getDescription(),
                         Logger::LOG_COMPONENT_GAME,
                         Logger::LOG_LEVEL_CRITICAL);
        return false;
    }

    return true;
}

