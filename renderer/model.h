#ifndef MODEL_H
#define MODEL_H

#include <framework/object.h>
#include <framework/updatable.h>

#include <framework/filesystem.h>
#include <framework/stringlist.h>
#include <framework/bitmath.h>

class ModelOptions;
class RigidBody;

#if VE_COMPILER == VE_COMPILER_GNUC && VE_COMPILER_VER >= 473 || VE_COMPILER == VE_COMPILER_CLANG
namespace Ogre
{
    class Entity;

    template<class T> class SharedPtr;
    typedef SharedPtr<Mesh> MeshPtr;
}
#else
#include <OGRE/OgreMesh.h>
#endif

class Model : public Object, public Updatable
{
    friend class RigidBody;

public:
    VE_DECLARE_STATIC_META_INFO(Model)

    const ClassName &getClassName() const override
    {
        return Model::getStaticClassName();
    }

    enum { ModelHash = BIT(0) };

    size_t getClassHash() const override
    {
        return ModelHash;
    }

    enum TransformSpace
    {
        TS_Local = 0,
        TS_Parent,
        TS_World
    };

    Model(const ModelOptions &options);

    virtual ~Model();

    const std::string &getName() const override
    {
        return mName;
    }

    void log(const std::string &msg, int level) const;

    bool event(const Event *event) override;

    void update(float deltaTime, float realDelta) override;

    virtual void initFromEntity(Ogre::Entity *entity);

    virtual void initFromMesh(const std::string &meshName,
                              const std::string &resourceGroup =
                                FileSystem::RG_DEFAULT);

    virtual void initFromMesh(const Ogre::MeshPtr &mesh);

    /** Shutdowns selected component
     ** @example shutdown(Hasher::getHash(VE_CLASS_NAME_STR(Renderer)))
     **     shutdowns low level Entity */
    virtual void shutdown(size_t componentHash);

    virtual void init(size_t componentHash, ModelOptions &options);

    /** Shutdowns and reinit componet and its dependenc
     ** @example @see Model::shutdown() */
    void reset(size_t componentHash, ModelOptions &options);

    /** Destroys handled Entity and sets it nullptr. */
    virtual bool destroyModelEntity();

    /** Checks that handled Entity is valid */
    bool isInitialised() const;

    Ogre::AnimationStateSet *getAllAnimationStates() const;

    /** True if Model has any animation state, false otherwise
     ** @see Model::getAllAnimationStates() */
    bool isAnimateable() const;

    static StringList getAllAnimationStatesNames(const Ogre::AnimationStateSet *animations);
    StringList getAllAnimationStatesNames() const;
    bool hasAnimationState(const std::string &name) const;
    Ogre::AnimationState *getAnimationState(const std::string &name) const;

    /** Enable/disable animation with name passed by @param name */
    bool setAnimationStateEnabled(const std::string &name, bool enable);
    bool setAnimationStateToggled(const std::string &name);
    bool isAnimationStateEnabled(const std::string &name) const;

    /** Enable/disable looping of animation with name passed by @param name
     ** Hint: enbled by default */
    bool setAnimationStateLoop(const std::string &name, bool loop);
    bool isAnimationStateLoop(const std::string &name) const;

    /** Coef of all animations speeds
     ** @example
     **     factor == 2 --> animation speed twice as normal */
    void setAnimationSpeedFactor(float factor);
    float getAnimationSpeedFactor() const;

    /** Coef of animation (named @param animName) speed */
    bool setAnimationSpeedFactor(const std::string &animName, float factor);
    float getAnimationSpeedFactor(const std::string &animName) const;

    bool setAnimationStatePaused(const std::string &animName, bool paused);
    bool isAnimationStatePaused(const std::string &animName) const;
    bool setAnimationStatePausedToggled(const std::string &animName);

    bool hasAnyAnimationStateEnabled() const;

    const Ogre::AxisAlignedBox &getBoundingBox() const;

    /** @returns SceneNode which is parent of Model Node */
    const Ogre::SceneNode *getModelNodeParent() const;

    /** @returns Model Node (parent SceneNode of handled Entity) */
    const Ogre::SceneNode *getModelNode() const;

    /** Sets parent of Model Node.
     ** If @node is nullptr then detaches Model Node
     ** from Scene Graph
     ** @returns true if @node is not nullptr and
     ** if Model Node is not Root, false otherwise */
    bool setModelNodeParent(Ogre::SceneNode *newParentNode);

    bool setModelNode(Ogre::SceneNode *node);

    /** Makes sure that the SceneNode is root.
     ** @remarks checks pointer */
    static bool isRootSceneNode(Ogre::SceneNode *node);

    /** Destroys SceneNode.
     ** @remarks checks pointer is not root and nullptr */
    static bool destroySceneNode(Ogre::SceneNode *node);

    /** -------------------- **/

    virtual void setPosition(const Ogre::Vector3 &pos);
    const Ogre::Vector3 &getPosition() const;

    virtual void setOrientation(const Ogre::Quaternion &orient);
    const Ogre::Quaternion &getOrientation() const;

    void pitch(const Ogre::Radian &angle);
    void roll(const Ogre::Radian &angle);
    void yaw(const Ogre::Radian &angle);

    void rotate(const Ogre::Vector3 &axis, const Ogre::Radian &angle);

    virtual void setScale(const Ogre::Vector3 &scale);
    const Ogre::Vector3 &getScale() const;

#ifndef SWIG
    virtual void translate(const Ogre::Vector3 &vector,
                           TransformSpace space = TS_Parent);
#endif

    virtual void translate(float x, float y, float z);

    const Ogre::Vector3 &getWorldPosition() const;

    void setMaterial(const Ogre::MaterialPtr &material);

    void setCastShadows(bool castShadows);

    void setShowBoundingBox(bool show);

    bool isBoundingBoxShowed() const;

#ifndef SWIG
    inline const Ogre::Entity *_getEntity() const
    {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(_getEntity() const),
                                Ogre::Entity,
                                mEntity,
                                Logger::LOG_COMPONENT_RENDERER);
        return mEntity;
    }

    inline Ogre::Entity *_getEntity()
    {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(_getEntity() const),
                                Ogre::Entity,
                                mEntity,
                                Logger::LOG_COMPONENT_RENDERER);
        return mEntity;
    }
#endif // SWIG

    Ogre::SceneNode *_getModelNode() const;

    static Ogre::SceneNode *getRootSceneNode();

    bool operator==(const Model &other)
    {
        if (getClassHash() == other.getClassHash())
        {
            return getName() == other.getName();
        }
        return false;
    }

protected:
    Model(const ModelOptions &options, bool meshInit);

    void _init(const ModelOptions &options, bool meshInit);

//    static std::string getNodeName(const std::string &modelName);
//    static std::string getMeshName(const std::string &modelName);

    static Ogre::SceneNode *createNode(const ModelOptions &options);

    void checkModelName(const ScopeName &scope,
                        const std::string &modelName);

    void checkMeshName(const ScopeName &scope,
                       const std::string &meshName);

    void _shutdown();

private:
    const std::string mName;
    float mAnimationSpeed;

    typedef std::map<std::string, float> AnimationSpeedFactors;
    AnimationSpeedFactors mAnimationSpeedFactors;
    AnimationSpeedFactors mAnimationSpeedFactorsCache;

    void _initFromMesh(const std::string &instanceName,
                       const std::string &meshName,
                       const std::string &resGroup);

    void _initFromMesh(const std::string &instanceName,
                       const Ogre::MeshPtr &mesh);

    /* Low level Ogre model */
    Ogre::Entity *mEntity;
    Ogre::SceneNode *mModelNode;

    void restoreNodesAndAttach(Ogre::Entity *newModelEntity);
};

#ifndef SWIG

class ModelMap : public std::map<std::string, Model *>
{
public:
    /** inserts { model->getName(), model } only if model is not nullptr */
    bool insert(Model *model);

    /** erases model by name only if model is not nullptr */
    bool erase(const Model *model);

    /** finds model by name
     ** @returns Model by name, nullptr otherwise */
    Model *findByName(const std::string &name) const;

    /** deletes all Model* ptrs and clears map */
    void deleteAll();

    /** is model in this map */
    bool contains(const Model *model) const;
};
#endif

#endif // MODEL_H
