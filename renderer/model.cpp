#include "model.h"

#include <renderer/modeloptions.h>

#include <renderer/renderer.h>
#include <renderer/modelevent.h>

#include <game/game.h>
#include <game/modelcontroller.h>

#include <OGRE/OgreEntity.h>
#include <OGRE/OgreTagPoint.h>

#include <OGRE/OgreSceneManager.h>
#include <OGRE/OgreSceneNode.h>

#include <framework/stringconverter.h>

/** ------ Physics ------ */

#include <physics/rigidbody.h>

Model::Model(const ModelOptions &options)
    : Model(options, true)
{ }

Model::Model(const ModelOptions &options, bool meshInit)
    : mName(options.modelName),
      mAnimationSpeed(1.0f),
      mEntity(nullptr),
      mModelNode(nullptr)
{
    _init(options, meshInit);
    game()->getModelController()->enableControll(this);
}

Model::~Model()
{
    game()->getModelController()->disableControll(this);

    _shutdown();
}

void Model::log(const std::string &msg, int level) const
{
    Object::log(msg, level, Logger::LOG_COMPONENT_RENDERER);
}

void Model::_init(const ModelOptions &options, bool meshInit)
{
    Ogre::SceneNode *modelNode = options.modelNode;

    // if model hasn't node and should has it
    if (!modelNode && options.nodeBased)
    {
        modelNode = createNode(options);
    }

    setModelNode(modelNode);

    if (options.addToScene)
    {
        if (options.parentNode)
        {
            setModelNodeParent(options.parentNode);
        }
        else
        {
            setModelNodeParent(getRootSceneNode());
        }
    }

    if (meshInit)
    {
        checkModelName(VE_SCOPE_NAME(_init(const ModelOptions &options, bool meshInit)),
                       options.modelName);

        checkMeshName(VE_SCOPE_NAME(_init(const ModelOptions &options, bool meshInit)),
                      options.meshName);

        initFromMesh(options.meshName, options.resourceGroup);
    }
}

void Model::_shutdown()
{
    destroyModelEntity();

    if (mModelNode && !mModelNode->getChildIterator().hasMoreElements())
    {
        setModelNode(nullptr);
    }
}

bool Model::event(const Event *event)
{
    VE_UNUSED(event);
    assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                         Logger::LOG_COMPONENT_RENDERER);
    return false;
}

void Model::update(float deltaTime, float realDelta)
{
    VE_UNUSED(realDelta);

    auto *animStates = getAllAnimationStates(); // equivalent to isAnimateable()
    if (animStates)
    {
        auto enabledAnimStates = animStates->getEnabledAnimationStateIterator();

        while (enabledAnimStates.hasMoreElements())
        {
            const std::string &name = enabledAnimStates.peekNext()->getAnimationName();
            const float speedFactor = mAnimationSpeed * getAnimationSpeedFactor(name) * deltaTime;
            enabledAnimStates.getNext()->addTime(speedFactor);
        }
    }
}

//------------------------------------------------

void Model::initFromMesh(const std::string &meshName, const std::string &resourceGroup)
{
    _initFromMesh(getName(), meshName, resourceGroup);
}

void Model::_initFromMesh(const std::string &instanceName,
                          const std::string &meshName,
                          const std::string &resGroup)
{
    try
    {
        Ogre::Entity *ent = renderer()->getSceneManager()->createEntity(instanceName,
                                                                        meshName,
                                                                        resGroup);
        initFromEntity(ent);
    }
    catch (const Ogre::FileNotFoundException &ex)
    {
        logAndSoftAssert(VE_SCOPE_NAME(void _initFromMesh(const std::string &instanceName,
                                                          const std::string &meshName,
                                                          const std::string &resGroup)),
                         ex.getDescription(),
                         Logger::LOG_COMPONENT_RENDERER,
                         Logger::LOG_LEVEL_CRITICAL);
    }
}

//------------------------------------------------

void Model::initFromMesh(const Ogre::MeshPtr &mesh)
{
    return _initFromMesh(getName(), mesh);
}

void Model::_initFromMesh(const std::string &instanceName, const Ogre::MeshPtr &mesh)
{
    Ogre::Entity *ent = renderer()->getSceneManager()->createEntity(instanceName, mesh);
    return initFromEntity(ent);
}

//------------------------------------------------

void Model::initFromEntity(Ogre::Entity *entity)
{
    if (!entity)
    {
        logAndAssert(VE_SCOPE_NAME(initFromEntity(Ogre::Entity *entity)),
                     "Parameter " + VE_AS_STR(Ogre::Entity *entity) + " is nullptr! "
                     "Initialization from " + VE_CLASS_NAME(Ogre::Entity).toString() + " failed!",
                     Logger::LOG_COMPONENT_RENDERER);
        return;
    }

    /* Detach and destroy old entity */
    destroyModelEntity();

    mEntity = entity;

    /* Attach new entity to mModelNode and mModelNode
       to mParentNode */
    restoreNodesAndAttach(mEntity);
}

//------------------------------------------------

void Model::shutdown(size_t componentHash)
{
    /// Stop rendering this model
    if (componentHash == Model::getStaticClassHash())
    {
        _shutdown();
    }
}

void Model::init(size_t componentHash, ModelOptions &options)
{
    if (componentHash == Model::getStaticClassHash())
    {
        const bool meshInit = true;
        _init(options, meshInit);
    }
}

void Model::reset(size_t componentHash, ModelOptions &options)
{
    shutdown(componentHash);
    init(componentHash, options);
}

//------------------------------------------------

bool Model::destroyModelEntity()
{
    if (mEntity)
    {
        if (mEntity->isAttached())
        {
            mEntity->detachFromParent();
        }

        renderer()->getSceneManager()->destroyEntity(mEntity);
        mEntity = nullptr;

        return true;
    }

    return false;
}

bool Model::isInitialised() const
{
    return (bool)mEntity;
}

Ogre::AnimationStateSet *Model::getAllAnimationStates() const
{
    return _getEntity()->getAllAnimationStates();
}

bool Model::isAnimateable() const
{
    return (bool)_getEntity()->getAllAnimationStates();
}

StringList Model::getAllAnimationStatesNames() const
{
    return getAllAnimationStatesNames(getAllAnimationStates());
}

StringList Model::getAllAnimationStatesNames(const Ogre::AnimationStateSet *anim)
{
    StringList names;

    Ogre::ConstAnimationStateIterator allStates = anim->getAnimationStateIterator();

    while (allStates.hasMoreElements())
    {
        names.push_back(allStates.peekNextKey());
        allStates.moveNext();
    }

    return names;
}

bool Model::hasAnimationState(const std::string &name) const
{
    return getAllAnimationStates()->hasAnimationState(name);
}

Ogre::AnimationState *Model::getAnimationState(const std::string &name) const
{
    try
    {
        return _getEntity()->getAnimationState(name);
    }
    catch (Ogre::Exception &e)
    {
        if (e.getNumber() == Ogre::Exception::ERR_ITEM_NOT_FOUND)
        {
            logAndAssert(VE_SCOPE_NAME(getAnimationState(const std::string &name) const),
                         "Entity has not AnimationState named \"" +
                         name + "\"!",
                         Logger::LOG_COMPONENT_RENDERER);
        }

#ifndef VE_NO_EXCEPTIONS
        throw;
#endif

    }

    return nullptr;
}

bool Model::isAnimationStateEnabled(const std::string &name) const
{
    const Ogre::AnimationState *state = getAnimationState(name);
    if (state)
    {
        return state->getEnabled();
    }
    return false;
}

bool Model::isAnimationStateLoop(const std::string &name) const
{
    const Ogre::AnimationState *state = getAnimationState(name);
    if (state)
    {
        return state->getLoop();
    }
    return false;
}

bool Model::setAnimationStateEnabled(const std::string &name, bool enable)
{
    Ogre::AnimationState *state = getAnimationState(name);
    if (state)
    {
        state->setEnabled(enable);
        return true;
    }

    return false;
}

bool Model::setAnimationStateToggled(const std::string &name)
{
    Ogre::AnimationState *state = getAnimationState(name);
    if (state)
    {
        state->setEnabled(!state->getEnabled());
        return true;
    }

    return false;
}

bool Model::setAnimationStateLoop(const std::string &name, bool loop)
{
    Ogre::AnimationState *state = getAnimationState(name);
    if (state)
    {
        state->setLoop(loop);
        return true;
    }

    return false;
}

void Model::setAnimationSpeedFactor(float factor)
{
    mAnimationSpeed = factor;
}

float Model::getAnimationSpeedFactor() const
{
    return mAnimationSpeed;
}

bool Model::setAnimationSpeedFactor(const std::string &animName, float factor)
{
    if (hasAnimationState(animName))
    {
        mAnimationSpeedFactors[animName] = factor;
        return true;
    }
    return false;
}

float Model::getAnimationSpeedFactor(const std::string &animName) const
{
    auto it = mAnimationSpeedFactors.find(animName);
    if (it != mAnimationSpeedFactors.end())
    {
        return it->second;
    }
    return 1.0f;
}

bool Model::setAnimationStatePaused(const std::string &animName, bool paused)
{
    if (hasAnimationState(animName))
    {
        if (paused)
        {
            mAnimationSpeedFactorsCache[animName] = getAnimationSpeedFactor(animName);
            mAnimationSpeedFactors[animName] = 0.0f;
        }
        else
        {
            float oldFactor = 1.0f;
            auto it = mAnimationSpeedFactorsCache.find(animName);
            if (it != mAnimationSpeedFactorsCache.end())
            {
                oldFactor = it->second;
            }
            mAnimationSpeedFactors[animName] = oldFactor;
        }
        return true;
    }

    return false;
}

bool Model::isAnimationStatePaused(const std::string &animName) const
{
    if (hasAnimationState(animName))
    {
        return (getAnimationSpeedFactor(animName) == 0.0f);
    }

    return false;
}

bool Model::setAnimationStatePausedToggled(const std::string &animName)
{
    return setAnimationStatePaused(animName, !isAnimationStatePaused(animName));
}

bool Model::hasAnyAnimationStateEnabled() const
{
    return getAllAnimationStates()->getEnabledAnimationStateIterator().hasMoreElements();
}

const Ogre::AxisAlignedBox &Model::getBoundingBox() const
{
    return _getEntity()->getBoundingBox();
}

bool Model::setModelNodeParent(Ogre::SceneNode *newParentNode)
{
    if (mModelNode)
    {
        Ogre::SceneNode *oldParentNode = mModelNode->getParentSceneNode();

        if (newParentNode)
        {
            /* attach if not already attached */
            if (oldParentNode != newParentNode)
            {
                /* if mModelNode was attached detach it */
                if (oldParentNode)
                {
                    oldParentNode->removeChild(mModelNode);
                }

                newParentNode->addChild(mModelNode);
            }
            return true;
        /* newParentNode is nullptr */
        }
        else
        {
            /* if mModelNode was attached detach it */
            if (oldParentNode)
            {
                oldParentNode->removeChild(mModelNode);
            }
        }
    }

    return false;
}

const Ogre::SceneNode *Model::getModelNodeParent() const
{
    return mModelNode->getParentSceneNode();
}

const Ogre::SceneNode *Model::getModelNode() const
{
    return mModelNode;
}

bool Model::setModelNode(Ogre::SceneNode *node)
{
    if (node && mModelNode != node)
    {
        if (isRootSceneNode(node))
        {
            logAndAssert(VE_SCOPE_NAME(setModelNode(Ogre::SceneNode *node)),
                         "Model's node cannot be root scene node!",
                         Logger::LOG_COMPONENT_RENDERER);
            return false;
        }

        if (mEntity)
        {
            Ogre::SceneNode *oldNode = mEntity->getParentSceneNode();

            mEntity->detachFromParent();

            /* destroy saved node if it has not used by any moveable */
            if (oldNode && oldNode->getChildIterator().hasMoreElements())
            {
                destroySceneNode(oldNode);
            }

            /* attach mEntity to new node */
            node->attachObject(mEntity);
        }

        /* we doesn't touch modelNodeParent */

    /* node is nullptr, so we want to destroy mModelNode */
    }
    else
    if (mModelNode)
    {
        destroySceneNode(mModelNode);
    }

    mModelNode = node;

    return true;
}

bool Model::isRootSceneNode(Ogre::SceneNode *node)
{
    if (VE_CHECK_NULL_PTR(Ogre::SceneNode, node,
                          Logger::LOG_COMPONENT_RENDERER))
    {
        return (node->getCreator()->getRootSceneNode() == node);
    }
    return false;
}

bool Model::destroySceneNode(Ogre::SceneNode *node)
{
    if (VE_CHECK_NULL_PTR(Ogre::SceneNode, node,
                          Logger::LOG_COMPONENT_RENDERER))
    {
        if (isRootSceneNode(node))
        {
            logAndSoftAssertStatic(VE_CLASS_NAME(Model),
                                   Ogre::StringUtil::BLANK,
                                   VE_SCOPE_NAME(destroySceneNode(Ogre::SceneNode *node)),
                                   "Attempt to destroy root SceneNode!",
                                   Logger::LOG_COMPONENT_RENDERER,
                                   Logger::LOG_LEVEL_CRITICAL);
            return false;
        }
        renderer()->getSceneManager()->destroySceneNode(node);

        return true;
    }
    else
    {
        logAndSoftAssertStatic(VE_CLASS_NAME(Model),
                               Ogre::StringUtil::BLANK,
                               VE_SCOPE_NAME(destroySceneNode(Ogre::SceneNode *node)),
                               "Attempt to destroy nullptr SceneNode!",
                               Logger::LOG_COMPONENT_RENDERER,
                               Logger::LOG_LEVEL_CRITICAL);
    }
    return false;
}

Ogre::SceneNode *Model::_getModelNode() const
{
    return mModelNode;
}

/*
std::string Model::getNodeName(const std::string &modelName)
{
    return modelName + "_node";
}

std::string Model::getMeshName(const std::string &modelName)
{
    return modelName + "_mesh";
}
*/

Ogre::SceneNode *Model::createNode(const ModelOptions &options)
{
    std::string name = options.getFallbackModelNodeName();
    if (renderer()->getSceneManager()->hasSceneNode(name))
    {
        logger()->logClassMessage(VE_CLASS_NAME(Model),
                                  VE_SCOPE_NAME(createNode(const ModelOptions &options)),
                                  "Detected naming collision while creating " +
                                  VE_CLASS_NAME(Ogre::SceneNode).toString() +
                                  " with name " + name + "!",
                                  Logger::LOG_LEVEL_WARNING,
                                  Logger::LOG_COMPONENT_RENDERER);
        unsigned long milliSec = renderer()->getTimer()->getMilliseconds();
        name += StringConverter::toString(milliSec);
    }
    return renderer()->getSceneManager()->createSceneNode(name);
}

Ogre::SceneNode *Model::getRootSceneNode()
{
    return renderer()->getSceneManager()->getRootSceneNode();
}

/** ------------------------------------- **/

void Model::restoreNodesAndAttach(Ogre::Entity *newModelEntity)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(Model::restoreNodesAndAttach(Ogre::Entity *newModelEntity)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->attachObject(newModelEntity);
    }
}

/** ------------------------------------- **/

void Model::checkModelName(const ScopeName &scope, const std::string &modelName)
{
    if (modelName.empty())
    {
        logAndAssert(scope, "Empty Model name!",
                     Logger::LOG_COMPONENT_RENDERER);
    }
}

void Model::checkMeshName(const ScopeName &scope, const std::string &meshName)
{
    if (meshName.empty())
    {
        logAndAssert(scope, "Empty mesh name!",
                     Logger::LOG_COMPONENT_RENDERER);
    }
}

/** ------------------------------------- **/

void Model::setPosition(const Ogre::Vector3 &pos)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(setPosition(const Ogre::Vector3 &pos)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->setPosition(pos);
    }
}

const Ogre::Vector3 &Model::getPosition() const
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getPosition() const),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        return mModelNode->getPosition();
    }
    return Ogre::Vector3::ZERO;
}

/** ------------------------------------- **/

void Model::setOrientation(const Ogre::Quaternion &orient)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(setOrientation(const Ogre::Quaternion &orient)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->setOrientation(orient);
    }
}

const Ogre::Quaternion &Model::getOrientation() const
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getOrientation() const),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        return mModelNode->getOrientation();
    }
    return Ogre::Quaternion::IDENTITY;
}

/** ------------------------------------- **/

void Model::pitch(const Ogre::Radian &angle)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(pitch(const Ogre::Radian &angle)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->pitch(angle);
    }
}

void Model::roll(const Ogre::Radian &angle)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(roll(const Ogre::Radian &angle)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->roll(angle);
    }
}

void Model::yaw(const Ogre::Radian &angle)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(yaw(const Ogre::Radian &angle)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->yaw(angle);
    }
}

void Model::rotate(const Ogre::Vector3 &axis, const Ogre::Radian &angle)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(rotate(const Ogre::Vector3 &axis, const Ogre::Radian &angle)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->rotate(axis, angle);
    }
}

/** ------------------------------------- **/

void Model::setScale(const Ogre::Vector3 &scale)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(setScale(const Ogre::Vector3 &scale)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->setScale(scale);
    }
}

const Ogre::Vector3 &Model::getScale() const
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getScale() const),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        return mModelNode->getScale();
    }

    return Ogre::Vector3::ZERO;
}

/** ------------------------------------- **/

void Model::translate(const Ogre::Vector3 &vector, TransformSpace space)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(translate(const Ogre::Vector3 &vector, TransformSpace space)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->translate(vector, (Ogre::Node::TransformSpace)space);
    }
}

void Model::translate(float x, float y, float z)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(translate(float x, float y, float z)),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        mModelNode->translate(x, y, z);
    }
}

const Ogre::Vector3 &Model::getWorldPosition() const
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(getWorldPosition() const),
                                Ogre::SceneNode,
                                mModelNode,
                                Logger::LOG_COMPONENT_RENDERER))
    {
        return mModelNode->_getDerivedPosition();
    }
    return Ogre::Vector3::ZERO;
}

void Model::setMaterial(const Ogre::MaterialPtr &material)
{
    _getEntity()->setMaterial(material);
}

void Model::setCastShadows(bool castShadows)
{
    _getEntity()->setCastShadows(castShadows);
}

void Model::setShowBoundingBox(bool show)
{
    mModelNode->showBoundingBox(show);
}

bool Model::isBoundingBoxShowed() const
{
    return mModelNode->getShowBoundingBox();
}

// --------------------------------------------

bool ModelMap::insert(Model *model)
{
    if (!model)
    {
        return false;
    }

    return std::map<std::string, Model *>::insert({ model->getName(), model }).second;
}

bool ModelMap::erase(const Model *model)
{
    if (!model)
    {
        return false;
    }

    return std::map<std::string, Model *>::erase(model->getName());
}

Model *ModelMap::findByName(const std::string &name) const
{
    auto it = find(name);
    if (it == end())
    {
        return nullptr;
    }

    return it->second;
}

void ModelMap::deleteAll()
{
    auto endIt = end();
    for (auto it = begin(); it != endIt; ++it)
    {
        delete it->second;
    }
    clear();
}

bool ModelMap::contains(const Model *model) const
{
    auto entIt = end();
    for (auto it = begin(); it != entIt; ++it)
    {
        if (model == it->second)
        {
            return true;
        }
    }

    return false;
}
