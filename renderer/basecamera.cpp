#include "basecamera.h"

#include <renderer/renderer.h>

#include <renderer/cameras/cameracontroller.h>

static void ogreCameraDeleter(Ogre::Camera *camera)
{
    auto sceneMgr = renderer()->getSceneManager();
    if (camera && sceneMgr)
    {
        sceneMgr->destroyCamera(camera);
    }
}

BaseCamera::BaseCamera()
    : mCameraController(nullptr)
{
}

BaseCamera::BaseCamera(Ogre::Camera *camera)
    : BaseCamera()
{
    attachOgreCamera(camera);
}

std::shared_ptr<Ogre::Camera> BaseCamera::getCameraImpl(bool checkPtr) const
{
    if (checkPtr)
    {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(BaseCamera::getCameraImpl(bool checkPtr)),
                                Ogre::Camera,
                                mCamera.get(),
                                Logger::LOG_COMPONENT_RENDERER);
    }
    return mCamera;
}

BaseCamera::BaseCamera(const std::string &name)
{
    if (renderer()->hasCamera(name))
    {
        attachOgreCamera(renderer()->getOgreCamera(name));
    }
}

BaseCamera::~BaseCamera()
{
    detachOgreCamera();
}

const std::string &BaseCamera::getName() const
{
    return getCameraImpl()->getName();
}

bool BaseCamera::isInitialized() const
{
    return (bool)mCamera;
}

Ogre::Viewport *BaseCamera::getLastViewport() const
{
    return getCameraImpl()->getViewport();
}

void BaseCamera::setPosition(const Ogre::Vector3 &pos)
{
    getCameraImpl()->setPosition(pos);
}

const Ogre::Vector3 &BaseCamera::getPosition() const
{
    return getCameraImpl()->getPosition();
}

void BaseCamera::setNearClipDistance(float nearDist)
{
    getCameraImpl()->setNearClipDistance(nearDist);
}

float BaseCamera::getNearClipDistance() const
{
    return getCameraImpl()->getNearClipDistance();
}

void BaseCamera::setFarClipDistance(float farDist)
{
    getCameraImpl()->setFarClipDistance(farDist);
}

float BaseCamera::getFarClipDistance() const
{
    return getCameraImpl()->getFarClipDistance();
}

void BaseCamera::setLookAt(const Ogre::Vector3 &target)
{
    getCameraImpl()->lookAt(target);
}

/** --------------------------- **/

void BaseCamera::setDirection(const Ogre::Vector3 &dir)
{
    getCameraImpl()->setDirection(dir);
}

Ogre::Vector3 BaseCamera::getDirection() const
{
    return getCameraImpl()->getDirection();
}

/** --------------------------- **/

void BaseCamera::setOrientation(const Ogre::Quaternion &orient)
{
    getCameraImpl()->setOrientation(orient);
}

const Ogre::Quaternion &BaseCamera::getOrientation() const
{
    return getCameraImpl()->getOrientation();
}

/** --------------------------- **/

void BaseCamera::roll(const Ogre::Radian &angle)
{
    getCameraImpl()->roll(angle);
}

void BaseCamera::yaw(const Ogre::Radian &angle)
{
    getCameraImpl()->yaw(angle);
}

void BaseCamera::pitch(const Ogre::Radian &angle)
{
    getCameraImpl()->pitch(angle);
}

/** --------------------------- **/

/*
bool BaseCamera::setAutoTracking(bool enabled, SceneNode *const target,
                                 const Vector3 &offset)
{
    if (isValid()) {
        mCamera->setAutoTracking(enabled, target, offset);
        return true;
    }
    return false;
}

SceneNode *BaseCamera::getAutoTrackTarget() const
{
    if (isValid())
        return mCamera->getAutoTrackTarget();
    return nullptr;
}

bool BaseCamera::isAutoTracking() const
{
    return (bool)getAutoTrackTarget();
}
*/

void BaseCamera::moveRelative(const Ogre::Vector3 &direction)
{
    getCameraImpl()->moveRelative(direction);
}

void BaseCamera::move(const Ogre::Vector3 &direction)
{
    getCameraImpl()->move(direction);
}

/** --------------------------- **/

void BaseCamera::setAutoTracking(bool enabled,
                                 Ogre::SceneNode *trackingTarget,
                                 const Ogre::Vector3 &lookAtOffset)
{
    getCameraImpl()->setAutoTracking(enabled, trackingTarget, lookAtOffset);
}

Ogre::SceneNode *BaseCamera::getAutoTrackingTarget() const
{
    return getCameraImpl()->getAutoTrackTarget();
}

const Ogre::Vector3 &BaseCamera::getAutoTrackingLookAtOffset() const
{
    return getCameraImpl()->getAutoTrackOffset();
}

bool BaseCamera::isAutoTrackingEnabled() const
{
    return (bool)getAutoTrackingTarget();
}

/** --------------------------- **/

void BaseCamera::setAspectRatio(float ratio)
{
    getCameraImpl()->setAspectRatio(ratio);
}

/** --------------------------- **/

void BaseCamera::setPolygonMode(PolygonMode mode)
{
    Ogre::PolygonMode ogrePolygonMode;
    switch (mode)
    {
    case PM_Points:     ogrePolygonMode = Ogre::PM_POINTS;      break;
    case PM_Wireframe:  ogrePolygonMode = Ogre::PM_WIREFRAME;   break;
    case PM_Solid:      ogrePolygonMode = Ogre::PM_SOLID;       break;
    }

    getCameraImpl()->setPolygonMode(ogrePolygonMode);
}

BaseCamera::PolygonMode BaseCamera::getPolygonMode() const
{
    const Ogre::PolygonMode ogrePolygonMode = getCameraImpl()->getPolygonMode();
    switch (ogrePolygonMode)
    {
    case Ogre::PM_POINTS:       return PM_Points;
    case Ogre::PM_WIREFRAME:    return PM_Wireframe;
    case Ogre::PM_SOLID:        return PM_Solid;
    }

    return PM_Solid;
}

BaseCamera::PolygonMode BaseCamera::getNextPolygonMode(PolygonMode mode)
{
    switch (mode)
    {
    case PM_Points:     return PM_Wireframe;
    case PM_Wireframe:  return PM_Solid;
    case PM_Solid:      return PM_Points;
    }

    return PM_Solid;
}

/** ----- Controller ----- */

void BaseCamera::setCameraController(CameraController *controller)
{
    if (controller != mCameraController)
    {
        delete mCameraController;
        mCameraController = controller;
    }
}

/** ------ Listener ------ */

void BaseCamera::cameraDestroyed(Ogre::Camera *cam)
{
    if (cam == mCamera.get())
    {
        mCamera.reset();
    }
}

Ogre::Camera *BaseCamera::createCameraImpl(const std::string &name)
{
    return renderer()->getSceneManager()->createCamera(name);
}

Ogre::Camera *BaseCamera::detachOgreCamera()
{
    if (renderer()->getSceneManager())
    {
        Ogre::Camera *camera = mCamera.get();
        mCamera.reset();

        if (camera && containsMap(renderer()->getSceneManager()->getCameras(), camera))
        {
            camera->removeListener(this);
        }
        return camera;
    }

    return nullptr;
}

Ogre::Camera *BaseCamera::attachOgreCamera(Ogre::Camera *camera)
{   
    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(BaseCamera::attachOgreCamera(Ogre::Camera *camera)),
                            Ogre::Camera,
                            camera,
                            Logger::LOG_COMPONENT_RENDERER);

    Ogre::Camera *oldCamera = detachOgreCamera();

    camera->addListener(this);
    mCamera.reset(camera, ogreCameraDeleter);

    return oldCamera;
}
