#ifndef RENDERWINDOWOPTIONS_H
#define RENDERWINDOWOPTIONS_H

#include <global.h>

#include <framework/size.h>
#include <framework/params.h>

class RenderWindowOptions
{
    friend class Renderer;

public:
    RenderWindowOptions(const std::string &name_ = "")
        : name(name_),
          size(800, 600),
          fullScreen(false),
          window(nullptr),
          context(nullptr),
          _mainContext(false)
    { }

    std::string name;
    ve::SizeU size;
    bool fullScreen;
    Params parameters;

    void *window;      /// manual created window  (nullptr by default)
    void *context;     /// manual created context (nullptr by default)

protected:
    bool _mainContext; /// main context created for this window
};

#endif // RENDERWINDOWOPTIONS_H
