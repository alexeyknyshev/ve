#ifndef MODELEVENT_H
#define MODELEVENT_H

#include <renderer/rendererevent.h>

class ModelEvent : public RendererEvent
{
public:
    ModelEvent(const ModelEvent &other)
        : RendererEvent(other),
          mDeltaTime(other.mDeltaTime)
    { }

    ModelEvent(float deltaTime,
               uint32_t priority = NorPriority,
               uint32_t lifeTime = DefLifeTime);

    Event *clone() const override
    {
        return new ModelEvent(*this);
    }

    inline float getDeltaTime() const
    {
        return mDeltaTime;
    }

    void toStdMap(Params &output) const;

protected:
    inline void setDeltaTime(float deltaTime)
    {
        mDeltaTime = deltaTime;
    }

private:
    float mDeltaTime;
};

#endif // MODELEVENT_H
