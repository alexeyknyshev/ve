#include "modeloptions.h"

#include <framework/filesystem.h>

ModelOptions::ModelOptions()
    : modelType(MT_Dynamic),
      modelName(VE_STR_BLANK),
      meshName(VE_STR_BLANK),
      modelNode(nullptr),
      parentNode(nullptr),
      resourceGroup(FileSystem::RG_DEFAULT),
      addToScene(true),
      nodeBased(true)
{ }
