#ifndef RENDEREREVENT_H
#define RENDEREREVENT_H

#include <framework/event.h>

class RendererEvent : public Event
{
public:
    RendererEvent(const RendererEvent &other)
        : Event(other)
    { }

    RendererEvent(uint32_t priority = NorPriority,
                  uint32_t lifeTime = DefLifeTime);
};

#endif // RENDEREREVENT_H
