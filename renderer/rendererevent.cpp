#include "rendererevent.h"

RendererEvent::RendererEvent(uint32_t priority, uint32_t lifeTime)
    : Event(ET_Renderer, priority, lifeTime)
{
}
