#ifndef DEBUGDRAWER_H
#define DEBUGDRAWER_H

#include <framework/object.h>
#include <framework/bitmath.h>

class DebugDrawer
{
public:
    VE_DECLARE_TYPE_INFO(DebugDrawer)

    enum DebugDrawMode
    {
        DDM_DrawNone = 0,
        DDM_DrawWireframe = BIT(0),
        DDM_DrawAabb = BIT(1)
    };

    virtual ~DebugDrawer();

    virtual void setDebugDrawMode(DebugDrawMode mask) = 0;
    virtual DebugDrawMode getDebugDrawMode() const = 0;

    void setDrawNone();
    void setDrawAabb(bool enable);
    void setDrawWireframe(bool enable);

    bool isDrawNone() const;
    bool isDrawAabb() const;
    bool isDrawWireframe() const;
};

#endif // DEBUGDRAWER_H
