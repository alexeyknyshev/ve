#ifndef PHYSICSWORLD_H
#define PHYSICSWORLD_H

#include <framework/object.h>

//#include <OgreAxisAlignedBox.h>
#include <OGRE/OgreSingleton.h>

class PhysicsManager;
class RigidBody;
class PhysicsModelOptions;
class VehicleModelOptions;
class RayCastVehicle;
class DebugDrawer;

namespace Ogre
{
    class AxisAlignedBox;
}

class PhysicsWorld : public Ogre::Singleton<PhysicsWorld>,
                     public Object
{
    friend class PhysicsManager;

public:
    VE_DECLARE_TYPE_INFO(PhysicsWorld)

    PhysicsWorld(Ogre::SceneManager *sceneMgr,
                 const Ogre::AxisAlignedBox &worldBounds,
                 const Ogre::Vector3 &gravity,
                 const ParamsMulti &options);

    virtual ~PhysicsWorld();

    bool event(const Event *event);

    virtual void update(float deltaTime, float realTime) = 0;

    virtual DebugDrawer *getDebugDrawer() const = 0;

    /*
    virtual void setShowDebugShapes(bool show) = 0;
    virtual bool getShowDebugShapes() const = 0;
    */

protected:
    virtual RigidBody *createRigidBody(const ClassName &type,
                                       const PhysicsModelOptions &options,
                                       Ogre::Entity *ent) const = 0;

    virtual RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                                 RigidBody *vehicleBody) const = 0;
};

PhysicsWorld *physicsWorld();

/** ------ Factory ------ */

class PhysicsWorldFactory
{
    friend class PhysicsManager;

public:
    virtual ~PhysicsWorldFactory() { }
    virtual const std::string &getWorldTypeName() const = 0;

protected:
    virtual PhysicsWorld *createWorld(Ogre::SceneManager *sceneMgr,
                                      const Ogre::AxisAlignedBox &worldBounds,
                                      const Ogre::Vector3 &gravity,
                                      const ParamsMulti &options) = 0;
};

#endif // PHYSICSWORLD_H
