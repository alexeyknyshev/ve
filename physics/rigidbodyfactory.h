#ifndef RIGIDBODYFACTORY_H
#define RIGIDBODYFACTORY_H

namespace Ogre
{
    class Entity;
}

class RigidBody;
class PhysicsModelOptions;

class RigidBodyFactory
{
public:
    virtual ~RigidBodyFactory();

    virtual RigidBody *createRigidBody(const PhysicsModelOptions &options,
                                       Ogre::Entity *ent) const = 0;
};

#endif // RIGIDBODYFACTORY_H
