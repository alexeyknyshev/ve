#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include <framework/object.h>
#include <OGRE/OgreVector3.h>

class PhysicsModelOptions;

class RigidBody : public Object
{
public:
    RigidBody(const PhysicsModelOptions &options);
    virtual ~RigidBody();

    virtual bool isDynamic() const = 0;

    virtual void setWorldPosition(const Ogre::Vector3 &position) = 0;
    virtual Ogre::Vector3 getWorldPosition() const = 0;

    virtual void setWorldOrientation(const Ogre::Quaternion &orient) = 0;
    virtual Ogre::Quaternion getWorldOrientation() const = 0;

    virtual void setLinearVelocity(const Ogre::Vector3 &velocity) = 0;
    virtual Ogre::Vector3 getLinearVelocity() const = 0;

    virtual void setAngularVelocity(const Ogre::Vector3 &velocity) = 0;
    virtual Ogre::Vector3 getAngularVelocity() const = 0;

    virtual void applyForce(const Ogre::Vector3 &force, const Ogre::Vector3 &position) = 0;

    virtual Ogre::Quaternion getCenterOfMassOrientation() const = 0;
    virtual Ogre::Vector3 getCenterOfMassPosition() const = 0;

    virtual void setAutoDeactivation(bool autoDeactivation) = 0;
    virtual bool getAutoDeactivation() const = 0;

    virtual void setActivationState(int state) = 0;
    virtual int getActivationState() const = 0;

    virtual const Ogre::AxisAlignedBox &getBoundingBox() const = 0;
    virtual float getBoundingRaduis() const = 0;

    virtual float getFriction() const = 0;

    virtual void setDamping(float linear, float angular) = 0;

    virtual float getLinearDamping() const = 0;
    virtual float getAngularDamping() const = 0;

    virtual void setDebugDrawEnabled(bool enabled) = 0;
    virtual bool isDebugDrawEnabled() const = 0;

    static std::string getBodyName(const PhysicsModelOptions &options);
};

#endif // RIGIDBODY_H
