#ifndef RAYCASTVEHICLEFACTORY_H
#define RAYCASTVEHICLEFACTORY_H

class RayCastVehicle;
class VehicleModelOptions;
class RigidBody;

class RayCastVehicleFactory
{
public:
    virtual ~RayCastVehicleFactory();

    virtual RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                                 RigidBody *vehicleBody) const = 0;
};

#endif // RAYCASTVEHICLEFACTORY_H
