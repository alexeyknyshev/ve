#ifndef DEFAULTVEHICLECONTROLLER_H
#define DEFAULTVEHICLECONTROLLER_H

#include <framework/controller/defaultinputlistener.h>

class RayCastVehicle;

class DefaultVehicleController : public DefaultInputListener
{
public:
    DefaultVehicleController(const std::string &name,
                             RayCastVehicle *vehicle,
                             float engineForceStep,
                             float steeringStep);

    void update(float deltaTime, float realTime);

private:
    RayCastVehicle *mVehicle;

    float mEngineForce;
    float mEngineForceStep;

    float mBrakeForce;
    float mBrakeForceStep;

    float mSteering;
    float mSteeringStep;
};

#endif // DEFAULTVEHICLECONTROLLER_H
