#include "defaultvehiclecontroller.h"

#include <physics/vehicle/raycastvehicle.h>

DefaultVehicleController::DefaultVehicleController(const std::string &name,
                                                   RayCastVehicle *vehicle,
                                                   float engineForceStep,
                                                   float steeringStep)
    : DefaultInputListener(name),
      mVehicle(vehicle),
      mEngineForce(0.0f),
      mEngineForceStep(engineForceStep),
      mBrakeForce(0.0f),
      mBrakeForceStep(engineForceStep * 4),
      mSteering(0.0f),
      mSteeringStep(steeringStep)
{
}

void DefaultVehicleController::update(float deltaTime, float realTime)
{
    VE_UNUSED(realTime);

    const float maxForce = mVehicle->getMaxEngineForce();
    const float deltaForce = (mEngineForceStep + mBrakeForce) * deltaTime;

    if (isKeyPressed(ve::KeyData::KEY_UP) && mEngineForce < (maxForce - deltaForce))
    {
        mEngineForce += deltaForce;
    }
    else
    if (isKeyPressed(ve::KeyData::KEY_DOWN) && mEngineForce > (deltaForce - maxForce))
    {
        mEngineForce -= deltaForce;
    }
    else
    {
        mEngineForce = 0.0f;
    }

    mVehicle->applyEngineForce(0, mEngineForce);
    mVehicle->applyEngineForce(1, mEngineForce);


    // ----------------------------------------

    const float maxSteeringAngle = mVehicle->getMaxSteeringAngle();
    const float deltaSteeringAngle = mSteeringStep * deltaTime;

    if (isKeyPressed(ve::KeyData::KEY_LEFT))
    {
        mSteering += deltaSteeringAngle;
        if (mSteering > maxSteeringAngle)
        {
            mSteering = maxSteeringAngle;
        }
    }
    else
    if (isKeyPressed(ve::KeyData::KEY_RIGHT))
    {
        mSteering -= deltaSteeringAngle;
        if (mSteering < -maxSteeringAngle)
        {
            mSteering = -maxSteeringAngle;
        }
    }

    mVehicle->setSteering(0, mSteering);
    mVehicle->setSteering(1, mSteering);
}
