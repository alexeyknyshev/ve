#include "raycastvehicle.h"

#include <renderer/models/vehicle/vehiclemodeloptions.h>

#include <global.h>

RayCastVehicle::RayCastVehicle(const VehicleModelOptions &options,
                               RigidBody *vehicleBody)
{
    VE_UNUSED(options);
    VE_UNUSED(vehicleBody);
}

RayCastVehicle::~RayCastVehicle()
{
}
