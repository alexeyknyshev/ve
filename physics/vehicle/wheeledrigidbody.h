#ifndef WHEELEDRIDGIDBODY_H
#define WHEELEDRIDGIDBODY_H

#include <physics/rigidbody.h>

class WheeledRidgidBody : public RigidBody
{
public:
    WheeledRidgidBody(const PhysicsModelOptions &options);
};

#endif // WHEELEDRIDGIDBODY_H
