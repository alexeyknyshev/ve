#ifndef RAYCASTVEHICLE_H
#define RAYCASTVEHICLE_H

class VehicleModelOptions;
class VehicleWheelModelOptions;
class RigidBody;

class RayCastVehicle
{
public:
    RayCastVehicle(const VehicleModelOptions &options,
                   RigidBody *vehicleBody);

    virtual ~RayCastVehicle();

    virtual void addWheel(const VehicleWheelModelOptions &wheelOpt) = 0;

    virtual void applyEngineForce(int wheelId, float force) = 0;
    virtual void setSteering(int wheelId, float steering) = 0;

    virtual void setBrake(int wheelId, float force) = 0;

    virtual bool isFrontWheel(int wheelId) const = 0;

    virtual float getSpeed() const = 0;
    virtual int getWheelsCount() const = 0;

    virtual void setMaxEngineForce(float force) = 0;
    virtual float getMaxEngineForce() const = 0;

    virtual void setMaxSteeringAngle(float angle) = 0;
    virtual float getMaxSteeringAngle() const = 0;

    virtual void setSuspensionLength(int wheelId, float length) = 0;
    virtual float getSuspensionLength(int wheelId) const = 0;

    virtual void setSuspensionRestLength(int wheelId, float length) = 0;
    virtual float getSuspensionRestLength(int wheelId) const = 0;
};

#endif // RAYCASTVEHICLE_H
