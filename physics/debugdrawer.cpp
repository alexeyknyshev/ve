#include "debugdrawer.h"

DebugDrawer::~DebugDrawer()
{

}

void DebugDrawer::setDrawNone()
{
    setDebugDrawMode(DDM_DrawNone);
}

void DebugDrawer::setDrawAabb(bool enable)
{
    if (enable) {
        setDebugDrawMode(DebugDrawMode(getDebugDrawMode() | DDM_DrawAabb));
    } else {
        setDebugDrawMode(DebugDrawMode(getDebugDrawMode() & ~DDM_DrawAabb));
    }
}

void DebugDrawer::setDrawWireframe(bool enable)
{
    if (enable) {
        setDebugDrawMode(DebugDrawMode(getDebugDrawMode() | DDM_DrawWireframe));
    } else {
        setDebugDrawMode(DebugDrawMode(getDebugDrawMode() & ~DDM_DrawWireframe));
    }
}

bool DebugDrawer::isDrawNone() const
{
    return (getDebugDrawMode() == DDM_DrawNone);
}

bool DebugDrawer::isDrawAabb() const
{
    return (getDebugDrawMode() & DDM_DrawAabb);
}

bool DebugDrawer::isDrawWireframe() const
{
    return (getDebugDrawMode() & DDM_DrawWireframe);
}
