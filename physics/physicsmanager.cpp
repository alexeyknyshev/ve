#include "physicsmanager.h"

#include <framework/event.h>
#include <framework/filesystem.h>
#include <framework/stringconverter.h>

#include <physics/dummy/dummyphysicsworld.h>
#include <physics/vehicle/raycastvehiclefactory.h>
#include <physics/rigidbodyfactory.h>

#include <renderer/renderer.h>
#include <renderer/models/physics/physicsmodeloptions.h>

VE_DECLARE_SINGLETON(PhysicsManager);

PhysicsManager::PhysicsManager(Ogre::SceneManager *sceneMgr, const ParamsMulti &options)
    : UpdateListener(false), // will not be deleted while cleaning updateListeners list
      mWorldFactory(nullptr),
      mWorld(nullptr)
{
    init(sceneMgr, options);
    setAutoUpdateEnabled(true);
}

PhysicsManager::~PhysicsManager()
{
    shutdown();

    delete mWorldFactory;
    mWorldFactory = nullptr;

    log("PhysicsManager destroyed", Logger::LOG_LEVEL_INFO);
}

void PhysicsManager::update(float deltaTime, float realTime)
{
    mWorld->update(deltaTime, realTime);
}

void PhysicsManager::log(const std::string &msg, int level) const
{
    Object::log(msg, level, Logger::LOG_COMPONENT_PHYSICS);
}

bool PhysicsManager::setWorldFactory(PhysicsWorldFactory *factory, bool force)
{
    if (factory != mWorldFactory)
    {
        if (!force && factory && mWorldFactory &&
            factory->getWorldTypeName() == mWorldFactory->getWorldTypeName())
        {
            return false;
        }

        delete mWorldFactory;
        mWorldFactory = factory;
    }
    return true;
}

bool PhysicsManager::event(const Event *event)
{
    assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                         Logger::LOG_COMPONENT_PHYSICS);

    if (event->getType() == Event::ET_Physics)
    {
        return true;
    }
    return true;
}

PhysicsWorld *PhysicsManager::getPhysicsWorld() const
{
    return mWorld;
}

const std::string &PhysicsManager::getPhysicsWorldTypeName() const
{
    if (mWorldFactory)
    {
        return mWorldFactory->getWorldTypeName();
    }
    return VE_STR_BLANK;
}

void PhysicsManager::init(Ogre::SceneManager *mgr, const ParamsMulti &options)
{
    const auto end = options.end();
    const auto enginePluginNameIt = options.find(VE_OPTION_PLUGIN_PHYSICS_SYSTEM);
    if (enginePluginNameIt != end)
    {
        std::string enginePluginName = enginePluginNameIt->second;
        if (enginePluginName.empty())
        {
            logAndSoftAssert(VE_SCOPE_NAME(init(Ogre::SceneManager *mgr, const ParamsMulti &options)),
                             "\"" VE_OPTION_PLUGIN_PHYSICS_SYSTEM "\" options is empty! Please check plugin name retrieving logic.",
                             Logger::LOG_COMPONENT_PHYSICS,
                             Logger::LOG_LEVEL_CRITICAL);
            return;
        }
        else
        {
            /** While loading plugin should install physics world factory by
              * calling setWorldFactory(factory) API */
            if (!renderer()->loadPlugin(FileSystem::getFullpathFromCwd(enginePluginName)))
            {
                logAndAssert(VE_SCOPE_NAME(init(Ogre::SceneManager *mgr, const ParamsMulti &options)),
                             "Loading Physics plugin \"" + enginePluginName +  "\" failed!",
                             Logger::LOG_COMPONENT_PHYSICS);
                return;
            }
        }
    }
    else
    {
        log("Options dump:", Logger::LOG_LEVEL_CRITICAL);
        log(options.toString(), Logger::LOG_LEVEL_CRITICAL);
        logAndSoftAssert(VE_SCOPE_NAME(init(Ogre::SceneManager *mgr, const ParamsMulti &options)),
                         "\"" VE_OPTION_PLUGIN_PHYSICS_SYSTEM "\" option is not set! Please check plugin name retrieving logic.",
                         Logger::LOG_COMPONENT_PHYSICS,
                         Logger::LOG_LEVEL_CRITICAL);
        auto *factory = new DummyPhysicsWorldFactory;
        if (!setWorldFactory(factory))
        {
            delete factory;
        }
    }

    Ogre::AxisAlignedBox worldBox;
    Ogre::Vector3 worldHalfExtents;
    const auto worldHalfExtentsIt = options.find(VE_OPTION_PHYSICS_WORLD_HALF_EXTENTS);
    if (worldHalfExtentsIt != end)
    {
        worldHalfExtents = StringConverter::parseVector3(
                    worldHalfExtentsIt->second,
                    VE_OPTION_PHYSICS_WORLD_HALF_EXTENTS_DEFAULT);

    }
    else
    {
        worldHalfExtents = VE_OPTION_PHYSICS_WORLD_HALF_EXTENTS_DEFAULT;
    }
    worldBox.setExtents(-worldHalfExtents, worldHalfExtents);

    Ogre::Vector3 gravity;
    const auto worldGravityIt = options.find(VE_OPTION_PHYSICS_WORLD_GRAVITY);
    if (worldGravityIt != end)
    {
        gravity = StringConverter::parseVector3(worldGravityIt->second,
                                                      VE_OPTION_PHYSICS_WORLD_GRAVITY_DEFAULT);

    }
    else
    {
        gravity = VE_OPTION_PHYSICS_WORLD_GRAVITY_DEFAULT;
    }

    /** --------------------------- */

    init(mgr, worldBox, gravity, options);
}

void PhysicsManager::init(Ogre::SceneManager *mgr,
                          const Ogre::AxisAlignedBox &worldBounds,
                          const Ogre::Vector3 &gravity,
                          const ParamsMulti &options)
{
    destroyWorld();

    log("PhysicsManager initialization...", Logger::LOG_LEVEL_DEBUG);

    mWorld = mWorldFactory->createWorld(mgr, worldBounds, gravity, options);

    log("PhysicsManager initialized", Logger::LOG_LEVEL_INFO);
}

void PhysicsManager::shutdown()
{
    log("Shutting down PhysicsManager...", Logger::LOG_LEVEL_DEBUG);

    destroyWorld();

    log("PhysicsManager is now halt", Logger::LOG_LEVEL_INFO);
}

void PhysicsManager::destroyWorld()
{
    if (mWorld)
    {
        const std::string worldName = mWorld->getName();
        log("Destroying " + worldName + "...", Logger::LOG_LEVEL_DEBUG);
        delete mWorld;
        mWorld = nullptr;
        log(worldName + " destroyed", Logger::LOG_LEVEL_DEBUG);
    }
}

RayCastVehicle *PhysicsManager::createRayCastVehicle(const VehicleModelOptions &options,
                                                     RigidBody *vehicleBody) const
{
    return mWorld->createRayCastVehicle(options, vehicleBody);
}

RigidBody *PhysicsManager::createRigidBody(const ClassName &type,
                                           const PhysicsModelOptions &options,
                                           Ogre::Entity *ent) const
{
    if (options.physicsType == PhysicsModelOptions::PMT_None)
    {
        return nullptr;
    }
    return mWorld->createRigidBody(type, options, ent);
}

/** -------------------- */

PhysicsManager *physics(bool checkPoiner)
{
    PhysicsManager *physicsMgr = PhysicsManager::getSingletonPtr();
    if (checkPoiner)
    {
        VE_CHECK_NULL_PTR(PhysicsManager, physicsMgr,
                          Logger::LOG_COMPONENT_PHYSICS);
    }
    return physicsMgr;
}
