#include "bulletphysicsworld.h"

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>

#include <physics/physicsmanager.h>

/// Shapes factories

#include <physics/bullet/shapes/bulletplanerigidbodyfactory.h>
#include <physics/bullet/shapes/bulletboxrigidbodyfactory.h>
#include <physics/bullet/shapes/bulletcylinderrigidbodyfactory.h>
#include <physics/bullet/shapes/bulletsphererigidbodyfactory.h>
#include <physics/bullet/shapes/bullettrimeshrigidbodyfactory.h>
#include <physics/bullet/shapes/bulletconvexhullrigidbodyfactory.h>

/// Vehicle

#include <physics/bullet/vehicle/bulletwheeledrigidbodyfactory.h>
#include <physics/bullet/vehicle/bulletraycastvehiclefactory.h>

/// Terrain factories

#include <physics/bullet/terrain/bulletheightmaprigidbodyfactory.h>

#include <physics/bullet/bulletdebugdrawer.h>

#include <renderer/models/physics/physicsmodeloptions.h>

#include <physics/shapes/physicsprerequisites.h>

BulletPhysicsWorld::BulletPhysicsWorld(Ogre::SceneManager *sceneMgr,
                                       const Ogre::AxisAlignedBox &worldBounds,
                                       const Ogre::Vector3 &gravity,
                                       const ParamsMulti &options)
    : PhysicsWorld(sceneMgr, worldBounds, gravity, options),
      mRayCastVehicleFactory(nullptr),
      mWorld(nullptr),
      mDebugDrawer(nullptr)
{
    /// @attention @fixme magic number last @arg
    mWorld = new OgreBulletDynamics::DynamicsWorld(sceneMgr, worldBounds, gravity,
                                                   true, true, 10000);

    mDebugDrawer = new BulletDebugDrawer;

    registerRigidBodyFactories();

    setRayCastVehicleFactory(new BulletRayCastVehicleFactory);

    setMaxSimulationSubSteps(parseMaxSimulationSubSteps(options));
}

BulletPhysicsWorld::~BulletPhysicsWorld()
{
    removeRayCastVehicleFactory();

    unregisterRigidBodyFactories();

    delete mDebugDrawer;
    mWorld = nullptr;

    delete mWorld;
    mWorld = nullptr;
}

const std::string &BulletPhysicsWorld::getName() const
{
    static const std::string ImplName = "Bullet Physics";
    return ImplName;
}

RigidBody *BulletPhysicsWorld::createRigidBody(const ClassName &type,
                                               const PhysicsModelOptions &options,
                                               Ogre::Entity *ent) const
{
    auto it = mRigidBodyFactories.find(type);
    if (it != mRigidBodyFactories.end())
    {
        const RigidBodyFactory *factory = it->second;
        RigidBody *body = factory->createRigidBody(options, ent);

        if (!body)
        {
            log("Creating of \"" + type.toString() + " for " + VE_AS_STR(PhysicsModel) +
                " named \"" + options.modelName + "\" failed.",
                Logger::LOG_LEVEL_WARNING, Logger::LOG_COMPONENT_PHYSICS);
        }

        return body;
    }

    log("Creating of \"" + type.toString() + " for " + VE_AS_STR(PhysicsModel) +
        " named \"" + options.modelName + "\" failed. There is no any registred factory for \""
        + type.toString() + "\"!",
        Logger::LOG_LEVEL_CRITICAL, Logger::LOG_COMPONENT_PHYSICS);

    return nullptr;
}

RayCastVehicle *BulletPhysicsWorld::createRayCastVehicle(const VehicleModelOptions &options,
                                                         RigidBody *vehicleBody) const
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(createRayCastVehicle(const VehicleModelOptions &options,
                                                                   RigidBody *vehicleBody) const),
                                BulletRayCastVehicleFactory,
                                mRayCastVehicleFactory,
                                Logger::LOG_COMPONENT_PHYSICS))
    {
        return mRayCastVehicleFactory->createRayCastVehicle(options, vehicleBody);
    }

    return nullptr;
}


void BulletPhysicsWorld::setRayCastVehicleFactory(BulletRayCastVehicleFactory *factory)
{
    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(setRayCastVehicleFactory(BulletRayCastVehicleFactory *factory)),
                            BulletRayCastVehicleFactory,
                            factory,
                            Logger::LOG_COMPONENT_PHYSICS);

    removeRayCastVehicleFactory();

    mRayCastVehicleFactory = factory;

    log(VE_CLASS_NAME(RayCastVehicleFactory).toString() + " registred",
        Logger::LOG_LEVEL_INFO, Logger::LOG_COMPONENT_PHYSICS);
}

void BulletPhysicsWorld::removeRayCastVehicleFactory()
{
    if (mRayCastVehicleFactory)
    {
        delete mRayCastVehicleFactory;
        mRayCastVehicleFactory = nullptr;

        log("\"" + VE_CLASS_NAME(BulletRayCastVehicleFactory).toString() + "\" destroyed",
            Logger::LOG_LEVEL_INFO, Logger::LOG_COMPONENT_PHYSICS);
    }
}

bool BulletPhysicsWorld::hasRayCastVehicleFactory() const
{
    return (bool)mRayCastVehicleFactory;
}

void BulletPhysicsWorld::addRigidBodyFactory(const ClassName &rigidBodyClassName,
                                             RigidBodyFactory *factory)
{
    if (VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(BulletPhysicsWorld::addRigidBodyFactory),
                                RigidBodyFactory,
                                factory,
                                Logger::LOG_COMPONENT_PHYSICS))
    {
        if (mRigidBodyFactories.insert({ rigidBodyClassName, factory }).second)
        {
            log(VE_CLASS_NAME(RigidBodyFactory).toString() + " of type \"" + rigidBodyClassName.toString() +
                "\" registred.", Logger::LOG_LEVEL_INFO, Logger::LOG_COMPONENT_PHYSICS);
        }
        else
        {
            removeRigidBodyFactory(rigidBodyClassName);

            mRigidBodyFactories.insert({ rigidBodyClassName, factory });

            log(VE_CLASS_NAME(RigidBodyFactory).toString() + " of type \"" + rigidBodyClassName.toString() +
                "\" registared instead of old one.",
                Logger::LOG_LEVEL_INFO, Logger::LOG_COMPONENT_PHYSICS);
        }
    }
}

void BulletPhysicsWorld::removeRigidBodyFactory(const ClassName &rigidBodyClassName)
{
    auto it = mRigidBodyFactories.find(rigidBodyClassName);
    if (it != mRigidBodyFactories.end())
    {
        delete it->second;
        mRigidBodyFactories.erase(it);

        log(VE_CLASS_NAME(RigidBodyFactory).toString() + " of type \"" + rigidBodyClassName.toString() +
            "\" destroyed",
            Logger::LOG_LEVEL_INFO, Logger::LOG_COMPONENT_PHYSICS);
    }
}

bool BulletPhysicsWorld::hasRigidBodyFactory(const ClassName &rigidBodyClassName) const
{
    return (mRigidBodyFactories.find(rigidBodyClassName) != mRigidBodyFactories.end());
}

void BulletPhysicsWorld::registerRigidBodyFactories()
{
    addRigidBodyFactory(ClassName(VE_AS_STR(PlaneRigidBody)),
                        new BulletPlaneRigidBodyFactory);

    addRigidBodyFactory(ClassName(VE_AS_STR(BoxRigidBody)),
                        new BulletBoxRigidBodyFactory);

    addRigidBodyFactory(ClassName(VE_AS_STR(CylinderRigidBody)),
                        new BulletCylinderRigidBodyFactory);

    addRigidBodyFactory(ClassName(VE_AS_STR(SphereRigidBody)),
                        new BulletSphereRigidBodyFactory);

    addRigidBodyFactory(ClassName(VE_AS_STR(TriMeshRigidBody)),
                        new BulletTriMeshRigidBodyFactory);

    addRigidBodyFactory(ClassName(VE_AS_STR(ConvexHullRigidBody)),
                        new BulletConvexHullRigidBodyFactory);

    // ---------------

    addRigidBodyFactory(ClassName(VE_AS_STR(HeightMapRigidBody)),
                        new BulletHeightMapRigidBodyFactory);

    // ---------------

    addRigidBodyFactory(ClassName(VE_AS_STR(WheeledRigidBody)),
                        new BulletWheeledRigidBodyFactory);
}

void BulletPhysicsWorld::unregisterRigidBodyFactories()
{
    removeRigidBodyFactory(ClassName(VE_AS_STR(PlaneRigidBody)));
    removeRigidBodyFactory(ClassName(VE_AS_STR(BoxRigidBody)));
    removeRigidBodyFactory(ClassName(VE_AS_STR(CylinderRigidBody)));
    removeRigidBodyFactory(ClassName(VE_AS_STR(SphereRigidBody)));
    removeRigidBodyFactory(ClassName(VE_AS_STR(TriMeshRigidBody)));
    removeRigidBodyFactory(ClassName(VE_AS_STR(ConvexHullRigidBody)));

    // ---------------

    removeRigidBodyFactory(ClassName(VE_AS_STR(HeightMapRigidBody)));

    // ---------------

    removeRigidBodyFactory(ClassName(VE_AS_STR(WheeledRigidBody)));
}

void BulletPhysicsWorld::update(float deltaTime, float realTime)
{
    /// TODO: additional args
    mWorld->stepSimulation(deltaTime, getMaxSimulationSubSteps());
}

OgreBulletDynamics::DynamicsWorld *BulletPhysicsWorld::getWorldImpl() const
{
    return mWorld;
}

DebugDrawer *BulletPhysicsWorld::getDebugDrawer() const
{
    return mDebugDrawer;
}

void BulletPhysicsWorld::setShowDebugShapes(bool show)
{
    mWorld->setShowDebugShapes(show);
}

bool BulletPhysicsWorld::getShowDebugShapes() const
{
    return mWorld->getShowDebugShapes();
}

BulletPhysicsWorld *bulletPhysicsWorld()
{
    return static_cast<BulletPhysicsWorld *>(PhysicsWorld::getSingletonPtr());
}

/** ------ Helpers ------ */

int BulletPhysicsWorld::parseMaxSimulationSubSteps(const ParamsMulti &options)
{
    const auto it = options.find("PhysicsMaxSimulationSubSteps");
    if (it != options.end())
    {
        return Ogre::StringConverter::parseInt(it->second, 1);
    }
    return 1;
}

/** ------ Factory ------ */

const std::string &BulletPhysicsWorldFactory::getWorldTypeName() const
{
    return _getWorldTypeNameStatic();
}

const std::string &BulletPhysicsWorldFactory::_getWorldTypeNameStatic()
{
    static ClassName className(VE_AS_STR(BulletPhysicsWorld));
    return className.toString();
}

BulletPhysicsWorld *BulletPhysicsWorldFactory::createWorld(
        Ogre::SceneManager *sceneMgr,
        const Ogre::AxisAlignedBox &worldBounds,
        const Ogre::Vector3 &gravity,
        const ParamsMulti &options)
{
    return new BulletPhysicsWorld(sceneMgr, worldBounds, gravity, options);
}
