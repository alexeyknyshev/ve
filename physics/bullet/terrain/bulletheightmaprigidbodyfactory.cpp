#include "bulletheightmaprigidbodyfactory.h"

#include <physics/bullet/bulletrigidbody.h>
#include <renderer/models/terrain/heightmapmodeloptions.h>

#include <bullet/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTerrainShape.h>

class BulletHeightMapRigidBody : public BulletRigidBody
{
    friend class BulletHeightMapRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletHeightMapRigidBody)

protected:
    BulletHeightMapRigidBody(OgreBulletCollisions::CollisionShape *shape,
                             const PhysicsModelOptions &options,
                             Ogre::Entity *ent,
                             const Ogre::Vector3 &terrianShiftPos)
        : BulletRigidBody(shape, options, ent, terrianShiftPos, Ogre::Quaternion::IDENTITY)
    { }
};


RigidBody *BulletHeightMapRigidBodyFactory::createRigidBody(const PhysicsModelOptions &options,
                                                            Ogre::Entity *ent) const
{
    const HeightMapModelOptions &hmOpt = static_cast<const HeightMapModelOptions &>(options);

    const Ogre::Vector3 terrainScale(hmOpt.worldSize / (hmOpt.terrainSize - 1.0f),
                                     hmOpt.maxHeight,
                                     hmOpt.worldSize / (hmOpt.terrainSize - 1.0f));

    const uint32_t imgWidth  = hmOpt.heightMapImage->getWidth();
    const uint32_t imgHeight = hmOpt.heightMapImage->getHeight();

    uint32_t imgStep = hmOpt.heightMapImageStep;
    if (imgStep > imgWidth / 2)
    {
        imgStep = imgWidth / 2;
    }
    if (imgStep > imgHeight / 2)
    {
        imgStep = imgHeight / 2;
    }

    const uint32_t widthStepCount  = imgWidth  / imgStep;
    const uint32_t heightStepCount = imgHeight / imgStep;

    float *heights = new float[widthStepCount * heightStepCount];

    uint32_t xHeightsIndex = 0;
    uint32_t xImgIndex = 0;

    for (; xHeightsIndex < widthStepCount; ++xHeightsIndex, xImgIndex += imgStep)
    {

        uint32_t yHeightsIndex = 0;
        uint32_t yImgIndex = 0;

        for (; yHeightsIndex < heightStepCount; ++yHeightsIndex, yImgIndex += imgStep)
        {
            const Ogre::ColourValue color = hmOpt.heightMapImage->getColourAt(xImgIndex, yImgIndex, 0);
            heights[xHeightsIndex + yHeightsIndex * heightStepCount] = color.r;
        }
    }

    auto *shape = new OgreBulletCollisions::HeightmapCollisionShape(hmOpt.worldSize,
                                                                    hmOpt.worldSize,
                                                                    terrainScale,
                                                                    heights,
                                                                    true);

    Ogre::Vector3 terrainShiftPos;
    terrainShiftPos.x = terrainScale.x * (widthStepCount - 1) / 2;
    terrainShiftPos.y = terrainScale.y * terrainScale.y / 2;
    terrainShiftPos.z = terrainScale.z * (heightStepCount - 1) / 2;

    return new BulletHeightMapRigidBody(shape, options, ent, terrainShiftPos);
}
