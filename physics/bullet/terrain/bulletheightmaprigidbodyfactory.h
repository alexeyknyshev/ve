#ifndef BULLETHEIGHTMAPRIGIDBODYFACTORY_H
#define BULLETHEIGHTMAPRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletHeightMapRigidBodyFactory : public RigidBodyFactory
{
public:
    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const override;
};

#endif // BULLETHEIGHTMAPRIGIDBODYFACTORY_H
