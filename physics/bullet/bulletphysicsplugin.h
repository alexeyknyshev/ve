#ifndef BULLETPHYSICSPLUGIN_H
#define BULLETPHYSICSPLUGIN_H

#include <OGRE/OgrePlugin.h>

class BulletPhysicsPlugin : public Ogre::Plugin
{
public:
    const Ogre::String &getName() const override
    {
        static Ogre::String PluginName = "BulletPhysics";
        return PluginName;
    }

    void install() override;

    void uninstall() override;

    void initialise() override { }

    void shutdown() override { }

public:
    static BulletPhysicsPlugin *plugin;
};

extern "C" void dllStartPlugin(void);
extern "C" void dllStopPlugin(void);


#endif // BULLETPHYSICSPLUGIN_H
