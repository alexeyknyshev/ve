#ifndef BULLETRAYCASTVEHICLEFACTORY_H
#define BULLETRAYCASTVEHICLEFACTORY_H

#include <physics/vehicle/raycastvehiclefactory.h>

class RayCastVehicle;

class BulletRayCastVehicleFactory : public RayCastVehicleFactory
{
public:
    RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                         RigidBody *vehicleBody) const override;
};

#endif // BULLETRAYCASTVEHICLEFACTORY_H
