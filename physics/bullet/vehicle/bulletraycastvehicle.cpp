#include "bulletraycastvehicle.h"

#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>

#include <renderer/models/vehicle/vehiclemodeloptions.h>
#include <renderer/models/vehicle/vehiclewheelmodel.h>

#include <physics/bullet/bulletphysicsworld.h>
#include <physics/bullet/vehicle/bulletwheeledrigidbody.h>

/** ------ Vehicle ------ */

BulletRayCastVehicle::BulletRayCastVehicle(const VehicleModelOptions &options,
                                           RigidBody *vehicleBody,
                                           bool init)
    : RayCastVehicle(options, vehicleBody),
      mRayCastVehicle(nullptr),
      mMaxEngineForce(options.maxEngineForce),
      mMaxSteeringAngle(options.maxSteeringAngle)
{
    if (init)
    {
        const BulletWheeledRigidBody *rigidBody =
                static_cast<const BulletWheeledRigidBody *>(vehicleBody);

        OgreBulletDynamics::WheeledRigidBody *implBody = rigidBody->getWheeledRigidBodyImpl();

        auto *tuning = new OgreBulletDynamics::VehicleTuning(options.suspensionStiffness,
                                                             options.suspensionCompression,
                                                             options.suspensionDamping,
                                                             options.maxSuspensionTravelCm,
                                                             options.maxSuspensionForce,
                                                             options.frictionSlip);

        OgreBulletDynamics::VehicleRayCaster *rayCaster =
                new OgreBulletDynamics::VehicleRayCaster(bulletPhysicsWorld()->getWorldImpl());

        mRayCastVehicle = new OgreBulletDynamics::RaycastVehicle(implBody,
                                                                 tuning,
                                                                 rayCaster);

        const int rightIndex = 0;   // x
        const int upIndex = 1;      // y
        const int forwardIndex = 2; // z
        mRayCastVehicle->setCoordinateSystem(rightIndex, upIndex, forwardIndex);
    }
}

BulletRayCastVehicle::~BulletRayCastVehicle()
{
    delete mRayCastVehicle;
    mRayCastVehicle = nullptr;
}

void BulletRayCastVehicle::addWheel(const VehicleWheelModelOptions &wheelOpt)
{
    const Ogre::Vector3 wheelDirection(0, -1, 0); /// TODO: remove stub!
    const Ogre::Vector3 wheelAxle(-1, 0, 0);



    mRayCastVehicle->addWheel(wheelOpt.modelNode,
                              wheelOpt.connectionPoint,
                              wheelDirection,
                              wheelAxle,
                              wheelOpt.suspensionRestLength,
                              wheelOpt.radius,
                              wheelOpt.isFront(),
                              wheelOpt.friction,
                              wheelOpt.rollInfluence);
}

bool BulletRayCastVehicle::isFrontWheel(int wheelId) const
{
    return mRayCastVehicle->getBulletVehicle()->getWheelInfo(wheelId).m_bIsFrontWheel;
}

void BulletRayCastVehicle::applyEngineForce(int wheelId, float force)
{
    mRayCastVehicle->applyEngineForce(force, wheelId);
}

void BulletRayCastVehicle::setSteering(int wheelId, float steering)
{
    mRayCastVehicle->setSteeringValue(steering, wheelId);
}

void BulletRayCastVehicle::setBrake(int wheelId, float force)
{
    mRayCastVehicle->getBulletVehicle()->setBrake(force, wheelId);
}

float BulletRayCastVehicle::getSpeed() const
{
    return mRayCastVehicle->getBulletVehicle()->getCurrentSpeedKmHour();
}

int BulletRayCastVehicle::getWheelsCount() const
{
    return mRayCastVehicle->getBulletVehicle()->getNumWheels();
}

void BulletRayCastVehicle::setMaxEngineForce(float force)
{
    mMaxEngineForce = force;
}

float BulletRayCastVehicle::getMaxEngineForce() const
{
    return mMaxEngineForce;
}

void BulletRayCastVehicle::setMaxSteeringAngle(float angle)
{
    mMaxSteeringAngle = angle;
}

float BulletRayCastVehicle::getMaxSteeringAngle() const
{
    return mMaxSteeringAngle;
}

void BulletRayCastVehicle::setSuspensionLength(int wheelId, float length)
{
    mRayCastVehicle->getBulletVehicle()->getWheelInfo(wheelId).m_raycastInfo.m_suspensionLength = length;
}

float BulletRayCastVehicle::getSuspensionLength(int wheelId) const
{
    return mRayCastVehicle->getBulletVehicle()->getWheelInfo(wheelId).m_raycastInfo.m_suspensionLength;
}

void BulletRayCastVehicle::setSuspensionRestLength(int wheelId, float length)
{
    mRayCastVehicle->getBulletVehicle()->getWheelInfo(wheelId).m_suspensionRestLength1 = length;
}

float BulletRayCastVehicle::getSuspensionRestLength(int wheelId) const
{
    return mRayCastVehicle->getBulletVehicle()->getWheelInfo(wheelId).m_suspensionRestLength1;
}

/*
RigidBody *BulletRayCastVehicle::getWheeledRidgidBody() const
{
    VE_NOT_IMPLEMENTED(BulletRayCastVehicle::getChassisBody);
}
*/
