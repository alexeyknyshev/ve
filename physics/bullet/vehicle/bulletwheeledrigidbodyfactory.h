#ifndef BULLETWHEELEDRIGIDBODYFACTORY_H
#define BULLETWHEELEDRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletWheeledRigidBodyFactory : public RigidBodyFactory
{
public:
    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const override;
};

#endif // BULLETWHEELEDRIGIDBODYFACTORY_H
