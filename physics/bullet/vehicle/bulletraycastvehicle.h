#ifndef BULLETRAYCASTVEHICLE_H
#define BULLETRAYCASTVEHICLE_H

#include <physics/vehicle/raycastvehicle.h>

class BulletWheeledRigidBody;
class VehicleModelOptions;

namespace OgreBulletDynamics
{
    class RaycastVehicle;
}

/// TODO: OgreBulletDynamics::RaycastVehicle deletes controlled RigidBody,
/// which causes double free (first free in BulletRigidBody)
class BulletRayCastVehicle : public RayCastVehicle
{
    friend class BulletRayCastVehicleFactory;

public:
    ~BulletRayCastVehicle();

    void addWheel(const VehicleWheelModelOptions &wheelOpt)  override;
    bool isFrontWheel(int wheelId) const  override;

    void applyEngineForce(int wheelId, float force)  override;
    void setSteering(int wheelId, float steering)  override;

    void setBrake(int wheelId, float force) override;

    float getSpeed() const override;
    int getWheelsCount() const;

    void setMaxEngineForce(float force) override;
    float getMaxEngineForce() const override;

    void setMaxSteeringAngle(float angle) override;
    float getMaxSteeringAngle() const override;

    void setSuspensionLength(int wheelId, float length) override;
    float getSuspensionLength(int wheelId) const override;

    void setSuspensionRestLength(int wheelId, float length) override;
    float getSuspensionRestLength(int wheelId) const override;

protected:
    BulletRayCastVehicle(const VehicleModelOptions &options,
                         RigidBody *vehicleBody, bool init);

    OgreBulletDynamics::RaycastVehicle *mRayCastVehicle;

private:
    float mMaxEngineForce;
    float mMaxSteeringAngle;
};

#endif // BULLETRAYCASTVEHICLE_H
