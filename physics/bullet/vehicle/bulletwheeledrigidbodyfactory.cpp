#include "bulletwheeledrigidbodyfactory.h"

#include <physics/bullet/vehicle/bulletwheeledrigidbody.h>

#include <renderer/models/vehicle/vehiclemodeloptions.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

RigidBody *BulletWheeledRigidBodyFactory::createRigidBody(
        const PhysicsModelOptions &options, Ogre::Entity *ent) const
{
    const VehicleModelOptions &vmOpt = static_cast<const VehicleModelOptions &>(options);

    /*
    Entity *ent = vmOpt._model->_getEntity();
    const Matrix4 &fullTransform = vmOpt._model->getModelNode()->_getFullTransform();

    /// TODO: remove stub!
    OgreBulletCollisions::StaticMeshToShapeConverter converter(ent, fullTransform);

    OgreBulletCollisions::BoxCollisionShape *shape = converter.createBox();
    */

    const Ogre::Vector3 bounds(1.0f, 0.75f, 2.1f);
    auto *chassisShape = new OgreBulletCollisions::BoxCollisionShape(bounds);
    auto *compound = new OgreBulletCollisions::CompoundCollisionShape;
    compound->addChildShape(chassisShape, vmOpt.chassisShift);

    return new BulletWheeledRigidBody(compound, vmOpt, ent);
}
