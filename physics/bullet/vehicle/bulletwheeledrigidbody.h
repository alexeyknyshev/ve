#ifndef BULLETWHEELEDRIGIDBODY_H
#define BULLETWHEELEDRIGIDBODY_H

#include <physics/bullet/bulletrigidbody.h>

namespace OgreBulletDynamics {
    class WheeledRigidBody;
}

class BulletWheeledRigidBodyFactory;
class VehicleModelOptions;

class BulletWheeledRigidBody : public BulletRigidBody
{
    friend class BulletWheeledRigidBodyFactory;
    friend class BulletRayCastVehicle;

public:
    VE_DECLARE_TYPE_INFO(BulletWheeledRigidBody)

protected:
    BulletWheeledRigidBody(OgreBulletCollisions::CollisionShape *shape,
                           const VehicleModelOptions &options,
                           Ogre::Entity *ent);

    void initFromShape(OgreBulletCollisions::CollisionShape *shape,
                       const VehicleModelOptions &options,
                       Ogre::Entity *ent);

    OgreBulletDynamics::WheeledRigidBody *getWheeledRigidBodyImpl() const;
};

#endif // BULLETWHEELEDRIGIDBODY_H
