#include "bulletraycastvehiclefactory.h"

#include <physics/bullet/vehicle/bulletraycastvehicle.h>

RayCastVehicle *BulletRayCastVehicleFactory::createRayCastVehicle(const VehicleModelOptions &options,
                                                                  RigidBody *vehicleBody) const
{
    return new BulletRayCastVehicle(options, vehicleBody, true);
}
