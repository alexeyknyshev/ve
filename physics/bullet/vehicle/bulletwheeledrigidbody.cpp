#include "bulletwheeledrigidbody.h"

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>

#include <renderer/models/vehicle/vehiclemodeloptions.h>
#include <physics/bullet/bulletphysicsworld.h>

BulletWheeledRigidBody::BulletWheeledRigidBody(OgreBulletCollisions::CollisionShape *shape,
                                               const VehicleModelOptions &options, Ogre::Entity *ent)
    : BulletRigidBody(options, ent)
{
    initFromShape(shape, options, ent);
}

void BulletWheeledRigidBody::initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                           const VehicleModelOptions &options, Ogre::Entity *ent)
{
    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                        const VehicleModelOptions &options, Ogre::Entity *ent)),
                            Ogre::Entity,
                            ent,
                            Logger::LOG_COMPONENT_PHYSICS);

    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                        const VehicleModelOptions &options, Ogre::Entity *ent)),
                            OgreBulletCollisions::CollisionShape,
                            shape,
                            Logger::LOG_COMPONENT_PHYSICS);

    mRigidBody = new OgreBulletDynamics::WheeledRigidBody(getBodyName(options),
                                                          bulletPhysicsWorld()->getWorldImpl());

    Ogre::SceneNode *node = ent->getParentSceneNode();

    mRigidBody->setShape(node,
                         shape,
                         options.restitution,
                         options.friction,
                         options.mass,
                         node->_getDerivedPosition(),
                         node->_getDerivedOrientation());

    setDamping(options.linearDamping, options.angularDamping);
}

OgreBulletDynamics::WheeledRigidBody *BulletWheeledRigidBody::getWheeledRigidBodyImpl() const
{
    return static_cast<OgreBulletDynamics::WheeledRigidBody *>(mRigidBody);
}
