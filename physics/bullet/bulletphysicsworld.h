#ifndef BULLETPHYSICSWORLD_H
#define BULLETPHYSICSWORLD_H

#include <physics/physicsworld.h>

namespace OgreBulletDynamics
{
    class DynamicsWorld;
}

class BulletDebugDrawer;
class RigidBodyFactory;
class BulletRayCastVehicleFactory;

class BulletPhysicsWorld : public PhysicsWorld
{
public:
    VE_DECLARE_TYPE_INFO(BulletPhysicsWorld)

    BulletPhysicsWorld(Ogre::SceneManager *sceneMgr,
                       const Ogre::AxisAlignedBox &worldBounds,
                       const Ogre::Vector3 &gravity,
                       const ParamsMulti &options);

    ~BulletPhysicsWorld();

    const std::string &getName() const override;

    void update(float deltaTime, float realTime) override;

    OgreBulletDynamics::DynamicsWorld *getWorldImpl() const;

    DebugDrawer *getDebugDrawer() const override;

    void setShowDebugShapes(bool show);
    bool getShowDebugShapes() const;

    inline void setMaxSimulationSubSteps(int steps)
    {
        mMaxSimulationSubSteps = steps;
    }

    inline int getMaxSimulationSubSteps() const
    {
        return mMaxSimulationSubSteps;
    }

protected:
    RigidBody *createRigidBody(const ClassName &type,
                               const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const override;

    RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                         RigidBody *vehicleBody) const override;

    /** ------------------------------- */

    typedef std::map<ClassName, RigidBodyFactory *> RigidBodyFactoryMap;

    void setRayCastVehicleFactory(BulletRayCastVehicleFactory *factory);
    void removeRayCastVehicleFactory();
    bool hasRayCastVehicleFactory() const;

    BulletRayCastVehicleFactory *mRayCastVehicleFactory;

    void addRigidBodyFactory(const ClassName &rigidBodyClassName,
                             RigidBodyFactory *factory);

    void removeRigidBodyFactory(const ClassName &rigidBodyClassName);
    bool hasRigidBodyFactory(const ClassName &rigidBodyClassName) const;

    RigidBodyFactoryMap mRigidBodyFactories;

private:
    void registerRigidBodyFactories();
    void unregisterRigidBodyFactories();

    static int parseMaxSimulationSubSteps(const ParamsMulti &options);

    OgreBulletDynamics::DynamicsWorld *mWorld;
    BulletDebugDrawer *mDebugDrawer;
    int mMaxSimulationSubSteps;
};

BulletPhysicsWorld *bulletPhysicsWorld();

/** ------ Factory ------ */

class BulletPhysicsWorldFactory : public PhysicsWorldFactory
{
public:
    const std::string &getWorldTypeName() const override;
    static const std::string &_getWorldTypeNameStatic();

protected:
    BulletPhysicsWorld *createWorld(Ogre::SceneManager *sceneMgr,
                                    const Ogre::AxisAlignedBox &worldBounds,
                                    const Ogre::Vector3 &gravity,
                                    const ParamsMulti &options) override;
};

#endif // BULLETPHYSICSWORLD_H
