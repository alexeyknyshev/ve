#include "bulletdebugdrawer.h"

BulletDebugDrawer::BulletDebugDrawer()
    : mDebugDrawer(nullptr)
{
    mDebugDrawer = new OgreBulletCollisions::DebugDrawer;
}

BulletDebugDrawer::~BulletDebugDrawer()
{
    delete mDebugDrawer;
    mDebugDrawer = nullptr;
}

void BulletDebugDrawer::setDebugDrawMode(DebugDrawMode mask)
{
    mDebugDrawer->setDebugMode(mask);
}

DebugDrawer::DebugDrawMode BulletDebugDrawer::getDebugDrawMode() const
{
    return (DebugDrawMode)mDebugDrawer->getDebugMode();
}
