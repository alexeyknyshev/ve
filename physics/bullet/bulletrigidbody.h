#ifndef BULLETRIGIDBODY_H
#define BULLETRIGIDBODY_H

#include <physics/rigidbody.h>

namespace OgreBulletDynamics
{
    class RigidBody;
}

namespace OgreBulletCollisions
{
    class CollisionShape;
}

class BulletRigidBody : public RigidBody
{
    friend class BulletRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletRigidBody)

    ~BulletRigidBody();

    bool isDynamic() const override;

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_PHYSICS);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const override;

    void setWorldPosition(const Ogre::Vector3 &position) override;
    Ogre::Vector3 getWorldPosition() const override;

    void setWorldOrientation(const Ogre::Quaternion &orient) override;
    Ogre::Quaternion getWorldOrientation() const override;

    void setLinearVelocity(const Ogre::Vector3 &velocity) override;
    Ogre::Vector3 getLinearVelocity() const override;

    void setAngularVelocity(const Ogre::Vector3 &velocity) override;
    Ogre::Vector3 getAngularVelocity() const override;

    void applyForce(const Ogre::Vector3 &force,
                    const Ogre::Vector3 &position) override;

    Ogre::Quaternion getCenterOfMassOrientation() const;
    Ogre::Vector3 getCenterOfMassPosition() const;

    void setAutoDeactivation(bool autoDeactivation) override;
    bool getAutoDeactivation() const override;

    void setActivationState(int state) override;
    int getActivationState() const override;

    const Ogre::AxisAlignedBox &getBoundingBox() const override;
    float getBoundingRaduis() const override;

    float getFriction() const override;

    void setDamping(float linear, float angular) override;

    float getLinearDamping() const override;
    float getAngularDamping() const override;

    void setDebugDrawEnabled(bool enabled) override;
    bool isDebugDrawEnabled() const override;

protected:
    BulletRigidBody(OgreBulletCollisions::CollisionShape *shape,
                    const PhysicsModelOptions &options,
                    Ogre::Entity *ent);

    BulletRigidBody(OgreBulletCollisions::CollisionShape *shape,
                    const PhysicsModelOptions &options,
                    Ogre::Entity *ent,
                    const Ogre::Vector3 &manualPosition,
                    const Ogre::Quaternion &manualOrientation);

    BulletRigidBody(const PhysicsModelOptions &options,
                    Ogre::Entity *ent);

    void initFromShape(OgreBulletCollisions::CollisionShape *shape,
                       const PhysicsModelOptions &options,
                       Ogre::Entity *ent);

    void initFromShape(OgreBulletCollisions::CollisionShape *shape,
                       const PhysicsModelOptions &options,
                       Ogre::Entity *ent,
                       const Ogre::Vector3 &manualPosition,
                       const Ogre::Quaternion &manualOrientation);

    OgreBulletDynamics::RigidBody *mRigidBody;

private:
    const std::string mName;
};

#endif // BULLETRIGIDBODY_H
