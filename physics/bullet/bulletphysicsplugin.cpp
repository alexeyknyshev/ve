#include "bulletphysicsplugin.h"

#include <physics/physicsmanager.h>
#include <physics/bullet/bulletphysicsworld.h>

#include <renderer/renderer.h>

BulletPhysicsPlugin *BulletPhysicsPlugin::plugin = nullptr;

void BulletPhysicsPlugin::install()
{
    physics()->setWorldFactory(new BulletPhysicsWorldFactory);
}

void BulletPhysicsPlugin::uninstall()
{
    if (physics()->getPhysicsWorldTypeName() ==
        BulletPhysicsWorldFactory::_getWorldTypeNameStatic())
    {
        physics()->setWorldFactory(nullptr);
    }
}

void dllStartPlugin()
{
    BulletPhysicsPlugin::plugin = new BulletPhysicsPlugin;
    renderer()->installPlugin(BulletPhysicsPlugin::plugin);
}

void dllStopPlugin()
{
    renderer()->uninstallPlugin(BulletPhysicsPlugin::plugin);
    delete BulletPhysicsPlugin::plugin;
    BulletPhysicsPlugin::plugin = nullptr;
}
