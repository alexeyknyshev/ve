#include "bulletrigidbody.h"

#include <renderer/models/shapes/plane/planemodeloptions.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>

#include <physics/bullet/bulletphysicsworld.h>

BulletRigidBody::BulletRigidBody(OgreBulletCollisions::CollisionShape *shape,
                                 const PhysicsModelOptions &options,
                                 Ogre::Entity *ent)
    : RigidBody(options),
      mRigidBody(nullptr),
      mName(getBodyName(options))
{
    initFromShape(shape, options, ent);
}

BulletRigidBody::BulletRigidBody(OgreBulletCollisions::CollisionShape *shape,
                                 const PhysicsModelOptions &options,
                                 Ogre::Entity *ent,
                                 const Ogre::Vector3 &manualPosition,
                                 const Ogre::Quaternion &manualOrientation)
    : RigidBody(options),
      mRigidBody(nullptr),
      mName(getBodyName(options))
{
    initFromShape(shape, options, ent, manualPosition, manualOrientation);
}


BulletRigidBody::BulletRigidBody(const PhysicsModelOptions &options, Ogre::Entity *ent)
    : RigidBody(options),
      mRigidBody(nullptr),
      mName(getBodyName(options))
{
    VE_UNUSED(ent);
}

BulletRigidBody::~BulletRigidBody()
{
    delete mRigidBody;
    mRigidBody = nullptr;
}

bool BulletRigidBody::isDynamic() const
{
    return !mRigidBody->isStaticObject();
}

const std::string &BulletRigidBody::getName() const
{
    return mName;
}

/** ------------------------------ */

void BulletRigidBody::setWorldPosition(const Ogre::Vector3 &position)
{   
    mRigidBody->getBulletRigidBody()->getWorldTransform().
            getOrigin().setValue(position.x, position.y, position.z);
}

Ogre::Vector3 BulletRigidBody::getWorldPosition() const
{
    return OgreBulletCollisions::convert(mRigidBody->getBulletRigidBody()->getWorldTransform().getOrigin());
}

/** ------------------------------ */

void BulletRigidBody::setWorldOrientation(const Ogre::Quaternion &orient)
{
    mRigidBody->getBulletRigidBody()->getWorldTransform()
            .setRotation(OgreBulletCollisions::convert(orient));
}

Ogre::Quaternion BulletRigidBody::getWorldOrientation() const
{
    return OgreBulletCollisions::convert(
                mRigidBody->getBulletRigidBody()->getWorldTransform().getRotation());
}

/** ------------------------------ */

void BulletRigidBody::setLinearVelocity(const Ogre::Vector3 &velocity)
{
    mRigidBody->setLinearVelocity(velocity);
}

Ogre::Vector3 BulletRigidBody::getLinearVelocity() const
{
    return mRigidBody->getLinearVelocity();
}

void BulletRigidBody::setAngularVelocity(const Ogre::Vector3 &velocity)
{
    mRigidBody->setAngularVelocity(velocity);
}

Ogre::Vector3 BulletRigidBody::getAngularVelocity() const
{
    return mRigidBody->getAngularVelocity();
}

/** ------------------------------ */

void BulletRigidBody::applyForce(const Ogre::Vector3 &force, const Ogre::Vector3 &position)
{
    mRigidBody->applyForce(force, position);
}

Ogre::Quaternion BulletRigidBody::getCenterOfMassOrientation() const
{
    return mRigidBody->getCenterOfMassOrientation();
}

Ogre::Vector3 BulletRigidBody::getCenterOfMassPosition() const
{
    return mRigidBody->getCenterOfMassPosition();
}

void BulletRigidBody::setAutoDeactivation(bool autoDeactivation)
{
    if (autoDeactivation)
    {
        mRigidBody->enableActiveState();
    }
    else
    {
        mRigidBody->disableDeactivation();
    }
}

void BulletRigidBody::setActivationState(int state)
{
    mRigidBody->setActivationState(state);
}

int BulletRigidBody::getActivationState() const
{
    return mRigidBody->getActivationState();
}

bool BulletRigidBody::getAutoDeactivation() const
{
    return (getActivationState() == DISABLE_DEACTIVATION);
}

const Ogre::AxisAlignedBox &BulletRigidBody::getBoundingBox() const
{
    return mRigidBody->getBoundingBox();
}

float BulletRigidBody::getBoundingRaduis() const
{
    return mRigidBody->getBoundingRadius();
}

float BulletRigidBody::getFriction() const
{
    return mRigidBody->getBulletRigidBody()->getFriction();
}

void BulletRigidBody::setDamping(float linear, float angular)
{
    mRigidBody->setDamping(linear, angular);
}

float BulletRigidBody::getLinearDamping() const
{
    return mRigidBody->getBulletRigidBody()->getLinearDamping();
}

float BulletRigidBody::getAngularDamping() const
{
    return mRigidBody->getBulletRigidBody()->getAngularDamping();
}

void BulletRigidBody::setDebugDrawEnabled(bool enabled)
{
    if (isDynamic())
    {
        mRigidBody->showDebugShape(enabled);
    }
}

bool BulletRigidBody::isDebugDrawEnabled() const
{
    return (bool)mRigidBody->getDebugShape();
}

// ----------------------------------------------------------

void BulletRigidBody::initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                    const PhysicsModelOptions &options,
                                    Ogre::Entity *ent)
{
    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                        const PhysicsModelOptions &options,
                                                        Ogre::Entity *ent)),
                            Ogre::Entity,
                            ent,
                            Logger::LOG_COMPONENT_PHYSICS);

    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                       const PhysicsModelOptions &options,
                                                       Ogre::Entity *ent)),
                            OgreBulletCollisions::CollisionShape,
                            shape,
                            Logger::LOG_COMPONENT_PHYSICS);

    mRigidBody = new OgreBulletDynamics::RigidBody(getBodyName(options),
                                                   bulletPhysicsWorld()->getWorldImpl());

    Ogre::SceneNode *node = ent->getParentSceneNode();

    switch (options.physicsType)
    {
    case PhysicsModelOptions::PMT_Dynamic:
        mRigidBody->setShape(node,
                             shape,
                             options.restitution,
                             options.friction,
                             options.mass,
                             node->_getDerivedPosition(),
                             node->_getDerivedOrientation());

        setDamping(options.linearDamping, options.angularDamping);
        break;
    case PhysicsModelOptions::PMT_Static:
        mRigidBody->setStaticShape(shape,
                                   options.restitution,
                                   options.friction,
                                   node->_getDerivedPosition(),
                                   node->_getDerivedOrientation());
        break;
    case PhysicsModelOptions::PMT_None:
    default:
        break;
    }
}

void BulletRigidBody::initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                    const PhysicsModelOptions &options,
                                    Ogre::Entity *ent,
                                    const Ogre::Vector3 &manualPosition,
                                    const Ogre::Quaternion &manualOrientation)
{
    // check Entity in case of movable object
    if (options.physicsType == PhysicsModelOptions::PMT_Dynamic)
    {
        VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                            const PhysicsModelOptions &options,
                                                            Ogre::Entity *ent,
                                                            const Ogre::Vector3 &manualPosition,
                                                            const Ogre::Quaternion &manualOrientation)),
                                Ogre::Entity,
                                ent,
                                Logger::LOG_COMPONENT_PHYSICS);
    }

    VE_CLASS_CHECK_NULL_PTR(VE_SCOPE_NAME(initFromShape(OgreBulletCollisions::CollisionShape *shape,
                                                        const PhysicsModelOptions &options,
                                                        Ogre::Entity *ent,
                                                        const Ogre::Vector3 &manualPosition,
                                                        const Ogre::Quaternion &manualOrientation)),
                            OgreBulletCollisions::CollisionShape,
                            shape,
                            Logger::LOG_COMPONENT_PHYSICS);

    mRigidBody = new OgreBulletDynamics::RigidBody(getBodyName(options),
                                                   bulletPhysicsWorld()->getWorldImpl());

    switch (options.physicsType)
    {
    case PhysicsModelOptions::PMT_Dynamic:
        mRigidBody->setShape(ent->getParentSceneNode(),
                             shape,
                             options.restitution,
                             options.friction,
                             options.mass,
                             manualPosition,
                             manualOrientation);

        setDamping(options.linearDamping, options.angularDamping);
        break;
    case PhysicsModelOptions::PMT_Static:
        mRigidBody->setStaticShape(shape,
                                   options.restitution,
                                   options.friction,
                                   manualPosition,
                                   manualOrientation);
        break;
    case PhysicsModelOptions::PMT_None:
    default:
        break;
    }
}
