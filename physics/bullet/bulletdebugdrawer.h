#ifndef BULLETDEBUGDRAWER_H
#define BULLETDEBUGDRAWER_H

#include <physics/debugdrawer.h>

#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>

class BulletDebugDrawer : public DebugDrawer
{
public:
    VE_DECLARE_TYPE_INFO(BulletDebugDrawer)

    BulletDebugDrawer();
    ~BulletDebugDrawer();

    void setDebugDrawMode(DebugDrawMode mask) override;
    DebugDrawMode getDebugDrawMode() const override;

protected:
    OgreBulletCollisions::DebugDrawer *mDebugDrawer;
};

#endif // BULLETDEBUGDRAWER_H
