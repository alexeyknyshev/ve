#ifndef BULLETTRIMESHRIGIDBODYFACTORY_H
#define BULLETTRIMESHRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletTriMeshRigidBodyFactory : public RigidBodyFactory
{
public:
    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;
};

#endif // BULLETTRIMESHRIGIDBODYFACTORY_H
