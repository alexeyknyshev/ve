#include "bulletplanerigidbodyfactory.h"

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>

#include <physics/bullet/bulletrigidbody.h>

#include <renderer/models/shapes/plane/planemodeloptions.h>

class BulletPlaneRigidBody : public BulletRigidBody
{
    friend class BulletPlaneRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletPlaneRigidBody)

protected:
    BulletPlaneRigidBody(OgreBulletCollisions::CollisionShape *shape,
                         const PhysicsModelOptions &options,
                         Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    { }
};

RigidBody *BulletPlaneRigidBodyFactory::createRigidBody(const PhysicsModelOptions &options,
                                                        Ogre::Entity *ent) const
{
    const PlaneModelOptions &pOpt = static_cast<const PlaneModelOptions &>(options);

    auto *shape = new OgreBulletCollisions::StaticPlaneCollisionShape(pOpt.plane.normal, 0);

    return new BulletPlaneRigidBody(shape, options, ent);
}
