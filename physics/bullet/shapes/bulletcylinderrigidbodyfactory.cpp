#include "bulletcylinderrigidbodyfactory.h"

#include <physics/bullet/bulletrigidbody.h>

#include <renderer/models/shapes/cylinder/cylindermodeloptions.h>

#include <framework/stringconverter.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCylinderShape.h>

class BulletCylinderRigidBody : public BulletRigidBody
{
    friend class BulletCylinderRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletCylinderRigidBody)

protected:
    BulletCylinderRigidBody(OgreBulletCollisions::CollisionShape *shape,
                            const PhysicsModelOptions &options,
                            Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    { }
};

class BulletCylinderCollisionShapeFactoryCache
{
public:
    ~BulletCylinderCollisionShapeFactoryCache()
    {
        clear();
    }

    void clear()
    {
        const auto end = mShapes.end();
        for (auto it = mShapes.begin(); it != end; ++it)
        {
            delete it->second;
        }
        mShapes.clear();
    }

    OgreBulletCollisions::CylinderCollisionShape *getShape(const Ogre::Vector3 &halfBounds)
    {
        const auto it = std::find_if(mShapes.begin(), mShapes.end(),
                                     [&](const VecCylinderPair &pair) { return pair.first == halfBounds; });

        if (it != mShapes.end())
        {
            return it->second;
        }

        logger()->logClassMessage(VE_CLASS_NAME(BulletCylinderCollisionShapeFactoryCache),
                                  VE_SCOPE_NAME(getShape(const Ogre::Vector3 &halfBounds)),
                                  "Shared " + VE_AS_STR(CylinderCollisionShape) + " (halfBounds = " +
                                  StringConverter::toString(halfBounds) + ") has been created.",
                                  Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_PHYSICS);
        auto shape = new OgreBulletCollisions::CylinderCollisionShape(halfBounds);
        mShapes.push_back({ halfBounds, shape });
        return shape;
    }

private:
    typedef std::pair<Ogre::Vector3, OgreBulletCollisions::CylinderCollisionShape *> VecCylinderPair;
    std::list<VecCylinderPair> mShapes;
};

BulletCylinderRigidBodyFactory::BulletCylinderRigidBodyFactory()
    : mCache(new BulletCylinderCollisionShapeFactoryCache)
{ }

BulletCylinderRigidBodyFactory::~BulletCylinderRigidBodyFactory()
{
    delete mCache;
}

RigidBody *BulletCylinderRigidBodyFactory::createRigidBody(
        const PhysicsModelOptions &options,
        Ogre::Entity *ent) const
{
    const CylinderModelOptions &cOpt = static_cast<const CylinderModelOptions &>(options);

    return new BulletCylinderRigidBody(mCache->getShape(cOpt.dimensions * 0.5), options, ent);
}
