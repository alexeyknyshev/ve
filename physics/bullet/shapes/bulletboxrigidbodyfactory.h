#ifndef BULLETBOXRIGIDBODYFACTORY_H
#define BULLETBOXRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletBoxCollisionShapeFactoryCache;

class BulletBoxRigidBodyFactory : public RigidBodyFactory
{
public:
    BulletBoxRigidBodyFactory();
    ~BulletBoxRigidBodyFactory();

    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;
private:
    BulletBoxCollisionShapeFactoryCache *mCache;
};

#endif // BULLETBOXRIGIDBODYFACTORY_H
