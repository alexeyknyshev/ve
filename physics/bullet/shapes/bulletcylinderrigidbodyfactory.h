#ifndef BULLETCYLINDERRIGIDBODYFACTORY_H
#define BULLETCYLINDERRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletCylinderCollisionShapeFactoryCache;

class BulletCylinderRigidBodyFactory : public RigidBodyFactory
{
public:
    BulletCylinderRigidBodyFactory();
    ~BulletCylinderRigidBodyFactory();

    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;

private:
    BulletCylinderCollisionShapeFactoryCache *mCache;
};

#endif // BULLETCYLINDERRIGIDBODYFACTORY_H

