#include "bulletsphererigidbodyfactory.h"

#include <framework/stringconverter.h>

#include <physics/bullet/bulletrigidbody.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>

#include <renderer/models/shapes/sphere/spheremodeloptions.h>

class BulletSphereRigidBody : public BulletRigidBody
{
    friend class BulletSphereRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletSphereRigidBody)

protected:
    BulletSphereRigidBody(OgreBulletCollisions::CollisionShape *shape,
                          const PhysicsModelOptions &options,
                          Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    {
    }

};

class BulletSphereCollisionShapeFactoryCache
{
public:
    ~BulletSphereCollisionShapeFactoryCache()
    {
        clear();
    }

    void clear()
    {
        const auto end = mShapes.end();
        for (auto it = mShapes.begin(); it != end; ++it)
        {
            delete it->second;
        }
        mShapes.clear();
    }

    OgreBulletCollisions::SphereCollisionShape *getShape(float radius)
    {
        const auto it = mShapes.find(radius);
        if (it != mShapes.end())
        {
            return it->second;
        }


        logger()->logClassMessage(VE_CLASS_NAME(BulletSphereCollisionShapeFactoryCache),
                                  VE_SCOPE_NAME(getShape(float radius)),
                                  "Shared " + VE_AS_STR(SphereCollisionShape) + " (radius = " +
                                  StringConverter::toString(radius) + ") has been created.",
                                  Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_PHYSICS);
        auto shape = new OgreBulletCollisions::SphereCollisionShape(radius);
        mShapes.insert({ radius, shape });
        return shape;
    }

private:
    std::map<float, OgreBulletCollisions::SphereCollisionShape *> mShapes;
};

BulletSphereRigidBodyFactory::BulletSphereRigidBodyFactory()
    : mCache(new BulletSphereCollisionShapeFactoryCache)
{ }

BulletSphereRigidBodyFactory::~BulletSphereRigidBodyFactory()
{
    delete mCache;
}

RigidBody *BulletSphereRigidBodyFactory::createRigidBody(const PhysicsModelOptions &options,
                                                         Ogre::Entity *ent) const
{
    const SphereModelOptions &cOpt = static_cast<const SphereModelOptions &>(options);

    return new BulletSphereRigidBody(mCache->getShape(cOpt.radius), options, ent);
}
