#ifndef BULLETCONVEXHULLRIGIDBODYFACTORY_H
#define BULLETCONVEXHULLRIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletConvexHullRigidBodyFactory : public RigidBodyFactory
{
public:
    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;
};

#endif // BULLETCONVEXHULLRIGIDBODYFACTORY_H
