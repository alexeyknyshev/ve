#include "bulletboxrigidbodyfactory.h"

#include <framework/stringconverter.h>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>

#include <physics/bullet/bulletrigidbody.h>
#include <renderer/models/shapes/box/boxmodeloptions.h>


class BulletBoxRigidBody : public BulletRigidBody
{
    friend class BulletBoxRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletBoxRigidBody)

protected:
    BulletBoxRigidBody(OgreBulletCollisions::CollisionShape *shape,
                       const PhysicsModelOptions &options,
                       Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    { }
};

class BulletBoxCollisionShapeFactoryCache
{
public:
    ~BulletBoxCollisionShapeFactoryCache()
    {
        clear();
    }

    void clear()
    {
        const auto end = mShapes.end();
        for (auto it = mShapes.begin(); it != end; ++it)
        {
            delete it->second;
        }
        mShapes.clear();
    }


    OgreBulletCollisions::BoxCollisionShape *getShape(const Ogre::Vector3 &halfBounds)
    {
        const auto it = std::find_if(mShapes.begin(), mShapes.end(),
                                     [&](const VecBoxPair &pair) { return pair.first == halfBounds; } );

        if (it != mShapes.end())
        {
            return it->second;
        }

        logger()->logClassMessage(VE_CLASS_NAME(BulletBoxCollisionShapeFactoryCache),
                                  VE_SCOPE_NAME(getShape(const Ogre::Vector3 &halfBounds)),
                                  "Shared " + VE_AS_STR(BoxCollisionShape) + " (halfBounds = " +
                                  StringConverter::toString(halfBounds) + ") has been created.",
                                  Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_PHYSICS);
        auto shape = new OgreBulletCollisions::BoxCollisionShape(halfBounds);
        mShapes.push_back({ halfBounds, shape });
        return shape;
    }

private:
    typedef std::pair<Ogre::Vector3, OgreBulletCollisions::BoxCollisionShape *> VecBoxPair;
    std::list<VecBoxPair> mShapes;
};

BulletBoxRigidBodyFactory::BulletBoxRigidBodyFactory()
    : mCache(new BulletBoxCollisionShapeFactoryCache)
{ }

BulletBoxRigidBodyFactory::~BulletBoxRigidBodyFactory()
{
    delete mCache;
}

RigidBody *BulletBoxRigidBodyFactory::createRigidBody(const PhysicsModelOptions &options,
                                                      Ogre::Entity *ent) const
{
    const BoxModelOptions &bOpt = static_cast<const BoxModelOptions &>(options);

    // Bullet requires only half of each side
    return new BulletBoxRigidBody(mCache->getShape(bOpt.dimensions * 0.5f), options, ent);
}
