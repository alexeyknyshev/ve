#include "bullettrimeshrigidbodyfactory.h"

#include <physics/bullet/bulletrigidbody.h>

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>

#include <renderer/models/shapes/trimesh/trimeshmodeloptions.h>

class BulletTriMeshRigidBody : public BulletRigidBody
{
    friend class BulletTriMeshRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletTriMeshRigidBody)

protected:
    BulletTriMeshRigidBody(OgreBulletCollisions::CollisionShape *shape,
                           const PhysicsModelOptions &options,
                           Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    { }
};

RigidBody *BulletTriMeshRigidBodyFactory::createRigidBody(
        const PhysicsModelOptions &options,
        Ogre::Entity *ent) const
{
    OgreBulletCollisions::TriangleMeshCollisionShape *shape = nullptr;
    const Ogre::Matrix4 &fullTransform = ent->getParentSceneNode()->_getFullTransform();

    BulletTriMeshRigidBody *body = nullptr;

    switch (options.physicsType)
    {
    case PhysicsModelOptions::PMT_Dynamic:
        {
            OgreBulletCollisions::AnimatedMeshToShapeConverter converter(ent, fullTransform);
            shape = converter.createTrimesh();
            body = new BulletTriMeshRigidBody(shape, options, ent);
        }
        break;
    case PhysicsModelOptions::PMT_Static:
        {
            OgreBulletCollisions::StaticMeshToShapeConverter converter(ent, fullTransform);
            shape = converter.createTrimesh();
            body = new BulletTriMeshRigidBody(shape, options, ent);
        }
        break;
    case PhysicsModelOptions::PMT_None:
    default:
        break;
    }

    return body;
}
