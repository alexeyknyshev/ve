#ifndef BULLETPLANERIGIDBODYFACTORY_H
#define BULLETPLANERIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletPlaneRigidBodyFactory : public RigidBodyFactory
{
public:
    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;
};

#endif // BULLETPLANERIGIDBODYFACTORY_H
