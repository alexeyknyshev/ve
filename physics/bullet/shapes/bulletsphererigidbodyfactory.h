#ifndef BULLETSPHERERIGIDBODYFACTORY_H
#define BULLETSPHERERIGIDBODYFACTORY_H

#include <physics/rigidbodyfactory.h>

class BulletSphereCollisionShapeFactoryCache;

class BulletSphereRigidBodyFactory : public RigidBodyFactory
{
public:
    BulletSphereRigidBodyFactory();
    ~BulletSphereRigidBodyFactory();

    RigidBody *createRigidBody(const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;

private:
    BulletSphereCollisionShapeFactoryCache *mCache;
};

#endif // BULLETSPHERERIGIDBODYFACTORY_H
