#include "bulletconvexhullrigidbodyfactory.h"

#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>

#include <renderer/models/shapes/convexhull/convexhullmodeloptions.h>
//#include <renderer/models/physics/physicsmodel.h>

#include <physics/bullet/bulletrigidbody.h>

class BulletConvexHullRigidBody : public BulletRigidBody
{
    friend class BulletConvexHullRigidBodyFactory;

public:
    VE_DECLARE_TYPE_INFO(BulletConvexHullRigidBody)

protected:
    BulletConvexHullRigidBody(OgreBulletCollisions::CollisionShape *shape,
                              const PhysicsModelOptions &options,
                              Ogre::Entity *ent)
        : BulletRigidBody(shape, options, ent)
    { }
};

RigidBody *BulletConvexHullRigidBodyFactory::createRigidBody(
        const PhysicsModelOptions &options,
        Ogre::Entity *ent) const
{
    const Ogre::Matrix4 &fullTransform = ent->getParentSceneNode()->_getFullTransform();
    OgreBulletCollisions::ConvexHullCollisionShape *shape = nullptr;

    BulletConvexHullRigidBody *body = nullptr;

    switch (options.physicsType)
    {
    case PhysicsModelOptions::PMT_Dynamic:
        {
            OgreBulletCollisions::AnimatedMeshToShapeConverter converter(ent, fullTransform);
            shape = converter.createConvex();
            body = new BulletConvexHullRigidBody(shape, options, ent);
        }
        break;
    case PhysicsModelOptions::PMT_Static:
        {
            OgreBulletCollisions::StaticMeshToShapeConverter converter(ent, fullTransform);
            shape = converter.createConvex();
            body = new BulletConvexHullRigidBody(shape, options, ent);
        }
        break;
    case PhysicsModelOptions::PMT_None:
    default:
        break;
    }

    return body;
}
