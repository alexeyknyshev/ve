#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

#include <framework/object.h>
#include <framework/updatelistener.h>

#include <OGRE/OgreSingleton.h>

#include <physics/physicsworld.h>

class RayCastVehicle;

class PhysicsModelOptions;

class PhysicsManager : public Object,
                       public Ogre::Singleton<PhysicsManager>,
                       public UpdateListener
{
public:
    VE_DECLARE_TYPE_INFO(PhysicsManager)

    PhysicsManager(Ogre::SceneManager *sceneMgr, const ParamsMulti &options);

    virtual ~PhysicsManager();

    void update(float deltaTime, float realTime) override;

    bool frameStarted(const Ogre::FrameEvent &evt);
    bool frameEnded(const Ogre::FrameEvent &evt);

    const std::string &getName() const override
    {
        static const std::string Name = "physicsManager";
        return Name;
    }

    void log(const std::string &msg, int level) const;

    /** @returns true if factory has been successfully installed,
      * @arg force indicates that function should ignore case when
      * Different factory (different PhysicsWorldFactory instances) with
      * same worldType exists and should force instalation of user provided
      * @remarks You should free mem in appropriate way (delete your factory)
      * if @return value is false */
    bool setWorldFactory(PhysicsWorldFactory *factory, bool force = false);

    bool event(const Event *event) override;

    PhysicsWorld *getPhysicsWorld() const;
    const std::string &getPhysicsWorldTypeName() const;

    RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                         RigidBody *vehicleBody) const;

    RigidBody *createRigidBody(const ClassName &type,
                               const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;

protected:
    void init(Ogre::SceneManager *mgr, const ParamsMulti &options);

    virtual void init(Ogre::SceneManager *mgr,
                      const Ogre::AxisAlignedBox &worldBounds,
                      const Ogre::Vector3 &gravity,
                      const ParamsMulti &options);

    virtual void shutdown();

private:
    void destroyWorld();

    PhysicsWorldFactory *mWorldFactory;
    PhysicsWorld *mWorld;
};

PhysicsManager *physics(bool checkPoiner = true);

#endif // PHYSICSMANAGER_H
