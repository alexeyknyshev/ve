#ifndef DUMMYPHYSICSWORLD_H
#define DUMMYPHYSICSWORLD_H

#include <physics/physicsworld.h>

class DummyPhysicsWorld : public PhysicsWorld
{
public:
    VE_DECLARE_TYPE_INFO(DummyPhysicsWorld)

    DummyPhysicsWorld(Ogre::SceneManager *sceneMgr,
                      const Ogre::AxisAlignedBox &worldBounds,
                      const Ogre::Vector3 &gravity,
                      const ParamsMulti &options);

    const std::string &getName() const;

    void update(float deltaTime, float realTime);

    DebugDrawer *getDebugDrawer() const;

protected:
    RigidBody *createRigidBody(const ClassName &type,
                               const PhysicsModelOptions &options,
                               Ogre::Entity *ent) const;

    RayCastVehicle *createRayCastVehicle(const VehicleModelOptions &options,
                                         RigidBody *vehicleBody) const;
};

/** ------ Factory --- */

class DummyPhysicsWorldFactory : public PhysicsWorldFactory
{
public:
    virtual const std::string &getWorldTypeName() const;

protected:
    virtual PhysicsWorld *createWorld(Ogre::SceneManager *sceneMgr,
                                      const Ogre::AxisAlignedBox &worldBounds,
                                      const Ogre::Vector3 &gravity,
                                      const ParamsMulti &options);
};

#endif // DUMMYPHYSICSWORLD_H
