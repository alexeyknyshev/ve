#include "dummyphysicsworld.h"

DummyPhysicsWorld::DummyPhysicsWorld(Ogre::SceneManager *sceneMgr,
                                     const Ogre::AxisAlignedBox &worldBounds,
                                     const Ogre::Vector3 &gravity,
                                     const ParamsMulti &options)
    : PhysicsWorld(sceneMgr, worldBounds, gravity, options)
{ }

const std::string &DummyPhysicsWorld::getName() const
{
    static const std::string ImplName = "Dummy Physics";
    return ImplName;
}

void DummyPhysicsWorld::update(float deltaTime, float realTime)
{
    VE_UNUSED(deltaTime); VE_UNUSED(realTime);
}

DebugDrawer *DummyPhysicsWorld::getDebugDrawer() const
{
    return nullptr;
}

RigidBody *DummyPhysicsWorld::createRigidBody(const ClassName &type,
                                              const PhysicsModelOptions &options,
                                              Ogre::Entity *ent) const
{
    VE_UNUSED(type);
    VE_UNUSED(options);
    VE_UNUSED(ent);
    return nullptr;
}

RayCastVehicle *DummyPhysicsWorld::createRayCastVehicle(const VehicleModelOptions &options,
                                                        RigidBody *vehicleBody) const
{
    VE_UNUSED(options);
    VE_UNUSED(vehicleBody);
    return nullptr;
}

/** ------ Factory ------ */

const std::string &DummyPhysicsWorldFactory::getWorldTypeName() const
{
    static ClassName className(VE_AS_STR(DummyPhysicsWorld));
    return className.toString();
}

PhysicsWorld *DummyPhysicsWorldFactory::createWorld(
        Ogre::SceneManager *sceneMgr,
        const Ogre::AxisAlignedBox &worldBounds,
        const Ogre::Vector3 &gravity,
        const ParamsMulti &options)
{
    return new DummyPhysicsWorld(sceneMgr, worldBounds, gravity, options);
}
