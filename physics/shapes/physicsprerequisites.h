#ifndef PHYSICSPREREQUISITES_H
#define PHYSICSPREREQUISITES_H

class PlaneRigidBody;
class BoxRigidBody;
class CylinderRigidBody;
class SphereRigidBody;
class TriMeshRigidBody;
class ConvexHullRigidBody;
class HeightMapRigidBody;
class WheeledRigidBody;

#endif // PHYSICSPREREQUISITES_H
