#include "rigidbody.h"

#include <renderer/models/physics/physicsmodeloptions.h>

RigidBody::RigidBody(const PhysicsModelOptions &options)
{
    VE_UNUSED(options);
}

RigidBody::~RigidBody()
{ }

std::string RigidBody::getBodyName(const PhysicsModelOptions &options)
{
    return options.modelName + "_body";
}
