#include "physicsworld.h"

#include <physics/debugdrawer.h>

VE_DECLARE_SINGLETON(PhysicsWorld);

PhysicsWorld::PhysicsWorld(Ogre::SceneManager *sceneMgr,
                           const Ogre::AxisAlignedBox &worldBounds,
                           const Ogre::Vector3 &gravity,
                           const ParamsMulti &options)
{
    VE_UNUSED(sceneMgr);
    VE_UNUSED(worldBounds);
    VE_UNUSED(gravity);
    VE_UNUSED(options);
}

PhysicsWorld::~PhysicsWorld()
{
}

bool PhysicsWorld::event(const Event *event)
{
    assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                                       Logger::LOG_COMPONENT_PHYSICS);

    VE_UNUSED(event);
    return false;
}

/// ------

PhysicsWorld *physicsWorld()
{
    return static_cast<PhysicsWorld *>(PhysicsWorld::getSingletonPtr());
}
