#include "glsltest.h"

#include <game/scriptedgame.h>

#include <framework/controller/inputcontroller.h>
#include <framework/controller/sdl/sdlinputbackend.h>

#include <framework/frontend/renderview.h>

#include <renderer/renderer.h>

#include <renderer/models/shapes/box/boxmodeloptions.h>
#include <renderer/models/shapes/box/boxmodel.h>

#include <framework/frontend/misc/exitkeylistener.h>

GLSLTest::GLSLTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupScene(game, view);
}

GLSLTest::~GLSLTest()
{

}

void GLSLTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new SDLInputBackendFactory);

    input->addInputListener(new ExitKeyListener(getClassName().toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

    view.setInputController(input);
}

void GLSLTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(view);

    Renderer *renderer_ = game.getRenderer();
    auto sceneMgr = renderer_->getSceneManager();

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    getClassName().toString());

    FileSystem::initialiseResourceGroup(getClassName().toString());

    Ogre::Light *dirLight = sceneMgr->createLight("dirLight");
    dirLight->setType(Ogre::Light::LT_DIRECTIONAL);
    dirLight->setDiffuseColour(Ogre::ColourValue::White);
    dirLight->setSpecularColour(Ogre::ColourValue::White);

    sceneMgr->setAmbientLight(Ogre::ColourValue::White);

    BoxModelOptions opt;
    opt.modelName = "box";
    opt.addToScene = true;
    opt.dimensions = { 1.0f, 1.0f, 1.0f };
    opt.mass = 0.0f;
    opt.physicsType = PhysicsModelOptions::PMT_None;

    BoxModel *model = new BoxModel(opt);
    auto &matManager = Ogre::MaterialManager::getSingleton();
    model->setMaterial(matManager.getByName(game.getSettingsValue("material", "orange")));

    mModels.insert(model);
}

void GLSLTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(getClassName().toString() + "Camera");

    view.setViewport({ camera, ViewportPosition::FULLSCREEN })->setBackgroundColour(Ogre::ColourValue::Black);

    camera.setPosition({ 10, 10, 10 });
    camera.setLookAt({ 0, 0, 0 });
}

/// ====================================================================================================

#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>

VE_TEST_FACTORY(GLSLTest, SDLRenderViewBackend);

int main(int argc, char *argv[])
{
    GLSLTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
