#ifndef SCRIPTCONSOLETEST_H
#define SCRIPTCONSOLETEST_H

#include <tests/test.h>

class ScriptConsole;

class ScriptConsoleTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(ScriptConsoleTest)

    ScriptConsoleTest(const ScriptedGame &game, RenderView &view);
    ~ScriptConsoleTest();

    bool event(const Event *event)
    {
        VE_NOT_IMPLEMENTED(ScriptConsoleTest::event, Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "scriptConsoleTest";
        return Name;
    }

private:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);
    void setupScene(const Game &game, RenderView &view);
    void setupResources();


    ScriptConsole *mConsole;
};

#endif // SCRIPTCONSOLETEST_H
