#ifndef RENDERVIEWTEST_H
#define RENDERVIEWTEST_H

#include <global.h>
#include <game/game.h>

#include <tests/test.h>

class RenderViewTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(RenderViewTest)

    RenderViewTest(const Game &game, RenderView &view);

    virtual bool event(const Event *event)
    {
        VE_NOT_IMPLEMENTED(RenderViewTest::event, Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    virtual const std::string &getName() const
    {
        static const std::string Name = "renderViewTest";
        return Name;
    }

    void testSplitScreen(uint16_t viewportsCount,
                         RenderView &view,
                         Renderer *renderer);

    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);
    void setupScene(const Game &game, RenderView &view);
};

#endif // RENDERVIEWTEST_H
