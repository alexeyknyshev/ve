#include "terraintest.h"

#include <game/game.h>
#include <renderer/renderer.h>
#include <framework/parser/conffileparser.h>
#include <framework/frontend/renderview.h>
#include <framework/controller/inputcontroller.h>
#include <framework/controller/ois/oisinputbackend.h>
#include <framework/filesystem.h>
#include <renderer/models/terrain/heightmapmodeloptions.h>
#include <renderer/models/terrain/heightmapmodel.h>
#include <renderer/cameras/flycameracontroller.h>
#include <tests/common/exitkeylistener.h>
#include <physics/physicsmanager.h>
#include <physics/rigidbody.h>

// tmp
#include <OGRE/Terrain/OgreTerrain.h>
#include <OGRE/Terrain/OgreTerrainGroup.h>
#include <OGRE/Terrain/OgreTerrainPagedWorldSection.h>
#include <OGRE/Terrain/OgreTerrainPaging.h>
// tmp

TerrainTest::TerrainTest(Game &game, RenderView &view)
    : Test(game, view)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupScene(game, view);
}

TerrainTest::~TerrainTest()
{
    for (Model *model : mModels)
    {
        delete model;
    }
    mModels.clear();
}

void TerrainTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(TerrainTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots;
    {
        ViewportOptions vOpt(camera, ViewportPosition::FULLSCREEN);
        viewpots.insert(std::make_pair(zOrder, vOpt));
    }

    view.setViewports(viewpots);
    view.getViewportByZOrder(zOrder)->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(0, 0, 10));
    camera.setLookAt(Ogre::Vector3::ZERO);
}

void TerrainTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(view);

    Ogre::SceneManager *mgr = game.getRenderer()->getSceneManager();
    mgr->setAmbientLight(Ogre::ColourValue::White);

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "Sinbad.zip"),
                FileSystem::LOC_TYPE_ZIP,
                VE_CLASS_NAME_STR(TerrainTest));

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    VE_CLASS_NAME_STR(TerrainTest));

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                VE_CLASS_NAME_STR(TerrainTest));

    FileSystem::initialiseResourceGroup(VE_CLASS_NAME_STR(TerrainTest));

    Ogre::SceneNode *rootNode = game.getRenderer()->getSceneManager()->getRootSceneNode();

    // ------------

//    /*
    ParamsMulti options;
    ConfFileParser(options, "terrain.cfg", VE_CLASS_NAME_STR(TerrainTest));

    HeightMapModelOptions opt(options);
    opt.modelName = "terrain";
    opt.parentNode = rootNode;
    opt.resourceGroup = VE_CLASS_NAME_STR(TerrainTest);

    HeightMapModel *terrain = new HeightMapModel(opt);
    terrain->getRigidBody()->setDebugDrawEnabled(true);

    mModels.insert(terrain);
//    */

//    mgr->setFog(Ogre::FOG_LINEAR, Ogre::ColourValue(0.7f, 0.7f, 0.8f), 0, 0, 100);

    /*

    Ogre::TerrainGlobalOptions *terrainGlobals = new Ogre::TerrainGlobalOptions;

    Ogre::Vector3 lightDir(0.55, -0.3, 0.75);
    lightDir.normalise();

    Ogre::Light *l = mgr->createLight("tstLight");
    l->setType(Ogre::Light::LT_DIRECTIONAL);
    l->setDirection(lightDir);
    l->setDiffuseColour(Ogre::ColourValue::White);
    l->setSpecularColour(Ogre::ColourValue(0.4f, 0.4f, 0.4f));

    Ogre::TerrainGroup *terrainGroup =
            new Ogre::TerrainGroup(mgr, Ogre::Terrain::ALIGN_X_Z, 513, 12000.0f);
    terrainGroup->setFilenameConvention("tstTerrain", "dat");
    terrainGroup->setOrigin(Ogre::Vector3::ZERO);

    terrainGlobals->setMaxPixelError(8);
    terrainGlobals->setCompositeMapDistance(3000.0f);
    terrainGlobals->getDefaultMaterialGenerator()->setLightmapEnabled(false);
    terrainGlobals->setCompositeMapAmbient(mgr->getAmbientLight());
    terrainGlobals->setCompositeMapDiffuse(l->getDiffuseColour());

    Ogre::Terrain::ImportData &defaultImport = terrainGroup->getDefaultImportSettings();
    defaultImport.terrainSize = 513;
    defaultImport.worldSize = 12000.0f;
    defaultImport.inputScale = 600;
    defaultImport.minBatchSize = 33;
    defaultImport.maxBatchSize = 65;
    defaultImport.layerList.resize(3);
    defaultImport.layerList[0].textureNames.push_back("dirt_grayrocky_diffusespecular.dds");
    defaultImport.layerList[0].textureNames.push_back("dirt_grayrocky_normalheight.dds");
    defaultImport.layerList[1].worldSize = 30;
    defaultImport.layerList[1].textureNames.push_back("grass_green-01_diffusespecular.dds");
    defaultImport.layerList[1].textureNames.push_back("grass_green-01_normalheight.dds");
    defaultImport.layerList[2].worldSize = 200;
    defaultImport.layerList[2].textureNames.push_back("growth_weirdfungus-03_diffusespecular.dds");
    defaultImport.layerList[2].textureNames.push_back("growth_weirdfungus-03_normalheight.dds");

    Ogre::Image img;
    img.load("terrain.png", VE_CLASS_NAME_STR(TerrainTest));
    terrainGroup->defineTerrain(0, 0, &img);
    terrainGroup->loadAllTerrains(true);

    Ogre::Terrain *terrain = terrainGroup->getTerrain(0, 0);
    Ogre::TerrainLayerBlendMap* blendMap0 = terrain->getLayerBlendMap(1);
    Ogre::TerrainLayerBlendMap* blendMap1 = terrain->getLayerBlendMap(2);

    float minHeight0 = 70.0f;
    float fadeDist0 = 40.0f;
    float minHeight1 = 70.0f;
    float fadeDist1 = 15.0f;

    float* pBlend1 = blendMap1->getBlendPointer();

    for (Ogre::uint16 y = 0; y < terrain->getLayerBlendMapSize(); ++y)
    {
        for (Ogre::uint16 x = 0; x < terrain->getLayerBlendMapSize(); ++x)
        {
            float tx, ty;

            blendMap0->convertImageToTerrainSpace(x, y, &tx, &ty);
            float height = terrain->getHeightAtTerrainPosition(tx, ty);
            float val = (height - minHeight0) / fadeDist0;
            Ogre::Math::Clamp(val, 0.0f, 1.0f);

            val = (height - minHeight1) / fadeDist1;
            val = Ogre::Math::Clamp(val, 0.0f, 1.0f);
            *pBlend1++ = val;
        }
    }

    blendMap0->dirty();
    blendMap1->dirty();

    blendMap0->update();
    blendMap1->update();


    terrainGroup->freeTemporaryResources();

    */


    // ------------

    ModelOptions sinbadOpt;
    sinbadOpt.modelName = "Sinbad";
    sinbadOpt.meshName = "Sinbad.mesh";
    sinbadOpt.parentNode = rootNode;

    Model *sinbad = new Model(sinbadOpt);
    sinbad->setPosition(Ogre::Vector3(0, 5, 0));

    sinbad->setAnimationStateEnabled("Dance", true);

    mModels.insert(sinbad);
}

void TerrainTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    ExitKeyListener *exitKeyListener = new ExitKeyListener(VE_AS_STR(TerrainTest) + VE_AS_STR(exitKeyListener),
                                                           ve::KeyData::KEY_ESCAPE);

    FlyCamera camera = view.getCamera(0);
    camera.setLookAt(Ogre::Vector3(10, 100, 10));
    FlyCameraController *cameraController =
            new FlyCameraController(VE_AS_STR(TerrainTest) + VE_AS_STR(FlyCameraController), camera, 30, 0.3f);

    input->addInputListener(exitKeyListener);
    input->addInputListener(cameraController);

    view.setInputController(input);
}
