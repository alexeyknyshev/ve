#include "luatest.h"

#include <game/scriptedgame.h>

#include <script/script.h>
#include <script/scripterrors.h>

#include <framework/logger/logger.h>

#include <framework/frontend/renderview.h>

LuaTest::LuaTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view)
{
    VE_UNUSED(view);

    logger()->setLogDetail(Logger::LOG_LEVEL_INFO);

    setupResources();

    std::string scriptName = game.getSettingsValue("game");
    testAssert(!scriptName.empty(), "\"game\" setting isn't set!");

    const Script *script = game.createScript(scriptName);

#ifndef VE_NO_EXCEPTIONS
    try {
#endif
        script->run();
#ifndef VE_NO_EXCEPTIONS
    } catch (const ScriptExeception &e) {
        log(e.backTrace(), Logger::LOG_LEVEL_CRITICAL, Logger::LOG_COMPONENT_TESTING);
    }
#endif

    delete script;

    game.exit();
}

void LuaTest::setupResources()
{
    FileSystem::clearResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS);

    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);
}

/// ====================================================================================================

#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
#include <script/lua/luascriptfactory.h>

VE_TEST_FACTORY(LuaTest, SDLRenderViewBackend, LuaScriptFactory);

int main(int argc, char *argv[])
{
    LuaTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
