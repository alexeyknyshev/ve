#include "physicstest2.h"

#include <game/scriptedgame.h>

#include <renderer/renderer.h>
#include <renderer/basecamera.h>

#include <renderer/cameras/flycameracontroller.h>
#include <renderer/cameras/followcameracontroller.h>

#include <renderer/models/shapes/box/boxmodeloptions.h>
#include <renderer/models/shapes/box/boxmodel.h>

#include <renderer/models/shapes/sphere/spheremodeloptions.h>
#include <renderer/models/shapes/sphere/spheremodel.h>

#include <physics/physicsmanager.h>
#include <physics/rigidbody.h>

#include <framework/frontend/renderview.h>
#include <framework/controller/inputcontroller.h>
#include <framework/controller/sdl/sdlinputbackend.h>

#include <tests/common/exitkeylistener.h>

class PhysicsTest2InputListener : public DefaultInputListener
{
public:
    PhysicsTest2InputListener(PhysicsTest2 *test)
        : DefaultInputListener("physicsTest2InputListener"),
          mTest(test)
    { }

    void update(float delta)
    {
        VE_UNUSED(delta);

        const PressedKeys &keys = getPressedKeys();

        Ogre::Camera *camera = renderer()->getSceneManager()->getCameras().begin()->second;

        const auto end = keys.end();
        for (auto it = keys.begin(); it != end; ++it)
        {
            switch (it->first)
            {
            case ve::KeyData::KEY_UP:
                mTest->mSphere->getRigidBody()->applyForce({0, 0, 15}, Ogre::Vector3::ZERO);
                break;
            case ve::KeyData::KEY_LEFT:
                mTest->mSphere->getRigidBody()->applyForce({-15, 0, 0}, Ogre::Vector3::ZERO);
                break;
            case ve::KeyData::KEY_RIGHT:
                mTest->mSphere->getRigidBody()->applyForce({15, 0, 0}, Ogre::Vector3::ZERO);
                break;
            case ve::KeyData::KEY_DOWN:
                mTest->mSphere->getRigidBody()->applyForce({0, 0, -15}, Ogre::Vector3::ZERO);
                break;
            case ve::KeyData::KEY_S:
                mTest->mSphere->getRigidBody()->applyForce({0, 150, 0}, Ogre::Vector3::ZERO);
                break;

            /// ------------------------------------------------------------------------- ///

            case ve::KeyData::KEY_B:
            {
                static Ogre::NameGenerator nameGenerator("box_");

                for (int i = 0; i < 10; ++i)
                {
                    SphereModelOptions opt;

                    opt.modelName = nameGenerator.generate();
                    opt.parentNode = renderer()->getSceneManager()->getRootSceneNode();
                    opt.radius = 10.f;
                    opt.restitution = 0.1f;
                    opt.mass = 10.0f;
                    opt.addToScene = true;

                    SphereModel *box = new SphereModel(opt);
                    box->setPosition({ (float)i, 0.5f, 10.0f - i });

                    box->setMaterial(Ogre::MaterialManager::getSingletonPtr()->getByName("pattern"));

                    box->getRigidBody()->setLinearVelocity(camera->getDirection().normalisedCopy() * 4);

                    mTest->mModels.insert(box);
                }

                break;
            }
            case ve::KeyData::KEY_C:
                break;
            case ve::KeyData::KEY_P:
                break;
            default:;
            }
        }
    }

    bool keyReleased(const KeyboardEvent &event)
    {
        if (event.data.key == ve::KeyData::KEY_V)
        {
            const auto &cameraList = renderer()->getSceneManager()->getCameras();
            for (const auto &nameCameraPair : cameraList)
            {
                Ogre::Camera *camera = nameCameraPair.second;
                auto mode = camera->getPolygonMode();
                if (mode == Ogre::PM_SOLID)
                {
                    camera->setPolygonMode(Ogre::PM_WIREFRAME);
                }
                else
                {
                    camera->setPolygonMode(Ogre::PM_SOLID);
                }
            }
        }
        else if (event.data.key == ve::KeyData::KEY_L)
        {
            if (mTest->getViewType() == PhysicsTest2::Fly)
            {
                mTest->setViewType(PhysicsTest2::Follow);
            }
            else
            {
                mTest->setViewType(PhysicsTest2::Fly);
            }
        }

        return DefaultInputListener::keyReleased(event);
    }

    bool event(const Event *event)
    {
        return false;
    }

    void registredAsInputListener(bool, InputController *) { }

private:
    PhysicsTest2 * const mTest;
};

PhysicsTest2::PhysicsTest2(const ScriptedGame &game, RenderView &view)
    : Test(game, view),
      mView(&view),
      mSphere(nullptr),
      mCurrentCameraController(nullptr)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupScene(game, view);
}

PhysicsTest2::~PhysicsTest2()
{
    for (Model *model : mModels)
    {
        delete model;
    }

    mModels.clear();

    mCurrentCameraController = nullptr;
    mSphere = nullptr;
    mView = nullptr;
}

void PhysicsTest2::setViewType(ViewType viewType)
{
    auto it = mCameraControllers.find(viewType);
    if (it != mCameraControllers.end())
    {
        AbstractInputListener *listener = it->second;
        if (listener && listener != mCurrentCameraController)
        {
            InputController *input = mView->getInputContoller();
            input->removeInputListener(mCurrentCameraController);
            input->addInputListener(listener);
            mCurrentCameraController = listener;
        }
    }
}

PhysicsTest2::ViewType PhysicsTest2::getViewType() const
{
    const auto end = mCameraControllers.end();
    for (auto it = mCameraControllers.begin(); it != end; ++it)
    {
        if (mCurrentCameraController == it->second)
        {
            return it->first;
        }
    }
    return Fly;
}

void PhysicsTest2::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(PhysicsTest2) + "Camera");


    view.setViewport({ camera, ViewportPosition::FULLSCREEN })->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(20, 20, 40));
    camera.setLookAt(Ogre::Vector3(0, 0, 0));
}

void PhysicsTest2::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController",
                                                 new SDLInputBackendFactory);

    BaseCamera camera = view.getCamera(0);

    auto *flyCameraController = new FlyCameraController("flyCameraController", camera, 10, Ogre::Math::PI / 16);

    mCameraControllers.insert({ Fly, flyCameraController });

    input->addInputListener(new ExitKeyListener(VE_AS_STR(PhysicsTest) + VE_AS_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    input->addInputListener(new PhysicsTest2InputListener(this));

    view.setInputController(input);

    setViewType(Fly);
}

void PhysicsTest2::setupScene(const Game &game, RenderView &view)
{
    if (game.getPhysics()->getPhysicsWorldTypeName() == "DummyPhysicsWorld")
    {
        logAndAssert(VE_SCOPE_NAME_STR(PhysicsTest::setupScene),
                     "DummyPhysicsWorld is invalid physics world provider! "
                     "Please, validate Game/Physics options.",
                     Logger::LOG_COMPONENT_PHYSICS);
    }

    Ogre::SceneManager *sceneMgr = game.getRenderer()->getSceneManager();

    /// Shadows
    sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED);

    /// add & initialize resource locations

    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation("media", FileSystem::LOC_TYPE_FS, VE_CLASS_NAME_STR(PhysicsTest2));
    FileSystem::addResourceLocation("materials", FileSystem::LOC_TYPE_FS, VE_CLASS_NAME_STR(PhysicsTest2));
    FileSystem::addResourceLocation("Sinbad.zip", FileSystem::LOC_TYPE_ZIP, VE_CLASS_NAME_STR(PhysicsTest2));

    FileSystem::initialiseResourceGroup(VE_CLASS_NAME_STR(PhysicsTest2));

    Ogre::MaterialManager *materialMgr = Ogre::MaterialManager::getSingletonPtr();

    Ogre::MaterialPtr mat = materialMgr->getByName("floor");

    ///

    BoxModelOptions opt;
    opt.physicsType = PhysicsModelOptions::PMT_Static;

    opt.modelName = "box1";
    opt.dimensions = { 70, 1, 25 };
    opt.restitution = 10;

    BoxModel *box1 = new BoxModel(opt);
    box1->setPosition({0, 0, 0});
    box1->setMaterial(mat);

    ///

    opt.modelName = "box2";

    BoxModel *box2 = new BoxModel(opt);
    box2->setPosition({0, 0, 30});
    box2->setMaterial(mat);

    mModels.insert(box1);
    mModels.insert(box2);

    ///

    opt.modelName = "box6r";
    opt.physicsType = PhysicsModelOptions::PMT_Dynamic;

    BoxModel *box3 = new BoxModel(opt);
    box3->setPosition({0, 0, 30});
    box3->setMaterial(mat);

    mModels.insert(box3);

    ///

    Ogre::Light *dirLight = sceneMgr->createLight("dirLight");
    dirLight->setType(Ogre::Light::LT_DIRECTIONAL);
    dirLight->setDiffuseColour(Ogre::ColourValue::White);
    dirLight->setSpecularColour(Ogre::ColourValue::White);

    dirLight->setDirection(Ogre::Vector3(Ogre::Vector3::NEGATIVE_UNIT_Y +
                                         Ogre::Vector3::UNIT_Z).normalisedCopy());

    Ogre::Light *spotLight = sceneMgr->createLight("spotLight");
    spotLight->setType(Ogre::Light::LT_SPOTLIGHT);
    spotLight->setDiffuseColour(Ogre::ColourValue::White);
    spotLight->setSpecularColour(Ogre::ColourValue::White);

    BoxModelOptions opt3;

    opt3.modelName = "box3";
    opt3.physicsType = BoxModelOptions::PMT_Dynamic;
    opt3.dimensions = { 5, 20, 0.5 };

    BoxModel *model3 = new BoxModel(opt3);
    model3->setPosition({ 0, 100, 3});
    model3->setMaterial(mat);

    mModels.insert(model3);

    ///

    SphereModelOptions opt4;
    opt4.parentNode = sceneMgr->getRootSceneNode();
    opt4.modelName = "sphere";
    opt4.radius = 2;
    opt4.physicsType = SphereModelOptions::PMT_Dynamic;
    opt4.restitution = 0.01f;

    auto transMat = materialMgr->getByName("floor");

    mSphere = new SphereModel(opt4);
    mSphere->getRigidBody()->setAutoDeactivation(false);
    mSphere->setPosition({0, 100, 0});
    mSphere->setMaterial(transMat);

    mModels.insert(mSphere);

    /// Follow camera (sphere)

    FollowCamera followCamera(view.getCamera(0));
    followCamera.setFollowNode(mSphere->_getModelNode());
    followCamera.setFollowOffset(Ogre::Vector3(0, 1, 1) * 10);

    auto *followCameraController = new FollowCameraController("followCameraController", followCamera, 10, Ogre::Math::PI / 16);
    mCameraControllers.insert({ Follow, followCameraController });
}
