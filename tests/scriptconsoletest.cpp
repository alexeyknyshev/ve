#include "scriptconsoletest.h"

#include <game/scriptedgame.h>

#include <renderer/renderer.h>
#include <renderer/basecamera.h>

#include <framework/frontend/renderview.h>
#include <framework/frontend/renderviewoptions.h>
#include <framework/frontend/viewportposition.h>

#include <framework/controller/inputcontroller.h>
#include <framework/controller/sdl/sdlinputbackend.h>

#include <framework/ui/uimanager.h>
#include <framework/ui/scriptconsole.h>

#include <tests/common/exitkeylistener.h>

ScriptConsoleTest::ScriptConsoleTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view),
      mConsole(nullptr)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupResources();
    setupScene(game, view);
}

ScriptConsoleTest::~ScriptConsoleTest()
{

}

void ScriptConsoleTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(ScriptConsoleTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, { camera, ViewportPosition::FULLSCREEN } }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(0, 0, 10));
    camera.setLookAt(Ogre::Vector3::ZERO);
}

void ScriptConsoleTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new SDLInputBackendFactory);

    input->addInputListener(new ExitKeyListener(VE_AS_STR(PhysicsTest) + VE_AS_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    view.setInputController(input);
}

void ScriptConsoleTest::setupResources()
{
    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    VE_CLASS_NAME_STR(ScriptConsoleTest));

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                VE_CLASS_NAME_STR(MarkupTextTest));
}

void ScriptConsoleTest::setupScene(const Game &game, RenderView &view)
{
    Ogre::Viewport *vp = view.getViewportByZOrder(0);

    ui()->loadAtlas("dejavu.gorilla");
    Gorilla::Screen *screen = ui()->createScreen(vp, "dejavu.gorilla");

    ScriptConsoleOptions opt;
    opt.name = "Console";
    opt.fontSize = 14;
    opt.promptText = "> ";
    opt.cursorBlinking = false;
    opt.watchLogger = true;

//    opt.position = { 20.0f, 20.0f };
//    opt.size = { screen->getWidth()  - 20.0f,
//                 screen->getHeight() - 20.0f };

    opt.borderWidth = 1.0f;

    opt.script = static_cast<const ScriptedGame &>(game).createScriptFromSource(Ogre::StringUtil::BLANK);
    opt.inputController = view.getInputContoller();

    mConsole = new ScriptConsole(screen, opt);
    mConsole->setVisible(true);
    mConsole->setBackgroundGradient(UG_Diagonal,
                                    Ogre::ColourValue(0.5f,  0.5f,  0.5f,  0.8f),
                                    Ogre::ColourValue(0.25f, 0.25f, 0.25f, 0.6f));

    mConsole->setBorderWidth(0.5f);
    mConsole->setBorderColor(Ogre::ColourValue::White);

}
