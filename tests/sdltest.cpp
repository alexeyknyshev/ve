#include "sdltest.h"

#include <framework/controller/inputcontroller.h>
#include <framework/controller/ois/oisinputbackend.h>
#include <framework/controller/sdl/sdlinputbackend.h>
#include <framework/frontend/renderview.h>
#include <game/game.h>
#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <tests/common/exitkeylistener.h>

SDLTest::SDLTest(const Game &game, RenderView &view)
    : Test(game, view)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupHUD(game, view);
}

void SDLTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(PhysicsTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, { camera, ViewportPosition::FULLSCREEN } }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue::Black);

    camera.setPosition(Ogre::Vector3(20, 20, 40));
    camera.setLookAt(Ogre::Vector3(0, 0, 20));
}

void SDLTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController",
                                                 new SDLInputBackendFactory);

//    BaseCamera camera = view.getCamera(0);

    input->addInputListener(new ExitKeyListener(VE_AS_STR(AnimationTest) + VE_AS_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    /*
    input->addInputListener(new FlyCameraController(VE_AS_STR(AnimationTest) + VE_AS_STR(FlyCameraController),
                                                    camera, 50.0f, Ogre::Math::HALF_PI * 0.2));
    */

    view.setInputController(input);
}

void SDLTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(game);
    VE_UNUSED(view);
}

void SDLTest::setupHUD(const Game &game, const RenderView &view)
{

}
