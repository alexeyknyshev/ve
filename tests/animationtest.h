#ifndef ANIMATIONTEST_H
#define ANIMATIONTEST_H

#include <tests/test.h>
#include <renderer/model.h>

class UiTextLabel;

class AnimationTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(AnimationTest)

    AnimationTest(const Game &game, RenderView &view);

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const override
    {
        static const std::string Name = "animationTest";
        return Name;
    }

    UiTextLabel *mFlyCameraControllerDebugLabel;

private:
    void setupCamera(const Game &game, RenderView &view) override;
    void setupControls(const Game &, RenderView &view) override;
    void setupResources();
    void setupScene(const Game &game, RenderView &view) override;
};

#endif // ANIMATIONTEST_H
