#ifndef SDLTEST_H
#define SDLTEST_H

#include <tests/test.h>

class SDLTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(SDLTest)

    SDLTest(const Game &game, RenderView &view);

    bool event(const Event *event)
    {
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = VE_CLASS_NAME_STR(SDLTest);
        return Name;
    }

private:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);
    void setupScene(const Game &game, RenderView &view);
    void setupHUD(const Game &game, const RenderView &view);
};

#endif // SDLTEST_H
