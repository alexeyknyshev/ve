#ifndef LUALOADFILETEST_H
#define LUALOADFILETEST_H

#include <tests/test.h>

class LuaLoadFileTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(LuaLoadFileTest)

    LuaLoadFileTest(const ScriptedGame &game, RenderView &view);

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        return false;
    }

    const std::string &getName() const override
    {
        const static std::string Name = "LuaLoadFileTest";
        return Name;
    }

    void setupCamera(const Game &, RenderView &) override { }
    void setupControls(const Game &, RenderView &) override { }
    void setupScene(const Game &, RenderView &) override { }
};

#endif // LUALOADFILETEST_H
