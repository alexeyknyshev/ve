#include "workqueuetest.h"

#include <game/game.h>
#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <renderer/model.h>
#include <renderer/modeloptions.h>
#include <framework/frontend/renderview.h>
#include <framework/controller/inputcontroller.h>
#include <framework/controller/ois/oisinputbackend.h>

#include <tests/common/exitkeylistener.h>

#include <OGRE/OgreWorkQueue.h>
#include <OGRE/OgreManualObject.h>

class WorkQueueResult
{
public:
    WorkQueueResult(std::vector<Ogre::Vector3> vec)
        : result(vec)
    { }

    friend std::ostream& operator<<(std::ostream &o, const WorkQueueResult &r) { return o; }
    std::vector<Ogre::Vector3> result;
};

class WorkQueueReqHandler : public Ogre::WorkQueue::RequestHandler,
                            public Ogre::WorkQueue::ResponseHandler
{
public:
    WorkQueueReqHandler(Ogre::ManualObject *obj, Ogre::Root *root)
        : mManualObj(obj),
          mWQ(nullptr)
    {
        mWQ = root->getWorkQueue();
        mWQChannel = mWQ->getChannel("WorkQueueResult");

        mWQ->addRequestHandler(mWQChannel, this);
        mWQ->addResponseHandler(mWQChannel, this);

        mWQ->addRequest(mWQChannel, 1, Ogre::Any());
    }

    ~WorkQueueReqHandler()
    {
        mWQ->processResponses();

        mWQ->removeResponseHandler(mWQChannel, this);
        mWQ->removeRequestHandler(mWQChannel, this);
    }

    Ogre::WorkQueue::Response *handleRequest(const Ogre::WorkQueue::Request *req,
                                             const Ogre::WorkQueue *srcQ)
    {
        const float accuracy = 100.0f;
        const float radius = 10.0f;

        std::vector<Ogre::Vector3> result;

        for (float theta = 0; theta <= 2 * Ogre::Math::PI; theta += Ogre::Math::PI / accuracy)
        {
            result.push_back( Ogre::Vector3( radius * cos(theta), radius * sin(theta), 0 ) );
        }

        const bool success = true;
        return OGRE_NEW Ogre::WorkQueue::Response(req, success, Ogre::Any(WorkQueueResult(result)));
    }

    void handleResponse(const Ogre::WorkQueue::Response *res,
                        const Ogre::WorkQueue *srcQ)
    {
        VE_UNUSED(srcQ);

        Ogre::SceneNode *rootNode = renderer()->getSceneManager()->getRootSceneNode();

        if (res->succeeded())
        {
            // React how you need it.
            WorkQueueResult r = Ogre::any_cast<WorkQueueResult>(res->getData());

            mManualObj->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_STRIP);

            for (std::vector<Ogre::Vector3>::const_iterator it = r.result.begin();
                 it != r.result.end(); ++it)
            {
                mManualObj->position(*it);
            }

            mManualObj->end();

            Ogre::SceneNode *node = rootNode->createChildSceneNode("ManualObjectNode");
            node->attachObject(mManualObj);
        }

        Ogre::SceneNode *sinbadNode = rootNode->createChildSceneNode("SinbadNode");

        ModelOptions opt;
        opt.modelName = "Sinbad";
        opt.meshName = "Sinbad.mesh";
        opt.modelNode = sinbadNode;
        opt.parentNode = rootNode;

        Model *sinbad = new Model(opt);

        static const std::string animName = "Dance";

        if (!sinbad->hasAnimationState(animName))
        {
//            logAndAssert(VE_SCOPE_NAME_STR(AnimationTest::setupContent),
//                         "No such animation state \"" + animName + "\" in " +
//                         VE_CLASS_NAME_STR(Model) + " named " + sinbad->getName() + " !");
        }

        sinbad->setAnimationStateEnabled(animName, true);

        sinbad->setAnimationStateLoop(animName, true);

        sinbad->setPosition(Ogre::Vector3(0, 5, 0));
        sinbad->setShowBoundingBox(true);
    }

private:
    Ogre::ManualObject *mManualObj;
    Ogre::WorkQueue *mWQ;
    Ogre::uint16 mWQChannel;
};

WorkQueueTest::WorkQueueTest(Game &game, RenderView &view)
    : Test(game, view),
      mHandler(nullptr)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupScene(game, view);

    Ogre::ManualObject *obj = game.getRenderer()->getSceneManager()->createManualObject();

    mHandler = new WorkQueueReqHandler(obj, game.getRenderer());

}

WorkQueueTest::~WorkQueueTest()
{
    delete mHandler;
}

void WorkQueueTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(game); VE_UNUSED(view);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS);

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS);

    FileSystem::addResourceLocation(FileSystem::concatenatePath(FileSystem::getCwd(), "Sinbad.zip"),
                                    FileSystem::LOC_TYPE_ZIP);

    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);
}

void WorkQueueTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(ScriptConsoleTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, { camera, ViewportPosition::FULLSCREEN } }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(0, 0, 10));
    camera.setLookAt(Ogre::Vector3::ZERO);
}

void WorkQueueTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    input->addInputListener(new ExitKeyListener(VE_AS_STR(WorkQueueTest) + VE_AS_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    view.setInputController(input);
}
