#include "lualoadfiletest.h"

#include <game/scriptedgame.h>
#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
#include <script/script.h>
#include <script/lua/luascriptfactory.h>

LuaLoadFileTest::LuaLoadFileTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view)
{
    VE_UNUSED(view);

    const std::string luaFile = game.getSettingsValue("script");
    std::string luaFileDir, luaFileBaseName;

    FileSystem::splitFileName(luaFile, luaFileDir, luaFileBaseName);
    luaFileDir = FileSystem::concatenatePath(FileSystem::getCwd(), luaFileDir);

    FileSystem::addResourceLocation(luaFileDir, FileSystem::LOC_TYPE_FS);
    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    testAssert(luaFile.size() > 0, "--script isn't set");

    Script *script = game.createScript(luaFileBaseName);
    testAssert(script->compile(), "script compilation failed: " + script->output());
    script->run();

    const std::string imagePath = script->getGlobalAsString("imagePath");

    delete script;

    game.exit();
}

/// ====================================================================================================

class LuaLoadFileTestFactory : public TestFactory
{
public:
    virtual Test *createTest(const ScriptedGame &game, RenderView &view) const
    {
        return new LuaLoadFileTest(game, view);
    }

    virtual RenderViewBackend *createRenderViewBackend() const
    {
        return new SDLRenderViewBackend;
    }

    virtual ScriptFactory *createScriptFactory() const
    {
        return new LuaScriptFactory;
    }
};


int main(int argc, char *argv[])
{
    LuaLoadFileTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
