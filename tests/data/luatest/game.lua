-- require "util"

l = ve.logger()
self:log("Hello world!", ve.LOG_LEVEL_DEBUG)
self:log("Hello world!", ve.LOG_LEVEL_INFO)
self:log("Hello world!", ve.LOG_LEVEL_CRITICAL)

-- camera = ve.BaseCamera("PhysicsTestCamera")
-- l:scriptLog("Current Camera name: " .. camera:getName(), ve.LOG_LEVEL_INFO);

self:log("package.path: " .. package.path, ve.LOG_LEVEL_CRITICAL)

self:log("Logger: " .. tostring(l), ve.LOG_LEVEL_INFO)
self:log("Logger: " .. tostring(self), ve.LOG_LEVEL_INFO)

-- self.log(

print("Hello world!")

r = ve.renderer()
cameraNames = r:getCameraNames()

for i = 1, cameraNames:size() do
    self:log(cameraNames.at(i), ve.LOG_LEVEL_INFO)
end

-- print(ve.logger());

-- options = ve.RenderViewOptions();

-- view = ve.RenderView();

ve.exit()
