renderer = ve.renderer()

planeModelOptions = ve.PlaneModelOptions()
planeModelOptions.modelName = "floor"
planeModelOptions.plane.normal = ve.Vector3(0, 1, 0)
planeModelOptions.plane.d = 0
planeModelOptions.position = ve.Vector3(0, 0, 0)
planeModelOptions.segments.x = 50
planeModelOptions.segments.y = 50
planeModelOptions.uTile = 10
planeModelOptions.vTile = 10
planeModelOptions.size = ve.SizeF(300, 300)
planeModelOptions.friction = 5
planeModelOptions.physicsType = ve.PhysicsModelOptions_PMT_Static;

planeModel = ve.PlaneModel(planeModelOptions)
