startingX = 6
startingY = 0
framesInRow = 4
rowNumber = 1
transparencyFill = true
imagePath = "RESOURSES/heroSprite.png"
transparencyColor = 
{
  r = 0,
  g = 0,
  b = 0,
}

sceneManagerNames = ve.renderer():getSceneManagerNames()
cameraNames = ve.renderer():getCameraNames()

for i = 1, sceneManagerNames:size() do
    ve.logger():scriptLog(sceneManagerNames[i], ve.LOG_LEVEL_CRITICAL)
end

for i = 1, cameraNames:size() do
    ve.logger():scriptLog(cameraNames[i], ve.LOG_LEVEL_CRITICAL)
end

sceneManagerNames[1] = "Hello world!"

size = ve.SizeU(100, 200)
ve.logger():scriptLog("size = " .. size.width .. "x" .. size.height, ve.LOG_LEVEL_CRITICAL)
ve.logger():scriptLog(tostring(size), ve.LOG_LEVEL_CRITICAL)

ve.logger():scriptLog("File successfully loaded!", ve.LOG_LEVEL_CRITICAL)
