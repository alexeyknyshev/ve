#include "test.h"

#include <framework/parser/cmdlineparser.h>
#include <framework/parser/conffileparser.h>

#include <framework/controller/inputcontroller.h>
#include <framework/controller/sdl/sdlinputbackend.h>

#include <renderer/renderer.h>
#include <framework/frontend/renderview.h>
#include <game/scriptedgame.h>

#include <framework/frontend/misc/renderviewcloselistener.h>

Test::Test(const Game &, RenderView &)
{ }

Test::~Test()
{
    mModels.deleteAll();
}

int Test::runTest(int argc, char *argv[],
                  const TestFactory &factory,
                  ParamsMulti gameOptions)
{
    CmdLineParser(gameOptions, argc, argv);

    Logger logger_;
    Ogre::Log *deflog = logger_.getDefaultLog();
    if (!deflog)
    {
        const std::string defaultPath = "ve.log";
        const std::string &logFilePath = gameOptions.get("log", defaultPath);
        logger_.createLog(logFilePath, true);
    }
    logger_.setLogDetail(Logger::LOG_LEVEL_DEBUG);


    /* Parse config file (path passed from command line) */
    const auto it = gameOptions.find("config");
    if (it != gameOptions.end())
    {
        bool ok = false;
        ConfFileParser(gameOptions, it->second, &ok);
        if (!ok)
        {
            return 255;
        }
    }

    Renderer renderer_(gameOptions);
    renderer_.setRenderViewBackend(factory.createRenderViewBackend());

    ve::SizeU viewSizeHint(800, 600);
    renderer_.getRenderViewSizeHint(viewSizeHint);

    ve::SizeU viewSize(gameOptions.get("resolution", viewSizeHint.toString()));

    RenderViewOptions opt;
    opt.name = "renderWindow";
    opt.size = viewSize;
    opt.fullScreen = StringConverter::parseBool(
                gameOptions.get("fullsreen", VE_STR_BLANK),
                false);

    {
        const std::string defaultFsaa = std::to_string(0);
        const std::string &fsaaVal = gameOptions.get("fsaa", defaultFsaa);
        if (veIsNumber(fsaaVal))
        {
            opt.parameters["FSAA"] = std::to_string(std::atoi(fsaaVal.c_str()));
        }
        else
        {
            logger_.logClassMessage(VE_CLASS_NAME(Test),
                                    VE_SCOPE_NAME(runTest(int, char*, const TestFactory &)),
                                    "Invalid game option \"fsaa\" = \"" + fsaaVal + "\".",
                                    Logger::LOG_LEVEL_CRITICAL,
                                    Logger::LOG_COMPONENT_TESTING);
        }
    }

    {
        const std::string defaultVsync = "no";
        std::string vsyncVal = gameOptions.get("vsync", defaultVsync);
        Ogre::StringUtil::toLowerCase(vsyncVal);
        if (vsyncVal == "yes" || vsyncVal == "no")
        {
            opt.parameters["VSync"] = vsyncVal;
        }
        else
        {
            logger_.logClassMessage(VE_CLASS_NAME(Test),
                                    VE_SCOPE_NAME(runTest(int, char*, const TestFactory &)),
                                    "Invalid game options \"vsync\" = \"" + vsyncVal + "\".",
                                    Logger::LOG_LEVEL_CRITICAL,
                                    Logger::LOG_COMPONENT_TESTING);
        }
    }

    {
        // force SDL to create own context and let OGRE use it
        const std::string defaultCurrentGLContext = "no";
        std::string currentGLContextVal =
                gameOptions.get(VE_OPTION_WINDOW_BACKEND_CONTEXT, defaultCurrentGLContext);
        if (StringConverter::parseBool(currentGLContextVal, false))
        {
            opt.parameters[VE_OPTION_WINDOW_BACKEND_CONTEXT] = currentGLContextVal;
        }
    }

    opt.listeners.insert(new RenderViewCloseListener("closeListener"));


    logger_.logClassMessage(
                VE_CLASS_NAME(Test),
                VE_SCOPE_NAME(runTest(int, char*, const TestFactory &)),
                "Creating RenderView...",
                Logger::LOG_LEVEL_DEBUG,
                Logger::LOG_COMPONENT_TESTING);

    RenderView *view = new RenderView(opt);

    ScriptedGame game("scriptedGame", &logger_,
                      &renderer_, gameOptions, *view);

    ScriptFactory *scriptFactory = factory.createScriptFactory();
    game.registerScriptFactory(scriptFactory);

    Test *test = factory.createTest(game, *view);
    view->setTitle(test->getClassName().toString());

    /* Enter event loop */
    int status = game.enter();
    delete test;

    delete view;

    return status;
}

void Test::testAssert(bool condition, const std::string desription) const
{
    veAssert(condition, getClassName(),
             VE_SCOPE_NAME(Test::testAssert),
             desription, Logger::LOG_COMPONENT_OTHER);
}
