#ifndef TEST_H
#define TEST_H

#include <renderer/model.h>

class Game;
class ScriptedGame;
class RenderView;

class TestFactory;

/**
 * @brief The Test @class
 *
 * The main testing framework for ve.
 * Q: How to use this?
 * A: 1. First of all you should add new class (.h + .cpp files) under tests dir.
 *       Usage of class_template.py script from project root is highly recommended.
 *          @see class_template.py --help
 *    2. Your test class should be public inherited from Test.
 *    3. Implement setupCamera, setupControls and setupScene abstract methods.
 *    4. Create public subclass of TestFactory and implement createTest and
 *       createRenderViewBackend methods.
 *       createTest should return new object of your test class.
 *       createRenderViewBackend should return new RenderViewBackend subclass
 *          @see framework/frontend/renderviewbackend.h
 *          SDLRenderViewBackend for example
 *    5. declare int main(int argc, char *argv[]) function.
 *    6. In main:
 *          a. create object of corresponding TestFactory subclass
 *             (hint: use VE_TEST_FACTORY macro to declare TestFactory subclass)
 *          b. call Test::runTest(argc, argv, factory);
 *          c. return runTest result (exit code) to system.
 */

class Test : public Object
{
public:
    Test(const Game &game, RenderView &view);
    virtual ~Test();

    static int runTest(int argc, char *argv[],
                       const TestFactory &factory,
                       ParamsMulti customGameOptions = ParamsMulti());

    void testAssert(bool condition, const std::string desription) const;

    inline const ModelMap &getModels() const { return mModels; }

protected:
    /// This methods must be overriden but on the other hand
    /// you should call them manually (if you need)
    virtual void setupCamera(const Game &game, RenderView &view) = 0;
    virtual void setupControls(const Game &game, RenderView &view) = 0;
    virtual void setupScene(const Game &game, RenderView &view) = 0;

    ModelMap mModels;
};

class RenderViewBackend;
class ScriptFactory;

/**
 * @brief The TestFactory @class
 *
 * Implement this interface and pass its instance to Test::runTest
 * as 3rd parameter
 *
 * @see Test @class
 */

class TestFactory
{
public:
    /**
     * @brief createTest
     * @return new instance of Test subclass */
    virtual Test *createTest(const ScriptedGame &game, RenderView &view) const = 0;

    /**
     * @brief createRenderViewBackend
     * @return new instance of RenderViewBackend subclass
     * @see SDLRenderViewBackend for example: framework/frontend/sdlviewbackend/sdlrenderviewbackend.h
     */
    virtual RenderViewBackend *createRenderViewBackend() const = 0;

    /**
     * @brief createScriptFactory
     * @return new instance of ScriptFactory subclass or nullptr (if no scripting will be used)
     * @see LuaScriptFactory for example: script/lua/luascriptfactory.h
     */
    virtual ScriptFactory *createScriptFactory() const = 0;
};

#define GET_MACRO_VE_TEST_FACTORY(_1, _2, _3, NAME, ...) NAME
#define VE_TEST_FACTORY(...) GET_MACRO_VE_TEST_FACTORY(__VA_ARGS__, _VE_TEST_FACTORY_3, _VE_TEST_FACTORY_2)(__VA_ARGS__)

// Three arguments variant with ScriptFactory
#define _VE_TEST_FACTORY_3(TestClass, RenderViewBackendClass, ScriptFactoryClass)\
class TestClass##Factory : public TestFactory\
{\
public: \
    Test *createTest(const ScriptedGame &game, RenderView &view) const override\
    {\
        return new TestClass(game, view);\
    }\
\
    RenderViewBackend *createRenderViewBackend() const override\
    {\
        return new RenderViewBackendClass();\
    }\
\
    ScriptFactory *createScriptFactory() const override\
    {\
        return new ScriptFactoryClass();\
    }\
}

// Two arguments variant. No ScriptFactory
#define _VE_TEST_FACTORY_2(TestClass, RenderViewBackendClass)\
class TestClass##Factory : public TestFactory\
{\
public: \
    Test *createTest(const ScriptedGame &game, RenderView &view) const override\
    {\
        return new TestClass(game, view);\
    }\
\
    RenderViewBackend *createRenderViewBackend() const override\
    {\
        return new RenderViewBackendClass();\
    }\
\
    ScriptFactory *createScriptFactory() const override\
    {\
        return nullptr;\
    }\
}


#endif // TEST_H
