#ifndef GLSLTEST_H
#define GLSLTEST_H

#include <tests/test.h>

class GLSLTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(GLSLTest)

    GLSLTest(const ScriptedGame &game, RenderView &view);
    ~GLSLTest();

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const override
    {
        static const std::string Name = "glslTest";
        return Name;
    }

protected:
    void setupControls(const Game &game, RenderView &view) override;
    void setupScene(const Game &game, RenderView &view) override;
    void setupCamera(const Game &game, RenderView &view) override;
};

#endif // GLSLTEST_H
