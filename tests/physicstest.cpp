#include "physicstest.h"

#include <game/scriptedgame.h>

#include <renderer/renderer.h>

#include <renderer/cameras/flycamera.h>
#include <renderer/cameras/flycameracontroller.h>

#include <renderer/cameras/followcamera.h>
#include <renderer/cameras/followcameracontroller.h>

#include <renderer/cameras/cameramodecontroller.h>

#include <renderer/models/shapes/box/boxmodeloptions.h>
#include <renderer/models/shapes/box/boxmodel.h>

#include <renderer/models/shapes/plane/planemodel.h>
#include <renderer/models/shapes/plane/planemodeloptions.h>

#include <renderer/models/shapes/sphere/spheremodel.h>
#include <renderer/models/shapes/sphere/spheremodeloptions.h>

#include <renderer/models/shapes/trimesh/trimeshmodel.h>
#include <renderer/models/shapes/trimesh/trimeshmodeloptions.h>

#include <renderer/models/shapes/cylinder/cylindermodel.h>
#include <renderer/models/shapes/cylinder/cylindermodeloptions.h>

#include <renderer/models/vehicle/vehiclemodeloptions.h>
#include <renderer/models/vehicle/vehiclemodel.h>

#include <renderer/models/vehicle/vehiclewheelmodeloptions.h>
#include <renderer/models/vehicle/vehiclewheelmodel.h>

#include <renderer/models/terrain/heightmapmodeloptions.h>
#include <renderer/models/terrain/heightmapmodel.h>

#include <physics/physicsmanager.h>
#include <physics/rigidbody.h>
#include <physics/vehicle/defaultvehiclecontroller.h>

#include <framework/frontend/renderview.h>

#include <framework/filesystem.h>
#include <framework/parser/conffileparser.h>

#include <framework/controller/inputcontroller.h>
#include <framework/controller/defaultinputlistener.h>
#include <framework/controller/ois/oisinputbackend.h>
#include <framework/controller/sdl/sdlinputbackend.h>

#include <framework/frontend/misc/exitkeylistener.h>

#include <framework/ui/uimanager.h>
#include <framework/ui/scriptconsole.h>
#include <framework/ui/uitextlabel.h>
#include <framework/ui/widgets/fpswidget.h>
#include <framework/ui/uidebugcolors.h>
#include <framework/ui/layouts/uiverticallayout.h>
#include <framework/ui/layouts/uihorizontallayout.h>
#include <framework/ui/layouts/uilayoutspacer.h>

#include <script/script.h>

#define CONSOLE_ENABLED 1

class PhysicsTestInputListener : public DefaultInputListener
{
    enum ShapeType
    {
        ST_Box = 0,
        ST_Cylinder,
        ST_Sphere,
        ST_None
    };

public:
    PhysicsTestInputListener(const std::string &name,
                             const BaseCamera &cam,
                             PhysicsTest *test)
        : DefaultInputListener(name),
          camera(cam),
          nameGenerator("throwed"),
          timeSinceThrow(0.0f),
          timeSinceDebugSwitch(0.0f),
          mTest(test)
    { }

    void update(float deltaTime, float realTime)
    {
        /*
        {
            static float deltaFpsUpd = 0.0f;
            static const float timeOut = 0.15f;

            deltaFpsUpd += realTime;

            if (deltaFpsUpd > timeOut)
            {
                auto viewport = mTest->mView->getViewportByIndex(0);

                std::string text = StringConverter::toString(viewport->getTarget()->getLastFPS(),
                                                                   2, 0, ' ', std::ios_base::fixed) + " FPS, " +
                                   StringConverter::toString(mTest->mModels.size()) + " models, " +
                                   StringConverter::toString(viewport->getTarget()->getTriangleCount()) + " triangles.";
                mTest->mFPSLabel->setText(text);
                deltaFpsUpd -= timeOut;
            }
        }
        */

        timeSinceThrow += deltaTime;
        timeSinceDebugSwitch += deltaTime;

        Ogre::SceneNode *rootNode = renderer()->getSceneManager()->getRootSceneNode();
        Ogre::MaterialManager *matMgr = Ogre::MaterialManager::getSingletonPtr();

        ShapeType throwType = ST_None;

        if (isKeyPressed(ve::KeyData::KEY_B))
        {
            throwType = ST_Box;
        }
        else if (isKeyPressed(ve::KeyData::KEY_C))
        {
            throwType = ST_Cylinder;
        }
        else if (isKeyPressed(ve::KeyData::KEY_P))
        {
            throwType = ST_Sphere;
        }
        else if (isKeyPressed(ve::KeyData::KEY_V))
        {
            if (mTest && timeSinceDebugSwitch > 0.2f)
            {
                mTest->setVehicleDebugEnabled(!mTest->isVehicleDebugEnabled());
                timeSinceDebugSwitch = 0.0f;
            }
        }
        else if (isKeyPressed(ve::KeyData::KEY_G))
        {
            if (mTest && timeSinceDebugSwitch > 0.2f)
            {
                mTest->setShapesDebugEnabled(!mTest->isShapesDebugEnabled());
                timeSinceDebugSwitch = 0.0f;
            }
        }
        else if (isKeyPressed(ve::KeyData::KEY_H) && timeSinceDebugSwitch > 0.2f)
        {
            Ogre::SceneManager *mgr = renderer()->getSceneManager();
            if (mgr->isShadowTechniqueInUse())
            {
                renderer()->getSceneManager()->setShadowTechnique(Ogre::SHADOWTYPE_NONE);
            }
            else
            {
                renderer()->getSceneManager()->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_MODULATIVE);
            }
            timeSinceDebugSwitch = 0.0f;
        }
        else if (isKeyPressed(ve::KeyData::KEY_L) && timeSinceDebugSwitch > 1.0f)
        {
            if (mTest->getViewType() == PhysicsTest::Fly)
            {
                mTest->setViewType(PhysicsTest::Follow);
            }
            else
            {
                mTest->setViewType(PhysicsTest::Fly);
            }
            timeSinceDebugSwitch = 0.0f;
        }

        if (timeSinceThrow > 0.01f && throwType != ST_None)
        {
            if (throwType == ST_Box)
            {
                BoxModelOptions opt;
                opt.modelName = nameGenerator.generate();
                opt.dimensions = 1.0f;
                opt.parentNode = rootNode;
                opt.restitution = 0.1f;
                opt.mass = 10.0f;

                BoxModel *box = new BoxModel(opt);
                box->setPosition(camera.getPosition() + camera.getDirection());

                box->setMaterial(matMgr->getByName("chess_pattern"));

                box->getRigidBody()->setLinearVelocity(camera.getDirection().normalisedCopy() * 40);

                mTest->mModels.insert(box);
            }
            else if (throwType == ST_Cylinder)
            {
                CylinderModelOptions opt;
                opt.modelName = nameGenerator.generate();
                opt.dimensions = 1.0f;
                opt.parentNode = rootNode;

                CylinderModel *cylinder = new CylinderModel(opt);
                cylinder->setPosition(camera.getPosition() + camera.getDirection());

                cylinder->setMaterial(matMgr->getByName("chess_pattern"));

                cylinder->getRigidBody()->setLinearVelocity(camera.getDirection().normalisedCopy() * 40);

                mTest->mModels.insert(cylinder);
            }
            else if (throwType == ST_Sphere)
            {
                SphereModelOptions opt;
                opt.modelName = nameGenerator.generate();
                opt.radius = 0.5f;
                opt.parentNode = rootNode;

                SphereModel *sphere = new SphereModel(opt);
                sphere->setPosition(camera.getPosition() + camera.getDirection());

                sphere->setMaterial(matMgr->getByName("chess_pattern"));

                sphere->getRigidBody()->setLinearVelocity(camera.getDirection().normalisedCopy() * 40);

                mTest->mModels.insert(sphere);
            }

            timeSinceThrow = 0.0f;
            throwType = ST_None;
        }
    }

    bool keyPressed(const KeyboardEvent &event)
    {
#if CONSOLE_ENABLED
        if (event.data.key == ve::KeyData::KEY_N)
        {
            //const bool debugEnabled = mTest->mConsole->isDebugEnabled();
            //mTest->mConsole->setDebugEnabled(!debugEnabled);
        }
#endif
        if (event.data.key == ve::KeyData::KEY_GRAVE)
        {
            const auto frames = ui()->getTopLevelFrames();
            const auto framesEnd = frames.end();
            for (auto it = frames.begin(); it != framesEnd; ++it)
            {
                UiFrame *frame = it->first;
                frame->setVisible(!frame->isVisible());
            }
        }

        return DefaultInputListener::keyPressed(event);
    }

protected:
    void registredAsInputListener(bool registred, InputController *controller)
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

private:
    BaseCamera camera;
    Ogre::NameGenerator nameGenerator;
    float timeSinceThrow;
    float timeSinceDebugSwitch;
    PhysicsTest *mTest;
};

class VehicleListener : public DefaultVehicleController
{
public:
    VE_DECLARE_TYPE_INFO(VehicleListener)

    VehicleListener(const std::string &name,
                    RayCastVehicle *vehicle,
                    float engineForceStep,
                    float steeringStep,
                    PhysicsTest *test)
        : DefaultVehicleController(name,
                                   vehicle,
                                   engineForceStep,
                                   steeringStep),
          mTest(test)
    { }

    bool event(const Event *event)
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        VE_UNUSED(event);
        return false;
    }

    void update(float deltaTime, float realTime)
    {
        DefaultVehicleController::update(deltaTime, realTime);
        if (isKeyPressed(ve::KeyData::KEY_R))
        {
            mTest->resetVehicle();
        }
    }

protected:
    void registredAsInputListener(bool registred, InputController *controller)
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

private:
    PhysicsTest *mTest;
};

class PhysicsTestFollowCameraController : public FollowCameraController
{
public:
    PhysicsTestFollowCameraController(const std::string &name, const FollowCamera &followCamera,
                                      float zoomSpeed, float rotationSpeed, PhysicsTest *test)
        : FollowCameraController(name, followCamera, zoomSpeed, rotationSpeed),
          mTest(test)
    { }

    void update(float deltaTime, float realTime)
    {
//        mTest->mMouseMonitor->setText(StringConverter::toString(mDeltaRotation));
        FollowCameraController::update(deltaTime, realTime);
    }

private:
    PhysicsTest *mTest;
};

PhysicsTest::PhysicsTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view),
      mShapesDebugEnabled(false),
      mVehicle(nullptr),
      mView(&view),
      mCurrentCameraController(nullptr),
      mConsole(nullptr),
      mFPSLabel(nullptr)
{
    setupCamera(game, view);

    setupScene(game, view);
    setupControls(game, view);
    setupVehicle(game, view);
    setupConsole(game, view);
}

PhysicsTest::~PhysicsTest()
{
    mView = nullptr;

    delete mConsole;
    mConsole = nullptr;
    mFPSLabel = nullptr;
}

void PhysicsTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    BaseCamera camera = view.getCamera(0);

    input->addInputListener(new ExitKeyListener(getClassName().toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

    input->addInputListener(new PhysicsTestInputListener(VE_CLASS_NAME(PhysicsTestInputListener).toString(),
                                                         camera, this));

    input->addInputListener(new CameraModeController(getClassName().toString() +
                                                     VE_CLASS_NAME(CameraModeController).toString(),
                                                     camera, ve::KeyData::KEY_M));

    view.setInputController(input);
}

void PhysicsTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(view);

    if (game.getPhysics()->getPhysicsWorldTypeName() == "DummyPhysicsWorld")
    {
        logAndAssert(VE_SCOPE_NAME(setupScene(const Game &game, RenderView &view)),
                     "DummyPhysicsWorld is invalid physics world provider! "
                     "Please, validate Game/Physics options.",
                     Logger::LOG_COMPONENT_OTHER);
    }

    Ogre::SceneManager *sceneMgr = game.getRenderer()->getSceneManager();

    /// Shadows
//    sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);

    Ogre::Light *dirLight = sceneMgr->createLight("dirLight");
    dirLight->setType(Ogre::Light::LT_DIRECTIONAL);
    dirLight->setDiffuseColour(Ogre::ColourValue::White);
    dirLight->setSpecularColour(Ogre::ColourValue::White);

    dirLight->setDirection(Ogre::Vector3(Ogre::Vector3::NEGATIVE_UNIT_Y +
                                         Ogre::Vector3::UNIT_Z).normalisedCopy());

    Ogre::Light *pointLight = sceneMgr->createLight("pointLight");
    pointLight->setType(Ogre::Light::LT_POINT);
    pointLight->setDiffuseColour(Ogre::ColourValue::White);
    pointLight->setSpecularColour(Ogre::ColourValue::White);
    pointLight->setPosition(Ogre::Vector3(0, 25, 0));
//    pointLight->setAttenuation(1000.0f, 1.0, 0.045f, 0.0075f);


    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    Ogre::SceneNode *rootNode = Model::getRootSceneNode();    /** ------ Floor ------ */

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    getClassName().toString());

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "Sinbad.zip"),
                FileSystem::LOC_TYPE_ZIP,
                getClassName().toString());

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                getClassName().toString());

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "media"),
                FileSystem::LOC_TYPE_FS,
                getClassName().toString());

    std::string scriptPath = game.getSettingsValue("script");
    std::string baseName;
    if (!scriptPath.empty())
    {
        std::string dirName;
        FileSystem::splitFileName(scriptPath, dirName, baseName);
        FileSystem::addResourceLocation(dirName, FileSystem::LOC_TYPE_FS, getClassName().toString());
    }
    else
    {
        log("Setting \"script\" is not set. Script would not be loaded",
            Logger::LOG_LEVEL_WARNING,
            Logger::LOG_COMPONENT_SCRIPT);
    }

    FileSystem::initialiseResourceGroup(getClassName().toString());

    if (!scriptPath.empty())
    {
        log("Loading script \"" + baseName + "\"",
            Logger::LOG_LEVEL_INFO,
            Logger::LOG_COMPONENT_SCRIPT);

        const ScriptedGame &scriptedGame = static_cast<const ScriptedGame &>(game);
        Script *script = scriptedGame.createScript(baseName);
        if (script->compile())
        {
            log("Script \"" + baseName + "\" compiled",
                Logger::LOG_LEVEL_INFO,
                Logger::LOG_COMPONENT_SCRIPT);

            script->run();
        }
        delete script;
    }

    /** ------ Floor ------ */

    PlaneModelOptions opt;
    opt.modelName = "floor";
    opt.parentNode = rootNode;
    opt.plane = Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);
    opt.position = Ogre::Vector3::ZERO;
    opt.segments.x = 50;
    opt.segments.y = 50;
    opt.uTile = 10;
    opt.vTile = 10;
    opt.size = ve::SizeF(300, 300);
//    opt.restitution = 0.1f;
    opt.friction = 5.f;
    opt.physicsType = PhysicsModelOptions::PMT_Static;

    PlaneModel *floor = new PlaneModel(opt);
    Ogre::MaterialManager &matMgr = Ogre::MaterialManager::getSingleton();
    Ogre::MaterialPtr material = matMgr.getByName("floor");
    floor->setMaterial(material);  // WTF? Breaks Ui system

    mModels.insert(floor);

    /** ------ Box ------ **/

    BoxModelOptions boxOpt;
    boxOpt.modelName = "charssis";
    boxOpt.dimensions = Ogre::Vector3(2, 3, 7);
    boxOpt.parentNode = rootNode;
    boxOpt.mass = 100.0f;

    BoxModel *boxChassis = new BoxModel(boxOpt);
    boxChassis->setCastShadows(true);
    boxChassis->setPosition(Ogre::Vector3(10, 100, 10));

    Ogre::MaterialPtr boxMaterial = matMgr.getByName("chess_pattern");
    boxChassis->setMaterial(boxMaterial);

    mModels.insert(boxChassis);

    // scaled cylinder

    CylinderModelOptions cylinderScaledOpt;
    cylinderScaledOpt.modelName = "scaledCylinder";
    cylinderScaledOpt.dimensions = Ogre::Vector3(1.0f, 1.0f, 2.0f);
    cylinderScaledOpt.parentNode = rootNode;

    CylinderModel *scaledCylinderModel = new CylinderModel(cylinderScaledOpt);
    scaledCylinderModel->setMaterial(boxMaterial);
    scaledCylinderModel->setPosition(Ogre::Vector3(10, 100, 10));

    // sphere

    SphereModelOptions sphereOpt;
    sphereOpt.modelName = "sphere";
    sphereOpt.radius = 0.5f;
    sphereOpt.parentNode = rootNode;

    SphereModel *sphereModel = new SphereModel(sphereOpt);
    sphereModel->setMaterial(boxMaterial);

    mModels.insert(scaledCylinderModel);
    mModels.insert(sphereModel);

    // springboard 1

    BoxModelOptions obstacleOpt;
    obstacleOpt.modelName = "obstacle";
    obstacleOpt.physicsType = PlaneModelOptions::PMT_Static;
    obstacleOpt.dimensions = Ogre::Vector3(60, 60, 1);
    obstacleOpt.modelNode =
            rootNode->createChildSceneNode(Ogre::Vector3(10, 2, 0),
                                           Ogre::Quaternion(Ogre::Radian(Ogre::Degree(-85.0)),
                                                            Ogre::Vector3::UNIT_X));
    obstacleOpt.parentNode = rootNode;

    BoxModel *obstacle = new BoxModel(obstacleOpt);
    obstacle->setMaterial(boxMaterial);

    mModels.insert(obstacle);

    // springboard 2

    BoxModelOptions obstacleOpt2;
    obstacleOpt2.modelName = "obstacle2";
    obstacleOpt2.physicsType = PlaneModelOptions::PMT_Static;
    obstacleOpt2.dimensions = Ogre::Vector3(60, 60, 1);
    obstacleOpt2.modelNode =
            rootNode->createChildSceneNode(Ogre::Vector3(-30, 6, 0),
                                           Ogre::Quaternion(Ogre::Radian(Ogre::Degree(75)),
                                                            Ogre::Vector3::UNIT_X));
    obstacleOpt2.parentNode = rootNode;

    Ogre::MaterialPtr transMat = matMgr.getByName("ve_transparent");
    BoxModel *obstacle2 = new BoxModel(obstacleOpt2);
    obstacle2->setMaterial(transMat);
    obstacle2->setCastShadows(false);
    obstacle2->setPosition(Ogre::Vector3(20, 20, 20));

    mModels.insert(obstacle2);


    BoxModelOptions obstacleOpt3;
    obstacleOpt3.modelName = "obstacle3";
    obstacleOpt3.physicsType = PlaneModelOptions::PMT_Static;
    obstacleOpt3.dimensions = Ogre::Vector3(30, 1, 30);
    obstacleOpt3.modelNode = rootNode->createChildSceneNode(Ogre::Vector3(20, 0, 90));
    obstacleOpt3.parentNode = rootNode;

    BoxModel *obstacle3 = new BoxModel(obstacleOpt3);
    obstacle3->setMaterial(boxMaterial);

    mModels.insert(obstacle3);

    BoxModelOptions obstacleOpt4;
    obstacleOpt4.modelName = "obstacle4";
    obstacleOpt4.physicsType = PlaneModelOptions::PMT_Static;
    obstacleOpt4.dimensions = Ogre::Vector3(20, 2, 20);
    obstacleOpt4.modelNode = rootNode->createChildSceneNode(Ogre::Vector3(30, 0, 70));
    obstacleOpt4.parentNode = rootNode;

    BoxModel *obstacle4 = new BoxModel(obstacleOpt4);
    obstacle4->setMaterial(boxMaterial);

    mModels.insert(obstacle4);


    // Test blender2ogre exporter

    /*ModelOptions blendOpt;
    blendOpt.modelName = "blender2ogre";
    blendOpt.meshName = "Monkey.mesh";
    Model *blendModel = new Model(blendOpt);
    blendModel->setPosition({ 0, 10, 0 });

    mModels.insert(blendModel);*/

    /*blendOpt.modelName = "m4";
    blendOpt.meshName = "m4.mesh";
    blendModel = new Model(blendOpt);
    blendModel->setPosition({10, 10, 0});
    blendModel->setScale({0.2, 0.2, 0.2});
    blendModel->setMaterial(material);
    blendModel->setCastShadows(true);

    mModels.insert(blendModel);*/

    // box wall

    createBoxWall(game);

    // terrain

    ParamsMulti terrainParams;
    ConfFileParser(terrainParams, "terrain.cfg", FileSystem::RG_DEFAULT);

}

void PhysicsTest::setupVehicle(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    resetVehicle();

    FollowCamera followCamera = view.getCamera(0);
    followCamera.setFollowNode(mVehicle->_getModelNode());
    followCamera.setFollowOffset(Ogre::Vector3(0, 10, 10));
    mCameraControllers.insert({ Follow,
                                new FollowCameraController(
                                                getClassName().toString() +
                                                VE_CLASS_NAME(FollowCameraController).toString(),
                                                followCamera, 20.0f, 100.0f)
                              });

//    mView->getInputContoller()->addInputListener(followCameraController);

    FlyCamera flyCamera = view.getCamera(0);
    FlyCameraController *controller = new FlyCameraController(getClassName().toString() +
                                                              VE_CLASS_NAME(FlyCameraController).toString(),
                                                              flyCamera, 50.0f, Ogre::Math::HALF_PI * 0.083f * 0.1f, 0.9f);
    controller->setDampingEasingFunction(Easing::ExponentialInOut);
    mCameraControllers.insert({ Fly, controller });

    setViewType(Fly);
}

void PhysicsTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(getClassName().toString() + "Camera");

    view.setViewport({ camera, ViewportPosition::FULLSCREEN })->setBackgroundColour(Ogre::ColourValue::Black);

    camera.setPosition(Ogre::Vector3(20, 20, 40));
    camera.setLookAt(Ogre::Vector3(0, 0, 20));
}

void PhysicsTest::setupConsole(const ScriptedGame &game, RenderView &view)
{
    VE_UNUSED(game);

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                getClassName().toString());

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    getClassName().toString());

    FileSystem::initialiseResourceGroup(getClassName().toString());

    Ogre::Viewport *vp = view.getViewportByZOrder(0);

    auto atlas = ui()->loadAtlas("dejavu.gorilla");
    auto screen = ui()->createScreen(vp, game.getRenderer()->getSceneManager(), atlas);

    UiFrame *topLevelFrame = new UiFrame("topLevelFrame", screen);
    topLevelFrame->setBackgroundColor(Ogre::ColourValue::Green);

    UiHorizontalLayout *topLevelLayout = new UiHorizontalLayout;
    topLevelLayout->setSpacing(5);

    UiFrame *testFrame1 = new UiFrame(topLevelFrame);
    testFrame1->setBackgroundColor(Ogre::ColourValue::Blue);

    mFPSLabel = new FPSWidget(testFrame1, mView->getViewportByIndex(0)->getTarget());
    mFPSLabel->setTextFontSize(24);
    mFPSLabel->setTextColor(Ogre::ColourValue::White);
    mFPSLabel->setTextAlignment(UA_BottomLeft);

//    UiHorizontalLayout *layout = new UiHorizontalLayout;
//    testFrame1->setLayout(layout);
//    layout->addFrame(textLabel1);


    topLevelLayout->setSpacing(5);
    topLevelLayout->addFrame(mFPSLabel);


    topLevelFrame->setLayout(topLevelLayout);

    topLevelFrame->setVisible(true);
}

void PhysicsTest::resetVehicle()
{
    Ogre::SceneNode *rootNode = renderer()->getSceneManager()->getRootSceneNode();

    if (!mVehicle)
    {
        const float gWheelRadius = 0.5f;
        const float gWheelWidth = 0.4f;

        const float gConnectionHeight = -0.5f;

        /// ------------------------------------------------

        VehicleModelOptions vehicleOpt;

        vehicleOpt.modelName = "test";
        vehicleOpt.meshName = "chassis.mesh";
        vehicleOpt.parentNode = rootNode;
        vehicleOpt.mass = 800.0f;
        vehicleOpt.maxEngineForce = 12000.0f;
        vehicleOpt.chassisShift.y = -0.1f;
        vehicleOpt.friction = 0.1f;

        Ogre::Vector3 connectionPoints[4];

        connectionPoints[0].x = 1.0f - (0.3 * gWheelWidth);
        connectionPoints[0].y = gConnectionHeight,
        connectionPoints[0].z = 2.0f - gWheelRadius;

        connectionPoints[1].x = -1.0f + (0.3 * gWheelWidth);
        connectionPoints[1].y = gConnectionHeight;
        connectionPoints[1].z = 2.0f - gWheelRadius;

        connectionPoints[2].x = -1.0f + (0.3 * gWheelWidth),
        connectionPoints[2].y = gConnectionHeight,
        connectionPoints[2].z = -2.0f + gWheelRadius;

        connectionPoints[3].x = 1.0f - (0.3 * gWheelWidth);
        connectionPoints[3].y = gConnectionHeight;
        connectionPoints[3].z = -2.0f + gWheelRadius;

        for (int i = 0; i < 4; ++i)
        {
            VehicleWheelModelOptions wheelOpt;
            wheelOpt.modelName = "test_" + StringConverter::toString(i);
            wheelOpt.resourceGroup = getClassName().toString();
            wheelOpt.meshName = "wheel.mesh";
            wheelOpt.parentNode = rootNode;
            wheelOpt.orientation = (VehicleWheelModelOptions::Orientation)i;
            wheelOpt.rollInfluence = 0.1f;
            wheelOpt.friction = 1000;
            wheelOpt.suspensionRestLength = 0.65f;
            wheelOpt.connectionPoint = connectionPoints[i];

            vehicleOpt.wheelOpts.push_back(wheelOpt);
        }

        VehicleModel *vehicleModel = new VehicleModel(vehicleOpt);
        vehicleModel->getRigidBody()->setAutoDeactivation(false);
        mVehicle = vehicleModel;

        auto *vehicleController = new VehicleListener(
                    getClassName().toString() +
                    VE_CLASS_NAME(VehicleListener).toString(),
                    vehicleModel->getRayCastVehicle(),
                    900.0f, Ogre::Math::HALF_PI * 0.3, this);

        mView->getInputContoller()->addInputListener(vehicleController);

        mVehicle->setPosition(Ogre::Vector3(0, 20, 40));
        mVehicle->setOrientation(Ogre::Quaternion::IDENTITY);
    }
    else
    {
        Ogre::Quaternion orient = mVehicle->getOrientation();
        orient.x = 0.0f;
        orient.z = 0.0f;
        orient.normalise();
        mVehicle->setOrientation(orient);
    }
}

void PhysicsTest::setViewType(ViewType viewType)
{
    auto it = mCameraControllers.find(viewType);
    if (it != mCameraControllers.end())
    {
        AbstractInputListener *listener = it->second;
        if (listener && listener != mCurrentCameraController)
        {
            InputController *input = mView->getInputContoller();
            input->removeInputListener(mCurrentCameraController);
            input->addInputListener(listener);
            mCurrentCameraController = listener;
        }
    }
}

PhysicsTest::ViewType PhysicsTest::getViewType() const
{
    const auto end = mCameraControllers.end();
    for (auto it = mCameraControllers.begin(); it != end; ++it)
    {
        if (mCurrentCameraController == it->second)
        {
            return it->first;
        }
    }
    return Fly;
}

void PhysicsTest::setVehicleDebugEnabled(bool enabled)
{
    if (mVehicle)
    {
        mVehicle->getRigidBody()->setDebugDrawEnabled(enabled);
    }
}

bool PhysicsTest::isVehicleDebugEnabled() const
{
    if (mVehicle)
    {
        return mVehicle->getRigidBody()->isDebugDrawEnabled();
    }
    return false;
}

void PhysicsTest::setShapesDebugEnabled(bool enabled)
{
    for (auto &pair : mModels)
    {
        Model *model = pair.second;
        if (model->getClassHash() & PhysicsModel::PhysicsModelHash)
        {
            PhysicsModel *phyModel = static_cast<PhysicsModel *>(model);
            phyModel->getRigidBody()->setDebugDrawEnabled(enabled);
        }
    }
    mShapesDebugEnabled = enabled;
}

bool PhysicsTest::isShapesDebugEnabled() const
{
    return mShapesDebugEnabled;
}

void PhysicsTest::setConsoleVisiable(bool visiable)
{
    /*
    if (mConsole)
    {
        mConsole->setVisible(visiable);
    }
    */
}

bool PhysicsTest::isConsoleVisiable() const
{
    /*
    if (mConsole)
    {
        return mConsole->isVisible();
    }
    */
    return false;
}

void PhysicsTest::createBoxWall(const Game &game)
{
    Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().getByName("chess_pattern");

    const Ogre::Vector3 basePos(0, 0, 50);

    for (int i = 0; i < 3/*0*/; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            for (int k = 0; k < 3; ++k)
            {
                BoxModelOptions boxOpt;
                boxOpt.modelName = "wallBox" + StringConverter::toString(i * 100 + j * 10 + k);
                boxOpt.dimensions = Ogre::Vector3(1, 1, 1);
                boxOpt.mass = 10.0f;
                boxOpt.parentNode = game.getRenderer()->getSceneManager()->getRootSceneNode();

                BoxModel *box = new BoxModel(boxOpt);
                box->setMaterial(mat);

                box->setPosition(basePos + Ogre::Vector3(i, j, k));

                mModels.insert(box);
            }
        }
    }
}

/// ====================================================================================================

#include <script/lua/luascriptfactory.h>
#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
//#include <framework/frontend/ogreviewbackend/ogrerenderviewbackend.h>

VE_TEST_FACTORY(PhysicsTest, SDLRenderViewBackend, LuaScriptFactory);

int main(int argc, char *argv[])
{
    PhysicsTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
