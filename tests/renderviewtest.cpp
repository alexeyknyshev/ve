#include "renderviewtest.h"

#include <framework/frontend/renderview.h>
#include <framework/frontend/renderviewlistener.h>
#include <renderer/renderer.h>

RenderViewTest::RenderViewTest(const Game &game, RenderView &view)
    : Test(game, view)
{
    testSplitScreen(4, view, game.getRenderer());
}

void RenderViewTest::testSplitScreen(uint16_t viewportsCount,
                                     RenderView &view,
                                     Renderer *renderer)
{
    ViewportOptionsMap viewports;

    if (viewportsCount <= 1)
    {
        viewports.insert( { 0, { renderer->createCamera("cam0"), ViewportPosition::FULLSCREEN } } );

    }
    else
    if (viewportsCount == 2)
    {

        ViewportOptions vOpt0(renderer->createCamera("cam0"), ViewportPosition(.0f, .0f, .5f, 1.0f));
        ViewportOptions vOpt1(renderer->createCamera("cam1"), ViewportPosition(.5f, .0f, .5f, 1.0f));

        viewports.insert(std::make_pair(0, vOpt0));
        viewports.insert(std::make_pair(1, vOpt1));

    }
    else
    if (viewportsCount == 3)
    {

        ViewportOptions vOpt0(renderer->createCamera("cam0"), ViewportPosition(.0f, .0f, .5f, 1.0f));
        ViewportOptions vOpt1(renderer->createCamera("cam1"), ViewportPosition(.5f, .0f, .5f, .5f));
        ViewportOptions vOpt2(renderer->createCamera("cam2"), ViewportPosition(.5f, .5f, .5f, .5f));

        viewports.insert(std::make_pair(0, vOpt0));
        viewports.insert(std::make_pair(1, vOpt1));
        viewports.insert(std::make_pair(2, vOpt2));

    }
    else
    if (viewportsCount >= 4)
    {

        ViewportOptions vOpt0(renderer->createCamera("cam0"), ViewportPosition(.0f, .0f, .5f, .5f));
        ViewportOptions vOpt1(renderer->createCamera("cam1"), ViewportPosition(.0f, .5f, .5f, .5f));
        ViewportOptions vOpt2(renderer->createCamera("cam2"), ViewportPosition(.5f, .0f, .5f, .5f));
        ViewportOptions vOpt3(renderer->createCamera("cam3"), ViewportPosition(.5f, .5f, .5f, .5f));

        viewports.insert(std::make_pair(0, vOpt0));
        viewports.insert(std::make_pair(1, vOpt1));
        viewports.insert(std::make_pair(2, vOpt2));
        viewports.insert(std::make_pair(3, vOpt3));
    }

    view.setViewports(viewports);

    ViewportList list = view.getViewports();
    int i = 0;
    for (auto it = list.begin(); it != list.end(); ++it, ++i)
    {
        Ogre::Viewport *viewport = *it;
        if (VE_CLASS_CHECK_NULL_PTR(RenderViewTest::testSplitScreen,
                                    Ogre::Viewport,
                                    viewport,
                                    Logger::LOG_COMPONENT_RENDERER))
        {
            float val = 0.8f / (i + 1);
            Ogre::ColourValue color(val, val, val);
            viewport->setBackgroundColour(color);
        }
    }
}

void RenderViewTest::setupCamera(const Game &game, RenderView &view) { VE_UNUSED(game); VE_UNUSED(view); }
void RenderViewTest::setupControls(const Game &game, RenderView &view) { VE_UNUSED(game); VE_UNUSED(view); }
void RenderViewTest::setupScene(const Game &game, RenderView &view) { VE_UNUSED(game); VE_UNUSED(view); }
