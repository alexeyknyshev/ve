#ifndef TERRAINTEST_H
#define TERRAINTEST_H

#include <tests/test.h>
#include <renderer/model.h>

class TerrainTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(TerrainTest)

    TerrainTest(Game &game, RenderView &view);
    ~TerrainTest();

    bool event(const Event *event)
    {
        VE_NOT_IMPLEMENTED(TerrainTest::event, Logger::LOG_COMPONENT_EVENTLOOP);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "terrainTest";
        return Name;
    }

protected:
    void setupScene(const Game &game, RenderView &view);
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);

    ModelSet mModels;
};

#endif // TERRAINTEST_H
