#ifndef PHYSICSTEST2_H
#define PHYSICSTEST2_H

#include <tests/test.h>

#include <renderer/model.h>

class SphereModel;
class AbstractInputListener;

class PhysicsTest2 : public Test
{
    friend class PhysicsTest2InputListener;

public:
    VE_DECLARE_TYPE_INFO(PhysicsTest2)

    PhysicsTest2(const ScriptedGame &game, RenderView &view);
    ~PhysicsTest2();

    bool event(const Event *event)
    {
        VE_NOT_IMPLEMENTED(PhysicsTest::event, Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "physicsTest2";
        return Name;
    }

    enum ViewType
    {
        Follow = 0,
        Fly
    };

    void setViewType(ViewType viewType);
    ViewType getViewType() const;

protected:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);
    void setupScene(const Game &game, RenderView &view);

private:
    RenderView *mView;
    ModelSet mModels;
    SphereModel *mSphere;

    std::map<ViewType, AbstractInputListener *> mCameraControllers;
    AbstractInputListener *mCurrentCameraController;
};

#endif // PHYSICSTEST2_H
