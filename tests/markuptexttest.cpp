#include "markuptexttest.h"

#include <game/scriptedgame.h>
#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/viewportposition.h>
#include <framework/ui/uimarkuptextedit.h>
#include <framework/ui/layouts/uidefaultlayout.h>
#include <framework/ui/uimanager.h>
#include <framework/controller/inputcontroller.h>
#include <framework/controller/ois/oisinputbackend.h>

#include <tests/common/exitkeylistener.h>

MarkupTextTest::MarkupTextTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view),
      mTextEdit(nullptr)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupResources();
    setupScene(game, view);
}

MarkupTextTest::~MarkupTextTest()
{
    delete mTextEdit;
    mTextEdit = nullptr;
}

void MarkupTextTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(PhysicsTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, { camera, ViewportPosition::FULLSCREEN } }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(0, 0, 10));
    camera.setLookAt(Ogre::Vector3::ZERO);
}

void MarkupTextTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    input->addInputListener(new ExitKeyListener(VE_AS_STR(PhysicsTest) + VE_AS_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    view.setInputController(input);
}

void MarkupTextTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    Ogre::Viewport *vp = view.getViewportByZOrder(0);

    auto atlas = ui()->loadAtlas("dejavu.gorilla");
    auto screen = ui()->createScreen(vp, atlas);

    UiFrame *topLevelFrame = new UiFrame("topLevelFrame", screen);
    topLevelFrame->setBackgroundColor(Ogre::ColourValue::Green + Ogre::ColourValue::Red);

    mTextEdit = new UiMarkupTextEdit(VE_CLASS_NAME_STR(MarkupTextTest) + "Edit", topLevelFrame);
    mTextEdit->setVisible(true);
    mTextEdit->setText("Hello world!\nHow are u?");
    mTextEdit->setBackgroundColor(Ogre::ColourValue::White);
    mTextEdit->setTextColor(Ogre::ColourValue::Green);

    mLayout = new UiDefaultLayout;
    mLayout->addFrame(mTextEdit);
    mLayout->setSpacing(10);
    topLevelFrame->setLayout(mLayout);
}

void MarkupTextTest::setupResources()
{
    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    VE_CLASS_NAME_STR(MarkupTextTest));

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                VE_CLASS_NAME_STR(MarkupTextTest));
}
