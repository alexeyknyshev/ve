#include "uidebugtest.h"

#include <game/scriptedgame.h>
#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/viewportposition.h>
#include <framework/ui/uimarkuptextedit.h>
#include <framework/ui/uitextlabel.h>
#include <framework/ui/layouts/uiverticallayout.h>
#include <framework/ui/uimanager.h>
#include <framework/controller/inputcontroller.h>
#include <framework/controller/ois/oisinputbackend.h>

#include <tests/common/exitkeylistener.h>

class UiDebugTestInputListener : public DefaultInputListener
{
public:
    UiDebugTestInputListener(const std::string &name, UiDebugTest *test, ve::KeyData::KeyButton showDebugKey)
        : DefaultInputListener(name),
          mTest(test),
          mShowDebugKey(showDebugKey)
    { }

    bool keyPressed(const KeyboardEvent &event)
    {
        if (event.data.key == mShowDebugKey)
        {
            mTest->setUiDebugEnabled(!mTest->isUiDebugEnabled());
        }

        return DefaultInputListener::keyPressed(event);
    }

    bool event(const Event *event)
    {
        VE_UNUSED(event);
        VE_NOT_IMPLEMENTED(UiDebugTestInputListener::event, Logger::LOG_COMPONENT_OTHER);
        return false;
    }

protected:
    void registredAsInputListener(bool registred, InputController *controller)
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

private:
    UiDebugTest * const mTest;
    const ve::KeyData::KeyButton mShowDebugKey;
};

UiDebugTest::UiDebugTest(const ScriptedGame &game, RenderView &view)
    : Test(game, view),
      mTopLevelFrame(nullptr)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupResources();
    setupScene(game, view);
}

UiDebugTest::~UiDebugTest()
{
    delete mTopLevelFrame;
}

void UiDebugTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(
                VE_CLASS_NAME_STR(UiDebugTest) + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, { camera, ViewportPosition::FULLSCREEN } }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue(.8f, .8f, .8f));

    camera.setPosition(Ogre::Vector3(0, 0, 10));
    camera.setLookAt(Ogre::Vector3::ZERO);
}

void UiDebugTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    input->addInputListener(new ExitKeyListener(VE_CLASS_NAME_STR(UiDebugTest) +
                                                VE_CLASS_NAME_STR(ExitKeyListener),
                                                ve::KeyData::KEY_ESCAPE));

    input->addInputListener(new UiDebugTestInputListener(VE_CLASS_NAME_STR(UiDebugTestInputListener),
                                                         this, ve::KeyData::KEY_D));

    view.setInputController(input);
}

void UiDebugTest::setupResources()
{
    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    VE_CLASS_NAME_STR(UiDebugTest));

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                VE_CLASS_NAME_STR(UiDebugTest));
}

void UiDebugTest::setupScene(const Game &game, RenderView &view)
{
    Ogre::Viewport *vp = view.getViewportByZOrder(0);

    ui()->loadAtlas("dejavu.gorilla");
    Gorilla::Screen *screen = ui()->createScreen(vp, "dejavu.gorilla");

    mTopLevelFrame = new UiFrame(VE_CLASS_NAME_STR(UiDebugTest) + "topLevelFrame", screen);
    mTopLevelFrame->setDebugBackgroundColor(Ogre::ColourValue::Red);

    UiMarkupTextEdit *edit = new UiMarkupTextEdit(VE_CLASS_NAME_STR(UiDebugTest) + "Edit", mTopLevelFrame);
    edit->setText("Hello world!\nHow are u?");
    edit->setDebugBackgroundColor(Ogre::ColourValue::Green);
    edit->setTextColor(Ogre::ColourValue::Black);
    edit->setTextFontSize(14);

    UiTextLabel *label = new UiTextLabel(VE_CLASS_NAME_STR(UiDebugTest) + "Label", mTopLevelFrame);
    label->setText("Label!");
    label->setDebugBackgroundColor(Ogre::ColourValue::Blue);
    label->setTextColor(Ogre::ColourValue::Blue);

    UiVerticalLayout *layout = new UiVerticalLayout;
    layout->addFrame(edit);
    layout->addFrame(label);
    layout->setSpacing(10);
    mTopLevelFrame->setLayout(layout);
}

/// ------------------------------- ///

void UiDebugTest::setUiDebugEnabled(bool enabled)
{
    mTopLevelFrame->setDebugEnabled(enabled);
}

bool UiDebugTest::isUiDebugEnabled() const
{
    return mTopLevelFrame->isDebugEnabled();
}
