#include "gorillatest.h"

#include <renderer/renderer.h>
#include <renderer/basecamera.h>
#include <renderer/models/shapes/plane/planemodel.h>
#include <renderer/models/shapes/plane/planemodeloptions.h>
#include <framework/frontend/renderview.h>
#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
#include <framework/frontend/viewportposition.h>
#include <framework/frontend/misc/exitkeylistener.h>
#include <framework/filesystem.h>
#include <framework/controller/sdl/sdlinputbackend.h>
#include <framework/controller/inputcontroller.h>
#include <framework/ui/uimanager.h>
#include <game/scriptedgame.h>
#include <renderer/cameras/flycameracontroller.h>

class GorillaTest;

class GorillaTestInputListener : public DefaultInputListener
{
public:
    GorillaTestInputListener(const std::string &name,
                             const BaseCamera &cam,
                             GorillaTest *test)
            : DefaultInputListener(name),
              mTest(test),
              mCamera(cam)
    { }

private:
    GorillaTest *mTest;
    BaseCamera mCamera;
};

GorillaTest::GorillaTest(const Game &game, RenderView &view)
    : Test(game, view)
{
    setupCamera(game, view);
    setupUi(game);
    setupControls(game, view);
}

GorillaTest::~GorillaTest()
{

}

void GorillaTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(getClassName().toString() + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewpots =
    {
        { zOrder, ViewportOptions(camera, ViewportPosition::FULLSCREEN) }
    };

    view.setViewports(viewpots);
    view.getViewportByZOrder(0)->setBackgroundColour(Ogre::ColourValue(.7f, .7f, .7f));

    camera.setPosition(Ogre::Vector3(6, 6, 15));
    camera.setLookAt(Ogre::Vector3(0, 0, 00));
}

void GorillaTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

    InputController *input = new InputController("inputController", new SDLInputBackendFactory);

    input->addInputListener(new ExitKeyListener(getClassName().toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

//    input->addInputListener(new GorillaTestInputListener(VE_CLASS_NAME_STR(GorillaTestInputListener),
//                                                         camera, this));

    input->addInputListener(new FlyCameraController("flyCamController", view.getCamera(0), 50.0f, Ogre::Math::HALF_PI * 0.0083f, 0.9f));

    view.setInputController(input);
}

void GorillaTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(view);

    Ogre::SceneManager *sceneMgr = game.getRenderer()->getSceneManager();
    auto rootNode = sceneMgr->getRootSceneNode();

    /// Shadows
    sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);

    Ogre::Light *dirLight = sceneMgr->createLight("dirLight");
    dirLight->setType(Ogre::Light::LT_DIRECTIONAL);
    dirLight->setDiffuseColour(Ogre::ColourValue::White);
    dirLight->setSpecularColour(Ogre::ColourValue::White);

    dirLight->setDirection(Ogre::Vector3(Ogre::Vector3::NEGATIVE_UNIT_Y +
                                         Ogre::Vector3::UNIT_Z).normalisedCopy());

    Ogre::Light *pointLight = sceneMgr->createLight("pointLight");
    pointLight->setType(Ogre::Light::LT_POINT);
    pointLight->setDiffuseColour(Ogre::ColourValue::White);
    pointLight->setSpecularColour(Ogre::ColourValue::White);
    pointLight->setPosition(Ogre::Vector3(0, 25, 0));

    PlaneModelOptions opt;
    opt.modelName = "floor";
    opt.parentNode = rootNode;
    opt.plane = Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);
    opt.position = Ogre::Vector3::ZERO;
    opt.segments.x = 50;
    opt.segments.y = 50;
    opt.uTile = 10;
    opt.vTile = 10;
    opt.size = ve::SizeF(300, 300);
//    opt.restitution = 0.1f;
    opt.friction = 1.8f;
    opt.physicsType = PhysicsModelOptions::PMT_Static;

    PlaneModel *floor = new PlaneModel(opt);
    Ogre::MaterialManager &matMgr = Ogre::MaterialManager::getSingleton();
    Ogre::MaterialPtr material = matMgr.getByName("floor");
    floor->setMaterial(material);  // WTF? Breaks Ui system

    mModels.insert(floor);
}

void GorillaTest::setupUi(const Game &game)
{
    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                getClassName().toString());

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    getClassName().toString());

    FileSystem::initialiseResourceGroup(getClassName().toString());

    auto atlas = ui()->loadAtlas("dejavu.gorilla");

    auto root = game.getRenderer()->getSceneManager()->getRootSceneNode();
    auto node = root->createChildSceneNode();

    auto renderable = ui()->createScreenRenderable(Ogre::Vector2(4, 1), atlas);

    node->attachObject(renderable);

    auto layer = renderable->createLayer(0);
    auto background = layer->createRectangle(0, 0, 500, 100);
//    background->background_gradient(Gorilla::Gradient_NorthSouth, Gorilla::rgb(94,97,255,155), Gorilla::rgb(94,97,255,155));
    background->background_colour(Ogre::ColourValue(1,1,1,0));

    auto layer1 = layer->createSubLayer();
    auto caption = layer1->createCaption(24, 20, 20, "Test UITEXT");
    caption->colour(Ogre::ColourValue::Red);
}

/// ====================================================================================================

VE_TEST_FACTORY(GorillaTest, SDLRenderViewBackend);

int main(int argc, char *argv[])
{
    GorillaTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
