#ifndef GORILLATEST_H
#define GORILLATEST_H

#include <tests/test.h>

class GorillaTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(GorillaTest)

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    const std::string &getName() const override
    {
        static const std::string Name = "gorillaTest";
        return Name;
    }

    GorillaTest(const Game &game, RenderView &view);
    ~GorillaTest();

private:
    void setupCamera(const Game &game, RenderView &view) override;
    void setupControls(const Game &game, RenderView &view) override;
    void setupScene(const Game &game, RenderView &view) override;
    void setupUi(const Game &game);
};

#endif // GORILLATEST_H
