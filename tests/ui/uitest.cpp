#include "uitest.h"

#include <game/scriptedgame.h>

#include <renderer/basecamera.h>
#include <renderer/renderer.h>

#include <framework/frontend/viewportposition.h>
#include <framework/frontend/renderview.h>
#include <framework/controller/sdl/sdlinputbackend.h>
#include <framework/controller/inputcontroller.h>

#include <framework/frontend/misc/exitkeylistener.h>

#include <framework/ui/uitextlabel.h>
#include <framework/ui/layouts/uilayoutspacer.h>
#include <framework/ui/layouts/uiverticallayout.h>
#include <framework/ui/layouts/uihorizontallayout.h>
#include <framework/ui/uimanager.h>

#include <framework/rect.h>

#include <time.h>

class UiFrameColorFlicker : public UiFrame
{
public:
    UiFrameColorFlicker(UiFrame *parent)
        : UiFrame(parent)
    {
    }

    bool mousePressed(const MouseEvent &e)
    {
        log("Mouse pressed!", Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_UI);

        Ogre::ColourValue color;
        color.r = Ogre::Math::UnitRandom();
        color.g = Ogre::Math::UnitRandom();
        color.b = Ogre::Math::UnitRandom();

//        mPrevColor = getBackgroundColor();
        setBackgroundColor(color);
        return UiFrame::mousePressed(e);
    }

    bool mouseReleased(const MouseEvent &e)
    {
        log("Mouse released!", Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_UI);
        return UiFrame::mouseReleased(e);
    }

private:
//    Ogre::ColourValue mPrevColor;
};

UiTest::UiTest(const Game &game, RenderView &view)
    : Test(game, view),
      mTopLevelFrame(nullptr)
{
    setupCamera(game, view);
    setupUi(game, view);
    setupControls(game, view);
}

UiTest::~UiTest()
{
    delete mTopLevelFrame;
}

void UiTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera(getClassName().toString() + "Camera");

    const int zOrder = 0;

    ViewportOptionsMap viewports =
    {
        { zOrder, ViewportOptions(camera, ViewportPosition::FULLSCREEN) }
    };

    view.setViewports(viewports);
    view.getViewportByZOrder(zOrder)->setBackgroundColour(Ogre::ColourValue::Black);

    camera.setPosition({ 20, 20, 40 });
    camera.setLookAt({ 0, 0, 20 });
}

void UiTest::setupControls(const Game &, RenderView &view)
{
    InputController *input = new InputController("inputController", new SDLInputBackendFactory);

    input->addInputListener(new ExitKeyListener(getClassName().toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

    view.setInputController(input);
}

void UiTest::setupScene(const Game &game, RenderView &view)
{
    VE_UNUSED(game);
    VE_UNUSED(view);
}

void UiTest::setupUi(const Game &game, const RenderView &view)
{
    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS,
                getClassName().toString());

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS,
                                    getClassName().toString());

    FileSystem::initialiseResourceGroup(getClassName().toString());

    Ogre::Viewport *viewport = view.getViewportByZOrder(0);

    auto atlas = ui()->loadAtlas("dejavu.gorilla");
    auto screen = ui()->createScreen(viewport, game.getRenderer()->getSceneManager(), atlas);

    mTopLevelFrame = new UiFrame("topLevelFrame", screen);
    mTopLevelFrame->setBackgroundColor(Ogre::ColourValue::Red);

#if 1
    UiFrame *llLvlFrame1 = new UiFrameColorFlicker(mTopLevelFrame);
    UiFrame *llLvlFrame2 = new UiFrame("llLvlFrame2", mTopLevelFrame);
    UiFrame *llLvlFrame3 = new UiFrame("llLvlFrame3", mTopLevelFrame);

    llLvlFrame1->setBackgroundColor(Ogre::ColourValue::Black);
    llLvlFrame2->setBackgroundColor(Ogre::ColourValue::Red +
                                    Ogre::ColourValue::Green);
    llLvlFrame3->setBackgroundColor(Ogre::ColourValue::White);

    UiHorizontalLayout *llLvlLayout = new UiHorizontalLayout;
    llLvlLayout->addFrame(llLvlFrame1);
    llLvlLayout->addFrame(llLvlFrame2);
    llLvlLayout->addFrame(llLvlFrame3);

    UiFrame *ll3Child1 = new UiFrameColorFlicker(llLvlFrame3);
    UiFrame *ll3Child2 = new UiFrameColorFlicker(llLvlFrame3);
    UiFrame *ll3Child3 = new UiFrameColorFlicker(llLvlFrame3);

    ll3Child1->setBackgroundColor(Ogre::ColourValue::Blue + Ogre::ColourValue::Green);
    ll3Child2->setBackgroundColor(Ogre::ColourValue::Green);
    ll3Child3->setBackgroundColor(Ogre::ColourValue(0.3f, 0.3f, 0.3f));

    UiVerticalLayout *vLayout = new UiVerticalLayout;
    vLayout->setSpacing(20);
    vLayout->addFrame(ll3Child1, -1, 2);
    vLayout->addFrame(ll3Child2);
    vLayout->addFrame(ll3Child3);


    llLvlFrame3->setLayout(vLayout);

    llLvlLayout->setSpacing(5);
    mTopLevelFrame->setLayout(llLvlLayout);
#else
    UiHorizontalLayout *topLevelLayout = new UiHorizontalLayout;
    UiFrame *tmpFrame = new UiFrame(mTopLevelFrame);
    tmpFrame->setBackgroundColor(Ogre::ColourValue::Black);
    topLevelLayout->setSpacing(40);
    topLevelLayout->addFrame(tmpFrame);

    mTopLevelFrame->setLayout(topLevelLayout);
#endif
    mTopLevelFrame->setVisible(true);
}

/// ====================================================================================================

#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>

VE_TEST_FACTORY(UiTest, SDLRenderViewBackend);

int main(int argc, char *argv[])
{
    UiTestFactory factory;
    Test::runTest(argc, argv, factory);
}
