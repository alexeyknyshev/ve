#ifndef UITEST_H
#define UITEST_H

#include <tests/test.h>

class UiFrame;

class UiTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(UiTest)

    bool event(const Event *event)
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "uiTest";
        return Name;
    }

    UiTest(const Game &game, RenderView &view);
    ~UiTest();

private:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &, RenderView &view);
    void setupScene(const Game &game, RenderView &view);
    void setupUi(const Game &game, const RenderView &view);

    UiFrame *mTopLevelFrame;
};

#endif // UITEST_H
