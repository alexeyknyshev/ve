#ifndef UIDEBUGTEST_H
#define UIDEBUGTEST_H

#include <tests/test.h>

class UiFrame;

class UiDebugTestInputListener;

class UiDebugTest : public Test
{
    friend class UiDebugTestInputListener;

public:
    VE_DECLARE_TYPE_INFO(UiDebugTest)

    UiDebugTest(const ScriptedGame &game, RenderView &view);
    virtual ~UiDebugTest();

    bool event(const Event *event)
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_EVENTLOOP);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "uiDebugTest";
        return Name;
    }

    void setUiDebugEnabled(bool enabled);
    bool isUiDebugEnabled() const;

private:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &, RenderView &view);
    void setupResources();
    void setupScene(const Game &game, RenderView &view);

    UiFrame *mTopLevelFrame;
};

#endif // UIDEBUGTEST_H
