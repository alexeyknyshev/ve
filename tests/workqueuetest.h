#ifndef WORKQUEUETEST_H
#define WORKQUEUETEST_H

#include <tests/test.h>

class WorkQueueReqHandler;

class WorkQueueTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(WorkQueueTest)

    WorkQueueTest(Game &game, RenderView &view);
    ~WorkQueueTest();

    bool event(const Event *event)
    {
        VE_NOT_IMPLEMENTED(TerrainTest::event, Logger::LOG_COMPONENT_EVENTLOOP);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "terrainTest";
        return Name;
    }

protected:
    void setupScene(const Game &game, RenderView &view);
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);

private:
    WorkQueueReqHandler *mHandler;
};

#endif // WORKQUEUETEST_H
