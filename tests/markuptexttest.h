#ifndef MARKUPTEXTTEST_H
#define MARKUPTEXTTEST_H

#include <tests/test.h>

class UiMarkupTextEdit;
class UiDefaultLayout;

class MarkupTextTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(MarkupTextTest)

    MarkupTextTest(const ScriptedGame &game, RenderView &view);
    ~MarkupTextTest();

    bool event(const Event *event)
    {
        VE_UNUSED(event);
        VE_NOT_IMPLEMENTED(MarkupTextTest::event, Logger::LOG_COMPONENT_OTHER);
        return false;
    }

    const std::string &getName() const
    {
        static const std::string Name = "markupTextTest";
        return Name;
    }

private:
    void setupCamera(const Game &game, RenderView &view);
    void setupControls(const Game &game, RenderView &view);
    void setupResources();
    void setupScene(const Game &game, RenderView &view);

    UiMarkupTextEdit *mTextEdit;
    UiDefaultLayout *mLayout;
};

#endif // MARKUPTEXTTEST_H
