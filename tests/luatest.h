#ifndef LUATEST_H
#define LUATEST_H

#include <tests/test.h>

class LuaTest : public Test
{
public:
    VE_DECLARE_TYPE_INFO(LuaTest)

    LuaTest(const ScriptedGame &game, RenderView &view);

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        VE_UNUSED(event);
        return false;
    }

    const std::string &getName() const override
    {
        const static std::string Name = "LuaTest";
        return Name;
    }

    void setupCamera(const Game &, RenderView &) override { }
    void setupControls(const Game &, RenderView &) override { }
    void setupScene(const Game &, RenderView &) override { }
    void setupResources();
};

#endif // LUATEST_H
