#include "animationtest.h"

#include <game/scriptedgame.h>

#include <framework/frontend/renderview.h>
//#include <framework/frontend/sdlviewbackend/sdlrenderviewbackend.h>
#include <framework/frontend/ogreviewbackend/ogrerenderviewbackend.h>

#include <framework/filesystem.h>
#include <framework/controller/inputcontroller.h>
//#include <framework/controller/sdl/sdlinputbackend.h>
#include <framework/controller/ois/oisinputbackend.h>

#include <framework/frontend/misc/exitkeylistener.h>

#include <renderer/basecamera.h>
#include <renderer/renderer.h>

#include <renderer/cameras/flycameracontroller.h>

#include <OGRE/OgreViewport.h>
#include <OGRE/OgreMeshManager.h>

#include <renderer/models/shapes/plane/planemodeloptions.h>
#include <renderer/models/shapes/plane/planemodel.h>

#if !(OGRE_VERSION_MAJOR >= 1 && OGRE_VERSION_MINOR >= 8) /* Ogre older then 1.8.0 */
#include <OGRE/OgreMaterialManager.h>
#endif

class AnimationTestInputListener : public DefaultInputListener
{
public:
    AnimationTestInputListener(const std::string &name,
                               const BaseCamera &camera,
                               AnimationTest *test)
        : DefaultInputListener(name),
          mCamera(camera),
          mTest(test)
    { }

    void update(float deltaTime)
    {
        VE_UNUSED(deltaTime);
    }

    bool event(const Event *event)
    {
        VE_UNUSED(event);
        return false;
    }

    bool keyPressed(const KeyboardEvent &event)
    {
        if (event.data.key == ve::KeyData::KEY_R)
        {
            Model *sinbad = mTest->getModels().findByName("Sinbad");
            if (!sinbad)
            {
                logAndAssert(VE_SCOPE_NAME(keyPressed(const KeyboardEvent &event)),
                             "Model named \"Sinbad\" does not exist!",
                             Logger::LOG_COMPONENT_TESTING);
            }
            else
            {
                sinbad->setAnimationStatePausedToggled("Dance");
            }
        }
        return true;
    }

protected:
    void registredAsInputListener(bool registred, InputController *controller)
    {
        VE_UNUSED(registred);
        VE_UNUSED(controller);
    }

private:
    const BaseCamera mCamera;
    AnimationTest *mTest;
};

AnimationTest::AnimationTest(const Game &game, RenderView &view)
    : Test(game, view)
{
    setupCamera(game, view);
    setupControls(game, view);
    setupResources();
    setupScene(game, view);
}

void AnimationTest::setupCamera(const Game &game, RenderView &view)
{
    BaseCamera camera = game.getRenderer()->createCamera("AnimationTestCamera");

    const int zOrder = 0;

    ViewportOptionsMap viewports =
    {
        { zOrder, ViewportOptions(camera) }
    };

    view.setViewports(viewports);

    Ogre::Viewport *viewport = view.getViewportByZOrder(zOrder);
    viewport->setBackgroundColour(Ogre::ColourValue(0.9f, 0.9f, 0.9f));
}

void AnimationTest::setupControls(const Game &game, RenderView &view)
{
    VE_UNUSED(game);

//    InputController *input = new InputController("inputController", new OISInputBackendFactory);

    InputController *input = new InputController("inputController",
                                                 new OISInputBackendFactory
                                                 );

    BaseCamera camera = view.getCamera(0);

//    auto input = view.getInputContoller();

    input->addInputListener(new ExitKeyListener(VE_CLASS_NAME(AnimationTest).toString() +
                                                VE_CLASS_NAME(ExitKeyListener).toString(),
                                                ve::KeyData::KEY_ESCAPE));

    input->addInputListener(new AnimationTestInputListener(VE_CLASS_NAME(AnimationTestInputListener).toString(),
                                                           camera, this));

    input->addInputListener(new FlyCameraController(VE_CLASS_NAME(AnimationTest).toString() +
                                                    VE_CLASS_NAME(FlyCameraController).toString(),
                                                    camera, 50.0f, Ogre::Math::HALF_PI * 0.05));

    view.setInputController(input);
}

void AnimationTest::setupResources()
{
    FileSystem::clearResourceGroup(FileSystem::RG_DEFAULT);

    FileSystem::addResourceLocation(FileSystem::getCwd(),
                                    FileSystem::LOC_TYPE_FS);

    FileSystem::addResourceLocation(
                FileSystem::concatenatePath(FileSystem::getCwd(), "materials"),
                FileSystem::LOC_TYPE_FS);

    FileSystem::addResourceLocation(FileSystem::concatenatePath(FileSystem::getCwd(), "Sinbad.zip"),
                                    FileSystem::LOC_TYPE_ZIP);

    FileSystem::initialiseResourceGroup(FileSystem::RG_DEFAULT);
}

void AnimationTest::setupScene(const Game &game, RenderView &view)
{
    logger()->setLogDetail(Logger::LOG_LEVEL_DEBUG);

//    ColourValue color(.9f, .9f, .9f);

    log("Creating viewport...", Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_TESTING);

    Ogre::SceneManager *sceneMgr = game.getRenderer()->getSceneManager();
//    sceneMgr->setFog(FOG_LINEAR, color, 0);

//    log("Setup shadows...", Logger::LOG_LEVEL_DEBUG);

//    sceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);
//    sceneMgr->setShadowColour(ColourValue(.5f, .5f, .5f));
//    sceneMgr->setShadowTextureSize(1024);
//    sceneMgr->setShadowTextureCount(1);

    sceneMgr->setAmbientLight(Ogre::ColourValue::White);

    log("Creating light...", Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_TESTING);

    Ogre::Light *light = sceneMgr->createLight();
    light->setType(Ogre::Light::LT_POINT);
    light->setPosition(-10, 40, 20);

    log("Creating plane...", Logger::LOG_LEVEL_DEBUG, Logger::LOG_COMPONENT_OTHER);


    Ogre::SceneNode *rootNode = sceneMgr->getRootSceneNode();

    PlaneModelOptions planeOpt;
    planeOpt.modelName = "floor";
    planeOpt.physicsType = PhysicsModelOptions::PMT_Static;
    planeOpt.friction = 0.1f;
    planeOpt.parentNode = rootNode;
    planeOpt.size = ve::SizeF(100, 100);
    planeOpt.plane = Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);
    planeOpt.segments.x = 50;
    planeOpt.segments.y = 50;
    planeOpt.uTile = 20;
    planeOpt.vTile = 20;

    PlaneModel *plane = new PlaneModel(planeOpt);

    Ogre::MaterialManager *materialMgr = Ogre::MaterialManager::getSingletonPtr();
    Ogre::MaterialPtr material = materialMgr->getByName("pattern");

    plane->setMaterial(material);

    mModels.insert(plane);

    // Sinbad

    Ogre::SceneNode *sinbadNode = rootNode->createChildSceneNode("SinbadNode");

    ModelOptions opt;
    opt.modelName = "Sinbad";
    opt.meshName = "Sinbad.mesh";
    opt.modelNode = sinbadNode;
    opt.parentNode = rootNode;

    Model *sinbad = new Model(opt);

    static const std::string animName = "Dance";

    if (!sinbad->hasAnimationState(animName))
    {
        logAndAssert(VE_SCOPE_NAME(setupScene(const Game &game, RenderView &view)),
                     "No such animation state \"" + animName + "\" in " +
                     VE_CLASS_NAME(Model).toString() + " named " + sinbad->getName() + " !",
                     Logger::LOG_COMPONENT_TESTING);
    }

    sinbad->setAnimationStateEnabled(animName, true);

    sinbad->setAnimationStateLoop(animName, true);

    sinbad->setPosition(Ogre::Vector3(0, 5, 0));
    sinbad->setShowBoundingBox(true);

    mModels.insert(sinbad);
}

/// ====================================================================================================

class AnimationTestFactory : public TestFactory
{
public:
    virtual Test *createTest(const ScriptedGame &game, RenderView &view) const
    {
        return new AnimationTest(game, view);
    }

    virtual RenderViewBackend *createRenderViewBackend() const
    {
        return new OGRERenderViewBackend;
    }

    virtual ScriptFactory *createScriptFactory() const
    {
        return nullptr;
    }
};


int main(int argc, char *argv[])
{
    AnimationTestFactory factory;
    return Test::runTest(argc, argv, factory);
}
