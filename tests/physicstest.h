#ifndef PHYSICSTEST_H
#define PHYSICSTEST_H

#include <tests/test.h>

#include <renderer/basecamera.h>

class ScriptConsole;
class PhysicsModel;
class AbstractInputListener;
class UiTextLabel;

class PhysicsTest : public Test
{
    friend class PhysicsTestFollowCameraController;
    friend class PhysicsTestInputListener;

public:
    VE_DECLARE_TYPE_INFO(PhysicsTest)

    PhysicsTest(const ScriptedGame &game, RenderView &view);
    ~PhysicsTest();

    bool event(const Event *event) override
    {
        assertNotImplemented(VE_SCOPE_NAME(event(const Event *event)),
                             Logger::LOG_COMPONENT_OTHER);
        return false;
    }

    const std::string &getName() const override
    {
        static const std::string Name = "physicsTest";
        return Name;
    }

    void resetVehicle();

    enum ViewType
    {
        Follow = 0,
        Fly
    };

    void setViewType(ViewType viewType);
    ViewType getViewType() const;

    void setVehicleDebugEnabled(bool enabled);
    bool isVehicleDebugEnabled() const;

    void setShapesDebugEnabled(bool enabled);
    bool isShapesDebugEnabled() const;

    void setConsoleVisiable(bool visiable);
    bool isConsoleVisiable() const;

protected:
    void setupControls(const Game &game, RenderView &view) override;
    void setupScene(const Game &game, RenderView &view) override;
    void setupCamera(const Game &game, RenderView &view) override;

private:
    void setupConsole(const ScriptedGame &game, RenderView &view);
    void setupVehicle(const Game &game, RenderView &view);

    void createBoxWall(const Game &game);

    bool mShapesDebugEnabled;

    PhysicsModel *mVehicle;

    RenderView *mView;
    std::map<ViewType, AbstractInputListener *> mCameraControllers;
    AbstractInputListener *mCurrentCameraController;

    ScriptConsole *mConsole;
    UiTextLabel *mFPSLabel;
};

#endif // PHYSICSTEST_H
