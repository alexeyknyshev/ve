#!/bin/bash -efu

SELF=$(basename "$0")

echo "$SELF: --- ve deleting bindings (swig) ---"

find . -name '*.cc' -type f -print 2>/dev/null |
while read path
do
	echo "$SELF: removing generated file: $path"
	rm -f $path
done

echo "$SELF: --- ve deleting bindings (swig) completed ---"
